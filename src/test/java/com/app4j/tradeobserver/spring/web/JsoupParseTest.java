package com.app4j.tradeobserver.spring.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.annotation.Resource;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app4j.tradeobserver.common.util.CustomUtils;
import com.app4j.tradeobserver.data.constants.DataCommonConstants;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/root-context.xml", "classpath:spring/appServlet/servlet-context.xml", "classpath:spring/hibernate-context.xml"})
public class JsoupParseTest {
	
	
//	@Test
//	@Transactional
//	@Rollback(false) 
//    public void jsoupSimpleTest() throws Exception {
//		
//		Document doc = Jsoup.connect("http://www.itooza.com/vclub/y10_page.php?cmp_cd=005930&mode=db&ss=10&sv=1&lsmode=1&lkmode=2&accmode=1").get();
//		Elements elements = doc.select("#y10_tb_1 tbody th");
//		
////		for(Element element : elements) {
////			
////			System.out.println(element.text());			
////		}
////		
//		elements = doc.select("#y10_tb_2 thead tr th");
//		
//		
//		CustomUtils.removeEmptyElement(elements);
//		
//		List<String> quaterList = CustomUtils.getElementText(elements, "[^0-9]");
//		
//		int size = elements.size();
//		
//		//������
//		elements = doc.select("#y10_tb_2 tbody tr td");
//		CustomUtils.removeEmptyElement(elements);
//		List<String> fieldNameList = CustomUtils.getHanguelFieldNames(BalanceSheetDto.class);
//		
//		ArrayList<BalanceSheetDto> balanceSheetDtos = new ArrayList<BalanceSheetDto>();
//		for(int i=0 ; i < elements.size() ; i++) {
//			BalanceSheetDto balanceSheetDto = null;
//			if( i / size > 0) {
//				balanceSheetDto = balanceSheetDtos.get(i % size);
//				FieldUtils.writeField(balanceSheetDto, fieldNameList.get(i / size), elements.get(i).text(), true);
//				System.out.println(balanceSheetDto.getYmDay());
//				
//			} else {
//				balanceSheetDto = new BalanceSheetDto();
//				balanceSheetDto.setYmDay(quaterList.get(i));
//				balanceSheetDto.setStockCode("005930");
//				FieldUtils.writeField(balanceSheetDto, fieldNameList.get(i / size), elements.get(i).text(), true);
//				balanceSheetDtos.add(balanceSheetDto);
//			}
//		}
//		
//		balanceSheetDao.saveOrUpdate(balanceSheetDtos.get(0));
//		
//    }
	
	@Test
	public void naverBalanceSheetElementTest() throws IOException {
		
		Document doc = Jsoup.connect("http://companyinfo.stock.naver.com/v1/company/cF3002.aspx?cmp_cd=037560&frq=0&rp=2&finGubun=IFRSL").maxBodySize(0).get();
		
		
		
		Elements elements = doc.select(DataCommonConstants.CASHFLOW_STATEMENT_YEAR_TABLE_SELECTOR + " .c1.line.txt");
		String curText = null;
		String compareText = null;
		System.out.println(elements.size());
		List<String> parentList = new ArrayList<String>();
		
		for (int i = 0 ; i < elements.size() ; i++) {
			int differ = 0;
			int quotient = 0;
			int remainder = 0;
			curText = elements.get(i).text();
			compareText = curText.replaceAll("[^��-�RA-Za-z()*]", "");
			differ = curText.length() - compareText.length();
			
			if (differ == 0) { 
				parentList.clear();
			}
			
			quotient = differ / 4;
			remainder = differ % 4;
			
			if( remainder == 0) {
				if(parentList.size() < quotient) {
					parentList.add(elements.get(i-1).text());
				} else if (parentList.size() > quotient && parentList.size() != 0) {
					while(parentList.size() > quotient) {
						parentList.remove(parentList.size()-1);
					}
					
				}
			}
			
			String prefixText = "";
			for(int j = 0; j < parentList.size(); j++) {
				prefixText = prefixText + parentList.get(j) + "_";				
			}
			System.out.println((prefixText + curText).replaceAll("[^��-�RA-Za-z_]", ""));
			
		}
//		for(Element element : elements) {
//			curText = element.text();
//			System.out.println(curText + "," + curText.replaceAll("[^��-�RA-Za-z()*]", "").length() + "," + curText.length());			
//		}
		
//		elements = doc.select("script[type=text/javascript]");
//		
//		System.out.println(elements.size());
//		for(Element element : elements) {
//			
//				if(element.childNodes().size() > 0 &&
//						element.childNodes().get(0).toString().startsWith("var changeYYMM")) {
//					]
//					System.out.println(element.childNodes().get(0).toString().replaceAll("[^0-9,]", ""));
//					
//				}
//				
//			
//			
//						
//		}		
	}
	
	@Test
	public void naverValueIndexParseTest() throws Exception{
		Document doc = Jsoup.connect("http://companyinfo.stock.naver.com/v1/company/cF4001.aspx?cmp_cd=037560&frq=Q&rpt=6&finGubun=IFRSL").get();
		
		Elements elements = doc.select(".chart").select("area");
		
		System.out.println(elements.size());
		for (Element element : elements) {
			String temp = element.attr("title");
			System.out.println(temp);
		}
	}
	
	@Test
	public void itoozaPrintElement() throws IOException {
		/*
			http://www.itooza.com/vclub/y10_page.php?cmp_cd=037560&mode=db&ss=10&sv=1&lsmode=1&lkmode=2&accmode=1  �繫����ǥ IFRS �б�
			http://www.itooza.com/vclub/y10_page.php?cmp_cd=037560&mode=dy&ss=10&sv=1&lsmode=1&lkmode=2&accmode=1
			http://www.itooza.com/vclub/y10_page.php?cmp_cd=037560&mode=db&ss=10&sv=1&lsmode=1&lkmode=2&accmode=2  �繫����ǥ GAAP �б� (����)

			http://www.itooza.com/vclub/y10_page.php?cmp_cd=037560&mode=db&ss=10&sv=1&lsmode=1&accmode=2&lkmode=1 	GAAP ����.
			
			http://www.itooza.com/vclub/y10_page.php?cmp_cd=037560&mode=db&ss=10&sv=2&lsmode=1&accmode=2&lkmode=1 ���� ��꼭
			http://www.itooza.com/vclub/y10_page.php?cmp_cd=037560&mode=db&ss=10&sv=4&lsmode=1&accmode=2&lkmode=1 ���� �帧ǥ.
			sv �繫����ǥ, ���Ͱ�꼭 , �����帧ǥ (1, 2, 4)
			ccmode ifrs ,gaap (1, 2)
			lkmode ���� ���� (1, 2)
		 */				
		//Document doc = Jsoup.connect("http://www.itooza.com/vclub/y10_page.php?cmp_cd=037560&mode=db&ss=10&sv=4&lsmode=1&lkmode=2&accmode=2").get();
		Document doc = Jsoup.connect("http://www.itooza.com/vclub/y10_page.php?cmp_cd=037560&mode=db&ss=10&sv=4&lsmode=1&lkmode=2&accmode=1").get();
		
		Elements elements = doc.select("#y10_tb_1 tbody th");
		
		Stack<String> stack = new Stack<String>();
		Map<String, String> idNameMap = new HashMap<String, String>();
		
		CustomUtils.removeEmptyElement(elements);
		String id = null;
		String classAttr = null;
		String parentName = null;
		String result= "";
		for(Element element : elements) {
			id = element.parent().attr("id");
			
			classAttr= element.parent().attr("class");
			element.parent().attr("class");
			idNameMap.put(id, element.text());
			stack.push(element.text());
			while ( classAttr.lastIndexOf("node") != -1) {  
				parentName = classAttr.substring(classAttr.lastIndexOf("node") , classAttr.length());
				
				stack.push(doc.select("#" + parentName + " th").get(0).text());
				classAttr = doc.select("#" + parentName).get(0).attr("class");	
			}
			
			result = stack.pop();
			while(!stack.isEmpty()) {
				result = result + "_" + stack.pop();
			}
			
			System.out.println(result.replaceAll("[^��-�RA-Za-z_ ]", ""));
		}
		
		
		
	}

}
