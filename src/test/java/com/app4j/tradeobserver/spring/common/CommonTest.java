package com.app4j.tradeobserver.spring.common;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app4j.tradeobserver.common.util.DateUtil;
import com.app4j.tradeobserver.common.util.StockUtil;

public class CommonTest {
	
    @After
    public void tearDown() throws Exception {
    }
    
    /*
     * data already inserted
     */
    @Test
    public void apacheDeteUtilTest() throws Exception {
    	Date today = new Date();
    	String todayStr = DateFormatUtils.format(today, "yyyyMMdd");
    	System.out.println(todayStr);
    	
    	String fiveMonthAgoDay = DateFormatUtils.format(DateUtils.addDays(today, 1), "yyyyMMdd");
    	System.out.println(fiveMonthAgoDay);
    	
    	System.out.println(DateUtil.daysBetween(
    			DateUtil.getYmdFormatDate(fiveMonthAgoDay)
    			,DateUtil.getYmdFormatDate(todayStr)));
    	
    	PearsonsCorrelation corr = new PearsonsCorrelation();
    	double[] arg0 = { 299, 4000, 80000};
    	double[] arg1 = { 1, 2, 3};
    	
    	System.out.println(corr.correlation(arg0, arg1));
    }
    
    @Test
	public void nullIntegerCastTest() {
		Integer intNum = (Integer) null;
		
		assertEquals(null, intNum);
	}
    
    @Test
	public void replaceStringTest() {
		String value = "�� : 3,962.73";
		
		value = value.replaceAll("[^0-9.]", "");
		assertEquals(value, "3962.73");
				
		String value2 = "���� : 2013/09";		
		value2 = value2.replaceAll("[^0-9]", "");
		assertEquals(value2, "201309");
		
		String value3 = "[Cash Cycle]";		
		value3 = value3.replaceAll("\\[\\] ", "");
		System.out.println(value3);
		value3 = value3.replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", "");
		System.out.println(value3);
		assertEquals(value3, "CashCycle");
		
		
		
	}
    
    
    
}
