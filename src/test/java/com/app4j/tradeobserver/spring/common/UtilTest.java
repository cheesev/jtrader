package com.app4j.tradeobserver.spring.common;

import static org.junit.Assert.assertEquals;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app4j.tradeobserver.common.util.StockUtil;

public class UtilTest {
	
	@Test
	public void stockUtilTest() {
		String[] associatedStock = StockUtil.getAllAssociatedStock("000020");
		
		assertEquals("00025", associatedStock[1]);
	}
	
	@Test
	public void defaultUtilTest() {
		System.out.println(StringUtils.substringBefore("PE R(��)", "("));
	}
	

}
