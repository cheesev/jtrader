package com.app4j.tradeobserver.spring.db;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.predicate.BetweenPredicate;
import com.app4j.tradeobserver.common.util.DateUtil;
import com.app4j.tradeobserver.stock.dao.StockCurrentAccountDao;
import com.app4j.tradeobserver.stock.entity.StockTradeSituationDto;
import com.app4j.tradeobserver.stock.service.SupplyAndDemandService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/root-context.xml", "classpath:spring/appServlet/servlet-context.xml", "classpath:spring/hibernate-context.xml"})
public class DaoTest {
	
	@Autowired
    private SupplyAndDemandService supplyAndDemandService;
	
	@Autowired
    private StockCurrentAccountDao stockCurrentAccountDao;

	/*samsung electronics*/
    private final String ymdDay = "20140523";
    private final String stockCode = "005930"; 
    private final int price = 1428000;    

    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    @Transactional
    public void test1() throws Exception {
    	stockCurrentAccountDao.getCurrentAccount();
    }
    
    /*
     * data already inserted
     */
    @Test
    public void getStockTradeSituationTest() throws Exception {
    	StockTradeSituationDto stockTradeSituation = supplyAndDemandService.getStockTradeSituation(ymdDay, stockCode);
        
        assertEquals(price, stockTradeSituation.getPrice());
    }
    
    @SuppressWarnings("unchecked")
	@Test
    public void getStockTradeSituationListTest() throws Exception {
    	List<StockTradeSituationDto> stockTradeSituationList = supplyAndDemandService.getStockTradeSituationList("20100514", "20140523", "005930");
    	
    	assertEquals(1000 , stockTradeSituationList.size());
    	
    	Predicate betweenPredicate = new BetweenPredicate(DateUtil.getYmdFormatDate("20140516"), DateUtil.getYmdFormatDate("20140523"));
    	
    	List<StockTradeSituationDto> selectedList = (List<StockTradeSituationDto>) CollectionUtils.select(stockTradeSituationList, betweenPredicate);
    	assertEquals(6 , selectedList.size());
    	
    	
    	List<StockTradeSituationDto> stockTradeSituationDtoList = 
				supplyAndDemandService.getCumulativeStockTradeSituationList(DateUtil.STOCK_TRADE_SITUATION_MIN_DAY, "20040106", stockCode);
    	
    	StockTradeSituationDto stockTradeSituationDto = supplyAndDemandService.getMaxBuyAmt(stockTradeSituationDtoList);
    	
    	assertEquals(stockTradeSituationDto.getAnt() , 55151);
    	assertEquals(stockTradeSituationDto.getForeigner() , -23139);
    	
    	stockTradeSituationDto = supplyAndDemandService.getCurBuyAmt(stockTradeSituationDtoList);
    	
    	assertEquals(stockTradeSituationDto.getAnt() , 17621);
    	
    	stockTradeSituationDtoList = supplyAndDemandService.getStockTradeSituationList("005930", 3);
    	
    	stockTradeSituationDto = supplyAndDemandService.sumSupplyAndSetAvgPrice(stockTradeSituationDtoList);
    	
    	assertEquals(stockTradeSituationDto.getAnt(), -28423);
    	
    	assertEquals(stockTradeSituationDtoList.get(0).getForceGrp(), 10201);
    	
    	
    	
    	
    }
    @Test
    public void getMaxYmdDayTest() throws Exception {
    	String maxDay = supplyAndDemandService.getMaxYmdDay("005930");
    	
    	assertEquals("20140523", maxDay);
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void compositeTest() throws Exception {
    	
    	String toDay = "20140522";
    	String stockCode = "005930";
    	
    	Predicate betweenPredicate = null;
    	betweenPredicate = new BetweenPredicate(DateUtil.addYmdDay(toDay, 0, 0, -20), toDay);
    	List<StockTradeSituationDto> cumulativeStockTradeList = 
				 supplyAndDemandService.getCumulativeStockTradeSituationList(DateUtil.STOCK_TRADE_SITUATION_MIN_DAY, toDay, stockCode);
    	
		List<StockTradeSituationDto> selectDtoList = ((List<StockTradeSituationDto>)CollectionUtils.select(cumulativeStockTradeList, betweenPredicate));
		
		assertEquals(selectDtoList.get(selectDtoList.size() -1).getYmdDay(), "20140522" );
		assertEquals(selectDtoList.get(selectDtoList.size() -1).getAnt(), -12988397 );
		
		
		/* calculate Average price */
		List<StockTradeSituationDto> stockDtoList = supplyAndDemandService.getStockTradeSituationList("20140429", "20140523", stockCode);
		assertEquals( supplyAndDemandService.getAvgPrice(stockDtoList), 1389382);
    }
    
    @Test
    public void etcTest() throws Exception {
    	String stockCode = "005930";
    	List<StockTradeSituationDto> cumulativStockTradeSituationDtoList =
    			supplyAndDemandService.getCumulativeStockTradeSituationList("20040102", "20140523", stockCode);
    	List<StockTradeSituationDto> stockTradeSituationDtoList = 
    			supplyAndDemandService.getStockTradeSituationList("20040102", "20140523", stockCode);
    	StockTradeSituationDto minDto = supplyAndDemandService.getMinBuyAmt(cumulativStockTradeSituationDtoList);
    	System.out.println(supplyAndDemandService.getAvgPriceByInvestors(stockTradeSituationDtoList, minDto).getAnt());
    }
}
