package com.app4j.tradeobserver.spring.service;

import static org.junit.Assert.assertEquals;
import javax.annotation.Resource;

import org.hibernate.criterion.Restrictions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.stock.dao.StockInfoDao;
import com.app4j.tradeobserver.stock.service.AccountService;
import com.app4j.tradeobserver.stock.service.MagicFomulaService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/root-context.xml", "classpath:spring/appServlet/servlet-context.xml" , "classpath:spring/hibernate-context.xml"})
public class StockServiceTest {
	
	@Resource(name="stockInfoDao")
	private StockInfoDao stockInfoDao;
	
	@Autowired
	private MagicFomulaService magicFomulaService;
	
	@Autowired
	private AccountService accountService;
	
	@Test
	@Transactional(readOnly=true)
	public void stockDaoTest() { 
		System.out.println(stockInfoDao.getByCriteria(Restrictions.isNotNull("stockCode")).size());
	}
	
	@Test
	public void singleMagicFomulaServiceTest() {
		magicFomulaService.getMagicFomulaList();
	}
	
	@Test
	public void accountHistTest() {
		
		assertEquals(390, accountService.getCumulativeStockAccountHistory("", "20140101", "20151231").size());
		assertEquals(293, accountService.getCumulativeStockAccountHistory("00701706181", "20140101", "20151231").size());
		
		
	}
	

}
