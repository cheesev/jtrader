package com.app4j.tradeobserver.spring.service;

import java.text.ParseException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app4j.tradeobserver.data.constants.ItoozaModeType;
import com.app4j.tradeobserver.data.constants.StatementType;
import com.app4j.tradeobserver.data.service.ItoozaCashFlowStatementGAAPService;
import com.app4j.tradeobserver.data.service.ItoozaCashFlowStatementIFRSService;
import com.app4j.tradeobserver.data.service.ItoozaFinancialPositionStatementGAAPService;
import com.app4j.tradeobserver.data.service.ItoozaFinancialPositionStatementIFRSService;
import com.app4j.tradeobserver.data.service.ItoozaProfitLossStatementGAAPService;
import com.app4j.tradeobserver.data.service.ItoozaProfitLossStatementIFRSService;
import com.app4j.tradeobserver.data.service.ItoozaValueIndexService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/root-context.xml", "classpath:spring/appServlet/servlet-context.xml" , "classpath:spring/hibernate-context.xml"})
public class ItoozaBalanceSheetServiceTest {
	
	@Autowired
	private ItoozaFinancialPositionStatementGAAPService itoozaFinancialPositionStatementGAAPService;
	
	@Autowired
	private ItoozaCashFlowStatementGAAPService itoozaCashFlowStatementGAAPService;
	
	@Autowired
	private ItoozaProfitLossStatementGAAPService itoozaProfitLossStatementGAAPService;
	
	@Autowired
	private ItoozaFinancialPositionStatementIFRSService itoozaFinancialPositionStatementIFRSService;
	
	@Autowired
	private ItoozaProfitLossStatementIFRSService itoozaProfitLossStatementIFRSService;
	
	@Autowired
	private ItoozaCashFlowStatementIFRSService itoozaCashFlowStatementIFRSService;
	
	@Autowired
	private ItoozaValueIndexService ItoozaValueIndexService;
		
	@Test
	public void itoozaFinancialPositionTest() throws ParseException {
		
		itoozaFinancialPositionStatementGAAPService.insertBalanceSheet("037560"
				, StatementType.FinancialPositionStatement, ItoozaModeType.dateQuater
				, ItoozaModeType.ccmodeGAAP , ItoozaModeType.lkmodeEach);
		
		//System.out.println(NumberFormat.getNumberInstance(Locale.US).parse("265,858"));
	}
	
	@Test
	public void itoozaCashFlowTest() throws ParseException {
		
		itoozaCashFlowStatementGAAPService.insertBalanceSheet("005930"
				, StatementType.CashFlowStatement, ItoozaModeType.dateQuater
				, ItoozaModeType.ccmodeGAAP , ItoozaModeType.lkmodeEach);
	}
	
	@Test
	public void itoozaProfitLossTest() throws ParseException {
		
		itoozaProfitLossStatementGAAPService.insertBalanceSheet("005930"
				, StatementType.ProfitLossStatement, ItoozaModeType.dateQuater
				, ItoozaModeType.ccmodeGAAP, ItoozaModeType.lkmodeEach);
			
	}
	
	@Test
	public void itoozaValueIndexTest() throws ParseException {
		
		ItoozaValueIndexService.insertData("005930", ItoozaModeType.dateQuater);
			
	}
	
	@Test
	public void itoozaIFRSFinancialPositionTest() throws ParseException {
		
		itoozaFinancialPositionStatementIFRSService.insertBalanceSheet("005930"
				, StatementType.FinancialPositionStatement, ItoozaModeType.dateQuater
				, ItoozaModeType.ccmodeIFRS , ItoozaModeType.lkmodeEach);
			
	}
	
	@Test
	public void itoozaIFRSProfitLossTest() throws ParseException {
		
		itoozaProfitLossStatementIFRSService.insertBalanceSheet("005930"
				, StatementType.ProfitLossStatement, ItoozaModeType.dateQuater
				, ItoozaModeType.ccmodeIFRS , ItoozaModeType.lkmodeEach);
			
	}
	
	@Test
	public void itoozaIFRSCashFlowTest() throws ParseException {
		
		itoozaCashFlowStatementIFRSService.insertBalanceSheet("005930"
				, StatementType.CashFlowStatement, ItoozaModeType.dateQuater
				, ItoozaModeType.ccmodeIFRS , ItoozaModeType.lkmodeEach);
			
	}

}
