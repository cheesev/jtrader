package com.app4j.tradeobserver.spring.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app4j.tradeobserver.data.constants.DataCommonConstants;
import com.app4j.tradeobserver.data.constants.FinancialAccountType;
import com.app4j.tradeobserver.data.service.CashFlowStatementIFRSService;
import com.app4j.tradeobserver.data.service.FinancialPositionStatementGAAPService;
import com.app4j.tradeobserver.data.service.FinancialPositionStatementIFRSService;
import com.app4j.tradeobserver.data.service.ProfitLossStatementGAAPService;
import com.app4j.tradeobserver.data.service.ProfitLossStatementIFRSService;
import com.app4j.tradeobserver.data.service.ValueIndexService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/root-context.xml", "classpath:spring/appServlet/servlet-context.xml" , "classpath:spring/hibernate-context.xml"})
public class BalanceSheetServiceTest {
	
	@Autowired
	ProfitLossStatementGAAPService profitLossStatementGAAPService;
	@Autowired
	ProfitLossStatementIFRSService profitLossStatementIFRSService;
	
	@Autowired
	FinancialPositionStatementGAAPService financialPositionStatementGAAPService;
	
	@Autowired
	FinancialPositionStatementIFRSService financialPositionStatementIFRSService;
	
	@Autowired
	CashFlowStatementIFRSService cashFlowStatementIFRSService;
	
	@Autowired
	ValueIndexService valueIndexService;
	
		
	@Test
	@Rollback(false) 
    public void profitLossIFRSTest() throws Exception {
		
//		FinancialStatementService<ProfitLossStatementGAAPDto, ProfitLossStatementGAAPDtoId> service =
//				new FinancialStatementService<ProfitLossStatementGAAPDto, ProfitLossStatementGAAPDtoId>(ProfitLossStatementGAAPDto.class);
		
		profitLossStatementIFRSService.insertFinancialStatement(
				"136480", FinancialAccountType.IFRS_EACH, DataCommonConstants.PROFITLOSS_STATEMENT_TABLE );
		//balanceSheetService.insertBalanceSheetGAAP("005930", "GAAPS");
		//profitLossStatementService.insertProfitLossStatement("005930", "GAAPL");
		
	}
	
	@Test
	@Rollback(false) 
    public void valueIndexServieTest() throws Exception {
		
//		FinancialStatementService<ProfitLossStatementGAAPDto, ProfitLossStatementGAAPDtoId> service =
//				new FinancialStatementService<ProfitLossStatementGAAPDto, ProfitLossStatementGAAPDtoId>(ProfitLossStatementGAAPDto.class);
		
		valueIndexService.insertValueIndex("007330");
		
	}
//	
//	@Test
//	@Rollback(false) 
//    public void financialPositionStatementGAAPServiceTest() throws Exception {
//		financialPositionStatementGAAPService.insertFinancialStatement("037560", "GAAPS", new String[] {DataCommonConstants.FINANCIAL_POSITION_STATMENT_YEAR_TABLE_SELECTOR , DataCommonConstants.FINANCIAL_POSITION_STATMENT_QUATER_TABLE_SELECTOR});
//		
//		
//	}
//	
//	@Test
//	@Rollback(false) 
//    public void financialPositionStatementIFRSServiceTest() throws Exception {
//		financialPositionStatementIFRSService.insertFinancialStatement("037560", "IFRSS", new String[] {DataCommonConstants.FINANCIAL_POSITION_STATMENT_YEAR_TABLE_SELECTOR , DataCommonConstants.FINANCIAL_POSITION_STATMENT_QUATER_TABLE_SELECTOR});
//	}
//	
//	@Test
//	@Rollback(false) 
//    public void cashFlowStatementIFRSServiceTest() throws Exception {
//		cashFlowStatementIFRSService.insertFinancialStatement("037560", "IFRSS", new String[] {DataCommonConstants.CASHFLOW_STATEMENT_YEAR_TABLE_SELECTOR , DataCommonConstants.CASHFLOW_STATEMENT_QUATER_TABLE_SELECTOR});
//	}


}
