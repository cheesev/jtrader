package com.app4j.tradeobserver.spring.service;

import javax.annotation.Resource;

import org.hibernate.criterion.Restrictions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.user.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/root-context.xml", "classpath:spring/appServlet/servlet-context.xml" , "classpath:spring/hibernate-context.xml"})
public class ServiceTest {
	
	@Autowired
	private UserService userService;
	
	@Test
	@Transactional(readOnly=true)
	public void stockDaoTest() { 
		System.out.println(userService.getAllUserList());
	}

}
