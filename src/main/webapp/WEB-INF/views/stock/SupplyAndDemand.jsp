<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--
  중/대형주를 기준으로 해야하고, 소형주에 이용할 시에는 반드시 거래원 분석이 병행되어야함.
  세력합
  1. 외국인 + 기관계 + 기타계(기타 + 국가)
  2. 외국인 + 기관계
  인데 1의방법이 이론적으로 맞으므로 이 방식을 택함.	
-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">


<link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/webjars/jquery-ui/1.10.3/themes/base/jquery-ui.css"/>" />
<link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/webjars/jqgrid/4.6.0/css/ui.jqgrid.css"/>" />
<link rel="stylesheet" type="text/css" href="/WEB-INF/css/jqgrid_bar.css"/>

<script src="<c:url value="/webjars/jquery/1.9.1/jquery.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/webjars/jquery-ui/1.10.3/ui/jquery-ui.js"/>" type="text/javascript"></script> 
<script src="<c:url value="/webjars/jqgrid/4.6.0/js/i18n/grid.locale-kr.js"/>" type="text/javascript"></script>
<script src="<c:url value="/webjars/jqgrid/4.6.0/js/jquery.jqGrid.js"/>" type="text/javascript"></script>

<style type="text/css">
.cellDiv 
{
    left: 0px; top:5px; height:22px;
    position:relative;padding:0;margin-right:-4px;border:0;
}
.cellTextRight
{
    position:relative;
    margin-right:2px;
    text-align:right;
    float:right;
}
.gradient1{
    /* fallback (Opera) */
    background: #008AEF;
    /* Mozilla: https://developer.mozilla.org/en/CSS/-moz-linear-gradient */
    background: -moz-linear-gradient(left, #008AEF, white);
    /* Chrome, Safari: http://webkit.org/blog/175/introducing-css-gradients/ */
    background: -webkit-gradient(linear, left top, right top, from(#008AEF), to(white));
    /* MSIE http://msdn.microsoft.com/en-us/library/ms532997(VS.85).aspx */
    filter: progid:DXImageTransform.Microsoft.Gradient(StartColorStr='#008AEF', EndColorStr='white', GradientType=1);
    /*ie8*/
    -ms-filter: "progid:DXImageTransform.Microsoft.Gradient(StartColorStr='#008AEF', EndColorStr='white', GradientType=1)";
    position: absolute; left: -2px; top:-5px; right: 2px; height:22px; float:left;
}
.gradient2{
    background: #63C384;
    /*background: -moz-linear-gradient(left, #63C384 0%, white 100%);
    background: -webkit-gradient(linear, left top, right top, from(#63C384), to(white));*/
    filter: progid:DXImageTransform.Microsoft.Gradient(StartColorStr='#63C384', EndColorStr='white', GradientType=1);
    -ms-filter: "progid:DXImageTransform.Microsoft.Gradient(StartColorStr='#63C384', EndColorStr='white', GradientType=1)";
    position: absolute; left: -2px; top:-5px; right: 2px; height:22px; float:left;
}
</style>
<title>Stock Situation By Investors</title>
</head>
<body>
<table id="summaryGrid"></table>
<table id="dayGrid"></table>
<table id="weekGrid"></table>
<table id="monthGrid"></table>
<table id="yearGrid"></table>

<script type="text/javascript">
var gradientNumberFormat = function (cellvalue, gradientClass, minDataValue,
        maxDataValue, minDisplayValue, maxDisplayValue) {
var dataAsNumber = parseFloat(cellvalue); /* parseInt(cellvalue, 10);*/
if (dataAsNumber > maxDataValue) {
dataAsNumber = maxDataValue;
}
if (dataAsNumber < minDataValue) {
dataAsNumber = minDataValue;
}
var prozentVal = minDisplayValue+(dataAsNumber-minDataValue)*(maxDisplayValue-
             minDisplayValue)/(maxDataValue-minDataValue);
return '<div class="cellDiv"><div class="'+gradientClass+'" style="width:'+
prozentVal+'%;"></div><div class="cellTextRight"><span style="font-weight:bold;color:black">'+cellvalue +
'</span></div></div>';
};

jQuery(document).ready(function (){
	jQuery("#summaryGrid").jqGrid({
		url:'/JTradeObserver/stock/trade-situation/get-summary?stockCode=${stockCode}'//-- 머시기
		,datatype: 'json'
		,colNames:['일자', '가격', '개인', '세력합', '외국인' ,
		   		 '기관' , '금융투자' , '투신', '보험', '은행',
		   		 '투자은행', '연기금', '사모펀드', '내외국인', '기타']
		,colModel: [
		            {name:'ymdDay',index:'ymdDay', width:60, formatter:fontFormatter },
		            {name:'price',index:'price', width:60, formatter:fontFormatter },
		            {name:'ant',index:'ant', width:60, formatter:fontFormatter },
		            {name:'forceGrp',index:'forceGrp', width:60, formatter:fontFormatter }, // 이거 개발해야함.		            
		            {name:'foreigner',index:'foreigner', width:60, formatter:fontFormatter },
		            {name:'institution',index:'institution', width:60, formatter:fontFormatter },
		            {name:'financialInvestment',index:'financialInvestment', width:60, formatter:fontFormatter },
		            {name:'trust',index:'trust', width:60, formatter:fontFormatter },
		            {name:'assurance',index:'assurance', width:60, formatter:fontFormatter },
		            {name:'bank',index:'bank', width:60, formatter:fontFormatter },
		            {name:'investmentBank',index:'investmentBank', width:60, formatter:fontFormatter },
		            {name:'pension',index:'pension', width:60, formatter:fontFormatter },
		            {name:'privateEquityFund',index:'privateEquityFund', width:60, formatter:fontFormatter },
		            {name:'innerForeigner',index:'innerForeigner', width:60, formatter:fontFormatter },
		            {name:'etc',index:'etc', width:60, formatter:fontFormatter }		                    
		]
		,rowNum:5
		,rowList:[10,20,30]
		,sortname:'ymdDay'
		,autowidth: false
		,viewrecords: false
		,loadonce:true
		,caption:"요약"
		,loadError:function(xhr, status, error) {
		    // 데이터 로드 실패시 실행되는 부분
		    alert(error); 
		}
		,jsonReader : {
	        root: "rows",
	        total: "total",
	        records: "records",
	        repeatitems: false,
	        cell: "cell",
	        id: "ymdDay"
	    }
	});
	
	jQuery("#dayGrid").jqGrid({
		url:'/JTradeObserver/stock/trade-situation/getlist?interval=1&stockCode=${stockCode}'//-- 머시기
		,datatype: 'json'
		,colNames:['일자', '가격', '개인', '세력합', '외국인' ,
		   		 '기관' , '금융투자' , '투신', '보험', '은행',
		   		 '투자은행', '연기금', '사모펀드', '내외국인', '기타']
		,colModel: [
		            {name:'ymdDay',index:'ymdDay', width:60, formatter:fontFormatter},
		            {name:'price',index:'price', width:60, formatter:fontFormatter },
		            {name:'ant',index:'ant', width:60, formatter:fontFormatter },
		            {name:'forceGrp',index:'forceGrp', width:60, formatter:fontFormatter }, // 이거 개발해야함.		            
		            {name:'foreigner',index:'foreigner', width:60, formatter:fontFormatter },
		            {name:'institution',index:'institution', width:60, formatter:fontFormatter },
		            {name:'financialInvestment',index:'financialInvestment', width:60, formatter:fontFormatter },
		            {name:'trust',index:'trust', width:60, formatter:fontFormatter },
		            {name:'assurance',index:'assurance', width:60, formatter:fontFormatter },
		            {name:'bank',index:'bank', width:60, formatter:fontFormatter },
		            {name:'investmentBank',index:'investmentBank', width:60, formatter:fontFormatter },
		            {name:'pension',index:'pension', width:60, formatter:fontFormatter },
		            {name:'privateEquityFund',index:'privateEquityFund', width:60, formatter:fontFormatter },
		            {name:'innerForeigner',index:'innerForeigner', width:60, formatter:fontFormatter },
		            {name:'etc',index:'etc', width:60, formatter:fontFormatter }		                    
		]
		,rowNum:5
		,rowList:[10,20,30]
		,sortname:'ymdDay'
		,autowidth: false
		,viewrecords: false
		,loadonce:true
		,caption:"일별"
		,loadError:function(xhr, status, error) {
		    // 데이터 로드 실패시 실행되는 부분
		    alert(error); 
		}
		,jsonReader : {
	        root: "rows",
	        total: "total",
	        records: "records",
	        repeatitems: false,
	        cell: "cell",
	        id: "ymdDay"
	    }
	});
	
	jQuery("#weekGrid").jqGrid({
		url:'/JTradeObserver/stock/trade-situation/getlist?interval=7&stockCode=${stockCode}'//-- 머시기
		,datatype: "json"
		,colNames:['일자', '가격', '개인', '세력합', '외국인' ,
		   		 '기관' , '금융투자' , '투신', '보험', '은행',
		   		 '투자은행', '연기금', '사모펀드', '내외국인', '기타']
		,colModel: [
		            {name:'ymdDay',index:'ymdDay', width:60, formatter:fontFormatter},
		            {name:'price',index:'price', width:60, formatter:fontFormatter },
		            {name:'ant',index:'ant', width:60, formatter:fontFormatter },
		            {name:'forceGrp',index:'forceGrp', width:60, formatter:fontFormatter }, // 이거 개발해야함.		            
		            {name:'foreigner',index:'foreigner', width:60, formatter:fontFormatter },
		            {name:'institution',index:'institution', width:60, formatter:fontFormatter },
		            {name:'financialInvestment',index:'financialInvestment', width:60, formatter:fontFormatter },
		            {name:'trust',index:'trust', width:60, formatter:fontFormatter },
		            {name:'assurance',index:'assurance', width:60, formatter:fontFormatter },
		            {name:'bank',index:'bank', width:60, formatter:fontFormatter },
		            {name:'investmentBank',index:'investmentBank', width:60, formatter:fontFormatter },
		            {name:'pension',index:'pension', width:60, formatter:fontFormatter },
		            {name:'privateEquityFund',index:'privateEquityFund', width:60, formatter:fontFormatter },
		            {name:'innerForeigner',index:'innerForeigner', width:60, formatter:fontFormatter },
		            {name:'etc',index:'etc', width:60, formatter:fontFormatter }		                    
		]
		,rowNum:5
		,rowList:[10,20,30]
		,sortname:'ymdDay'
		,autowidth: false
		,viewrecords: false
		,loadonce:true
		,caption:"주별"
		,loadError:function(xhr, status, error) {
		    // 데이터 로드 실패시 실행되는 부분
		    alert(error); 
		}
		,jsonReader : {
	        root: "rows",
	        total: "total",
	        records: "records",
	        repeatitems: false,
	        cell: "cell",
	        id: "ymdDay"
	    }
	});
	
	jQuery("#monthGrid").jqGrid({
		url:'/JTradeObserver/stock/trade-situation/getlist?interval=30&stockCode=${stockCode}'//-- 머시기
		,datatype: "json"
		,colNames:['일자', '가격', '개인', '세력합', '외국인' ,
		   		 '기관' , '금융투자' , '투신', '보험', '은행',
		   		 '투자은행', '연기금', '사모펀드', '내외국인', '기타']
		,colModel: [
		            {name:'ymdDay',index:'ymdDay', width:60, formatter:fontFormatter},
		            {name:'price',index:'price', width:60, formatter:fontFormatter },
		            {name:'ant',index:'ant', width:60, formatter:fontFormatter },
		            {name:'forceGrp',index:'forceGrp', width:60, formatter:fontFormatter }, // 이거 개발해야함.		            
		            {name:'foreigner',index:'foreigner', width:60, formatter:fontFormatter },
		            {name:'institution',index:'institution', width:60, formatter:fontFormatter },
		            {name:'financialInvestment',index:'financialInvestment', width:60, formatter:fontFormatter },
		            {name:'trust',index:'trust', width:60, formatter:fontFormatter },
		            {name:'assurance',index:'assurance', width:60, formatter:fontFormatter },
		            {name:'bank',index:'bank', width:60, formatter:fontFormatter },
		            {name:'investmentBank',index:'investmentBank', width:60, formatter:fontFormatter },
		            {name:'pension',index:'pension', width:60, formatter:fontFormatter },
		            {name:'privateEquityFund',index:'privateEquityFund', width:60, formatter:fontFormatter },
		            {name:'innerForeigner',index:'innerForeigner', width:60, formatter:fontFormatter },
		            {name:'etc',index:'etc', width:60, formatter:fontFormatter }		                    
		]
		,rowNum:5
		,rowList:[10,20,30]
		,sortname:'ymdDay'
		,autowidth: false
		,viewrecords: false
		,loadonce:true
		,caption:"월별"
		,loadError:function(xhr, status, error) {
		    // 데이터 로드 실패시 실행되는 부분
		    alert(error); 
		}
		,jsonReader : {
	        root: "rows",
	        total: "total",
	        records: "records",
	        repeatitems: false,
	        cell: "cell",
	        id: "ymdDay"
	    }
	});
	
	jQuery("#yearGrid").jqGrid({
		url:'/JTradeObserver/stock/trade-situation/getlist?interval=365&stockCode=${stockCode}'//-- 머시기
		,datatype: "json"
		,colNames:['일자', '가격', '개인', '세력합', '외국인' ,
		   		 '기관' , '금융투자' , '투신', '보험', '은행',
		   		 '투자은행', '연기금', '사모펀드', '내외국인', '기타']
		,colModel: [
		            {name:'ymdDay',index:'ymdDay', width:60, formatter:fontFormatter},
		            {name:'price',index:'price', width:60, formatter:fontFormatter },
		            {name:'ant',index:'ant', width:60, formatter:fontFormatter },
		            {name:'forceGrp',index:'forceGrp', width:60, formatter:fontFormatter }, // 이거 개발해야함.		            
		            {name:'foreigner',index:'foreigner', width:60, formatter:fontFormatter },
		            {name:'institution',index:'institution', width:60, formatter:fontFormatter },
		            {name:'financialInvestment',index:'financialInvestment', width:60, formatter:fontFormatter },
		            {name:'trust',index:'trust', width:60, formatter:fontFormatter },
		            {name:'assurance',index:'assurance', width:60, formatter:fontFormatter },
		            {name:'bank',index:'bank', width:60, formatter:fontFormatter },
		            {name:'investmentBank',index:'investmentBank', width:60, formatter:fontFormatter },
		            {name:'pension',index:'pension', width:60, formatter:fontFormatter },
		            {name:'privateEquityFund',index:'privateEquityFund', width:60, formatter:fontFormatter },
		            {name:'innerForeigner',index:'innerForeigner', width:60, formatter:fontFormatter },
		            {name:'etc',index:'etc', width:60, formatter:fontFormatter }		                    
		]
		,rowNum:5
		,rowList:[10,20,30]
		,sortname:'ymdDay'
		,autowidth: false
		,viewrecords: false
		,loadonce:true
		,caption:"년별"
		,loadError:function(xhr, status, error) {
		    // 데이터 로드 실패시 실행되는 부분
		    alert(error); 
		}
		,jsonReader : {
	        root: "rows",
	        total: "total",
	        records: "records",
	        repeatitems: false,
	        cell: "cell",
	        id: "ymdDay"
	    }
	});
});

function fontFormatter(cellValue, opts, rowObject){
	if(rowObject['ymdDay'] == "분산비율") {
		// the number 200 will be mapped to the 10% filled color bar
        // the number 400 will be mapped to the 90% filled color bar
		 //return gradientNumberFormat(cellValue,"gradient2",200,400,10,90);
		 return gradientNumberFormat(cellValue,"gradient2",0,100,0,100);
	} else if (rowObject['ymdDay'] == "상관계수") {
		if (cellValue != "상관계수") cellValue = cellValue / 100;
		return gradientNumberFormat(cellValue,"gradient2",-1,1,0,100);
	} else {
		if(cellValue > 0 ) return '<div class="cellTextRight"><span style="color:red">'+cellValue+'</span></div>';
	    else return '<div class="cellTextRight"><span style="color:blue">'+cellValue+'</span></div>';
	}
	
     
}
</script>

<!-- pager는 없음 -->


</body>
</html>