<html>
  <head>
  	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/global_layout.css">
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript">
    
    // Load the Visualization API and the piechart package.
    google.load('visualization', '1', {'packages':['corechart']});
      
    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);
      
    function drawChart() {
      $.ajax({
          url: "/JTradeObserver/stock/overview/current-stock",
          dataType:"json",
          contentType: 'application/json',
          mimeType: 'application/json',
          success: function(result) {
        	  var data = new google.visualization.DataTable();
        	  data.addColumn('string', 'Topping');
              data.addColumn('number', 'Slices');
              for (x in result.rows) {
        		  var rowObj = result.rows[x];
        		  data.addRow([rowObj.accountName +'/'+rowObj.stockName, rowObj.buyPrice]);
        	  }
              var options = {title: 'My Current Stock Account'};

              var chart = new google.visualization.PieChart(document.getElementById('bar_chart_div'));
              chart.draw(data, options);
           },
	      error:function(data,status,er) { 
	          alert("error: "+data+" status: "+status+" er:"+er);
	      }
      });
      
      
      $.ajax({
          url: "/JTradeObserver/stock/overview/account-history",
          dataType:"json",
          contentType: 'application/json',
          mimeType: 'application/json',
          success: function(result) {
        	  console.log(result);
        	  var data = new google.visualization.DataTable(result);
              
              var options = {
            	        title: 'Account Profit Loss Trend',
            	        hAxis: {
            	          title: 'Time'
            	        },
            	        vAxis: {
            	          title: 'Profit Loss Ratio'
            	        },
            	        series: {
            	          0: {curveType: 'function'},
            	          1: {curveType: 'function'}
            	        }
               };

              var chart = new google.visualization.LineChart(document.getElementById('ex2'));

              chart.draw(data, options);
           },
	      error:function(data,status,er) { 
	          alert("error: "+data+" status: "+status+" er:"+er);
	      }
      });
          
      // Create our data table out of JSON data loaded from server.
/*       var data = new google.visualization.DataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.PieChart(document.getElementById('bar_chart_div'));
      chart.draw(data, {width: 400, height: 240}); */
    }

    </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
    <div id="main">
    	<div id="bar_chart_div" class="pieChart"></div>
    	<div id="ex2" class="lineChart"></div>
    </div>
  </body>
</html>