<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script src="<c:url value="/webjars/jquery/1.9.1/jquery.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/webjars/jquery-ui/1.10.3/ui/jquery-ui.js"/>" type="text/javascript"></script> 
<script src="<c:url value="/webjars/jqgrid/4.6.0/js/i18n/grid.locale-kr.js"/>" type="text/javascript"></script>
<script src="<c:url value="/webjars/jqgrid/4.6.0/js/jquery.jqGrid.js"/>" type="text/javascript"></script>

<script src="${pageContext.request.contextPath}/resources/js/Chart.js" type="text/javascript"></script>
