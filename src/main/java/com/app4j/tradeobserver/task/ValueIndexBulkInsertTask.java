package com.app4j.tradeobserver.task;

import java.util.concurrent.Callable;

import com.app4j.tradeobserver.data.service.ValueIndexService;

public class ValueIndexBulkInsertTask implements Callable<String>{
	
	private String stockCode;
	private ValueIndexService valueIndexService;
	
	public ValueIndexBulkInsertTask(ValueIndexService valueIndexService
			, String stockCode) {
		this.valueIndexService = valueIndexService;
		this.stockCode = stockCode;
	}

	@Override
	public String call() throws Exception {
		valueIndexService.insertValueIndex(stockCode);
		return stockCode;
	}

}
