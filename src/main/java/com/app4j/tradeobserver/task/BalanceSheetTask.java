package com.app4j.tradeobserver.task;

import java.util.concurrent.Callable;

import com.app4j.tradeobserver.common.service.FinancialStatementService;
import com.app4j.tradeobserver.common.service.ItoozaFinancialStatementService;
import com.app4j.tradeobserver.data.constants.FinancialAccountType;
import com.app4j.tradeobserver.data.constants.ItoozaModeType;
import com.app4j.tradeobserver.data.constants.StatementType;

public class BalanceSheetTask implements Callable<String>{
	
	private FinancialStatementService financialStatementService;
	private String stockCode;
	private FinancialAccountType financialAccountType;
	private String[] tableSelector;
	
	public BalanceSheetTask(FinancialStatementService financialStatementService
			, String stockCode
			, FinancialAccountType financialAccountType
			, String[] tableSelector) {
		this.financialStatementService = financialStatementService;
		this.stockCode = stockCode;
		this.financialAccountType = financialAccountType;
		this.tableSelector = tableSelector;
	}

	@Override
	public String call() throws Exception {
		financialStatementService.insertFinancialStatement(stockCode, financialAccountType, tableSelector);
		return stockCode;
	}

}
