package com.app4j.tradeobserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app4j.tradeobserver.user.entity.User;
import com.app4j.tradeobserver.user.service.UserService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private UserService userService;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@RequestMapping(value = "/trade-situation", method = RequestMethod.GET)
	public String getStockTradeSituationPage(@RequestParam String stockCode, Model model) {
		
		model.addAttribute("stockCode", stockCode);
		return "stock/SupplyAndDemand";
	}
	
	
	@RequestMapping("/home")
	public ModelAndView getAllUserList() {
		//logger.info("Welcome home! The client locale is {}.", locale);
		
		Map<String, Object> model = new HashMap<String, Object>();
		//ModelAndView modelAndView = new ModelAndView("list-of-teams");
		List<User> users = userService.getAllUserList();
		model.put("users", users);
//		model.addAttribute("users", users);
//		modelAndView.addObject("users", users);
/*		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );*/
		return new ModelAndView("home", model);
		
//		return new ModelAndView("addEmployee", model); 
		//return "home";
	}
	
}