package com.app4j.tradeobserver.user.service;

import java.util.List;

import com.app4j.tradeobserver.user.entity.User;

public interface UserService {
	
	List<User> getAllUserList();

}
