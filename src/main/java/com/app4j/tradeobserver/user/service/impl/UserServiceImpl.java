package com.app4j.tradeobserver.user.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.user.dao.UserDao;
import com.app4j.tradeobserver.user.entity.User;
import com.app4j.tradeobserver.user.service.UserService;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	@Resource(name="userDao")
	private UserDao userDao;
	
	@Override
	public List<User> getAllUserList() {
		return userDao.getByCriteria(Restrictions.isNotNull("id"));
	}
}
