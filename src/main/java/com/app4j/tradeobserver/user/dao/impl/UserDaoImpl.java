package com.app4j.tradeobserver.user.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.user.dao.UserDao;
import com.app4j.tradeobserver.user.entity.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDaoImpl<User, String> implements UserDao {
	protected UserDaoImpl() {
        super(User.class);
    }
}