package com.app4j.tradeobserver.user.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.user.entity.User;

public interface UserDao extends AbstractDao<User, String>{

}

