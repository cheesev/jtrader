package com.app4j.tradeobserver.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/stock")
public class MediatorController {
	/**
     * Retrieves the JSP page that contains our JqGrid
     */
	@RequestMapping(value = "/trade-situation", method = RequestMethod.GET)
	public String getStockTradeSituationPage(@RequestParam String stockCode, Model model) {
		
		model.addAttribute("stockCode", stockCode);
		return "stock/SupplyAndDemand";
	}
	
	@RequestMapping(value = "/overview", method = RequestMethod.GET)
	public String getAccountOverviewPage(Model model) {		
		return "account/overview";
	}

}
