package com.app4j.tradeobserver.common.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;

import java.io.Serializable;
import java.util.List;

public interface AbstractDao<E, I extends Serializable> {

	Session getCurrentSession();
	void getColumnList();
    E getById(I id);
    void saveOrUpdate(E e);
    void multiSaveOrUpdate(List<E> e);
    void delete(E e);
    Criteria getCriteria();
    List<E> getByCriteria(Criterion criterion);
	
}
