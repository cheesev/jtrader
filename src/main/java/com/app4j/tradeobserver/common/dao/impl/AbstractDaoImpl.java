package com.app4j.tradeobserver.common.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.ProfitLossStatementGAAPDto;

public class AbstractDaoImpl<E, I extends Serializable> implements AbstractDao<E,I> {

	@Autowired
    private SessionFactory sessionFactory;
    private Class<E> entityClass;
    
    public AbstractDaoImpl(Class<E> entityClass) {
        this.entityClass = entityClass;                
    }


    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public E getById(I id) {
        return (E) getCurrentSession().get(entityClass, id);
    }

    @Override
    public void saveOrUpdate(E e) {
        getCurrentSession().saveOrUpdate(e);
        
    }
    
    @Override
    public void multiSaveOrUpdate(List<E> e) {
    	for( E el : e) {    		
    		saveOrUpdate(el);    		
    	}
    }

    @Override
    public void delete(E e) {
        getCurrentSession().delete(e);
    }
    
    @Override
    public Criteria getCriteria() {
    	return getCurrentSession().createCriteria(entityClass);
    }

    @Override
    public List<E> getByCriteria(Criterion criterion) {
        Criteria criteria = getCriteria();
        criteria.add(criterion);
        return criteria.list();
    }

	@Override
	public void getColumnList() {
		AbstractEntityPersister aep=((AbstractEntityPersister)getCurrentSession().getSessionFactory().getClassMetadata(entityClass)); 
		String[] properties=aep.getPropertyNames();  
		for(int nameIndex=0;nameIndex!=properties.length;nameIndex++){  
			   System.out.println("Property name: "+properties[nameIndex]);  
			   String[] columns=aep.getPropertyColumnNames(nameIndex);  
			   for(int columnIndex=0;columnIndex!=columns.length;columnIndex++){  
			      System.out.println("Column name: "+columns[columnIndex]);  
			   }
		}  
		
	}
}
