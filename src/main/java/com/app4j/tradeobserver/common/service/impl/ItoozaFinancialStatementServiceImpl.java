package com.app4j.tradeobserver.common.service.impl;

import java.io.IOException;
import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.reflect.FieldUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.common.service.ItoozaFinancialStatementService;
import com.app4j.tradeobserver.common.util.CustomUtils;
import com.app4j.tradeobserver.data.constants.DataCommonConstants;
import com.app4j.tradeobserver.data.constants.ItoozaModeType;
import com.app4j.tradeobserver.data.constants.StatementType;
import com.app4j.tradeobserver.data.entity.ItoozaValueIndexDto;

@Transactional
public class ItoozaFinancialStatementServiceImpl<E, I extends Serializable, T_DAO extends AbstractDao<E,I>> implements ItoozaFinancialStatementService {
	
	@Autowired
	private T_DAO tDao;
	private Class<E> type;

	public ItoozaFinancialStatementServiceImpl(Class<E> type) { 
		this.type = type;
	}
	
	@Override
	public void insertBalanceSheet(String stockCode, StatementType statement, ItoozaModeType dateType,ItoozaModeType ccmode, ItoozaModeType lkmode) {
		String balanceSheetUrl = DataCommonConstants.ITOOZA_FINANCIAL_STATEMENT_URL_TEMPLATE;
		balanceSheetUrl = balanceSheetUrl.replace("%stockCode%", stockCode);
		balanceSheetUrl = balanceSheetUrl.replace("%dateType%", dateType.getModeType());
		balanceSheetUrl = balanceSheetUrl.replace("%statement%", statement.getStatementType());
		balanceSheetUrl = balanceSheetUrl.replace("%ccmode%", ccmode.getModeType());
		balanceSheetUrl = balanceSheetUrl.replace("%lkmode%", lkmode.getModeType());
		
		List<E> balanceSheetDtos = 
				this.getBalanceSheetDtoList(balanceSheetUrl, stockCode, dateType, ccmode);
		
		tDao.multiSaveOrUpdate(balanceSheetDtos);
	}	
	/*
	 * url로 접근하여 재무제표데이터를 받아온다....
	 */
	private List<E> getBalanceSheetDtoList(String balanceSheetUrl, String stockCode, ItoozaModeType dateType,ItoozaModeType ccmode) {
		
		Document doc = null;
		try {
			doc = Jsoup.connect(balanceSheetUrl).get();
		} catch (IOException e1) {
			throw new RuntimeException("ERROR : getBalanceSheetDtoList, cannot connect through jsoup" , e1);
		}
		Elements elements = doc.select(DataCommonConstants.BALANCE_SHEET_YMDAY_SELECTOR);
		CustomUtils.removeEmptyElement(elements);
		
		List<String> quaterList = CustomUtils.getElementText(elements, "[^0-9]");
		
		int size = elements.size();
		
		//데이터
		elements = doc.select(DataCommonConstants.BALANCE_SHEET_DATA_SELECTOR);
		CustomUtils.removeEmptyElement(elements);
		List<String> fieldNameList = CustomUtils.getHanguelFieldNames(type);
		
		ArrayList<E> dtoList = new ArrayList<E>();
		try {
			for(int i=0 ; i < elements.size() ; i++) {
				E dto = null;
				String curValue = elements.get(i).text();
				if (DataCommonConstants.NOT_APPLICABLE.equals(curValue)) {
					curValue = "0";
				} else {
					curValue = NumberFormat.getNumberInstance(Locale.US).parse(curValue).toString();
				}
				if( i / size > 0) {
					dto = dtoList.get(i % size);
					//FieldUtils.writeField(dto, fieldNameList.get(i / size), Double.valueOf(curValue), true);					
				} else {
					dto = this.createInstance(type);					
					dtoList.add(dto);
					FieldUtils.writeField(dto, "ymDay", quaterList.get(i), true);
					FieldUtils.writeField(dto, "stockCode", stockCode, true);
					FieldUtils.writeField(dto, "dateType", dateType.getModeType(), true);		
				}
				
				FieldUtils.writeField(dto, fieldNameList.get(i / size), Double.valueOf(curValue), true);
			}
			
		} catch (Exception e ) {
			e.printStackTrace();
			throw new RuntimeException("ERROR : getBalanceSheetDtoList" , e);
		}
		
		
		return dtoList;
	}
	
	E createInstance(Class<E> clazz)
    {
        try {
			return clazz.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return null;
    }


}
