package com.app4j.tradeobserver.common.service;

import java.util.List;

import com.app4j.tradeobserver.data.constants.ItoozaModeType;
import com.app4j.tradeobserver.data.constants.StatementType;

public interface ItoozaFinancialStatementService {
	void insertBalanceSheet(String stockCode, StatementType statement, ItoozaModeType dateType,ItoozaModeType ccmode, ItoozaModeType lkmode);
	
}
