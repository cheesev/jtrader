package com.app4j.tradeobserver.common.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.ParameterizedType;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.reflect.FieldUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.common.service.FinancialStatementService;
import com.app4j.tradeobserver.common.util.CustomUtils;
import com.app4j.tradeobserver.data.constants.DataCommonConstants;
import com.app4j.tradeobserver.data.constants.FinancialAccountType;

@Transactional
public class FinancialStatementServiceImpl<E, I extends Serializable, T_DAO extends AbstractDao<E,I>> implements FinancialStatementService {
	
	@Autowired
	private T_DAO tDao;
	private Class<E> type;
	
	@SuppressWarnings("unchecked")
    public FinancialStatementServiceImpl() {
        this.type = (Class<E>)
                ((ParameterizedType)getClass()
                .getGenericSuperclass())
                .getActualTypeArguments()[0];
    }
	
	public FinancialStatementServiceImpl(Class<E> type) { 
		this.type = type;
	}
	
	@Override
	public void insertFinancialStatement(String stockCode, FinancialAccountType financialAccountType, String[] tableSelector) {
		String dataType = financialAccountType.toString();
		String targetUrl = DataCommonConstants.FINANCIAL_STATEMENT_URL_TEMPLATE;
		targetUrl = targetUrl.replace("STOCK_CODE", stockCode);
		targetUrl = targetUrl.replace("DATA_TYPE", dataType);
		
		String[] ymDayArr = null;
		List<E> dataList = new ArrayList<E>();
		try {
			
			Document doc = Jsoup.connect(targetUrl).maxBodySize(0).timeout(10000).get();	// 타임아웃 10초좀.		
			Elements elements = doc.select("script[type=text/javascript]");
			for(Element element : elements) {				
					if(element.childNodes().size() > 0 
						&& element.childNodes().get(0).toString().startsWith("var changeYYMM")) {
						
						String ymDayStr = element.childNodes().get(0).toString().replaceAll("[^0-9,]", "");
						ymDayArr = ymDayStr.split(",");
					}
			}
			
			if (ymDayArr == null || ymDayArr.length < 1) {
				//에러로그...
				return;
			}			
			dataList.addAll(this.getProfitLossStatement(doc, stockCode, "Y", tableSelector[0], dataType, ymDayArr, 0)); // tableSelector 0 : 연간
			
			dataList.addAll(this.getProfitLossStatement(doc, stockCode, "Q", tableSelector[1], dataType, ymDayArr, DataCommonConstants.DATA_CNT));// tableSelector 1 : 분기
			
			tDao.multiSaveOrUpdate(dataList);
//			List<String> fieldNameList = CustomUtils.getHanguelFieldNames(BalanceSheetGAAPDto.class);
//			
//			//연간이.. 5개.
//			for(int lineNum = DataCommonConstants.BALANCE_SHEET_LINE_START_NUM;
//					lineNum < DataCommonConstants.BALANCE_SHEET_LINE_START_NUM + DataCommonConstants.BALANCE_SHEET_DATA_CNT; lineNum++) {
//				
//				elements = doc.select(DataCommonConstants.BALANCE_SHEET_YEAR_TABLE_SELECTOR + " " + 
//									  DataCommonConstants.BALANCE_SHEET_LINE_SELECTOR.replace("%start_num%", String.valueOf(lineNum)));
//				
//				
//				BalanceSheetGAAPDto balanceSheetGAAPDto = new BalanceSheetGAAPDto();
//				balanceSheetGAAPDto.setStockCode(stockCode);
//				balanceSheetGAAPDto.setYmDay(ymDayArr[ymdDayArrCursor++]);
//				balanceSheetGAAPDto.setDateType("Y");
//				balanceSheetGAAPDto.setDataType(dataType);
//				
//				
//				for(int i=0 ; i < elements.size() ; i++) {
//					String curText = elements.get(i).text();
//					if (curText != null && curText.length() > 0)
//						FieldUtils.writeField(balanceSheetGAAPDto, fieldNameList.get(i), Double.valueOf(curText), true);
//					balanceSheetGAAPDtos.add(balanceSheetGAAPDto);			
//				}
//			}
			
			//분기
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("ERROR :" + e.toString());
		}		
	}	
	
	private List<E> getProfitLossStatement(Document doc, String stockCode, String dateType , String tableSelector
									,String dataType, String[] ymDayArr, int ymdDayArrCursor) throws NumberFormatException, IllegalAccessException {
		List<String> dtoFieldException = new ArrayList<String>();
		dtoFieldException.add("stockCode");
		dtoFieldException.add("ymDay");
		dtoFieldException.add("dateType");
		dtoFieldException.add("dataType");
		
		Elements elements = null;
		List<E> dtoList = new ArrayList<E>();
		
		List<String> fieldNameList = CustomUtils.getFieldNames(type, dtoFieldException);		// 4개의 field를 제외하고 가져옴.. 근데 할때마다 가져오는건 문제임.
//		String tableSelector = null;
//		if ("Y".equals(dateType)) {
//			tableSelector = DataCommonConstants.YEAR_TABLE_SELECTOR;
//		} else {
//			tableSelector = DataCommonConstants.QUATER_TABLE_SELECTOR;
//		}
		
		//연간, 분기 모두..5개.
		for(int lineNum = DataCommonConstants.LINE_START_NUM;
				lineNum < DataCommonConstants.LINE_START_NUM + DataCommonConstants.DATA_CNT; lineNum++) {
			
			if ( StringUtils.isEmpty(ymDayArr[ymdDayArrCursor]) ) {
				ymdDayArrCursor++;
				continue; // ymday .. 상장을 최근에 한 경우에는 없으니까.. 계속 진행해버림.			
			}
			
			elements = doc.select(tableSelector + " " + 
								  DataCommonConstants.LINE_SELECTOR.replace("%start_num%", String.valueOf(lineNum)));
			
			E dto = this.createInstance(type);
			
			FieldUtils.writeField(dto , "stockCode", stockCode, true);
			FieldUtils.writeField(dto , "ymDay", ymDayArr[ymdDayArrCursor++], true);
			FieldUtils.writeField(dto , "dateType", dateType, true);
			FieldUtils.writeField(dto , "dataType", dataType, true);			
			
			for(int i=0 ; i < elements.size() ; i++) {
				String curText = elements.get(i).text();
				if (curText != null && curText.length() > 0) {
					curText = curText.replaceAll("," , "");
					FieldUtils.writeField(dto, fieldNameList.get(i), Double.valueOf(curText), true);
				}
			}
			
			dtoList.add(dto);
		}
		
		return dtoList;
				
	}
	
	E createInstance(Class<E> clazz)
    {
        try {
			return clazz.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return null;
    }

}
