package com.app4j.tradeobserver.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;

public class DateUtil {
	
	public static final String STOCK_TRADE_SITUATION_MIN_DAY = "20040101";
	public static final String DEFAULT_DATE_FORMAT = "yyyyMMdd";
	static final long ONE_HOUR = 60 * 60 * 1000L;
	
	
	
	
	public static String addYmdDay(Date date, int year, int month, int day){
		date = DateUtils.addYears(date, year);
		date = DateUtils.addMonths(date, month);
		date = DateUtils.addDays(date, day);
		
		return DateFormatUtils.format(date, "yyyyMMdd");
	}
	
	public static String addYmdDay(String ymdDay, int year, int month, int day){
		Date date = DateUtil.getYmdFormatDate(ymdDay);
		date = DateUtils.addYears(date, year);
		date = DateUtils.addMonths(date, month);
		date = DateUtils.addDays(date, day);
		
		return DateFormatUtils.format(date, "yyyyMMdd");
	}
	
	public static Date getYmdFormatDate(String ymdDay) {
		Date resultDate = null;
		try {
			resultDate = new SimpleDateFormat("yyyyMMdd").parse(ymdDay);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultDate;
	}
	
	//deprecated
	public static long daysBetween(Date fromDate, Date toDate) {
		Date temp = null;
		if(fromDate.after(toDate)) {
			temp = fromDate;
			fromDate = toDate;
			toDate = temp;
		}			
		return ((toDate.getTime() - fromDate.getTime() + ONE_HOUR) / (ONE_HOUR * 24));
	}
	
	public static long daysBetween(String fromDay, String toDay) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT);
		LocalDate fromDate = LocalDate.parse(fromDay , formatter);
		LocalDate toDate = LocalDate.parse(toDay , formatter);
		
		return ChronoUnit.DAYS.between(fromDate, toDate);
	}
	
	public static String getCurDateYmdFormat() {
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	    Date now = new Date();
	    String strDate = sdf.format(now);
	    return strDate;
	}
}
