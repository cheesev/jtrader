package com.app4j.tradeobserver.common.util;

import java.math.BigDecimal;
import java.util.List;

public class ListUtil {

	public static BigDecimal sum(List<?> list) {

		BigDecimal result = new BigDecimal("0");

		if (list == null || list.size() < 1) return null;

		for (Object i : list) {
			result = result.add(new BigDecimal(i.toString()));
		}

		return result;

	}

}
