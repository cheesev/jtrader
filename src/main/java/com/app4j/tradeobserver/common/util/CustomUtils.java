package com.app4j.tradeobserver.common.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CustomUtils {
	
	public static List<String> getHanguelFieldNames(Class<?> cls) {
		List<String> fieldNameList = new ArrayList<String>();
		
		for(Field field : cls.getDeclaredFields()) {
			if (!field.getName().matches("^[a-zA-Z0-9]+$")) {
				fieldNameList.add(field.getName());			
			}
		}
		
		return fieldNameList;
		
	}
	
	public static List<String> getFieldNames(Class<?> cls, List<String> exception) {
		List<String> fieldNameList = new ArrayList<String>();
		
		for(Field field : cls.getDeclaredFields()) {
			if(!exception.contains(field.getName())) {
				fieldNameList.add(field.getName());
			}				
		}	
		return fieldNameList;
	}
	
	public static void removeEmptyElement(Elements elements) {
		for (int i=0; i < elements.size(); i++) {
			if(elements.get(i).text().isEmpty()) elements.remove(i);
		}
	}
	
	public static void removeElement(Elements elements, String str) {
		for (int i=0; i < elements.size(); i++) {
			if(str.equals(elements.get(i).text())) elements.remove(i);
		}
	}
	
	/*
	 * element text를 특정 형식에 맞게 변형해서 리턴..
	 */
	public static List<String> getElementText(Elements elements, String regex) {
		List<String> textList = new ArrayList<String>();
		for(Element element : elements) {
			String thisText = element.text().replaceAll(regex, "");
			
			if (!StringUtils.isEmpty(thisText)) {
				textList.add(thisText);	
			}
		}		
		return textList;
	}
	
	

}
