package com.app4j.tradeobserver.common.util;

import java.util.ArrayList;
import java.util.List;

public class StockUtil {
	
	// 본주를 포함한.. 우선주까지 연관된 주식을 배열로 리턴.
	public static String[] getAllAssociatedStock(String stockCode) {
		List<String> stockCodeList = new ArrayList<String>();
		int intStockCode = Integer.valueOf(stockCode);
		stockCodeList.add(stockCode);
		if ( intStockCode % 10 == 0 ) {
			stockCodeList.add(String.format("%06d", intStockCode + 5));
			stockCodeList.add(String.format("%06d", intStockCode + 7));
			stockCodeList.add(String.format("%06d", intStockCode + 9));			
		}
		
		return stockCodeList.toArray(new String[stockCodeList.size()]);
	
	}

}
