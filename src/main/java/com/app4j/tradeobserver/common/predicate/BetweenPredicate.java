package com.app4j.tradeobserver.common.predicate;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.time.DateUtils;

import com.app4j.tradeobserver.common.util.DateUtil;
import com.app4j.tradeobserver.stock.entity.StockTradeSituationDto;

/*
 * SQL 과 마찬가지로 between 도 to, fromDate 포함..함
 */
public class BetweenPredicate implements Predicate{
	private Date fromDate;
	private Date toDate;
	
	public BetweenPredicate(Date fromDate, Date toDate) {
		this.fromDate = fromDate;
		this.toDate = toDate;
	}
	
	public BetweenPredicate(String fromYmdDay, String toYmdDay) {
		this.fromDate = DateUtil.getYmdFormatDate(fromYmdDay);
		this.toDate = DateUtil.getYmdFormatDate(toYmdDay);
	}

	@Override
	public boolean evaluate(Object object){
		// TODO Auto-generated method stub
		if(object instanceof StockTradeSituationDto) {
			Date ymdDate = null;
			ymdDate = DateUtil .getYmdFormatDate(((StockTradeSituationDto) object).getYmdDay());
			
			return ymdDate.after(DateUtils.addDays(fromDate, -1))
				&& ymdDate.before(DateUtils.addDays(toDate, +1));
		}
		return false;
	}
}
