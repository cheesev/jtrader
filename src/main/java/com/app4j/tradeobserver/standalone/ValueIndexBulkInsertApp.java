package com.app4j.tradeobserver.standalone;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.app4j.tradeobserver.data.constants.DataCommonConstants;
import com.app4j.tradeobserver.data.constants.FinancialAccountType;
import com.app4j.tradeobserver.data.service.CashFlowStatementIFRSService;
import com.app4j.tradeobserver.data.service.FinancialPositionStatementIFRSService;
import com.app4j.tradeobserver.data.service.ProfitLossStatementIFRSService;
import com.app4j.tradeobserver.data.service.ValueIndexService;
import com.app4j.tradeobserver.stock.entity.StockInfoDto;
import com.app4j.tradeobserver.stock.service.StockInfoService;
import com.app4j.tradeobserver.task.BalanceSheetTask;
import com.app4j.tradeobserver.task.ValueIndexBulkInsertTask;

public class ValueIndexBulkInsertApp {
		
	public static int THREAD_NUM = 10;
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"spring/root-context.xml", "spring/appServlet/servlet-context.xml", "spring/hibernate-context.xml"});
		
		ValueIndexService valueIndexService
		 = (ValueIndexService)context.getBean("valueIndexService");
		
		StockInfoService stockInfoService = (StockInfoService)context.getBean("stockInfoService");
		
		
		ExecutorService executor = Executors.newFixedThreadPool(THREAD_NUM);
		
		List<ValueIndexBulkInsertTask> valueIndexBulkInsertTaskList = new ArrayList<ValueIndexBulkInsertTask>();		
		
		List<StockInfoDto> stockInfoDtoList = stockInfoService.getAllStockInfoList();		
		
		for(int idx = 0; idx < stockInfoDtoList.size() ; idx++) {
			ValueIndexBulkInsertTask valueIndexBulkInsertTask = new ValueIndexBulkInsertTask(
					valueIndexService, stockInfoDtoList.get(idx).getStockCode());
			
			valueIndexBulkInsertTaskList.add(valueIndexBulkInsertTask);
			
			if(valueIndexBulkInsertTaskList.size() == THREAD_NUM || idx == stockInfoDtoList.size() -1) {
				List<Future<String>> futures = executor.invokeAll(valueIndexBulkInsertTaskList);

				for(Future<String> future : futures){
					System.out.println("success code : " + future.get());				    
				}
				valueIndexBulkInsertTaskList.clear();
			}
			
		}
		
		System.out.println("end program");
		

		executor.shutdown();
		
		
	}

}
