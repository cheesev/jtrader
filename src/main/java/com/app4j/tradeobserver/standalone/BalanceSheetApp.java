package com.app4j.tradeobserver.standalone;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.app4j.tradeobserver.data.constants.DataCommonConstants;
import com.app4j.tradeobserver.data.constants.FinancialAccountType;
import com.app4j.tradeobserver.data.service.CashFlowStatementIFRSService;
import com.app4j.tradeobserver.data.service.FinancialPositionStatementIFRSService;
import com.app4j.tradeobserver.data.service.ProfitLossStatementIFRSService;
import com.app4j.tradeobserver.stock.entity.StockInfoDto;
import com.app4j.tradeobserver.stock.service.StockInfoService;
import com.app4j.tradeobserver.task.BalanceSheetTask;

public class BalanceSheetApp {
		
	public static int THREAD_NUM = 15;
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"spring/root-context.xml", "spring/appServlet/servlet-context.xml", "spring/hibernate-context.xml"});
		
		FinancialPositionStatementIFRSService financialPositionStatementIFRSService
		 = (FinancialPositionStatementIFRSService)context.getBean("financialPositionStatementIFRSService");
		
		ProfitLossStatementIFRSService profitLossStatementIFRSService
		 = (ProfitLossStatementIFRSService)context.getBean("profitLossStatementIFRSService");
		CashFlowStatementIFRSService cashFlowStatementIFRSService
		 = (CashFlowStatementIFRSService)context.getBean("cashFlowStatementIFRSService");
		
		StockInfoService stockInfoService = (StockInfoService)context.getBean("stockInfoService");
		
		
		ExecutorService executor = Executors.newFixedThreadPool(THREAD_NUM);
		
		List<BalanceSheetTask> balanceSheetTaskList = new ArrayList<BalanceSheetTask>();		
		
		List<StockInfoDto> stockInfoDtoList = stockInfoService.getAllStockInfoList();		
		
		for(int idx = 0; idx < stockInfoDtoList.size() ; idx++) {
			BalanceSheetTask financialPositionTask = new BalanceSheetTask(financialPositionStatementIFRSService
					, stockInfoDtoList.get(idx).getStockCode()
					, FinancialAccountType.IFRS_EACH
					, DataCommonConstants.FINANCIAL_POSITION_STATMENT_TABLE);
			BalanceSheetTask ProfitLossTask = new BalanceSheetTask(profitLossStatementIFRSService
					, stockInfoDtoList.get(idx).getStockCode()
					, FinancialAccountType.IFRS_EACH
					, DataCommonConstants.PROFITLOSS_STATEMENT_TABLE);
			BalanceSheetTask cashFlowTask = new BalanceSheetTask(cashFlowStatementIFRSService
					, stockInfoDtoList.get(idx).getStockCode()
					, FinancialAccountType.IFRS_EACH
					, DataCommonConstants.CASHFLOW_STATEMENT_TABLE);	
			
			balanceSheetTaskList.add(financialPositionTask);
			balanceSheetTaskList.add(ProfitLossTask);
			balanceSheetTaskList.add(cashFlowTask);
			
			if(balanceSheetTaskList.size() == THREAD_NUM || idx == stockInfoDtoList.size() -1) {
				System.out.println("start... code :" + stockInfoDtoList.get(idx).getStockCode());
				List<Future<String>> futures = executor.invokeAll(balanceSheetTaskList);

				for(Future<String> future : futures){
					System.out.println("processing... code : " + future.get());				    
				}
				System.out.println("end...");
				
				balanceSheetTaskList.clear();
			}
		}
		
		

		executor.shutdown();
		
	}

}
