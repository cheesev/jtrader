package com.app4j.tradeobserver.stock.service.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.reflect.FieldUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.util.DateUtil;
import com.app4j.tradeobserver.stock.dao.StockTradeSituationDao;
import com.app4j.tradeobserver.stock.entity.StockTradeSituationDto;
import com.app4j.tradeobserver.stock.service.SupplyAndDemandService;


@Service("supplyAndDemandService")
@Transactional(readOnly=true)
public class SupplyAndDemandServiceImpl implements SupplyAndDemandService{

//	@Autowired
	@Resource(name="stockTradeSituationDao")
	private StockTradeSituationDao stockTradeSituationDao;
	
	@Override
	public StockTradeSituationDto getStockTradeSituation(String ymdDay, String stockCode) {
		return stockTradeSituationDao.getStockTradeSituation(ymdDay, stockCode);		
	}

	@Override
	public List<StockTradeSituationDto> getStockTradeSituationList(String fromDay,
			String toDay, String stockCode) {
		return stockTradeSituationDao.getStockTradeSituationList(fromDay, toDay, stockCode);
	}
	
	@Override
	public List<StockTradeSituationDto> getStockTradeSituationList(String stockCode, int size) {
		return stockTradeSituationDao.getStockTradeSituationList(stockCode, size);
	}

	@Override
	public String getMaxYmdDay(String stockCode) {
		return stockTradeSituationDao.getMaxYmdDay(stockCode);
	}

	@Override
	public StockTradeSituationDto sumSupplyAndSetAvgPrice(
			List<StockTradeSituationDto> stockTradeSituationList) {
		StockTradeSituationDto resultDto = new StockTradeSituationDto();
		for (StockTradeSituationDto stockTradeSituationDto : stockTradeSituationList) {
			resultDto.addBuyAmt(stockTradeSituationDto);
		}
		resultDto.setPrice(this.getAvgPrice(stockTradeSituationList));
		resultDto.setYmdDay(stockTradeSituationList.size() + "일");
		return resultDto;
	}
	
	@Override
	public List<StockTradeSituationDto> getCumulativeStockTradeSituationList(String fromDay, String toDay, String stockCode) {
		StockTradeSituationDto addCloneDto = null;
		List<StockTradeSituationDto> cumulativeStockTradeSituationList = new ArrayList<StockTradeSituationDto>();
		List<StockTradeSituationDto> stockTradeSituationDtoList = this.getStockTradeSituationList(fromDay, toDay, stockCode);
		
		for(StockTradeSituationDto stockTradeSituationDto : stockTradeSituationDtoList) {
			if(cumulativeStockTradeSituationList.size() == 0) {
				cumulativeStockTradeSituationList.add(stockTradeSituationDto);
			} else {
				//addCloneDto = (StockTradeSituationDto) cumulativeStockTradeSituationList.get(cumulativeStockTradeSituationList.size() - 1).clone();
				addCloneDto = (StockTradeSituationDto) stockTradeSituationDto.clone();
				addCloneDto.addBuyAmt(cumulativeStockTradeSituationList.get(cumulativeStockTradeSituationList.size() - 1));
				cumulativeStockTradeSituationList.add(addCloneDto);
			}
		}
//		for(int i = stockTradeSituationDtoList.size() - 1 ; i >= 0 ; i --) {
//			if(i == stockTradeSituationDtoList.size() -1) {
//				cumulativeStockTradeSituationList.add(stockTradeSituationDtoList.get(i));
//			} else {
//				//addCloneDto = (StockTradeSituationDto) cumulativeStockTradeSituationList.get(cumulativeStockTradeSituationList.size() - 1).clone();
//				addCloneDto = (StockTradeSituationDto) stockTradeSituationDtoList.get(i).clone();
//				addCloneDto.addBuyAmt(cumulativeStockTradeSituationList.get(cumulativeStockTradeSituationList.size() - 1));
//				cumulativeStockTradeSituationList.add(addCloneDto);
//			}
//		}		
		return cumulativeStockTradeSituationList;
	}

	@Override
	public StockTradeSituationDto getMaxBuyAmt(List<StockTradeSituationDto> cumulativeTradeSituationList) {
		StockTradeSituationDto maxDto = null;
		
		for(StockTradeSituationDto stockTradeSituationDto : cumulativeTradeSituationList) {
			if(maxDto == null) { 
				maxDto = (StockTradeSituationDto) stockTradeSituationDto.clone();
			} else {
				maxDto.setComparableMax(stockTradeSituationDto);
			}
		}
		maxDto.setYmdDay("최대수량");
		maxDto.setPrice(0);
		return maxDto;
	}
	
	@Override
	public StockTradeSituationDto getMinBuyAmt(List<StockTradeSituationDto> cumulativeTradeSituationList) {
		StockTradeSituationDto minDto = null;
		
		for(StockTradeSituationDto stockTradeSituationDto : cumulativeTradeSituationList) {
			if(minDto == null) {
				minDto = (StockTradeSituationDto) stockTradeSituationDto.clone();
			} else {
				minDto.setComparableMin(stockTradeSituationDto);
			}
		}
		return minDto;
	}
	
	@Override
	public StockTradeSituationDto getCurBuyAmt(List<StockTradeSituationDto> stockTradeSituationDtoList) {		
		StockTradeSituationDto resultDto = stockTradeSituationDtoList.get(stockTradeSituationDtoList.size() -1);
		
		resultDto.setYmdDay("현재수량");
		resultDto.setPrice(0);
		
		return resultDto;
	}

	@Override
	public StockTradeSituationDto getVarianceRatio(
			StockTradeSituationDto curBuyAmtDto,
			StockTradeSituationDto minBuyAmtDto,
			StockTradeSituationDto maxBuyAmtDto) {
		StockTradeSituationDto resultDto = new StockTradeSituationDto();
		resultDto.setYmdDay("분산비율");
		resultDto.setForceGrp( (maxBuyAmtDto.getForceGrp() == 0) ? 0 : ( curBuyAmtDto.getForceGrp() - minBuyAmtDto.getForceGrp() ) * 100 / maxBuyAmtDto.getForceGrp() ); 
		resultDto.setAnt( (maxBuyAmtDto.getAnt() == 0) ? 0 : ( curBuyAmtDto.getAnt() - minBuyAmtDto.getAnt() ) * 100 / maxBuyAmtDto.getAnt() );
		resultDto.setForeigner( (maxBuyAmtDto.getForeigner() == 0) ? 0 : ( curBuyAmtDto.getForeigner() - minBuyAmtDto.getForeigner()) * 100/ maxBuyAmtDto.getForeigner() );
		resultDto.setInstitution( (maxBuyAmtDto.getInstitution() == 0) ? 0 : (curBuyAmtDto.getInstitution() - minBuyAmtDto.getInstitution()) * 100 / maxBuyAmtDto.getInstitution() );
		resultDto.setFinancialInvestment( (maxBuyAmtDto.getFinancialInvestment() == 0) ? 0 : (curBuyAmtDto.getFinancialInvestment() - minBuyAmtDto.getFinancialInvestment()) * 100 / maxBuyAmtDto.getFinancialInvestment() );
		resultDto.setTrust( (maxBuyAmtDto.getTrust() == 0) ? 0 : (curBuyAmtDto.getTrust() - minBuyAmtDto.getTrust()) * 100 / maxBuyAmtDto.getTrust() );
		resultDto.setAssurance( (maxBuyAmtDto.getAssurance() == 0) ? 0 : (curBuyAmtDto.getAssurance() - minBuyAmtDto.getAssurance()) * 100 / maxBuyAmtDto.getAssurance() );
		resultDto.setBank( (maxBuyAmtDto.getBank() == 0) ? 0 : (curBuyAmtDto.getBank() - minBuyAmtDto.getBank()) * 100 / maxBuyAmtDto.getBank() );
		resultDto.setInvestmentBank( (maxBuyAmtDto.getInvestmentBank() == 0) ? 0 : (curBuyAmtDto.getInvestmentBank() - minBuyAmtDto.getInvestmentBank()) * 100 / maxBuyAmtDto.getInvestmentBank() );
		resultDto.setPension( (maxBuyAmtDto.getPension() == 0) ? 0 : (curBuyAmtDto.getPension() - minBuyAmtDto.getPension()) * 100 / maxBuyAmtDto.getPension() );		
		resultDto.setPrivateEquityFund( (maxBuyAmtDto.getPrivateEquityFund() == 0) ? 0 : (curBuyAmtDto.getPrivateEquityFund() - minBuyAmtDto.getPrivateEquityFund()) * 100 / maxBuyAmtDto.getPrivateEquityFund() );
		resultDto.setInnerForeigner( (maxBuyAmtDto.getInnerForeigner() == 0) ? 0 : (curBuyAmtDto.getInnerForeigner() - minBuyAmtDto.getInnerForeigner()) * 100 / maxBuyAmtDto.getInnerForeigner() );
		resultDto.setEtc( (maxBuyAmtDto.getEtc() == 0) ? 0 : (curBuyAmtDto.getEtc() - minBuyAmtDto.getEtc()) * 100 / maxBuyAmtDto.getEtc() );
		
		return resultDto;
	}

	/*
	 * 상관계수는 cumulativeStockDtoList 로 해야한다. 누적치 기준으로 누가 주가에 영향을 더주나.. 이런식으로 함.
	 */
	@Override
	public StockTradeSituationDto getCorrelation(
			List<StockTradeSituationDto> stockTradeSituationDtoList) {
		
		HashMap<String, Double> corrResultMap = new HashMap<String, Double>();
		PearsonsCorrelation corr = new PearsonsCorrelation();
		
		StockTradeSituationDto resultDto = new StockTradeSituationDto();
		HashMap<String, List<Double>> investorListMap = new HashMap<String, List<Double>>();
		
		investorListMap.put("price", new ArrayList<Double>());
		investorListMap.put("forceGrp", new ArrayList<Double>());
		investorListMap.put("ant", new ArrayList<Double>());
		investorListMap.put("foreigner", new ArrayList<Double>());
		investorListMap.put("institution", new ArrayList<Double>());
		investorListMap.put("financialInvestment", new ArrayList<Double>());
		investorListMap.put("trust", new ArrayList<Double>());
		investorListMap.put("bank", new ArrayList<Double>());
		investorListMap.put("investmentBank", new ArrayList<Double>());
		investorListMap.put("pension", new ArrayList<Double>());
		investorListMap.put("privateEquityFund", new ArrayList<Double>());
		investorListMap.put("innerForeigner", new ArrayList<Double>());
		investorListMap.put("etc", new ArrayList<Double>());
		
		for(StockTradeSituationDto stockTradeSituationDto : stockTradeSituationDtoList) {
			investorListMap.get("price").add((double)stockTradeSituationDto.getPrice());
			investorListMap.get("forceGrp").add((double)stockTradeSituationDto.getForceGrp());
			investorListMap.get("ant").add((double)stockTradeSituationDto.getAnt());
			investorListMap.get("foreigner").add((double)stockTradeSituationDto.getForeigner());
			investorListMap.get("institution").add((double)stockTradeSituationDto.getInstitution());
			investorListMap.get("financialInvestment").add((double)stockTradeSituationDto.getFinancialInvestment());
			investorListMap.get("trust").add((double)stockTradeSituationDto.getTrust());
			investorListMap.get("bank").add((double)stockTradeSituationDto.getBank());
			investorListMap.get("investmentBank").add((double)stockTradeSituationDto.getInvestmentBank());
			investorListMap.get("pension").add((double)stockTradeSituationDto.getPension());
			investorListMap.get("privateEquityFund").add((double)stockTradeSituationDto.getPrivateEquityFund());
			investorListMap.get("innerForeigner").add((double)stockTradeSituationDto.getInnerForeigner());
			investorListMap.get("etc").add((double)stockTradeSituationDto.getEtc());
		}		
		
		Double[] priceArr = investorListMap.get("price").toArray(new Double[investorListMap.get("price").size()]);
		
		for(String key : investorListMap.keySet()) {
			Double[] keyArr = investorListMap.get(key).toArray(new Double[investorListMap.get(key).size()]);						
			corrResultMap.put( 	key, 
							  	corr.correlation(ArrayUtils.toPrimitive(priceArr), ArrayUtils.toPrimitive(keyArr)) ); 
		}
		resultDto.setYmdDay("상관계수");
		resultDto.setForceGrp( (int)Math.round(corrResultMap.get("forceGrp") * 100) );
		resultDto.setAnt( (int)Math.round(corrResultMap.get("ant") * 100) );
		resultDto.setForeigner( (int)Math.round(corrResultMap.get("foreigner") * 100) );
		resultDto.setInstitution( (int)Math.round(corrResultMap.get("institution") * 100) );
		resultDto.setFinancialInvestment( (int)Math.round(corrResultMap.get("financialInvestment") * 100) );
		resultDto.setTrust( (int)Math.round(corrResultMap.get("trust") * 100) );
		resultDto.setBank( (int)Math.round(corrResultMap.get("bank") * 100) );
		resultDto.setInvestmentBank( (int)Math.round(corrResultMap.get("investmentBank") * 100) );
		resultDto.setPension( (int)Math.round(corrResultMap.get("pension") * 100) );
		resultDto.setPrivateEquityFund( (int)Math.round(corrResultMap.get("privateEquityFund") * 100) );
		resultDto.setInnerForeigner( (int)Math.round(corrResultMap.get("innerForeigner") * 100) );
		resultDto.setEtc( (int)Math.round(corrResultMap.get("etc") * 100) );
		
		return resultDto;
	}

	@Override
	public int getAvgPrice(String fromDay, String toDay, String stockCode) {
		// TODO Have to Implement
		return -10000;
	}

	/*
	 * average of forceGrp + ant buy price 
	 */
	@Override
	public int getAvgPrice(List<StockTradeSituationDto> stockTradeSituationDtoList) {
		//List는 asc로 정렬되어있다고 가정.;
		long cumulativeBuyAmt = 0;
		long cumulativeBuyPrice = 0;
		for(StockTradeSituationDto stockTradeSituationDto : stockTradeSituationDtoList) {
			if ( stockTradeSituationDto.getAnt() + stockTradeSituationDto.getForceGrp()  > 0 ) {
				cumulativeBuyAmt += stockTradeSituationDto.getAnt() + stockTradeSituationDto.getForceGrp();
				cumulativeBuyPrice += ( (long)stockTradeSituationDto.getAnt() + (long)stockTradeSituationDto.getForceGrp() ) * (long)stockTradeSituationDto.getPrice();
			}
		}
		if (cumulativeBuyAmt < 0)
			return -10000;
		
		return (int)(cumulativeBuyPrice / cumulativeBuyAmt);
	}
	
	@Override
	public StockTradeSituationDto getAvgPriceByInvestors(
			List<StockTradeSituationDto> stockTradeSituationDtoList, StockTradeSituationDto minDto) {
		//List 는 asc로 정렬되어있다고 가정.
		
		String fieldName = null;
		long avgPrice = 0;
		long curPrice;
		long cumulativeBuyAmt;
		long cumulativeBuyPrice;
		long curBuyAmt =0;
		StockTradeSituationDto stockTradeAvgDto = new StockTradeSituationDto();
		
		for(Field field : StockTradeSituationDto.class.getDeclaredFields() ) {
			fieldName = field.getName();
			cumulativeBuyAmt = 0;
			cumulativeBuyPrice = 0;
			curPrice = stockTradeSituationDtoList.get(0).getPrice();
			if( field.getType().getCanonicalName() == "int" 
					&& !fieldName.equals("price")) {
				try {
					cumulativeBuyAmt = ((Integer) FieldUtils.readDeclaredField(minDto, fieldName, true)) * -1;
					if( cumulativeBuyAmt != 0) { 
						cumulativeBuyPrice = curPrice * cumulativeBuyAmt;
						avgPrice = (cumulativeBuyPrice / cumulativeBuyAmt);
					}
					for(StockTradeSituationDto stockTradeSituationDto : stockTradeSituationDtoList) {						
							curBuyAmt = (Integer) FieldUtils.readField(stockTradeSituationDto, fieldName, true);
							curPrice = stockTradeSituationDto.getPrice();
							cumulativeBuyAmt += curBuyAmt;
							if(curBuyAmt >=0) cumulativeBuyPrice += (curPrice * curBuyAmt);
							else cumulativeBuyPrice = cumulativeBuyAmt * avgPrice;
							
							if (cumulativeBuyAmt == 0) continue;
							else if(cumulativeBuyAmt < 0) {
								System.out.println(stockTradeSituationDto.getYmdDay() + " :: " + fieldName);
								throw new RuntimeException();
							
							}
							
							avgPrice = (cumulativeBuyPrice / cumulativeBuyAmt);
					}
					FieldUtils.writeDeclaredField(stockTradeAvgDto, fieldName, (int)avgPrice , true);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					System.out.println(fieldName + " : " + stockTradeAvgDto.getYmdDay());
				}
			}
		}
		stockTradeAvgDto.setYmdDay("평균가격");
		return stockTradeAvgDto;
		
	}
	
}



