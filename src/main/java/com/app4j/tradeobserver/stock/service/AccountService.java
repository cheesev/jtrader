package com.app4j.tradeobserver.stock.service;

import java.util.List;

import com.app4j.tradeobserver.stock.entity.StockAccountHistoryDto;
import com.app4j.tradeobserver.stock.entity.StockCurrentAccountDto;
import com.app4j.tradeobserver.stock.entity.StockInfoDto;

public interface AccountService {
	
	List<StockCurrentAccountDto> getCurrentAccount();	
	List<StockAccountHistoryDto> getCumulativeStockAccountHistory(String acctNum, String fromDate, String toDate);

}
