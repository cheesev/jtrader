package com.app4j.tradeobserver.stock.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.util.DateUtil;
import com.app4j.tradeobserver.stock.dao.StockAccountHistoryDao;
import com.app4j.tradeobserver.stock.dao.StockCurrentAccountDao;
import com.app4j.tradeobserver.stock.entity.StockAccountHistoryDto;
import com.app4j.tradeobserver.stock.entity.StockCurrentAccountDto;
import com.app4j.tradeobserver.stock.service.AccountService;


@Service("accountService")
@Transactional(readOnly=true)
public class AccountServiceImpl implements AccountService{
	
	@Resource(name="stockCurrentAccountDao")
	private StockCurrentAccountDao stockCurrentAccountDao;
	
	@Resource(name="stockAccountHistoryDao")
	private StockAccountHistoryDao stockAccountHistoryDao;

	@Override
	public List<StockCurrentAccountDto> getCurrentAccount() {		
		return stockCurrentAccountDao.getCurrentAccount();	
	}
	
	@Override
	public List<StockAccountHistoryDto> getCumulativeStockAccountHistory(String acctNum, String fromDate, String toDate) {
		List<StockAccountHistoryDto> acctHistDtos =
				stockAccountHistoryDao.getCumulativeStockAccountHistory(acctNum, fromDate, toDate);
		
		String strtDate = acctHistDtos.get(0).getYmdDay();
		BigDecimal beginNetAsset = new BigDecimal( acctHistDtos.get(0).getBeginingNetAssets() ); //기초자산
		
		BigDecimal investedBalanceAmt = new BigDecimal("0");
		
		for( int idx = 0; idx < acctHistDtos.size() ; idx++) {
			String endDate = acctHistDtos.get(idx).getYmdDay();
			BigDecimal totalDeposit = new BigDecimal("0");
			BigDecimal totalWithdrawal = new BigDecimal("0"); // 그날까지의 total 의 의미임.
			BigDecimal durationDay = new BigDecimal(String.valueOf(DateUtil.daysBetween(strtDate, endDate) + 1));
			BigDecimal subDurationDay = new BigDecimal("0");
			BigDecimal subSumProfitLoss = new BigDecimal("0");
			
			for ( int subIdx = 0; subIdx <= idx ; subIdx++) {
				subDurationDay = new BigDecimal(
						String.valueOf(DateUtil.daysBetween(acctHistDtos.get(subIdx).getYmdDay(), endDate) + 1));
				totalDeposit = totalDeposit.add( new BigDecimal(acctHistDtos.get(subIdx).getDepositAmt()).multiply(subDurationDay));
				
				totalWithdrawal = totalWithdrawal.add( new BigDecimal(acctHistDtos.get(subIdx).getWithdrawalAmt()).multiply(subDurationDay));
				
				subSumProfitLoss = subSumProfitLoss.add( new BigDecimal(acctHistDtos.get(subIdx).getReturnOnInvestment()));
			}
			
			investedBalanceAmt = ( (beginNetAsset.multiply(durationDay)).add(totalDeposit).subtract(totalWithdrawal) ).divide(durationDay, RoundingMode.DOWN);
			acctHistDtos.get(idx).setProfitLossRatio( (subSumProfitLoss.multiply(new BigDecimal("100")).divide(investedBalanceAmt, 2, RoundingMode.HALF_UP)) );
		}		
		
		return acctHistDtos;
	}	
	
	
}

