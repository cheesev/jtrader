package com.app4j.tradeobserver.stock.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.stock.dao.StockInfoDao;
import com.app4j.tradeobserver.stock.entity.StockInfoDto;
import com.app4j.tradeobserver.stock.service.StockInfoService;


@Service("stockInfoService")
@Transactional(readOnly=true)
public class StockInfoServiceImpl implements StockInfoService{
	
	@Resource(name="stockInfoDao")
	private StockInfoDao stockInfoDao;

	@Override
	public List<StockInfoDto> getAllStockInfoList() {
		return stockInfoDao.getByCriteria(Restrictions.isNotNull("stockCode"));
	}
	
	
	
	
}
