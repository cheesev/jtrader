package com.app4j.tradeobserver.stock.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.util.ListUtil;
import com.app4j.tradeobserver.common.util.StockUtil;
import com.app4j.tradeobserver.data.dao.ItoozaFinancialPositionStatementIFRSDao;
import com.app4j.tradeobserver.data.dao.ItoozaProfitLossStatementIFRSDao;
import com.app4j.tradeobserver.data.entity.ItoozaFinancialPositionStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.ItoozaProfitLossStatementIFRSDto;
import com.app4j.tradeobserver.stock.dao.StockInfoDao;
import com.app4j.tradeobserver.stock.entity.StockInfoDto;
import com.app4j.tradeobserver.stock.service.MagicFomulaService;

@Service("magicFomulaService")
@Transactional(readOnly=true)
public class MagicFomulaServiceImpl implements MagicFomulaService{
	
	@Resource(name="stockInfoDao")
	private StockInfoDao stockInfoDao;
	
	@Resource(name="itoozaFinancialPositionStatementIFRSDao")
	private ItoozaFinancialPositionStatementIFRSDao itoozaFinancialPositionStatementIFRSDao;
	
	@Resource(name="itoozaProfitLossStatementIFRSDao")
	private ItoozaProfitLossStatementIFRSDao itoozaProfitLossStatementIFRSDao;

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getMagicFomulaList() {
		// TODO Auto-generated method stub
		
		List<StockInfoDto> stockInfoDtoList = stockInfoDao.getByCriteria(Restrictions.isNotNull("stockCode"));
		
		for(StockInfoDto stockInfoDto : stockInfoDtoList) {
			String stockCode = stockInfoDto.getStockCode();
			// 시가 총액 조회 ( 우선주 포함해서 조회함 )
			Number integerMarketValue = (Number) stockInfoDao.getCriteria().add(Restrictions.in("stockCode", StockUtil.getAllAssociatedStock(stockCode)))
			.setProjection(Projections.sum("marketValue")).uniqueResult();
			
			BigDecimal totalMarketValue = null;
			BigDecimal sumProfit = null;
			
			if(integerMarketValue != null) {
				totalMarketValue = new BigDecimal( integerMarketValue.toString() );
			} else {
				System.out.println("시가총액 정보없음 : " + stockCode);
				continue;	// 시가총액 정보가 없을때.. 그냥 bypass
			}
			
			// 최근 4분기 영업이익 합계
			sumProfit = ListUtil.sum(itoozaProfitLossStatementIFRSDao.getCriteria()
					.setProjection(Projections.property("영업이익"))
					.add(Restrictions.eq("stockCode", stockCode))
					.addOrder(Order.desc("ymDay"))
					.setMaxResults(4).list()
					);
			
			//recent4QuaterProfitLoss.setProjection(Projections.sum("영업이익"));
			
//			if (sumProfitNum != null) {
//				sumProfit = new BigDecimal(sumProfitNum.toString());
//			} else {
//				System.out.println("영업이익 정보없음 : " + stockCode);
//				continue;
//			}
			
			
			// 자본 수익률 계산 = 최근 4분기 영업이익 합 / {영업용 순운전자본 + 영업용 순고정자산 } 의 최근 4분기 산술평균
			
			List<ItoozaFinancialPositionStatementIFRSDto> itoozaFinancialPositionStatementIFRSDtos
			 = itoozaFinancialPositionStatementIFRSDao.getCriteria()
			.add(Restrictions.eq("stockCode", stockCode))
			.addOrder(Order.desc("ymDay"))
			.setMaxResults(4).list();
			
			BigDecimal quaterSum = new BigDecimal("0");
			BigDecimal recentInterestDebt = new BigDecimal("0"); // 최근 분기 순이자 부담 부채
			for(ItoozaFinancialPositionStatementIFRSDto financialPositionStatementIFRSDto : itoozaFinancialPositionStatementIFRSDtos) {
				// 영업용 순운전자본.. BigDecimal 을 쓰기위해 String으로 바꿔야함.
				if (quaterSum.intValue() == 0) {
					recentInterestDebt = new BigDecimal(financialPositionStatementIFRSDto.get유동부채_유동성장기부채()).add( new BigDecimal( financialPositionStatementIFRSDto.get비유동부채_장기금융부채()) )
										.subtract( new BigDecimal(financialPositionStatementIFRSDto.get당좌자산_현금및현금성자산()).add( new BigDecimal(financialPositionStatementIFRSDto.get당좌자산_단기금융자산()) ) );
				}
				/*
				 *  미지급금및 기타채무에서.. 파생상품 부분을 빼야하지만 여기서는 그대로 넣어줌..
				 */
				quaterSum = quaterSum.add
						(
						new BigDecimal(financialPositionStatementIFRSDto.get당좌자산_매출채권및기타채권()).add(new BigDecimal(financialPositionStatementIFRSDto.get재고자산())).add(new BigDecimal(financialPositionStatementIFRSDto.get기타유동자산()))
						.subtract( new BigDecimal(financialPositionStatementIFRSDto.get유동부채_매입채무및기타채무()).add(new BigDecimal(financialPositionStatementIFRSDto.get유동부채_단기충당부채())).add(new BigDecimal(financialPositionStatementIFRSDto.get유동부채_기타유동부채())) )
						.add( new BigDecimal(financialPositionStatementIFRSDto.get유형자산()) )
						//기타 순영업자산		
						.add( new BigDecimal(financialPositionStatementIFRSDto.get무형자산()).add(new BigDecimal(financialPositionStatementIFRSDto.get생물자산())).add(new BigDecimal(financialPositionStatementIFRSDto.get기타비유동자산()))
							  .subtract( new BigDecimal(financialPositionStatementIFRSDto.get비유동부채_장기충당부채()).add(new BigDecimal(financialPositionStatementIFRSDto.get비유동부채_기타비유동부채())) ))
						);
						// 이익 수익률 계산.
			}
			
			// 자본 수익률			
			BigDecimal returnOnCommonEquity = sumProfit.multiply(new BigDecimal("100")).divide(quaterSum, 2, RoundingMode.DOWN);			
			// 이익 수익률			
			BigDecimal earningYield = sumProfit.multiply(new BigDecimal("100")).divide(totalMarketValue.add(recentInterestDebt) , 2, RoundingMode.DOWN);
			
			// dto (혹은 map) 에 add..
			
			
		}
		return null;
	}
	
	
	
}
