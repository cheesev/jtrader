package com.app4j.tradeobserver.stock.service;

import java.util.HashMap;
import java.util.List;

import com.app4j.tradeobserver.stock.entity.StockTradeSituationDto;

public interface SupplyAndDemandService {
	
	StockTradeSituationDto getStockTradeSituation(String ymdDay, String stockCode);
	
	List<StockTradeSituationDto> getStockTradeSituationList(String fromDay, String toDay, String stockCode);
	List<StockTradeSituationDto> getStockTradeSituationList(String stockCode, int size);
	List<StockTradeSituationDto> getCumulativeStockTradeSituationList(String fromDay, String toDay, String stockCode);
	
	String getMaxYmdDay(String stockCode);
	int getAvgPrice(String fromDay, String toDay, String stockCode);
	int getAvgPrice(List<StockTradeSituationDto> stockTradeSituationDtoList);
	StockTradeSituationDto getAvgPriceByInvestors(List<StockTradeSituationDto> stockTradeSituationDtoList, StockTradeSituationDto minDto);
	
	StockTradeSituationDto sumSupplyAndSetAvgPrice(List<StockTradeSituationDto> stockTradeSituationList); /* sum by various investors) */
	
	StockTradeSituationDto getMaxBuyAmt(List<StockTradeSituationDto> stockTradeSituationDtoList);
	StockTradeSituationDto getMinBuyAmt(List<StockTradeSituationDto> stockTradeSituationDtoList);
	StockTradeSituationDto getCurBuyAmt(List<StockTradeSituationDto> stockTradeSituationDtoList);
	
	StockTradeSituationDto getVarianceRatio(StockTradeSituationDto curBuyAmtDto
											,StockTradeSituationDto minBuyAmtDto
											,StockTradeSituationDto maxBuyAmtDto);
	
	StockTradeSituationDto getCorrelation(List<StockTradeSituationDto> stockTradeSituationDtoList);
	
}
