package com.app4j.tradeobserver.stock.service;

import java.util.Map;

public interface MagicFomulaService {
	
	Map<String, String> getMagicFomulaList();	

}
