package com.app4j.tradeobserver.stock.service;

import java.util.List;

import com.app4j.tradeobserver.stock.entity.StockInfoDto;

public interface StockInfoService {
	
	List<StockInfoDto> getAllStockInfoList();

}
