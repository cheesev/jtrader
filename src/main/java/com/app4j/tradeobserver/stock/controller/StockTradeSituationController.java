package com.app4j.tradeobserver.stock.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app4j.tradeobserver.common.json.CustomDefaultResponse;
import com.app4j.tradeobserver.common.util.DateUtil;
import com.app4j.tradeobserver.stock.entity.StockTradeSituationDto;
import com.app4j.tradeobserver.stock.service.SupplyAndDemandService;

@Controller
@RequestMapping("/stock/trade-situation")
public class StockTradeSituationController {
	
	@Value("#{cacheManager.getCache('stockTradeSituationCache')}")
	private Cache stockTradeSituationCache;
	final int RECORD_MAX_SIZE = 6;
	final int SELECT_SIZE = 20;
	
	//@Autowired
	@Resource(name="supplyAndDemandService")
	private SupplyAndDemandService supplyAndDemandService;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getlist", method = RequestMethod.GET)
     public @ResponseBody CustomDefaultResponse<StockTradeSituationDto> getList(
       @RequestParam("interval") int interval,
       @RequestParam("stockCode") String stockCode
     )  {
			
		/*
		 * 한달전의 의미는.. 처음 데이터부터 한달전까지의 누적 순매수량을 보여주고
		 * 일년전도 똑같이 그런식으로..
		 */
		
		 List<StockTradeSituationDto> jqgridViewList = new ArrayList<StockTradeSituationDto>();
		 
		 if (interval == 1) {
			 jqgridViewList = supplyAndDemandService.getStockTradeSituationList(stockCode, RECORD_MAX_SIZE);
		 } else {
			 for (int i = 1; i <= RECORD_MAX_SIZE; i++) {
				 jqgridViewList.add(
						 supplyAndDemandService.sumSupplyAndSetAvgPrice(
						 supplyAndDemandService.getStockTradeSituationList(stockCode, interval * i)));
			 }
		 }		 
		 
		 CustomDefaultResponse<StockTradeSituationDto> response = new CustomDefaultResponse<StockTradeSituationDto>();
		 
		 response.setRows(jqgridViewList);		 
		 response.setRecords(String.valueOf(jqgridViewList.size()));		 
		 response.setPage("1");		 
		 response.setTotal("10");
		 
		 return response;	 
	 }
	
	@RequestMapping(value = "/get-summary", method = RequestMethod.GET)
    public @ResponseBody CustomDefaultResponse<StockTradeSituationDto> getSummaryList(
      @RequestParam("stockCode") String stockCode
    )  {
		
		List<StockTradeSituationDto> jqgridViewList = new ArrayList<StockTradeSituationDto>();
		List<StockTradeSituationDto> cumulativeStockTradeSituationDtoList = 
				supplyAndDemandService.getCumulativeStockTradeSituationList(DateUtil.STOCK_TRADE_SITUATION_MIN_DAY, DateUtil.getCurDateYmdFormat(), stockCode);
		List<StockTradeSituationDto> stockTradeSituationDtoList = 
				supplyAndDemandService.getStockTradeSituationList(DateUtil.STOCK_TRADE_SITUATION_MIN_DAY, DateUtil.getCurDateYmdFormat(), stockCode);
		
		StockTradeSituationDto maxDto = supplyAndDemandService.getMaxBuyAmt(cumulativeStockTradeSituationDtoList);
		StockTradeSituationDto curDto = supplyAndDemandService.getCurBuyAmt(cumulativeStockTradeSituationDtoList);
		StockTradeSituationDto minDto = supplyAndDemandService.getMinBuyAmt(cumulativeStockTradeSituationDtoList);		
		
		jqgridViewList.add(maxDto);
		jqgridViewList.add(curDto);
		jqgridViewList.add(supplyAndDemandService.getAvgPriceByInvestors(stockTradeSituationDtoList, minDto));
		jqgridViewList.add(supplyAndDemandService.getVarianceRatio(curDto, minDto, maxDto));
		jqgridViewList.add(supplyAndDemandService.getCorrelation(cumulativeStockTradeSituationDtoList));
		
		CustomDefaultResponse<StockTradeSituationDto> response = new CustomDefaultResponse<StockTradeSituationDto>();
		response.setRows(jqgridViewList);
		response.setRecords(String.valueOf(jqgridViewList.size()));		 
		response.setPage("1");
		response.setTotal("10");
		
		return response;
	}
	 
	

}
