package com.app4j.tradeobserver.stock.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app4j.tradeobserver.common.json.CustomDefaultResponse;
import com.app4j.tradeobserver.common.json.GoogleChartDto;
import com.app4j.tradeobserver.common.util.DateUtil;
import com.app4j.tradeobserver.stock.entity.StockAccountHistoryDto;
import com.app4j.tradeobserver.stock.entity.StockCurrentAccountDto;
import com.app4j.tradeobserver.stock.entity.StockTradeSituationDto;
import com.app4j.tradeobserver.stock.service.AccountService;
import com.google.gson.Gson;

@Controller
@RequestMapping("/stock/overview")
public class StockAccountOverviewController {
	
	@Resource(name="accountService")
	private AccountService accountService;
	
	@RequestMapping(value = "/current-stock", method = RequestMethod.GET)
     public @ResponseBody CustomDefaultResponse<?> getCurrentStockInfo(ModelMap model) {
		
		CustomDefaultResponse<StockCurrentAccountDto> response = new CustomDefaultResponse<StockCurrentAccountDto>();
		
		response.setRows(accountService.getCurrentAccount());
		
		return response;
	}
	
	@RequestMapping(value = "/account-history", method = RequestMethod.GET)
    public @ResponseBody String getStockAccountHistory(ModelMap model) {
		
		GoogleChartDto chartDto = new GoogleChartDto();
		chartDto.addColumn("date", "X");
		chartDto.addColumn("number", "TRUST");
		chartDto.addColumn("number", "CMA");
		Map<String, String> acctMap = new HashMap<String,String>();
		acctMap.put("TRUST", "00701706181");
		acctMap.put("CMA", "20041353801");
		
		// 계좌는 2개로 설정하였으며 추후 추가시 .. 코드를 바꿔줘야한다.
		
		List<StockAccountHistoryDto> trustAcctHistDtos =
				accountService.getCumulativeStockAccountHistory(acctMap.get("TRUST"), "20140430", DateUtil.getCurDateYmdFormat());
		
		List<StockAccountHistoryDto> cmaAcctHistDtos =
				accountService.getCumulativeStockAccountHistory(acctMap.get("CMA"), "20140430", DateUtil.getCurDateYmdFormat());
		
		chartDto.createRows(trustAcctHistDtos.size());
		
		String minYmdDay = cmaAcctHistDtos.get(0).getYmdDay();
		
		int cmaIdx = 0;
		for (int i=0 ; i < trustAcctHistDtos.size() ; i++) {
			String curYmdDay = trustAcctHistDtos.get(i).getYmdDay();
			chartDto.addCell(i, chartDto.makeDateStr(trustAcctHistDtos.get(i).getYmdDay()));
			chartDto.addCell(i, trustAcctHistDtos.get(i).getProfitLossRatio());
			if(DateUtil.getYmdFormatDate(curYmdDay).compareTo(DateUtil.getYmdFormatDate(minYmdDay))>=0) { // 현재날짜가 cma minDay보다 나중이거나 같을때
				chartDto.addCell(i, cmaAcctHistDtos.get(cmaIdx++).getProfitLossRatio());
			}
		}
		
		Gson gson = new Gson();
		return gson.toJson(chartDto.getResult());
	}
		
		
	
	 
	

}
