package com.app4j.tradeobserver.stock.entity.id;

import java.io.Serializable;

import com.app4j.tradeobserver.stock.entity.StockAccountHistoryDto;

public class StockAccountHistoryDtoId implements Serializable {
	protected String ymdDay;
	protected String accountNum;

    public StockAccountHistoryDtoId() {}

    public StockAccountHistoryDtoId(String ymdDay, String accountNum) {
        this.ymdDay = ymdDay;
        this.accountNum = accountNum;
    }
    
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StockAccountHistoryDto stockAccountHistoryDto = (StockAccountHistoryDto) o;

        return accountNum.equals(stockAccountHistoryDto.getAccountNum()) 
        	&& ymdDay.equals(stockAccountHistoryDto.getYmdDay());
    }
    
    public int hashCode() {		
		int result;
	    result = ymdDay.hashCode();
	    result = 29 * result + accountNum.hashCode();
	    return result;
    }
}