package com.app4j.tradeobserver.stock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.app4j.tradeobserver.stock.entity.id.StockCurrentAccountDtoId;

/* reference : 
 * http://stackoverflow.com/questions/3585034/how-to-map-a-composite-key-with-hibernate
 * */
@Entity
@Table (name = "STOCK_CURRENT_ACCOUNT")
@IdClass(StockCurrentAccountDtoId.class)
public class StockCurrentAccountDto implements Cloneable{
	
	@Id
    @Column (name = "STOCK_CODE")
    private String stockCode;
	@Id
	@Column(name="ACCOUNT_NUM")
	private String accountNum;
	
	@Transient
	private String stockName; 
	
	@Transient
	private String accountName;
	
	
	@Column(name="BALANCE_AMOUNT")
	private Double balanceAmount;
	@Column(name="AVG_PRICE")
	private Double avgPrice;
	@Column(name="BUY_PRICE")
	private Double buyPrice;
	@Column(name="CURRENT_PRICE")
	private Double currentPrice;
	@Column(name="ASSESSMENT_PRICE")
	private Double assessmentPrice;
	@Column(name="ASSESSMENT_PROFIT_LOSS")
	private Double assessmentProfitLoss;
	@Column(name="PROFIT_LOSS_RATE")
	private Double profitLossRate;
	@Column(name="INVSESTMENT_PROPORTION")
	private Double invsestmentProportion;
	
	
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	
	public String getAccountNum() {
		return accountNum;
	}
	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}
	public Double getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(Double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public Double getAvgPrice() {
		return avgPrice;
	}
	public void setAvgPrice(Double avgPrice) {
		this.avgPrice = avgPrice;
	}
	public Double getBuyPrice() {
		return buyPrice;
	}
	public void setBuyPrice(Double buyPrice) {
		this.buyPrice = buyPrice;
	}
	public Double getCurrentPrice() {
		return currentPrice;
	}
	public void setCurrentPrice(Double currentPrice) {
		this.currentPrice = currentPrice;
	}
	public Double getAssessmentPrice() {
		return assessmentPrice;
	}
	public void setAssessmentPrice(Double assessmentPrice) {
		this.assessmentPrice = assessmentPrice;
	}
	public Double getAssessmentProfitLoss() {
		return assessmentProfitLoss;
	}
	public void setAssessmentProfitLoss(Double assessmentProfitLoss) {
		this.assessmentProfitLoss = assessmentProfitLoss;
	}
	public Double getProfitLossRate() {
		return profitLossRate;
	}
	public void setProfitLossRate(Double profitLossRate) {
		this.profitLossRate = profitLossRate;
	}
	public Double getInvsestmentProportion() {
		return invsestmentProportion;
	}
	public void setInvsestmentProportion(Double invsestmentProportion) {
		this.invsestmentProportion = invsestmentProportion;
	}
	
	

}

