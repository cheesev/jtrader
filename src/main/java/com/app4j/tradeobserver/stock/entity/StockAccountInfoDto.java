package com.app4j.tradeobserver.stock.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "STOCK_ACCOUNT_INFO")
public class StockAccountInfoDto {
	
	@Id
    @Column (name = "ACCOUNT_NUM")
	private String accountNum;
	@Column (name = "ACCOUNT_NAME")
	private String accountName;
	@Column (name = "ACCOUNT_PWD")
	private String accountPwd;	
	
	public String getAccountNum() {
		return accountNum;
	}
	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getAccountPwd() {
		return accountPwd;
	}
	public void setAccountPwd(String accountPwd) {
		this.accountPwd = accountPwd;
	}
	
	
	
	
	
	

}
