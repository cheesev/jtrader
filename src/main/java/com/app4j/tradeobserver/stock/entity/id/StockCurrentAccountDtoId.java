package com.app4j.tradeobserver.stock.entity.id;

import java.io.Serializable;

import com.app4j.tradeobserver.stock.entity.StockCurrentAccountDto;

public class StockCurrentAccountDtoId implements Serializable {
	protected String stockCode;
	protected String accountNum;

    public StockCurrentAccountDtoId() {}

    public StockCurrentAccountDtoId(String stockCode, String accountNum) {
        this.stockCode = stockCode;
        this.accountNum = accountNum;
    }
    
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StockCurrentAccountDto stockCurrentAccountDto = (StockCurrentAccountDto) o;

        return accountNum.equals(stockCurrentAccountDto.getAccountNum()) 
        	&& stockCode.equals(stockCurrentAccountDto.getStockCode());
    }
    
    public int hashCode() {		
		int result;
	    result = stockCode.hashCode();
	    result = 29 * result + accountNum.hashCode();
	    return result;
    }
}