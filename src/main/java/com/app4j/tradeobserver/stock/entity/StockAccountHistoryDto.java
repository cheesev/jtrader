package com.app4j.tradeobserver.stock.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.app4j.tradeobserver.stock.entity.id.StockAccountHistoryDtoId;

@Entity
@Table (name = "STOCK_ACCOUNT_HISTORY")
@IdClass(StockAccountHistoryDtoId.class)
public class StockAccountHistoryDto {
	
	@Id
    @Column (name = "ACCOUNT_NUM")
	private String accountNum;
	@Id
	@Column (name = "YMD_DAY")
	private String ymdDay;
	@Column (name = "BEGINING_NET_ASSETS")
	private String beginingNetAssets;
	
	@Column (name = "ENDING_NET_ASSETS")
	private String endingNetAssets;
	@Column (name = "INVESTED_AVERAGE_BALANCE")
	private String investedAverageBalance;
	@Column (name = "DEPOSIT_AMOUNT")
	private String depositAmt;
	@Column (name = "WITHDRAWAL_AMOUNT")
	private String withdrawalAmt;
	@Column (name = "RETURN_ON_INVESTMENT")
	private String returnOnInvestment;
	
	@Transient
	private BigDecimal profitLossRatio;

	public String getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public String getYmdDay() {
		return ymdDay;
	}

	public void setYmdDay(String ymdDay) {
		this.ymdDay = ymdDay;
	}

	public String getBeginingNetAssets() {
		return beginingNetAssets;
	}

	public void setBeginingNetAssets(String beginingNetAssets) {
		this.beginingNetAssets = beginingNetAssets;
	}

	public String getEndingNetAssets() {
		return endingNetAssets;
	}

	public void setEndingNetAssets(String endingNetAssets) {
		this.endingNetAssets = endingNetAssets;
	}

	public String getInvestedAverageBalance() {
		return investedAverageBalance;
	}

	public void setInvestedAverageBalance(String investedAverageBalance) {
		this.investedAverageBalance = investedAverageBalance;
	}

	public String getDepositAmt() {
		return depositAmt;
	}

	public void setDepositAmt(String depositAmt) {
		this.depositAmt = depositAmt;
	}

	public String getWithdrawalAmt() {
		return withdrawalAmt;
	}

	public void setWithdrawalAmt(String withdrawalAmt) {
		this.withdrawalAmt = withdrawalAmt;
	}

	public String getReturnOnInvestment() {
		return returnOnInvestment;
	}

	public void setReturnOnInvestment(String returnOnInvestment) {
		this.returnOnInvestment = returnOnInvestment;
	}

	public BigDecimal getProfitLossRatio() {
		return profitLossRatio;
	}

	public void setProfitLossRatio(BigDecimal profitLossRatio) {
		this.profitLossRatio = profitLossRatio;
	}
	
	
	

}
