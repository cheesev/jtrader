package com.app4j.tradeobserver.stock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.app4j.tradeobserver.stock.entity.id.StockTradeSituationDtoId;

@Entity
@Table (name = "STOCK_INFO")
public class StockInfoDto {
	
	@Id
    @Column (name = "STOCK_CODE")
	private String stockCode;
	@Column (name = "STOCK_NAME")
	private String stockName;
	@Column (name = "ETF_GUBUN")
	private String etfGubun;
	@Column (name = "GUBUN")
	private String gubun;
	@Column (name = "PRICE")
	private Integer price;
	@Column (name = "MARKET_VALUE")
	private Integer marketValue; // �ð� �Ѿ�
	
	
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public String getEtfGubun() {
		return etfGubun;
	}
	public void setEtfGubun(String etfGubun) {
		this.etfGubun = etfGubun;
	}
	public String getGubun() {
		return gubun;
	}
	public void setGubun(String gubun) {
		this.gubun = gubun;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Integer getMarketValue() {
		return marketValue;
	}
	public void setMarketValue(int marketValue) {
		this.marketValue = marketValue;
	}
	
	

}
