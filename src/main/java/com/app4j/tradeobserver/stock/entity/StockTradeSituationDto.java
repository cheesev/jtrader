package com.app4j.tradeobserver.stock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.app4j.tradeobserver.stock.entity.id.StockTradeSituationDtoId;

/* reference : 
 * http://stackoverflow.com/questions/3585034/how-to-map-a-composite-key-with-hibernate
 * */
@Entity
@Table (name = "STOCK_TRADE_SITUATION")
@IdClass(StockTradeSituationDtoId.class)
public class StockTradeSituationDto implements Cloneable{
	
	@Id
    @Column (name = "YMD_DAY")
    private String ymdDay;
	@Id
    @Column (name = "STOCK_CODE")
    private String stockCode;

    @Column (name = "PRICE")
    private int price;

    @Column (name = "DIFF")
    private double diff;

    @Column (name = "ANT")
    private int ant;
    
    @Column (name = "FOREIGNER")
    private int foreigner;
    
    @Column (name = "INSTITUTION")
    private int institution;
    
    @Column (name = "FINANCIAL_INVESTMENT")
    private int financialInvestment;
    
    @Column (name = "TRUST")
    private int trust;
    
    @Column (name = "ASSURANCE")
    private int assurance;
    
    @Column (name = "BANK")
    private int bank;
    
    @Column (name = "INVESTMENT_BANK")
    private int investmentBank;
    
    @Column (name = "PENSION")
    private int pension;
    
    @Column (name = "PRIVATE_EQUITY_FUND")
    private int privateEquityFund;
    
    @Column (name = "INNER_FOREIGNER")
    private int innerForeigner;
    
    @Column (name = "ETC")
    private int etc;
        
    @Transient
    private int forceGrp;    

	public StockTradeSituationDto() {
    	this.ymdDay = null;
    	this.stockCode = null;
    	this.price = 0;
    	this.diff = 0;
    	this.forceGrp = 0;
    	this.ant = 0;
    	this.foreigner = 0;
    	this.institution = 0;
    	this.financialInvestment = 0;
    	this.trust = 0;
    	this.assurance = 0;
    	this.bank = 0;
    	this.investmentBank = 0;
    	this.pension = 0;
    	this.privateEquityFund = 0;
    	this.innerForeigner = 0;
    	this.etc = 0;    	
    }
	
	public int getForceGrp() {
		return forceGrp;
	}
	// 1. 외국인 + 기관계 + 기타계(기타 + 국가)
	public void setForceGrp() {
		this.forceGrp = this.foreigner + this.institution + this.etc;
	}
	
	public void setForceGrp(int forceGrp) {
		this.forceGrp = forceGrp;
	}

	public String getYmdDay() {
		return ymdDay;
	}

	public void setYmdDay(String ymdDay) {
		this.ymdDay = ymdDay;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public double getDiff() {
		return diff;
	}

	public void setDiff(double diff) {
		this.diff = diff;
	}

	public int getAnt() {
		return ant;
	}

	public void setAnt(int ant) {
		this.ant = ant;
	}

	public int getForeigner() {
		return foreigner;
	}

	public void setForeigner(int foreigner) {
		this.foreigner = foreigner;
	}

	public int getInstitution() {
		return institution;
	}

	public void setInstitution(int institution) {
		this.institution = institution;
	}

	public int getFinancialInvestment() {
		return financialInvestment;
	}

	public void setFinancialInvestment(int financial_investment) {
		this.financialInvestment = financial_investment;
	}

	public int getTrust() {
		return trust;
	}

	public void setTrust(int trust) {
		this.trust = trust;
	}

	public int getAssurance() {
		return assurance;
	}

	public void setAssurance(int assurance) {
		this.assurance = assurance;
	}

	public int getBank() {
		return bank;
	}

	public void setBank(int bank) {
		this.bank = bank;
	}

	public int getInvestmentBank() {
		return investmentBank;
	}

	public void setInvestmentBank(int investment_bank) {
		this.investmentBank = investment_bank;
	}

	public int getPension() {
		return pension;
	}

	public void setPension(int pension) {
		this.pension = pension;
	}

	public int getPrivateEquityFund() {
		return privateEquityFund;
	}

	public void setPrivateEquityFund(int private_equity_fund) {
		this.privateEquityFund = private_equity_fund;
	}

	public int getInnerForeigner() {
		return innerForeigner;
	}

	public void setInnerForeigner(int inner_foreigner) {
		this.innerForeigner = inner_foreigner;
	}

	public int getEtc() {
		return etc;
	}

	public void setEtc(int etc) {
		this.etc = etc;
	}
	
	public Object clone() {
		 try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			System.out.println("ERR ] Clone not supported");
		}
		return null;
	}
	
	/*
	 * add buy amount as much as parameter dto
	 * use parameter set - dto price   
	 */
	public void addBuyAmt(StockTradeSituationDto dto) {
		this.forceGrp += dto.getForceGrp();
		this.ant += dto.getAnt();
		this.foreigner += dto.getForeigner();
		this.institution += dto.getInstitution();
		this.financialInvestment += dto.getFinancialInvestment();
		this.trust += dto.getTrust();
		this.assurance += dto.getAssurance();
		this.bank += dto.getBank();
		this.investmentBank += dto.getInvestmentBank();
		this.pension += dto.getPension();
		this.privateEquityFund += dto.getPrivateEquityFund();
		this.innerForeigner += dto.getInnerForeigner();
		this.etc += dto.getEtc();
	}

	/*
	 * set maximum value compare to parameter value
	 */
	public void setComparableMax(StockTradeSituationDto dto) {
		// TODO Auto-generated method stub
		this.forceGrp = Math.max(this.forceGrp, dto.getForceGrp());
		this.ant = Math.max(this.ant, dto.getAnt());
		this.foreigner = Math.max(this.foreigner, dto.getForeigner());
		this.institution = Math.max(this.institution, dto.getInstitution());
		this.financialInvestment = Math.max(this.financialInvestment, dto.getFinancialInvestment());
		this.trust = Math.max(this.trust, dto.getTrust());
		this.assurance = Math.max(this.assurance, dto.getAssurance());
		this.bank = Math.max(this.bank, dto.getBank());
		this.investmentBank = Math.max(this.investmentBank, dto.getInvestmentBank());
		this.pension = Math.max(this.pension, dto.getPension());
		this.privateEquityFund = Math.max(this.privateEquityFund, dto.getPrivateEquityFund());
		this.innerForeigner = Math.max(this.innerForeigner, dto.getInnerForeigner());
		this.etc = Math.max(this.etc, dto.getEtc());
	}
	
	/*
	 * set minimum value compare to parameter value
	 */
	public void setComparableMin(StockTradeSituationDto dto) {
		// TODO Auto-generated method stub
		this.forceGrp = Math.min(this.forceGrp, dto.getForceGrp());
		this.ant = Math.min(this.ant, dto.getAnt());
		this.foreigner = Math.min(this.foreigner, dto.getForeigner());
		this.institution = Math.min(this.institution, dto.getInstitution());
		this.financialInvestment = Math.min(this.financialInvestment, dto.getFinancialInvestment());
		this.trust = Math.min(this.trust, dto.getTrust());
		this.assurance = Math.min(this.assurance, dto.getAssurance());
		this.bank = Math.min(this.bank, dto.getBank());
		this.investmentBank = Math.min(this.investmentBank, dto.getInvestmentBank());
		this.pension = Math.min(this.pension, dto.getPension());
		this.privateEquityFund = Math.min(this.privateEquityFund, dto.getPrivateEquityFund());
		this.innerForeigner = Math.min(this.innerForeigner, dto.getInnerForeigner());
		this.etc = Math.min(this.etc, dto.getEtc());
	}
}

