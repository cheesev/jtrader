package com.app4j.tradeobserver.stock.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.stock.dao.StockCurrentAccountDao;
import com.app4j.tradeobserver.stock.entity.StockCurrentAccountDto;
import com.app4j.tradeobserver.stock.entity.StockTradeSituationDto;

@Repository("stockCurrentAccountDao")
public class StockCurrentAccountDaoImpl extends AbstractDaoImpl<StockCurrentAccountDto, String> implements StockCurrentAccountDao{
	
	protected StockCurrentAccountDaoImpl() {
        super(StockCurrentAccountDto.class);
    }

	@Override
	public List<StockCurrentAccountDto> getCurrentAccount() {
		List<StockCurrentAccountDto> stockCurrentAccountDtos = new ArrayList<StockCurrentAccountDto>();
		@SuppressWarnings("unchecked")
		List<Object[]> queryList = getCurrentSession().createQuery("select account, acctInfo.accountName, stockInfo.stockName"
				+ " from StockCurrentAccountDto account, StockAccountInfoDto acctInfo, StockInfoDto  stockInfo"
				+ " where account.accountNum = acctInfo.accountNum"
				+ " and account.stockCode = stockInfo.stockCode"
				+ " order by account.buyPrice desc").list();
		
		for(Object[] objectArr : queryList) {
			StockCurrentAccountDto stockCurAcctDto = (StockCurrentAccountDto)objectArr[0];
			stockCurAcctDto.setAccountName(objectArr[1].toString());
			stockCurAcctDto.setStockName(objectArr[2].toString());
			
			stockCurrentAccountDtos.add(stockCurAcctDto);
		}
		
		
		
		return stockCurrentAccountDtos;
		
	}	
	
}
