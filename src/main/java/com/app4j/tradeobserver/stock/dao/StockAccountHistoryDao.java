package com.app4j.tradeobserver.stock.dao;

import java.util.List;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.stock.entity.StockAccountHistoryDto;
import com.app4j.tradeobserver.stock.entity.StockCurrentAccountDto;

public interface StockAccountHistoryDao extends AbstractDao<StockAccountHistoryDto, String> {
	
	List<StockAccountHistoryDto> getCumulativeStockAccountHistory(String acctNum, String fromDate, String toDate);
}
