package com.app4j.tradeobserver.stock.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.stock.dao.StockAccountHistoryDao;
import com.app4j.tradeobserver.stock.dao.StockCurrentAccountDao;
import com.app4j.tradeobserver.stock.entity.StockAccountHistoryDto;
import com.app4j.tradeobserver.stock.entity.StockCurrentAccountDto;
import com.app4j.tradeobserver.stock.entity.StockTradeSituationDto;

@Repository("stockAccountHistoryDao")
public class StockAccountHistoryDaoImpl extends AbstractDaoImpl<StockAccountHistoryDto, String> implements StockAccountHistoryDao{
	
	protected StockAccountHistoryDaoImpl() {
        super(StockAccountHistoryDto.class);
    }

	@Override
	public List<StockAccountHistoryDto> getCumulativeStockAccountHistory(String acctNum, String fromDate, String toDate) {
		// TODO Auto-generated method stub
		
		Criteria criteria = getCurrentSession().createCriteria(StockAccountHistoryDto.class);
		criteria.add(Restrictions.and(
				Restrictions.eq("accountNum", acctNum), Restrictions.between("ymdDay", fromDate, toDate)));
		criteria.addOrder(Order.asc("ymdDay"));
		
		return (List<StockAccountHistoryDto>)criteria.list();
	}
	
}
