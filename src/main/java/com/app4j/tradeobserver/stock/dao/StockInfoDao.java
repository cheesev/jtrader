package com.app4j.tradeobserver.stock.dao;

import java.util.List;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.stock.entity.StockInfoDto;

public interface StockInfoDao extends AbstractDao<StockInfoDto, String> {
}
