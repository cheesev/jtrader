package com.app4j.tradeobserver.stock.dao;

import java.util.List;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.stock.entity.StockCurrentAccountDto;

public interface StockCurrentAccountDao extends AbstractDao<StockCurrentAccountDto, String> {
	
	List<StockCurrentAccountDto> getCurrentAccount();
}
