package com.app4j.tradeobserver.stock.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.stock.dao.StockTradeSituationDao;
import com.app4j.tradeobserver.stock.entity.StockTradeSituationDto;
import com.app4j.tradeobserver.stock.entity.id.StockTradeSituationDtoId;

@Repository("stockTradeSituationDao")
public class StockTradeSituationDaoImpl extends AbstractDaoImpl<StockTradeSituationDto, StockTradeSituationDtoId> implements StockTradeSituationDao{
	
	protected StockTradeSituationDaoImpl() {
        super(StockTradeSituationDto.class);
    }
	
	@Override
	public StockTradeSituationDto getStockTradeSituation(String ymdDay, String stockCode) {
		return getById(new StockTradeSituationDtoId(ymdDay, stockCode));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StockTradeSituationDto> getStockTradeSituationList(String fromDay,
			String toDay, String stockCode) {
		List<StockTradeSituationDto> stockTradeSituationDtoList = null;
		
		Criteria criteria = getCurrentSession().createCriteria(StockTradeSituationDto.class);
		criteria.add(Restrictions.and(
				Restrictions.eq("stockCode", stockCode), Restrictions.between("ymdDay", fromDay, toDay)));
		criteria.addOrder(Order.asc("ymdDay"));
		//criteria.addOrder(Order.desc("ymdDay"));
		
		stockTradeSituationDtoList = criteria.list();		
		setForceGrp(stockTradeSituationDtoList);
		
		return stockTradeSituationDtoList;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<StockTradeSituationDto> getStockTradeSituationList(String stockCode, int size) {
		List<StockTradeSituationDto> stockTradeSituationDtoList = null;

		Criteria criteria = getCurrentSession().createCriteria(StockTradeSituationDto.class);
		criteria.add(Restrictions.eq("stockCode", stockCode))
				.addOrder(Order.desc("ymdDay"))
				.setFirstResult(0)
				.setMaxResults(size);
		stockTradeSituationDtoList =   criteria.list();
		setForceGrp(stockTradeSituationDtoList);
		
		return stockTradeSituationDtoList;
	}
	
	private void setForceGrp(List<StockTradeSituationDto> stockTradeSituationDtoList) {
		for(StockTradeSituationDto dto : stockTradeSituationDtoList ) {
			dto.setForceGrp();
		}
	}

	@Override
	public String getMaxYmdDay(String stockCode) {
		// TODO Auto-generated method stub
		Criteria cr = getCurrentSession().createCriteria(StockTradeSituationDto.class);
		cr.add(Restrictions.eq("stockCode", stockCode));
		cr.setProjection(Projections.max("ymdDay"));
		
		return cr.list().get(0).toString();
	}

}
