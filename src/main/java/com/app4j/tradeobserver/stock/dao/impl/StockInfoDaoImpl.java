package com.app4j.tradeobserver.stock.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.stock.dao.StockInfoDao;
import com.app4j.tradeobserver.stock.entity.StockInfoDto;
import com.app4j.tradeobserver.stock.entity.StockTradeSituationDto;

@Repository("stockInfoDao")
public class StockInfoDaoImpl extends AbstractDaoImpl<StockInfoDto, String> implements StockInfoDao{
	
	protected StockInfoDaoImpl() {
        super(StockInfoDto.class);
    }
	
	
}
