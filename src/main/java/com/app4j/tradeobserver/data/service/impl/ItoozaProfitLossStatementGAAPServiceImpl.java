package com.app4j.tradeobserver.data.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.service.impl.ItoozaFinancialStatementServiceImpl;
import com.app4j.tradeobserver.data.dao.ItoozaProfitLossStatementGAAPDao;
import com.app4j.tradeobserver.data.entity.ItoozaProfitLossStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaProfitLossStatementGAAPDtoId;
import com.app4j.tradeobserver.data.service.ItoozaProfitLossStatementGAAPService;

@Service("itoozaProfitLossStatementGAAPService")
@Transactional
public class ItoozaProfitLossStatementGAAPServiceImpl 
				extends ItoozaFinancialStatementServiceImpl<ItoozaProfitLossStatementGAAPDto, ItoozaProfitLossStatementGAAPDtoId, ItoozaProfitLossStatementGAAPDao>
				implements ItoozaProfitLossStatementGAAPService {
		
	public ItoozaProfitLossStatementGAAPServiceImpl() {		
		super(ItoozaProfitLossStatementGAAPDto.class);
		
	}
}
