package com.app4j.tradeobserver.data.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.service.impl.FinancialStatementServiceImpl;
import com.app4j.tradeobserver.data.dao.CashFlowStatementIFRSDao;
import com.app4j.tradeobserver.data.entity.CashFlowStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.CashFlowStatementIFRSDtoId;
import com.app4j.tradeobserver.data.service.CashFlowStatementIFRSService;

@Service("cashFlowStatementIFRSService")
@Transactional
public class CashFlowStatementIFRSServiceImpl 
				extends FinancialStatementServiceImpl<CashFlowStatementIFRSDto, CashFlowStatementIFRSDtoId, CashFlowStatementIFRSDao>
				implements CashFlowStatementIFRSService{
		
	public CashFlowStatementIFRSServiceImpl() {		
		super(CashFlowStatementIFRSDto.class);
		
	}
}
