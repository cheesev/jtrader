package com.app4j.tradeobserver.data.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.service.impl.ItoozaFinancialStatementServiceImpl;
import com.app4j.tradeobserver.data.dao.ItoozaCashFlowStatementIFRSDao;
import com.app4j.tradeobserver.data.entity.ItoozaCashFlowStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaCashFlowStatementIFRSDtoId;
import com.app4j.tradeobserver.data.service.ItoozaCashFlowStatementIFRSService;

@Service("itoozaCashFlowStatementIFRSService")
@Transactional
public class ItoozaCashFlowStatementIFRSServiceImpl 
				extends ItoozaFinancialStatementServiceImpl<ItoozaCashFlowStatementIFRSDto, ItoozaCashFlowStatementIFRSDtoId, ItoozaCashFlowStatementIFRSDao>
				implements ItoozaCashFlowStatementIFRSService {
		
	public ItoozaCashFlowStatementIFRSServiceImpl() {		
		super(ItoozaCashFlowStatementIFRSDto.class);
		
	}
}
