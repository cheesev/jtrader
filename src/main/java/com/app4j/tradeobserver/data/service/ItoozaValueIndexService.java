package com.app4j.tradeobserver.data.service;

import com.app4j.tradeobserver.data.constants.ItoozaModeType;

public interface ItoozaValueIndexService {
	
	
	void insertData(String stockCode, ItoozaModeType dateType);
	
}
