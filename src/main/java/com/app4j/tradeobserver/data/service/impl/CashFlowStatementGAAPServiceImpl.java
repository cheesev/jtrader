package com.app4j.tradeobserver.data.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.service.impl.FinancialStatementServiceImpl;
import com.app4j.tradeobserver.data.dao.CashFlowStatementGAAPDao;
import com.app4j.tradeobserver.data.entity.CashFlowStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.CashFlowStatementGAAPDtoId;
import com.app4j.tradeobserver.data.service.CashFlowStatementGAAPService;

@Service("cashFlowStatementGAAPService")
@Transactional
public class CashFlowStatementGAAPServiceImpl 
				extends FinancialStatementServiceImpl<CashFlowStatementGAAPDto, CashFlowStatementGAAPDtoId, CashFlowStatementGAAPDao>
				implements CashFlowStatementGAAPService{
		
	public CashFlowStatementGAAPServiceImpl() {		
		super(CashFlowStatementGAAPDto.class);
		
	}
}
