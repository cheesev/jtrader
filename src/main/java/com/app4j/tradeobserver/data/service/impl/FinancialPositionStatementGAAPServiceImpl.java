package com.app4j.tradeobserver.data.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.service.impl.FinancialStatementServiceImpl;
import com.app4j.tradeobserver.data.dao.FinancialPositionStatementGAAPDao;
import com.app4j.tradeobserver.data.entity.FinancialPositionStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.FinancialPositionStatementGAAPDtoId;
import com.app4j.tradeobserver.data.service.FinancialPositionStatementGAAPService;

@Service("financialPositionStatementGAAPService")
@Transactional
public class FinancialPositionStatementGAAPServiceImpl 
				extends FinancialStatementServiceImpl<FinancialPositionStatementGAAPDto, FinancialPositionStatementGAAPDtoId, FinancialPositionStatementGAAPDao>
				implements FinancialPositionStatementGAAPService{
		
	public FinancialPositionStatementGAAPServiceImpl() {		
		super(FinancialPositionStatementGAAPDto.class);
		
	}
}
