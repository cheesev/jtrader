package com.app4j.tradeobserver.data.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.service.impl.ItoozaFinancialStatementServiceImpl;
import com.app4j.tradeobserver.data.dao.ItoozaFinancialPositionStatementGAAPDao;
import com.app4j.tradeobserver.data.entity.ItoozaFinancialPositionStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaFinancialPositionStatementGAAPDtoId;
import com.app4j.tradeobserver.data.service.ItoozaFinancialPositionStatementGAAPService;

@Service("itoozaFinancialPositionStatementGAAPService")
@Transactional
public class ItoozaFinancialPositionStatementGAAPServiceImpl 
				extends ItoozaFinancialStatementServiceImpl<ItoozaFinancialPositionStatementGAAPDto, ItoozaFinancialPositionStatementGAAPDtoId, ItoozaFinancialPositionStatementGAAPDao>
				implements ItoozaFinancialPositionStatementGAAPService {
		
	public ItoozaFinancialPositionStatementGAAPServiceImpl() {		
		super(ItoozaFinancialPositionStatementGAAPDto.class);
		
	}
}
