package com.app4j.tradeobserver.data.service.impl;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.reflect.FieldUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.util.CustomUtils;
import com.app4j.tradeobserver.data.constants.DataCommonConstants;
import com.app4j.tradeobserver.data.constants.ItoozaModeType;
import com.app4j.tradeobserver.data.dao.ItoozaValueIndexDao;
import com.app4j.tradeobserver.data.entity.ItoozaValueIndexDto;
import com.app4j.tradeobserver.data.service.ItoozaValueIndexService;

@Service("itoozaValueIndexService")
@Transactional
public class ItoozaValueIndexServiceImpl implements ItoozaValueIndexService {
	
	@Autowired
	ItoozaValueIndexDao ItoozaValueIndexDao;

	@Override
	public void insertData(String stockCode, ItoozaModeType dateType) {
		String valueIndexUrl = DataCommonConstants.ITOOZA_VALUE_INDEX_URL_TEMPLATE;
		valueIndexUrl = valueIndexUrl.replace("%stockCode%", stockCode);
		valueIndexUrl = valueIndexUrl.replace("%dateType%", dateType.getModeType());
		
		try {
			Document doc = Jsoup.connect(valueIndexUrl).get();
			
			Elements elements = doc.select(DataCommonConstants.VALUE_INDEX_YMDAY_SELECTOR);
			CustomUtils.removeEmptyElement(elements);
			
			List<String> quaterList = CustomUtils.getElementText(elements, "[^0-9]");
			int size = quaterList.size();
			
			elements = doc.select(DataCommonConstants.VALUE_INDEX_DATA_SELECTOR);			
			CustomUtils.removeEmptyElement(elements);
			CustomUtils.removeElement(elements, ".");
			
			List<String> fieldNameList = CustomUtils.getHanguelFieldNames(ItoozaValueIndexDto.class);
			
			
			List<ItoozaValueIndexDto> itoozaValueIndexDtoList = new ArrayList<ItoozaValueIndexDto>();
			
			for( int i=0 ; i < elements.size() ; i++ ) {
				
				ItoozaValueIndexDto itoozaValueIndexDto = null;
				String curValue = elements.get(i).text();
				if (".".equals(curValue) || StringUtils.isEmpty(curValue)) {
					continue;
				} else if (DataCommonConstants.NOT_APPLICABLE.equals(curValue)) {
					curValue = "0";
				} else {
					curValue = NumberFormat.getNumberInstance(Locale.US).parse(curValue).toString();
				}
				
				if( i / size > 0) {
					itoozaValueIndexDto = itoozaValueIndexDtoList.get(i % size);					
				} else {
					itoozaValueIndexDto = new ItoozaValueIndexDto();
					itoozaValueIndexDtoList.add(itoozaValueIndexDto);
					FieldUtils.writeField(itoozaValueIndexDto, "ymDay", quaterList.get(itoozaValueIndexDtoList.size()-1), true);
					FieldUtils.writeField(itoozaValueIndexDto, "stockCode", stockCode, true);
					FieldUtils.writeField(itoozaValueIndexDto, "dateType", dateType.getModeType(), true);		
				}
				
				FieldUtils.writeField(itoozaValueIndexDto, fieldNameList.get(i / size), Double.valueOf(curValue), true);
				
			}
			
			ItoozaValueIndexDao.multiSaveOrUpdate(itoozaValueIndexDtoList);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
		
	}
}
