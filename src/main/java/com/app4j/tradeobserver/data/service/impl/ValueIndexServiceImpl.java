package com.app4j.tradeobserver.data.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.reflect.FieldUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.data.constants.DataCommonConstants;
import com.app4j.tradeobserver.data.constants.FinancialAccountType;
import com.app4j.tradeobserver.data.dao.ValueIndexDao;
import com.app4j.tradeobserver.data.entity.ValueIndexDto;
import com.app4j.tradeobserver.data.entity.id.FinancialStatementCommonDtoId;
import com.app4j.tradeobserver.data.service.ValueIndexService;

@Service("valueIndexService")
@Transactional
public class ValueIndexServiceImpl implements ValueIndexService {
	
	@Resource(name="valueIndexDao")
	private ValueIndexDao valueIndexDao;
	
	@Override
	public void insertValueIndex(String stockCode) {
		// TODO Auto-generated method stub
		String valueIndexUrl = DataCommonConstants.VALUE_INDEX_URL_TEMPLATE;		
		valueIndexUrl = valueIndexUrl.replace("STOCK_CODE", stockCode);
		
		
		//원래 2부터 6까지 돌리는거자나 (2 : 수익성, 3 : 성장성, 4: 안정성, 5: 회전율(활동성) ,6: 가치지표		
		for (int valueType = 2; valueType <= 6 ; valueType++) {
			String curValueIndex = valueIndexUrl.replace("VALUE_TYPE", String.valueOf(valueType));
			
			//IFRS 만.. 일단 넣기
			this.parseValueIndex(curValueIndex, stockCode, "Q", FinancialAccountType.IFRS_EACH.getStatementType());
			this.parseValueIndex(curValueIndex, stockCode, "Y", FinancialAccountType.IFRS_LINK.getStatementType());
		}
		
	}
	
	private void parseValueIndex(String curValueIndex, String stockCode, String dateType, String dataType) {
		Document doc = null;
		try {
			doc = Jsoup.connect(
					curValueIndex.replace("DATE_TYPE", dateType).replace("DATA_TYPE", dataType)
					).timeout(5000).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Elements elements = doc.select(".chart").select("area");
		
		for (Element element : elements) {
			ValueIndexDto valueIndexDto = null;
			
			String[] fieldValue = element.attr("title").split("\n"); // 0이 필드네임, 1이 yyymm, 2가 값			
			if (fieldValue.length != 3 ) continue;
			
			String fieldName = StringUtils.substringBefore(fieldValue[0], "(")
								.replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", "");
			String ymDay = fieldValue[1].replaceAll("[^0-9]", "");
			String value = fieldValue[2].replaceAll("[^0-9.]", "");
			
			if (StringUtils.isEmpty(value)) continue;
			
			valueIndexDto = valueIndexDao.getById(new FinancialStatementCommonDtoId
												(stockCode, ymDay, dateType, dataType));
			if( valueIndexDto == null) {
				valueIndexDto = new ValueIndexDto();
				valueIndexDto.setStockCode(stockCode);
				valueIndexDto.setYmDay(ymDay);
				valueIndexDto.setDateType(dateType);
				valueIndexDto.setDataType(dataType);
			}
			
			try {
				FieldUtils.writeField(valueIndexDto, fieldName, Double.valueOf(value), true);
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			valueIndexDao.saveOrUpdate(valueIndexDto);
		}
		
	}
	
	
}
