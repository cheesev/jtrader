package com.app4j.tradeobserver.data.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.service.impl.FinancialStatementServiceImpl;
import com.app4j.tradeobserver.data.dao.ProfitLossStatementGAAPDao;
import com.app4j.tradeobserver.data.entity.ProfitLossStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.ProfitLossStatementGAAPDtoId;
import com.app4j.tradeobserver.data.service.ProfitLossStatementGAAPService;

@Service("profitLossStatementGAAPService")
@Transactional
public class ProfitLossStatementGAAPServiceImpl 
				extends FinancialStatementServiceImpl<ProfitLossStatementGAAPDto, ProfitLossStatementGAAPDtoId, ProfitLossStatementGAAPDao>
				implements ProfitLossStatementGAAPService{
		
	public ProfitLossStatementGAAPServiceImpl() {		
		super(ProfitLossStatementGAAPDto.class);
		
	}
	
	
}
