package com.app4j.tradeobserver.data.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.service.impl.ItoozaFinancialStatementServiceImpl;
import com.app4j.tradeobserver.data.dao.ItoozaProfitLossStatementIFRSDao;
import com.app4j.tradeobserver.data.entity.ItoozaProfitLossStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaProfitLossStatementIFRSDtoId;
import com.app4j.tradeobserver.data.service.ItoozaProfitLossStatementIFRSService;

@Service("itoozaProfitLossStatementIFRSService")
@Transactional
public class ItoozaProfitLossStatementIFRSServiceImpl 
				extends ItoozaFinancialStatementServiceImpl<ItoozaProfitLossStatementIFRSDto, ItoozaProfitLossStatementIFRSDtoId, ItoozaProfitLossStatementIFRSDao>
				implements ItoozaProfitLossStatementIFRSService {
		
	public ItoozaProfitLossStatementIFRSServiceImpl() {		
		super(ItoozaProfitLossStatementIFRSDto.class);
		
	}
}
