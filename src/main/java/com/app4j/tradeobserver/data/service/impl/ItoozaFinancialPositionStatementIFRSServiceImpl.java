package com.app4j.tradeobserver.data.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.service.impl.ItoozaFinancialStatementServiceImpl;
import com.app4j.tradeobserver.data.dao.ItoozaFinancialPositionStatementIFRSDao;
import com.app4j.tradeobserver.data.entity.ItoozaFinancialPositionStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaFinancialPositionStatementIFRSDtoId;
import com.app4j.tradeobserver.data.service.ItoozaFinancialPositionStatementIFRSService;

@Service("itoozaFinancialPositionStatementIFRSService")
@Transactional
public class ItoozaFinancialPositionStatementIFRSServiceImpl 
				extends ItoozaFinancialStatementServiceImpl<ItoozaFinancialPositionStatementIFRSDto, ItoozaFinancialPositionStatementIFRSDtoId, ItoozaFinancialPositionStatementIFRSDao>
				implements ItoozaFinancialPositionStatementIFRSService {
		
	public ItoozaFinancialPositionStatementIFRSServiceImpl() {		
		super(ItoozaFinancialPositionStatementIFRSDto.class);
		
	}
}
