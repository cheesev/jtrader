package com.app4j.tradeobserver.data.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.service.impl.FinancialStatementServiceImpl;
import com.app4j.tradeobserver.data.dao.FinancialPositionStatementIFRSDao;
import com.app4j.tradeobserver.data.entity.FinancialPositionStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.FinancialPositionStatementIFRSDtoId;
import com.app4j.tradeobserver.data.service.FinancialPositionStatementIFRSService;

@Service("financialPositionStatementIFRSService")
@Transactional
public class FinancialPositionStatementIFRSServiceImpl 
				extends FinancialStatementServiceImpl<FinancialPositionStatementIFRSDto, FinancialPositionStatementIFRSDtoId, FinancialPositionStatementIFRSDao>
				implements FinancialPositionStatementIFRSService{
		
	public FinancialPositionStatementIFRSServiceImpl() {		
		super(FinancialPositionStatementIFRSDto.class);
		
	}
}
