package com.app4j.tradeobserver.data.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.service.impl.ItoozaFinancialStatementServiceImpl;
import com.app4j.tradeobserver.data.dao.ItoozaCashFlowStatementGAAPDao;
import com.app4j.tradeobserver.data.entity.ItoozaCashFlowStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaCashFlowStatementGAAPDtoId;
import com.app4j.tradeobserver.data.service.ItoozaCashFlowStatementGAAPService;

@Service("itoozaCashFlowStatementGAAPService")
@Transactional
public class ItoozaCashFlowStatementGAPPServiceImpl 
				extends ItoozaFinancialStatementServiceImpl<ItoozaCashFlowStatementGAAPDto, ItoozaCashFlowStatementGAAPDtoId, ItoozaCashFlowStatementGAAPDao>
				implements ItoozaCashFlowStatementGAAPService {
		
	public ItoozaCashFlowStatementGAPPServiceImpl() {		
		super(ItoozaCashFlowStatementGAAPDto.class);
	}
}
