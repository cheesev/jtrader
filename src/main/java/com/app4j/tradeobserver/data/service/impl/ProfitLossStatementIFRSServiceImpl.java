package com.app4j.tradeobserver.data.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app4j.tradeobserver.common.service.impl.FinancialStatementServiceImpl;
import com.app4j.tradeobserver.data.dao.ProfitLossStatementIFRSDao;
import com.app4j.tradeobserver.data.entity.ProfitLossStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.ProfitLossStatementIFRSDtoId;
import com.app4j.tradeobserver.data.service.ProfitLossStatementIFRSService;

@Service("profitLossStatementIFRSService")
@Transactional
public class ProfitLossStatementIFRSServiceImpl 
				extends FinancialStatementServiceImpl<ProfitLossStatementIFRSDto, ProfitLossStatementIFRSDtoId, ProfitLossStatementIFRSDao>
				implements ProfitLossStatementIFRSService{
		
	public ProfitLossStatementIFRSServiceImpl() {		
		super(ProfitLossStatementIFRSDto.class);
	}
}
