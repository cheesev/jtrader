package com.app4j.tradeobserver.data.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.ItoozaValueIndexDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaValueIndexDtoId;

public interface ItoozaValueIndexDao  extends AbstractDao <ItoozaValueIndexDto , ItoozaValueIndexDtoId> {
			
}
