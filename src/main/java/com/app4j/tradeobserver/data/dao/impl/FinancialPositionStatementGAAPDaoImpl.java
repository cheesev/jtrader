package com.app4j.tradeobserver.data.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.data.dao.FinancialPositionStatementGAAPDao;
import com.app4j.tradeobserver.data.entity.FinancialPositionStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.FinancialPositionStatementGAAPDtoId;

@Repository("financialPositionStatementGAAPDao")
public class FinancialPositionStatementGAAPDaoImpl extends AbstractDaoImpl<FinancialPositionStatementGAAPDto, FinancialPositionStatementGAAPDtoId> implements FinancialPositionStatementGAAPDao{

	protected FinancialPositionStatementGAAPDaoImpl() {
        super(FinancialPositionStatementGAAPDto.class);
    }
	
}
