package com.app4j.tradeobserver.data.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.FinancialPositionStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.FinancialPositionStatementGAAPDtoId;

public interface FinancialPositionStatementGAAPDao  extends AbstractDao <FinancialPositionStatementGAAPDto , FinancialPositionStatementGAAPDtoId> {
			
}
