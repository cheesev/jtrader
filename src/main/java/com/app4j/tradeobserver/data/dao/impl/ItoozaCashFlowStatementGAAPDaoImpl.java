package com.app4j.tradeobserver.data.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.data.dao.ItoozaCashFlowStatementGAAPDao;
import com.app4j.tradeobserver.data.entity.ItoozaCashFlowStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaCashFlowStatementGAAPDtoId;

@Repository("itoozaCashFlowStatementGAAPDao")
public class ItoozaCashFlowStatementGAAPDaoImpl extends AbstractDaoImpl<ItoozaCashFlowStatementGAAPDto, ItoozaCashFlowStatementGAAPDtoId> implements ItoozaCashFlowStatementGAAPDao{

	protected ItoozaCashFlowStatementGAAPDaoImpl() {
        super(ItoozaCashFlowStatementGAAPDto.class);
    }
	
}
