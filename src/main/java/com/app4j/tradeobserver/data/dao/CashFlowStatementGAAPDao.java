package com.app4j.tradeobserver.data.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.CashFlowStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.CashFlowStatementGAAPDtoId;

public interface CashFlowStatementGAAPDao  extends AbstractDao <CashFlowStatementGAAPDto , CashFlowStatementGAAPDtoId> {
			
}
