package com.app4j.tradeobserver.data.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.data.dao.ProfitLossStatementGAAPDao;
import com.app4j.tradeobserver.data.entity.ProfitLossStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.ProfitLossStatementGAAPDtoId;

@Repository("profitLossStatementGAAPDao")
public class ProfitLossStatementGAAPDaoImpl extends AbstractDaoImpl<ProfitLossStatementGAAPDto, ProfitLossStatementGAAPDtoId> implements ProfitLossStatementGAAPDao{

	protected ProfitLossStatementGAAPDaoImpl() {
        super(ProfitLossStatementGAAPDto.class);
    }
	
}
