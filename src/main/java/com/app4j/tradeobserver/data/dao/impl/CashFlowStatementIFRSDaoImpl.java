package com.app4j.tradeobserver.data.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.data.dao.CashFlowStatementIFRSDao;
import com.app4j.tradeobserver.data.entity.CashFlowStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.CashFlowStatementIFRSDtoId;

@Repository("cashFlowStatementIFRSDao")
public class CashFlowStatementIFRSDaoImpl extends AbstractDaoImpl<CashFlowStatementIFRSDto, CashFlowStatementIFRSDtoId> implements CashFlowStatementIFRSDao{

	protected CashFlowStatementIFRSDaoImpl() {
        super(CashFlowStatementIFRSDto.class);
    }
	
}
