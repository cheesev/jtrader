package com.app4j.tradeobserver.data.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.data.dao.FinancialPositionStatementIFRSDao;
import com.app4j.tradeobserver.data.entity.FinancialPositionStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.FinancialPositionStatementIFRSDtoId;

@Repository("financialPositionStatementIFRSDao")
public class FinancialPositionStatementIFRSDaoImpl extends AbstractDaoImpl<FinancialPositionStatementIFRSDto, FinancialPositionStatementIFRSDtoId> implements FinancialPositionStatementIFRSDao{

	protected FinancialPositionStatementIFRSDaoImpl() {
        super(FinancialPositionStatementIFRSDto.class);
    }
	
}
