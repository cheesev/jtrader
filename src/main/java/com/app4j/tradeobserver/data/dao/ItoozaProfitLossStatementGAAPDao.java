package com.app4j.tradeobserver.data.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.ItoozaProfitLossStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaProfitLossStatementGAAPDtoId;

public interface ItoozaProfitLossStatementGAAPDao  extends AbstractDao <ItoozaProfitLossStatementGAAPDto , ItoozaProfitLossStatementGAAPDtoId> {
			
}
