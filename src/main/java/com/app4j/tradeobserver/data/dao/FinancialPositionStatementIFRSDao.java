package com.app4j.tradeobserver.data.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.FinancialPositionStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.FinancialPositionStatementIFRSDtoId;

public interface FinancialPositionStatementIFRSDao  extends AbstractDao <FinancialPositionStatementIFRSDto , FinancialPositionStatementIFRSDtoId> {
			
}
