package com.app4j.tradeobserver.data.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.CashFlowStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.CashFlowStatementIFRSDtoId;

public interface CashFlowStatementIFRSDao  extends AbstractDao <CashFlowStatementIFRSDto , CashFlowStatementIFRSDtoId> {
			
}
