package com.app4j.tradeobserver.data.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.data.dao.ItoozaProfitLossStatementGAAPDao;
import com.app4j.tradeobserver.data.entity.ItoozaProfitLossStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaProfitLossStatementGAAPDtoId;

@Repository("itoozaProfitLossStatementGAAPDao")
public class ItoozaProfitLossStatementGAAPDaoImpl extends AbstractDaoImpl<ItoozaProfitLossStatementGAAPDto, ItoozaProfitLossStatementGAAPDtoId> implements ItoozaProfitLossStatementGAAPDao{

	protected ItoozaProfitLossStatementGAAPDaoImpl() {
        super(ItoozaProfitLossStatementGAAPDto.class);
    }
	
}
