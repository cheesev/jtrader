package com.app4j.tradeobserver.data.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.ItoozaCashFlowStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaCashFlowStatementGAAPDtoId;

public interface ItoozaCashFlowStatementGAAPDao  extends AbstractDao <ItoozaCashFlowStatementGAAPDto , ItoozaCashFlowStatementGAAPDtoId> {
			
}
