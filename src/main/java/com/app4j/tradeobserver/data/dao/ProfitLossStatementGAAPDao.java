package com.app4j.tradeobserver.data.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.ProfitLossStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.ProfitLossStatementGAAPDtoId;

public interface ProfitLossStatementGAAPDao  extends AbstractDao <ProfitLossStatementGAAPDto , ProfitLossStatementGAAPDtoId> {
			

}
