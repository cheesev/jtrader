package com.app4j.tradeobserver.data.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.ItoozaProfitLossStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaProfitLossStatementIFRSDtoId;

public interface ItoozaProfitLossStatementIFRSDao  extends AbstractDao <ItoozaProfitLossStatementIFRSDto , ItoozaProfitLossStatementIFRSDtoId> {
			
}
