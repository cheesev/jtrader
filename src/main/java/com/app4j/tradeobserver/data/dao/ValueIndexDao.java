package com.app4j.tradeobserver.data.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.ValueIndexDto;
import com.app4j.tradeobserver.data.entity.id.FinancialStatementCommonDtoId;

public interface ValueIndexDao  extends AbstractDao <ValueIndexDto , FinancialStatementCommonDtoId> {
			

}
