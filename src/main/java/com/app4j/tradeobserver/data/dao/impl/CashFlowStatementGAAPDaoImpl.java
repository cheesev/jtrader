package com.app4j.tradeobserver.data.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.data.dao.CashFlowStatementGAAPDao;
import com.app4j.tradeobserver.data.entity.CashFlowStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.CashFlowStatementGAAPDtoId;

@Repository("cashFlowStatementGAAPDao")
public class CashFlowStatementGAAPDaoImpl extends AbstractDaoImpl<CashFlowStatementGAAPDto, CashFlowStatementGAAPDtoId> implements CashFlowStatementGAAPDao{

	protected CashFlowStatementGAAPDaoImpl() {
        super(CashFlowStatementGAAPDto.class);
    }
	
}
