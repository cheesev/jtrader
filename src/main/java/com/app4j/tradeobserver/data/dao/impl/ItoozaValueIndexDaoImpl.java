package com.app4j.tradeobserver.data.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.data.dao.ItoozaValueIndexDao;
import com.app4j.tradeobserver.data.entity.ItoozaValueIndexDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaValueIndexDtoId;

@Repository("itoozaValueIndexDao")
public class ItoozaValueIndexDaoImpl extends AbstractDaoImpl<ItoozaValueIndexDto, ItoozaValueIndexDtoId> implements ItoozaValueIndexDao{

	protected ItoozaValueIndexDaoImpl() {
        super(ItoozaValueIndexDto.class);
    }
	
}
