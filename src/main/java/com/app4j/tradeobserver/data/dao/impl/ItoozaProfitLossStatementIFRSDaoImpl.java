package com.app4j.tradeobserver.data.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.data.dao.ItoozaProfitLossStatementIFRSDao;
import com.app4j.tradeobserver.data.entity.ItoozaProfitLossStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaProfitLossStatementIFRSDtoId;

@Repository("itoozaProfitLossStatementIFRSDao")
public class ItoozaProfitLossStatementIFRSDaoImpl extends AbstractDaoImpl<ItoozaProfitLossStatementIFRSDto, ItoozaProfitLossStatementIFRSDtoId> implements ItoozaProfitLossStatementIFRSDao{

	protected ItoozaProfitLossStatementIFRSDaoImpl() {
        super(ItoozaProfitLossStatementIFRSDto.class);
    }
	
}
