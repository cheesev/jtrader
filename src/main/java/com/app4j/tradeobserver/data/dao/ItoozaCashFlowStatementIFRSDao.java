package com.app4j.tradeobserver.data.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.ItoozaCashFlowStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaCashFlowStatementIFRSDtoId;

public interface ItoozaCashFlowStatementIFRSDao  extends AbstractDao <ItoozaCashFlowStatementIFRSDto , ItoozaCashFlowStatementIFRSDtoId> {
			
}
