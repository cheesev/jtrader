package com.app4j.tradeobserver.data.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.data.dao.ItoozaFinancialPositionStatementIFRSDao;
import com.app4j.tradeobserver.data.entity.ItoozaFinancialPositionStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaFinancialPositionStatementIFRSDtoId;

@Repository("itoozaFinancialPositionStatementIFRSDao")
public class ItoozaFinancialPositionStatementIFRSDaoImpl extends AbstractDaoImpl<ItoozaFinancialPositionStatementIFRSDto, ItoozaFinancialPositionStatementIFRSDtoId> implements ItoozaFinancialPositionStatementIFRSDao{

	protected ItoozaFinancialPositionStatementIFRSDaoImpl() {
        super(ItoozaFinancialPositionStatementIFRSDto.class);
    }
	
}
