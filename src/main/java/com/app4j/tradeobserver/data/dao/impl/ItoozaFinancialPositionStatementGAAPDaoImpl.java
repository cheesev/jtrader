package com.app4j.tradeobserver.data.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.data.dao.ItoozaFinancialPositionStatementGAAPDao;
import com.app4j.tradeobserver.data.entity.ItoozaFinancialPositionStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaFinancialPositionStatementGAAPDtoId;

@Repository("itoozaFinancialPositionStatementGAAPDao")
public class ItoozaFinancialPositionStatementGAAPDaoImpl extends AbstractDaoImpl<ItoozaFinancialPositionStatementGAAPDto, ItoozaFinancialPositionStatementGAAPDtoId> implements ItoozaFinancialPositionStatementGAAPDao{

	protected ItoozaFinancialPositionStatementGAAPDaoImpl() {
        super(ItoozaFinancialPositionStatementGAAPDto.class);
    }
	
}
