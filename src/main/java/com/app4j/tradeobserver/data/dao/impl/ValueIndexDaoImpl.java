package com.app4j.tradeobserver.data.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.data.dao.ProfitLossStatementGAAPDao;
import com.app4j.tradeobserver.data.dao.ProfitLossStatementIFRSDao;
import com.app4j.tradeobserver.data.dao.ValueIndexDao;
import com.app4j.tradeobserver.data.entity.ProfitLossStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.ProfitLossStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.ValueIndexDto;
import com.app4j.tradeobserver.data.entity.id.FinancialStatementCommonDtoId;
import com.app4j.tradeobserver.data.entity.id.ProfitLossStatementGAAPDtoId;
import com.app4j.tradeobserver.data.entity.id.ProfitLossStatementIFRSDtoId;

@Repository("valueIndexDao")
public class ValueIndexDaoImpl extends AbstractDaoImpl<ValueIndexDto, FinancialStatementCommonDtoId> implements ValueIndexDao{

	protected ValueIndexDaoImpl() {
        super(ValueIndexDto.class);
    }
	
	
	
}
