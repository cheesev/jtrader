package com.app4j.tradeobserver.data.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.data.dao.ItoozaCashFlowStatementIFRSDao;
import com.app4j.tradeobserver.data.entity.ItoozaCashFlowStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaCashFlowStatementIFRSDtoId;

@Repository("itoozaCashFlowStatementIFRSDao")
public class ItoozaCashFlowStatementIFRSDaoImpl extends AbstractDaoImpl<ItoozaCashFlowStatementIFRSDto, ItoozaCashFlowStatementIFRSDtoId> implements ItoozaCashFlowStatementIFRSDao{

	protected ItoozaCashFlowStatementIFRSDaoImpl() {
        super(ItoozaCashFlowStatementIFRSDto.class);
    }
	
}
