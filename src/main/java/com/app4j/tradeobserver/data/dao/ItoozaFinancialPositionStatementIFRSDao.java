package com.app4j.tradeobserver.data.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.ItoozaFinancialPositionStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaFinancialPositionStatementIFRSDtoId;

public interface ItoozaFinancialPositionStatementIFRSDao  extends AbstractDao <ItoozaFinancialPositionStatementIFRSDto , ItoozaFinancialPositionStatementIFRSDtoId> {
			
}
