package com.app4j.tradeobserver.data.dao.impl;

import org.springframework.stereotype.Repository;

import com.app4j.tradeobserver.common.dao.impl.AbstractDaoImpl;
import com.app4j.tradeobserver.data.dao.ProfitLossStatementGAAPDao;
import com.app4j.tradeobserver.data.dao.ProfitLossStatementIFRSDao;
import com.app4j.tradeobserver.data.entity.ProfitLossStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.ProfitLossStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.ProfitLossStatementGAAPDtoId;
import com.app4j.tradeobserver.data.entity.id.ProfitLossStatementIFRSDtoId;

@Repository("profitLossStatementIFRSDao")
public class ProfitLossStatementIFRSDaoImpl extends AbstractDaoImpl<ProfitLossStatementIFRSDto, ProfitLossStatementIFRSDtoId> implements ProfitLossStatementIFRSDao{

	protected ProfitLossStatementIFRSDaoImpl() {
        super(ProfitLossStatementIFRSDto.class);
    }
	
}
