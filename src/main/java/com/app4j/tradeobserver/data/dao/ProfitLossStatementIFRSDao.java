package com.app4j.tradeobserver.data.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.ProfitLossStatementIFRSDto;
import com.app4j.tradeobserver.data.entity.id.ProfitLossStatementIFRSDtoId;

public interface ProfitLossStatementIFRSDao  extends AbstractDao <ProfitLossStatementIFRSDto , ProfitLossStatementIFRSDtoId> {
			

}
