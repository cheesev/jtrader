package com.app4j.tradeobserver.data.dao;

import com.app4j.tradeobserver.common.dao.AbstractDao;
import com.app4j.tradeobserver.data.entity.ItoozaFinancialPositionStatementGAAPDto;
import com.app4j.tradeobserver.data.entity.id.ItoozaFinancialPositionStatementGAAPDtoId;

public interface ItoozaFinancialPositionStatementGAAPDao  extends AbstractDao <ItoozaFinancialPositionStatementGAAPDto , ItoozaFinancialPositionStatementGAAPDtoId> {
			
}
