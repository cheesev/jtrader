package com.app4j.tradeobserver.data.entity.id;

import java.io.Serializable;

import com.app4j.tradeobserver.data.entity.CashFlowStatementIFRSDto;

public class CashFlowStatementIFRSDtoId implements Serializable {
	
	protected String stockCode;
	protected String ymDay;
	protected String dateType;
	protected String dataType;
	
	public CashFlowStatementIFRSDtoId() {
	}

	public CashFlowStatementIFRSDtoId(String stockCode, String ymDay, String dateType, String dataType) {
		this.ymDay = ymDay;
		this.stockCode = stockCode;
		this.dateType = dateType;
		this.dataType = dataType;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		CashFlowStatementIFRSDto cashFlowStatementIFRSDto = (CashFlowStatementIFRSDto) o;

		return stockCode.equals(cashFlowStatementIFRSDto.getStockCode())
				&& ymDay.equals(cashFlowStatementIFRSDto.getYmDay())
			    && dateType.equals(cashFlowStatementIFRSDto.getDateType())
			    && dataType.equals(cashFlowStatementIFRSDto.getDataType());
	}

	public int hashCode() {
		int result;
		result = dataType.hashCode();
		result = 13 * dateType.hashCode();
		result = 17 * result + ymDay.hashCode();
		result = 29 * result + stockCode.hashCode();
		return result;
	}

}
