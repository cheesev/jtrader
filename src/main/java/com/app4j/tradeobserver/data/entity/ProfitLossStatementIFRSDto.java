package com.app4j.tradeobserver.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.app4j.tradeobserver.data.entity.id.ProfitLossStatementIFRSDtoId;

@Entity
@Table (name = "PROFIT_LOSS_STATEMENT_IFRS")
@IdClass(ProfitLossStatementIFRSDtoId.class)
public class ProfitLossStatementIFRSDto {
	
	@Id
    @Column (name = "STOCK_CODE")
    private String stockCode;	
	@Id
	@Column (name="YM_DAY")
	private String ymDay;
	@Id
	@Column (name="DATE_TYPE")
	private String dateType;		// 분기, 연간 (Q, Y)
	@Id
	@Column (name="DATA_TYPE")
	private String dataType;		// 연결, 별도 (L, S)
	
	@Column(name="매출액수익")
	private Double 매출액수익;
	@Column(name="내수")
	private Double 내수;
	@Column(name="수출")
	private Double 수출;
	@Column(name="수출_제품매출액")
	private Double 수출_제품매출액;
	@Column(name="수출_상품매출액")
	private Double 수출_상품매출액;
	@Column(name="수출_제품상품매출액")
	private Double 수출_제품상품매출액;
	@Column(name="수출_공사수익")
	private Double 수출_공사수익;
	@Column(name="수출_분양수익")
	private Double 수출_분양수익;
	@Column(name="수출_용역수익")
	private Double 수출_용역수익;
	@Column(name="수출_지분법이익")
	private Double 수출_지분법이익;
	@Column(name="수출_금융수익")
	private Double 수출_금융수익;
	@Column(name="수출_배당수익")
	private Double 수출_배당수익;
	@Column(name="수출_이자수익")
	private Double 수출_이자수익;
	@Column(name="수출_기타")
	private Double 수출_기타;
	@Column(name="매출원가")
	private Double 매출원가;
	@Column(name="매출원가_제품매출원가")
	private Double 매출원가_제품매출원가;
	@Column(name="매출원가_상품매출원가")
	private Double 매출원가_상품매출원가;
	@Column(name="매출원가_제품상품매출원가")
	private Double 매출원가_제품상품매출원가;
	@Column(name="매출원가_건설계약원가")
	private Double 매출원가_건설계약원가;
	@Column(name="매출원가_분양원가")
	private Double 매출원가_분양원가;
	@Column(name="매출원가_용역원가")
	private Double 매출원가_용역원가;
	@Column(name="매출원가_지분법손실")
	private Double 매출원가_지분법손실;
	@Column(name="매출원가_금융비용")
	private Double 매출원가_금융비용;
	@Column(name="매출원가_이자비용")
	private Double 매출원가_이자비용;
	@Column(name="매출원가_기타")
	private Double 매출원가_기타;
	@Column(name="매출총이익")
	private Double 매출총이익;
	@Column(name="상각후원가로측정하는금융자산의제거로부터발생한이익")
	private Double 상각후원가로측정하는금융자산의제거로부터발생한이익;
	@Column(name="상각후원가로측정하는금융자산의제거로부터발생한손실")
	private Double 상각후원가로측정하는금융자산의제거로부터발생한손실;
	@Column(name="판매비와관리비")
	private Double 판매비와관리비;
	@Column(name="판매비와관리비_급여")
	private Double 판매비와관리비_급여;
	@Column(name="판매비와관리비_퇴직급여")
	private Double 판매비와관리비_퇴직급여;
	@Column(name="판매비와관리비_명예퇴직금")
	private Double 판매비와관리비_명예퇴직금;
	@Column(name="판매비와관리비_복리후생비")
	private Double 판매비와관리비_복리후생비;
	@Column(name="판매비와관리비_주식보상비")
	private Double 판매비와관리비_주식보상비;
	@Column(name="판매비와관리비_교육훈련비")
	private Double 판매비와관리비_교육훈련비;
	@Column(name="판매비와관리비_수도광열비")
	private Double 판매비와관리비_수도광열비;
	@Column(name="판매비와관리비_세금과공과")
	private Double 판매비와관리비_세금과공과;
	@Column(name="판매비와관리비_임차료")
	private Double 판매비와관리비_임차료;
	@Column(name="판매비와관리비_보험료")
	private Double 판매비와관리비_보험료;
	@Column(name="판매비와관리비_지급수수료")
	private Double 판매비와관리비_지급수수료;
	@Column(name="판매비와관리비_감가상각비")
	private Double 판매비와관리비_감가상각비;
	@Column(name="판매비와관리비_개발비상각")
	private Double 판매비와관리비_개발비상각;
	@Column(name="판매비와관리비_기타무형자산상각비")
	private Double 판매비와관리비_기타무형자산상각비;
	@Column(name="판매비와관리비_연구개발비")
	private Double 판매비와관리비_연구개발비;
	@Column(name="판매비와관리비_특허권등사용료")
	private Double 판매비와관리비_특허권등사용료;
	@Column(name="판매비와관리비_기타관리비")
	private Double 판매비와관리비_기타관리비;
	@Column(name="판매비와관리비_광고선전비")
	private Double 판매비와관리비_광고선전비;
	@Column(name="판매비와관리비_수출비용")
	private Double 판매비와관리비_수출비용;
	@Column(name="판매비와관리비_판매촉진비")
	private Double 판매비와관리비_판매촉진비;
	@Column(name="판매비와관리비_판매수수료")
	private Double 판매비와관리비_판매수수료;
	@Column(name="판매비와관리비_기타물류원가")
	private Double 판매비와관리비_기타물류원가;
	@Column(name="판매비와관리비_애프터서비스비")
	private Double 판매비와관리비_애프터서비스비;
	@Column(name="판매비와관리비_대손상각비")
	private Double 판매비와관리비_대손상각비;
	@Column(name="판매비와관리비_기타판매비")
	private Double 판매비와관리비_기타판매비;
	@Column(name="판매비와관리비_기타")
	private Double 판매비와관리비_기타;
	@Column(name="판매비와관리비_인건비및복리후생비")
	private Double 판매비와관리비_인건비및복리후생비;
	@Column(name="판매비와관리비_일반관리비")
	private Double 판매비와관리비_일반관리비;
	@Column(name="판매비와관리비_판매비")
	private Double 판매비와관리비_판매비;
	@Column(name="영업이익")
	private Double 영업이익;
	@Column(name="기타영업손익")
	private Double 기타영업손익;
	@Column(name="기타영업손익_기타영업수익")
	private Double 기타영업손익_기타영업수익;
	@Column(name="기타영업손익_기타영업수익_이자수익")
	private Double 기타영업손익_기타영업수익_이자수익;
	@Column(name="기타영업손익_기타영업수익_배당금수익")
	private Double 기타영업손익_기타영업수익_배당금수익;
	@Column(name="기타영업손익_기타영업수익_외환거래이익")
	private Double 기타영업손익_기타영업수익_외환거래이익;
	@Column(name="기타영업손익_기타영업수익_외환거래이익_외환차익")
	private Double 기타영업손익_기타영업수익_외환거래이익_외환차익;
	@Column(name="기타영업손익_기타영업수익_외환거래이익_외화환산이익")
	private Double 기타영업손익_기타영업수익_외환거래이익_외화환산이익;
	@Column(name="기타영업손익_기타영업수익_외환거래이익_기타외화거래이익")
	private Double 기타영업손익_기타영업수익_외환거래이익_기타외화거래이익;
	@Column(name="기타영업손익_기타영업수익_임대료")
	private Double 기타영업손익_기타영업수익_임대료;
	@Column(name="기타영업손익_기타영업수익_대손충당금환입")
	private Double 기타영업손익_기타영업수익_대손충당금환입;
	@Column(name="기타영업손익_기타영업수익_자산처분이익")
	private Double 기타영업손익_기타영업수익_자산처분이익;
	@Column(name="기타영업손익_기타영업수익_자산처분이익_투자부동산처분이익")
	private Double 기타영업손익_기타영업수익_자산처분이익_투자부동산처분이익;
	@Column(name="기타영업손익_기타영업수익_자산처분이익_투자자산처분이익")
	private Double 기타영업손익_기타영업수익_자산처분이익_투자자산처분이익;
	@Column(name="기타영업손익_기타영업수익_자산처분이익_유형리스자산처분이익")
	private Double 기타영업손익_기타영업수익_자산처분이익_유형리스자산처분이익;
	@Column(name="기타영업손익_기타영업수익_자산처분이익_기타")
	private Double 기타영업손익_기타영업수익_자산처분이익_기타;
	@Column(name="기타영업손익_기타영업수익_투자자산평가이익")
	private Double 기타영업손익_기타영업수익_투자자산평가이익;
	@Column(name="기타영업손익_기타영업수익_자산재평가이익")
	private Double 기타영업손익_기타영업수익_자산재평가이익;
	@Column(name="기타영업손익_기타영업수익_파생상품이익")
	private Double 기타영업손익_기타영업수익_파생상품이익;
	@Column(name="기타영업손익_기타영업수익_자산손상차손환입")
	private Double 기타영업손익_기타영업수익_자산손상차손환입;
	@Column(name="기타영업손익_기타영업수익_지분법관련이익")
	private Double 기타영업손익_기타영업수익_지분법관련이익;
	@Column(name="기타영업손익_기타영업수익_기타")
	private Double 기타영업손익_기타영업수익_기타;
	@Column(name="기타영업손익_기타영업비용")
	private Double 기타영업손익_기타영업비용;
	@Column(name="기타영업손익_기타영업비용_이자비용")
	private Double 기타영업손익_기타영업비용_이자비용;
	@Column(name="기타영업손익_기타영업비용_외환거래손실")
	private Double 기타영업손익_기타영업비용_외환거래손실;
	@Column(name="기타영업손익_기타영업비용_외환거래손실_외환차손")
	private Double 기타영업손익_기타영업비용_외환거래손실_외환차손;
	@Column(name="기타영업손익_기타영업비용_외환거래손실_외화환산손실")
	private Double 기타영업손익_기타영업비용_외환거래손실_외화환산손실;
	@Column(name="기타영업손익_기타영업비용_외환거래손실_기타외환거래손실")
	private Double 기타영업손익_기타영업비용_외환거래손실_기타외환거래손실;
	@Column(name="기타영업손익_기타영업비용_기타대손상각비")
	private Double 기타영업손익_기타영업비용_기타대손상각비;
	@Column(name="기타영업손익_기타영업비용_충당부채전입액")
	private Double 기타영업손익_기타영업비용_충당부채전입액;
	@Column(name="기타영업손익_기타영업비용_자산처분손실")
	private Double 기타영업손익_기타영업비용_자산처분손실;
	@Column(name="기타영업손익_기타영업비용_자산처분손실_투자부동산처분손실")
	private Double 기타영업손익_기타영업비용_자산처분손실_투자부동산처분손실;
	@Column(name="기타영업손익_기타영업비용_자산처분손실_투자자산처분손실")
	private Double 기타영업손익_기타영업비용_자산처분손실_투자자산처분손실;
	@Column(name="기타영업손익_기타영업비용_자산처분손실_유형리스자산처분손실")
	private Double 기타영업손익_기타영업비용_자산처분손실_유형리스자산처분손실;
	@Column(name="기타영업손익_기타영업비용_자산처분손실_기타")
	private Double 기타영업손익_기타영업비용_자산처분손실_기타;
	@Column(name="기타영업손익_기타영업비용_투자자산평가손실")
	private Double 기타영업손익_기타영업비용_투자자산평가손실;
	@Column(name="기타영업손익_기타영업비용_자산재평가손실")
	private Double 기타영업손익_기타영업비용_자산재평가손실;
	@Column(name="기타영업손익_기타영업비용_파생상품손실")
	private Double 기타영업손익_기타영업비용_파생상품손실;
	@Column(name="기타영업손익_기타영업비용_자산손상차손")
	private Double 기타영업손익_기타영업비용_자산손상차손;
	@Column(name="기타영업손익_기타영업비용_지분법관련손실")
	private Double 기타영업손익_기타영업비용_지분법관련손실;
	@Column(name="기타영업손익_기타영업비용_기타")
	private Double 기타영업손익_기타영업비용_기타;
	@Column(name="지분법손익")
	private Double 지분법손익;
	@Column(name="지분법손익_지분법이익")
	private Double 지분법손익_지분법이익;
	@Column(name="지분법손익_지분법손실")
	private Double 지분법손익_지분법손실;
	@Column(name="지분법손익_기타")
	private Double 지분법손익_기타;
	@Column(name="지분법손익_구KIFRS영업이익")
	private Double 지분법손익_구KIFRS영업이익;
	@Column(name="금융수익")
	private Double 금융수익;
	@Column(name="금융수익_이자수익")
	private Double 금융수익_이자수익;
	@Column(name="금융수익_배당금수익")
	private Double 금융수익_배당금수익;
	@Column(name="금융수익_외환거래이익")
	private Double 금융수익_외환거래이익;
	@Column(name="금융수익_외환거래이익_외환차익")
	private Double 금융수익_외환거래이익_외환차익;
	@Column(name="금융수익_외환거래이익_외화환산이익")
	private Double 금융수익_외환거래이익_외화환산이익;
	@Column(name="금융수익_외환거래이익_기타외화거래이익")
	private Double 금융수익_외환거래이익_기타외화거래이익;
	@Column(name="금융수익_대손충당금환입액")
	private Double 금융수익_대손충당금환입액;
	@Column(name="금융수익_금융자산처분이익")
	private Double 금융수익_금융자산처분이익;
	@Column(name="금융수익_금융자산처분이익_단기금융자산처분이익")
	private Double 금융수익_금융자산처분이익_단기금융자산처분이익;
	@Column(name="금융수익_금융자산처분이익_장기금융자산처분이익")
	private Double 금융수익_금융자산처분이익_장기금융자산처분이익;
	@Column(name="금융수익_금융자산처분이익_기타금융자산처분이익")
	private Double 금융수익_금융자산처분이익_기타금융자산처분이익;
	@Column(name="금융수익_금융자산평가이익")
	private Double 금융수익_금융자산평가이익;
	@Column(name="금융수익_금융자산평가이익_단기금융자산평가이익")
	private Double 금융수익_금융자산평가이익_단기금융자산평가이익;
	@Column(name="금융수익_금융자산평가이익_장기금융자산평가이익")
	private Double 금융수익_금융자산평가이익_장기금융자산평가이익;
	@Column(name="금융수익_금융자산평가이익_기타금융자산평가이익")
	private Double 금융수익_금융자산평가이익_기타금융자산평가이익;
	@Column(name="금융수익_파생상품이익")
	private Double 금융수익_파생상품이익;
	@Column(name="금융수익_금융자산손상차손환입")
	private Double 금융수익_금융자산손상차손환입;
	@Column(name="금융수익_지분법관련이익")
	private Double 금융수익_지분법관련이익;
	@Column(name="금융수익_기타")
	private Double 금융수익_기타;
	@Column(name="금융비용")
	private Double 금융비용;
	@Column(name="금융비용_이자비용")
	private Double 금융비용_이자비용;
	@Column(name="금융비용_외환거래손실")
	private Double 금융비용_외환거래손실;
	@Column(name="금융비용_외환거래손실_외환차손")
	private Double 금융비용_외환거래손실_외환차손;
	@Column(name="금융비용_외환거래손실_외화환산손실")
	private Double 금융비용_외환거래손실_외화환산손실;
	@Column(name="금융비용_외환거래손실_기타외환거래손실")
	private Double 금융비용_외환거래손실_기타외환거래손실;
	@Column(name="금융비용_대손상각비")
	private Double 금융비용_대손상각비;
	@Column(name="금융비용_금융자산처분손실")
	private Double 금융비용_금융자산처분손실;
	@Column(name="금융비용_금융자산처분손실_단기금융자산처분손실")
	private Double 금융비용_금융자산처분손실_단기금융자산처분손실;
	@Column(name="금융비용_금융자산처분손실_장기금융자산처분손실")
	private Double 금융비용_금융자산처분손실_장기금융자산처분손실;
	@Column(name="금융비용_금융자산처분손실_기타금융자산처분손실")
	private Double 금융비용_금융자산처분손실_기타금융자산처분손실;
	@Column(name="금융비용_금융자산평가손실")
	private Double 금융비용_금융자산평가손실;
	@Column(name="금융비용_금융자산평가손실_단기금융자산평가손실")
	private Double 금융비용_금융자산평가손실_단기금융자산평가손실;
	@Column(name="금융비용_금융자산평가손실_장기금융자산평가손실")
	private Double 금융비용_금융자산평가손실_장기금융자산평가손실;
	@Column(name="금융비용_금융자산평가손실_기타금융자산평가손실")
	private Double 금융비용_금융자산평가손실_기타금융자산평가손실;
	@Column(name="금융비용_파생상품손실")
	private Double 금융비용_파생상품손실;
	@Column(name="금융비용_금융자산손상차손")
	private Double 금융비용_금융자산손상차손;
	@Column(name="금융비용_지분법관련손실")
	private Double 금융비용_지분법관련손실;
	@Column(name="금융비용_기타")
	private Double 금융비용_기타;
	@Column(name="기타영업외손익")
	private Double 기타영업외손익;
	@Column(name="기타영업외손익_기타영업외수익")
	private Double 기타영업외손익_기타영업외수익;
	@Column(name="기타영업외손익_기타영업외수익_외환거래이익")
	private Double 기타영업외손익_기타영업외수익_외환거래이익;
	@Column(name="기타영업외손익_기타영업외수익_외환거래이익_외환차익")
	private Double 기타영업외손익_기타영업외수익_외환거래이익_외환차익;
	@Column(name="기타영업외손익_기타영업외수익_외환거래이익_외화환산이익")
	private Double 기타영업외손익_기타영업외수익_외환거래이익_외화환산이익;
	@Column(name="기타영업외손익_기타영업외수익_외환거래이익_기타외화거래이익")
	private Double 기타영업외손익_기타영업외수익_외환거래이익_기타외화거래이익;
	@Column(name="기타영업외손익_기타영업외수익_임대료")
	private Double 기타영업외손익_기타영업외수익_임대료;
	@Column(name="기타영업외손익_기타영업외수익_대손충당금환입")
	private Double 기타영업외손익_기타영업외수익_대손충당금환입;
	@Column(name="기타영업외손익_기타영업외수익_자산처분이익")
	private Double 기타영업외손익_기타영업외수익_자산처분이익;
	@Column(name="기타영업외손익_기타영업외수익_자산처분이익_투자부동산처분이익")
	private Double 기타영업외손익_기타영업외수익_자산처분이익_투자부동산처분이익;
	@Column(name="기타영업외손익_기타영업외수익_자산처분이익_투자자산처분이익")
	private Double 기타영업외손익_기타영업외수익_자산처분이익_투자자산처분이익;
	@Column(name="기타영업외손익_기타영업외수익_자산처분이익_유형리스자산처분이익")
	private Double 기타영업외손익_기타영업외수익_자산처분이익_유형리스자산처분이익;
	@Column(name="기타영업외손익_기타영업외수익_자산처분이익_기타")
	private Double 기타영업외손익_기타영업외수익_자산처분이익_기타;
	@Column(name="기타영업외손익_기타영업외수익_투자자산평가이익")
	private Double 기타영업외손익_기타영업외수익_투자자산평가이익;
	@Column(name="기타영업외손익_기타영업외수익_자산재평가이익")
	private Double 기타영업외손익_기타영업외수익_자산재평가이익;
	@Column(name="기타영업외손익_기타영업외수익_자산손상차손환입")
	private Double 기타영업외손익_기타영업외수익_자산손상차손환입;
	@Column(name="기타영업외손익_기타영업외수익_지분법관련이익")
	private Double 기타영업외손익_기타영업외수익_지분법관련이익;
	@Column(name="기타영업외손익_기타영업외수익_기타")
	private Double 기타영업외손익_기타영업외수익_기타;
	@Column(name="기타영업외손익_기타영업외비용")
	private Double 기타영업외손익_기타영업외비용;
	@Column(name="기타영업외손익_기타영업외비용_외환거래손실")
	private Double 기타영업외손익_기타영업외비용_외환거래손실;
	@Column(name="기타영업외손익_기타영업외비용_외환거래손실_외환차손")
	private Double 기타영업외손익_기타영업외비용_외환거래손실_외환차손;
	@Column(name="기타영업외손익_기타영업외비용_외환거래손실_외화환산손실")
	private Double 기타영업외손익_기타영업외비용_외환거래손실_외화환산손실;
	@Column(name="기타영업외손익_기타영업외비용_외환거래손실_기타외환거래손실")
	private Double 기타영업외손익_기타영업외비용_외환거래손실_기타외환거래손실;
	@Column(name="기타영업외손익_기타영업외비용_기타대손상각비")
	private Double 기타영업외손익_기타영업외비용_기타대손상각비;
	@Column(name="기타영업외손익_기타영업외비용_충당부채전입액")
	private Double 기타영업외손익_기타영업외비용_충당부채전입액;
	@Column(name="기타영업외손익_기타영업외비용_자산처분손실")
	private Double 기타영업외손익_기타영업외비용_자산처분손실;
	@Column(name="기타영업외손익_기타영업외비용_자산처분손실_매출채권처분손실")
	private Double 기타영업외손익_기타영업외비용_자산처분손실_매출채권처분손실;
	@Column(name="기타영업외손익_기타영업외비용_자산처분손실_투자부동산처분손실")
	private Double 기타영업외손익_기타영업외비용_자산처분손실_투자부동산처분손실;
	@Column(name="기타영업외손익_기타영업외비용_자산처분손실_투자자산처분손실")
	private Double 기타영업외손익_기타영업외비용_자산처분손실_투자자산처분손실;
	@Column(name="기타영업외손익_기타영업외비용_자산처분손실_유형리스자산처분손실")
	private Double 기타영업외손익_기타영업외비용_자산처분손실_유형리스자산처분손실;
	@Column(name="기타영업외손익_기타영업외비용_자산처분손실_기타")
	private Double 기타영업외손익_기타영업외비용_자산처분손실_기타;
	@Column(name="기타영업외손익_기타영업외비용_투자자산평가손실")
	private Double 기타영업외손익_기타영업외비용_투자자산평가손실;
	@Column(name="기타영업외손익_기타영업외비용_자산재평가손실")
	private Double 기타영업외손익_기타영업외비용_자산재평가손실;
	@Column(name="기타영업외손익_기타영업외비용_자산손상차손")
	private Double 기타영업외손익_기타영업외비용_자산손상차손;
	@Column(name="기타영업외손익_기타영업외비용_지분법관련손실")
	private Double 기타영업외손익_기타영업외비용_지분법관련손실;
	@Column(name="기타영업외손익_기타영업외비용_기타")
	private Double 기타영업외손익_기타영업외비용_기타;
	@Column(name="기타영업외손익_기타영업외비용_종속기업공동지배기업및관계기업관련손익")
	private Double 기타영업외손익_기타영업외비용_종속기업공동지배기업및관계기업관련손익;
	@Column(name="기타영업외손익_지분법관련손익")
	private Double 기타영업외손익_지분법관련손익;
	@Column(name="기타영업외손익_지분법관련손익_지분법이익")
	private Double 기타영업외손익_지분법관련손익_지분법이익;
	@Column(name="기타영업외손익_지분법관련손익_지분법손실")
	private Double 기타영업외손익_지분법관련손익_지분법손실;
	@Column(name="기타영업외손익_지분법관련손익_기타")
	private Double 기타영업외손익_지분법관련손익_기타;
	@Column(name="기타영업외손익_관계기업처분손익")
	private Double 기타영업외손익_관계기업처분손익;
	@Column(name="기타영업외손익_관계기업처분손익_관계기업처분이익")
	private Double 기타영업외손익_관계기업처분손익_관계기업처분이익;
	@Column(name="기타영업외손익_관계기업처분손익_관계기업처분손실")
	private Double 기타영업외손익_관계기업처분손익_관계기업처분손실;
	@Column(name="기타영업외손익_관계기업처분손익_기타")
	private Double 기타영업외손익_관계기업처분손익_기타;
	@Column(name="기타영업외손익_종속기업관련손익")
	private Double 기타영업외손익_종속기업관련손익;
	@Column(name="기타영업외손익_종속기업관련손익_종속기업처분이익")
	private Double 기타영업외손익_종속기업관련손익_종속기업처분이익;
	@Column(name="기타영업외손익_종속기업관련손익_종속기업처분손실")
	private Double 기타영업외손익_종속기업관련손익_종속기업처분손실;
	@Column(name="기타영업외손익_종속기업관련손익_기타")
	private Double 기타영업외손익_종속기업관련손익_기타;
	@Column(name="기타영업외손익_기타")
	private Double 기타영업외손익_기타;
	@Column(name="법인세비용차감전계속사업이익")
	private Double 법인세비용차감전계속사업이익;
	@Column(name="법인세비용")
	private Double 법인세비용;
	@Column(name="기타")
	private Double 기타;
	@Column(name="계속사업이익")
	private Double 계속사업이익;
	@Column(name="중단사업이익")
	private Double 중단사업이익;
	@Column(name="중단사업법인세효과")
	private Double 중단사업법인세효과;
	@Column(name="당기순이익")
	private Double 당기순이익;
	@Column(name="당기순이익_지배주주지분당기순이익")
	private Double 당기순이익_지배주주지분당기순이익;
	@Column(name="당기순이익_비지배주주지분당기순이익")
	private Double 당기순이익_비지배주주지분당기순이익;
	@Column(name="당기순이익_지배주주지분연결당기순이익")
	private Double 당기순이익_지배주주지분연결당기순이익;
	@Column(name="기타포괄이익")
	private Double 기타포괄이익;
	@Column(name="기타포괄이익_금융자산평가손익")
	private Double 기타포괄이익_금융자산평가손익;
	@Column(name="기타포괄이익_매도가능금융자산평가손익")
	private Double 기타포괄이익_매도가능금융자산평가손익;
	@Column(name="기타포괄이익_관계기업등기타포괄이익")
	private Double 기타포괄이익_관계기업등기타포괄이익;
	@Column(name="기타포괄이익_환산관련외환차이환율변동차이")
	private Double 기타포괄이익_환산관련외환차이환율변동차이;
	@Column(name="기타포괄이익_현금흐름위험회피적립금")
	private Double 기타포괄이익_현금흐름위험회피적립금;
	@Column(name="기타포괄이익_재평가손익")
	private Double 기타포괄이익_재평가손익;
	@Column(name="기타포괄이익_기타포괄이익관련법인세")
	private Double 기타포괄이익_기타포괄이익관련법인세;
	@Column(name="기타포괄이익_기타")
	private Double 기타포괄이익_기타;
	@Column(name="총포괄이익")
	private Double 총포괄이익;
	@Column(name="총포괄이익_지배주주지분총포괄이익")
	private Double 총포괄이익_지배주주지분총포괄이익;
	@Column(name="총포괄이익_비지배주주지분총포괄이익")
	private Double 총포괄이익_비지배주주지분총포괄이익;
	@Column(name="주당계속사업이익")
	private Double 주당계속사업이익;
	@Column(name="주당순이익")
	private Double 주당순이익;
	@Column(name="희석주당계속사업이익")
	private Double 희석주당계속사업이익;
	@Column(name="희석주당순이익")
	private Double 희석주당순이익;
	@Column(name="지배주주지분주당계속사업이익")
	private Double 지배주주지분주당계속사업이익;
	@Column(name="지배주주지분주당순이익")
	private Double 지배주주지분주당순이익;
	@Column(name="지배주주지분희석주당계속사업이익")
	private Double 지배주주지분희석주당계속사업이익;
	@Column(name="지배주주지분희석주당순이익")
	private Double 지배주주지분희석주당순이익;
	@Column(name="EBITDA")
	private Double EBITDA;
	@Column(name="EBIT")
	private Double EBIT;
	@Column(name="구영업외수익KGAAP")
	private Double 구영업외수익KGAAP;
	@Column(name="구영업외비용KGAAP")
	private Double 구영업외비용KGAAP;
	@Column(name="구경상이익년이전발생")
	private Double 구경상이익년이전발생;
	@Column(name="구특별이익년이전발생")
	private Double 구특별이익년이전발생;
	@Column(name="구특별손실년이전발생")
	private Double 구특별손실년이전발생;
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getYmDay() {
		return ymDay;
	}
	public void setYmDay(String ymDay) {
		this.ymDay = ymDay;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public Double get매출액수익() {
		return 매출액수익;
	}
	public void set매출액수익(Double 매출액수익) {
		this.매출액수익 = 매출액수익;
	}
	public Double get내수() {
		return 내수;
	}
	public void set내수(Double 내수) {
		this.내수 = 내수;
	}
	public Double get수출() {
		return 수출;
	}
	public void set수출(Double 수출) {
		this.수출 = 수출;
	}
	public Double get수출_제품매출액() {
		return 수출_제품매출액;
	}
	public void set수출_제품매출액(Double 수출_제품매출액) {
		this.수출_제품매출액 = 수출_제품매출액;
	}
	public Double get수출_상품매출액() {
		return 수출_상품매출액;
	}
	public void set수출_상품매출액(Double 수출_상품매출액) {
		this.수출_상품매출액 = 수출_상품매출액;
	}
	public Double get수출_제품상품매출액() {
		return 수출_제품상품매출액;
	}
	public void set수출_제품상품매출액(Double 수출_제품상품매출액) {
		this.수출_제품상품매출액 = 수출_제품상품매출액;
	}
	public Double get수출_공사수익() {
		return 수출_공사수익;
	}
	public void set수출_공사수익(Double 수출_공사수익) {
		this.수출_공사수익 = 수출_공사수익;
	}
	public Double get수출_분양수익() {
		return 수출_분양수익;
	}
	public void set수출_분양수익(Double 수출_분양수익) {
		this.수출_분양수익 = 수출_분양수익;
	}
	public Double get수출_용역수익() {
		return 수출_용역수익;
	}
	public void set수출_용역수익(Double 수출_용역수익) {
		this.수출_용역수익 = 수출_용역수익;
	}
	public Double get수출_지분법이익() {
		return 수출_지분법이익;
	}
	public void set수출_지분법이익(Double 수출_지분법이익) {
		this.수출_지분법이익 = 수출_지분법이익;
	}
	public Double get수출_금융수익() {
		return 수출_금융수익;
	}
	public void set수출_금융수익(Double 수출_금융수익) {
		this.수출_금융수익 = 수출_금융수익;
	}
	public Double get수출_배당수익() {
		return 수출_배당수익;
	}
	public void set수출_배당수익(Double 수출_배당수익) {
		this.수출_배당수익 = 수출_배당수익;
	}
	public Double get수출_이자수익() {
		return 수출_이자수익;
	}
	public void set수출_이자수익(Double 수출_이자수익) {
		this.수출_이자수익 = 수출_이자수익;
	}
	public Double get수출_기타() {
		return 수출_기타;
	}
	public void set수출_기타(Double 수출_기타) {
		this.수출_기타 = 수출_기타;
	}
	public Double get매출원가() {
		return 매출원가;
	}
	public void set매출원가(Double 매출원가) {
		this.매출원가 = 매출원가;
	}
	public Double get매출원가_제품매출원가() {
		return 매출원가_제품매출원가;
	}
	public void set매출원가_제품매출원가(Double 매출원가_제품매출원가) {
		this.매출원가_제품매출원가 = 매출원가_제품매출원가;
	}
	public Double get매출원가_상품매출원가() {
		return 매출원가_상품매출원가;
	}
	public void set매출원가_상품매출원가(Double 매출원가_상품매출원가) {
		this.매출원가_상품매출원가 = 매출원가_상품매출원가;
	}
	public Double get매출원가_제품상품매출원가() {
		return 매출원가_제품상품매출원가;
	}
	public void set매출원가_제품상품매출원가(Double 매출원가_제품상품매출원가) {
		this.매출원가_제품상품매출원가 = 매출원가_제품상품매출원가;
	}
	public Double get매출원가_건설계약원가() {
		return 매출원가_건설계약원가;
	}
	public void set매출원가_건설계약원가(Double 매출원가_건설계약원가) {
		this.매출원가_건설계약원가 = 매출원가_건설계약원가;
	}
	public Double get매출원가_분양원가() {
		return 매출원가_분양원가;
	}
	public void set매출원가_분양원가(Double 매출원가_분양원가) {
		this.매출원가_분양원가 = 매출원가_분양원가;
	}
	public Double get매출원가_용역원가() {
		return 매출원가_용역원가;
	}
	public void set매출원가_용역원가(Double 매출원가_용역원가) {
		this.매출원가_용역원가 = 매출원가_용역원가;
	}
	public Double get매출원가_지분법손실() {
		return 매출원가_지분법손실;
	}
	public void set매출원가_지분법손실(Double 매출원가_지분법손실) {
		this.매출원가_지분법손실 = 매출원가_지분법손실;
	}
	public Double get매출원가_금융비용() {
		return 매출원가_금융비용;
	}
	public void set매출원가_금융비용(Double 매출원가_금융비용) {
		this.매출원가_금융비용 = 매출원가_금융비용;
	}
	public Double get매출원가_이자비용() {
		return 매출원가_이자비용;
	}
	public void set매출원가_이자비용(Double 매출원가_이자비용) {
		this.매출원가_이자비용 = 매출원가_이자비용;
	}
	public Double get매출원가_기타() {
		return 매출원가_기타;
	}
	public void set매출원가_기타(Double 매출원가_기타) {
		this.매출원가_기타 = 매출원가_기타;
	}
	public Double get매출총이익() {
		return 매출총이익;
	}
	public void set매출총이익(Double 매출총이익) {
		this.매출총이익 = 매출총이익;
	}
	public Double get상각후원가로측정하는금융자산의제거로부터발생한이익() {
		return 상각후원가로측정하는금융자산의제거로부터발생한이익;
	}
	public void set상각후원가로측정하는금융자산의제거로부터발생한이익(Double 상각후원가로측정하는금융자산의제거로부터발생한이익) {
		this.상각후원가로측정하는금융자산의제거로부터발생한이익 = 상각후원가로측정하는금융자산의제거로부터발생한이익;
	}
	public Double get상각후원가로측정하는금융자산의제거로부터발생한손실() {
		return 상각후원가로측정하는금융자산의제거로부터발생한손실;
	}
	public void set상각후원가로측정하는금융자산의제거로부터발생한손실(Double 상각후원가로측정하는금융자산의제거로부터발생한손실) {
		this.상각후원가로측정하는금융자산의제거로부터발생한손실 = 상각후원가로측정하는금융자산의제거로부터발생한손실;
	}
	public Double get판매비와관리비() {
		return 판매비와관리비;
	}
	public void set판매비와관리비(Double 판매비와관리비) {
		this.판매비와관리비 = 판매비와관리비;
	}
	public Double get판매비와관리비_급여() {
		return 판매비와관리비_급여;
	}
	public void set판매비와관리비_급여(Double 판매비와관리비_급여) {
		this.판매비와관리비_급여 = 판매비와관리비_급여;
	}
	public Double get판매비와관리비_퇴직급여() {
		return 판매비와관리비_퇴직급여;
	}
	public void set판매비와관리비_퇴직급여(Double 판매비와관리비_퇴직급여) {
		this.판매비와관리비_퇴직급여 = 판매비와관리비_퇴직급여;
	}
	public Double get판매비와관리비_명예퇴직금() {
		return 판매비와관리비_명예퇴직금;
	}
	public void set판매비와관리비_명예퇴직금(Double 판매비와관리비_명예퇴직금) {
		this.판매비와관리비_명예퇴직금 = 판매비와관리비_명예퇴직금;
	}
	public Double get판매비와관리비_복리후생비() {
		return 판매비와관리비_복리후생비;
	}
	public void set판매비와관리비_복리후생비(Double 판매비와관리비_복리후생비) {
		this.판매비와관리비_복리후생비 = 판매비와관리비_복리후생비;
	}
	public Double get판매비와관리비_주식보상비() {
		return 판매비와관리비_주식보상비;
	}
	public void set판매비와관리비_주식보상비(Double 판매비와관리비_주식보상비) {
		this.판매비와관리비_주식보상비 = 판매비와관리비_주식보상비;
	}
	public Double get판매비와관리비_교육훈련비() {
		return 판매비와관리비_교육훈련비;
	}
	public void set판매비와관리비_교육훈련비(Double 판매비와관리비_교육훈련비) {
		this.판매비와관리비_교육훈련비 = 판매비와관리비_교육훈련비;
	}
	public Double get판매비와관리비_수도광열비() {
		return 판매비와관리비_수도광열비;
	}
	public void set판매비와관리비_수도광열비(Double 판매비와관리비_수도광열비) {
		this.판매비와관리비_수도광열비 = 판매비와관리비_수도광열비;
	}
	public Double get판매비와관리비_세금과공과() {
		return 판매비와관리비_세금과공과;
	}
	public void set판매비와관리비_세금과공과(Double 판매비와관리비_세금과공과) {
		this.판매비와관리비_세금과공과 = 판매비와관리비_세금과공과;
	}
	public Double get판매비와관리비_임차료() {
		return 판매비와관리비_임차료;
	}
	public void set판매비와관리비_임차료(Double 판매비와관리비_임차료) {
		this.판매비와관리비_임차료 = 판매비와관리비_임차료;
	}
	public Double get판매비와관리비_보험료() {
		return 판매비와관리비_보험료;
	}
	public void set판매비와관리비_보험료(Double 판매비와관리비_보험료) {
		this.판매비와관리비_보험료 = 판매비와관리비_보험료;
	}
	public Double get판매비와관리비_지급수수료() {
		return 판매비와관리비_지급수수료;
	}
	public void set판매비와관리비_지급수수료(Double 판매비와관리비_지급수수료) {
		this.판매비와관리비_지급수수료 = 판매비와관리비_지급수수료;
	}
	public Double get판매비와관리비_감가상각비() {
		return 판매비와관리비_감가상각비;
	}
	public void set판매비와관리비_감가상각비(Double 판매비와관리비_감가상각비) {
		this.판매비와관리비_감가상각비 = 판매비와관리비_감가상각비;
	}
	public Double get판매비와관리비_개발비상각() {
		return 판매비와관리비_개발비상각;
	}
	public void set판매비와관리비_개발비상각(Double 판매비와관리비_개발비상각) {
		this.판매비와관리비_개발비상각 = 판매비와관리비_개발비상각;
	}
	public Double get판매비와관리비_기타무형자산상각비() {
		return 판매비와관리비_기타무형자산상각비;
	}
	public void set판매비와관리비_기타무형자산상각비(Double 판매비와관리비_기타무형자산상각비) {
		this.판매비와관리비_기타무형자산상각비 = 판매비와관리비_기타무형자산상각비;
	}
	public Double get판매비와관리비_연구개발비() {
		return 판매비와관리비_연구개발비;
	}
	public void set판매비와관리비_연구개발비(Double 판매비와관리비_연구개발비) {
		this.판매비와관리비_연구개발비 = 판매비와관리비_연구개발비;
	}
	public Double get판매비와관리비_특허권등사용료() {
		return 판매비와관리비_특허권등사용료;
	}
	public void set판매비와관리비_특허권등사용료(Double 판매비와관리비_특허권등사용료) {
		this.판매비와관리비_특허권등사용료 = 판매비와관리비_특허권등사용료;
	}
	public Double get판매비와관리비_기타관리비() {
		return 판매비와관리비_기타관리비;
	}
	public void set판매비와관리비_기타관리비(Double 판매비와관리비_기타관리비) {
		this.판매비와관리비_기타관리비 = 판매비와관리비_기타관리비;
	}
	public Double get판매비와관리비_광고선전비() {
		return 판매비와관리비_광고선전비;
	}
	public void set판매비와관리비_광고선전비(Double 판매비와관리비_광고선전비) {
		this.판매비와관리비_광고선전비 = 판매비와관리비_광고선전비;
	}
	public Double get판매비와관리비_수출비용() {
		return 판매비와관리비_수출비용;
	}
	public void set판매비와관리비_수출비용(Double 판매비와관리비_수출비용) {
		this.판매비와관리비_수출비용 = 판매비와관리비_수출비용;
	}
	public Double get판매비와관리비_판매촉진비() {
		return 판매비와관리비_판매촉진비;
	}
	public void set판매비와관리비_판매촉진비(Double 판매비와관리비_판매촉진비) {
		this.판매비와관리비_판매촉진비 = 판매비와관리비_판매촉진비;
	}
	public Double get판매비와관리비_판매수수료() {
		return 판매비와관리비_판매수수료;
	}
	public void set판매비와관리비_판매수수료(Double 판매비와관리비_판매수수료) {
		this.판매비와관리비_판매수수료 = 판매비와관리비_판매수수료;
	}
	public Double get판매비와관리비_기타물류원가() {
		return 판매비와관리비_기타물류원가;
	}
	public void set판매비와관리비_기타물류원가(Double 판매비와관리비_기타물류원가) {
		this.판매비와관리비_기타물류원가 = 판매비와관리비_기타물류원가;
	}
	public Double get판매비와관리비_애프터서비스비() {
		return 판매비와관리비_애프터서비스비;
	}
	public void set판매비와관리비_애프터서비스비(Double 판매비와관리비_애프터서비스비) {
		this.판매비와관리비_애프터서비스비 = 판매비와관리비_애프터서비스비;
	}
	public Double get판매비와관리비_대손상각비() {
		return 판매비와관리비_대손상각비;
	}
	public void set판매비와관리비_대손상각비(Double 판매비와관리비_대손상각비) {
		this.판매비와관리비_대손상각비 = 판매비와관리비_대손상각비;
	}
	public Double get판매비와관리비_기타판매비() {
		return 판매비와관리비_기타판매비;
	}
	public void set판매비와관리비_기타판매비(Double 판매비와관리비_기타판매비) {
		this.판매비와관리비_기타판매비 = 판매비와관리비_기타판매비;
	}
	public Double get판매비와관리비_기타() {
		return 판매비와관리비_기타;
	}
	public void set판매비와관리비_기타(Double 판매비와관리비_기타) {
		this.판매비와관리비_기타 = 판매비와관리비_기타;
	}
	public Double get판매비와관리비_인건비및복리후생비() {
		return 판매비와관리비_인건비및복리후생비;
	}
	public void set판매비와관리비_인건비및복리후생비(Double 판매비와관리비_인건비및복리후생비) {
		this.판매비와관리비_인건비및복리후생비 = 판매비와관리비_인건비및복리후생비;
	}
	public Double get판매비와관리비_일반관리비() {
		return 판매비와관리비_일반관리비;
	}
	public void set판매비와관리비_일반관리비(Double 판매비와관리비_일반관리비) {
		this.판매비와관리비_일반관리비 = 판매비와관리비_일반관리비;
	}
	public Double get판매비와관리비_판매비() {
		return 판매비와관리비_판매비;
	}
	public void set판매비와관리비_판매비(Double 판매비와관리비_판매비) {
		this.판매비와관리비_판매비 = 판매비와관리비_판매비;
	}
	public Double get영업이익() {
		return 영업이익;
	}
	public void set영업이익(Double 영업이익) {
		this.영업이익 = 영업이익;
	}
	public Double get기타영업손익() {
		return 기타영업손익;
	}
	public void set기타영업손익(Double 기타영업손익) {
		this.기타영업손익 = 기타영업손익;
	}
	public Double get기타영업손익_기타영업수익() {
		return 기타영업손익_기타영업수익;
	}
	public void set기타영업손익_기타영업수익(Double 기타영업손익_기타영업수익) {
		this.기타영업손익_기타영업수익 = 기타영업손익_기타영업수익;
	}
	public Double get기타영업손익_기타영업수익_이자수익() {
		return 기타영업손익_기타영업수익_이자수익;
	}
	public void set기타영업손익_기타영업수익_이자수익(Double 기타영업손익_기타영업수익_이자수익) {
		this.기타영업손익_기타영업수익_이자수익 = 기타영업손익_기타영업수익_이자수익;
	}
	public Double get기타영업손익_기타영업수익_배당금수익() {
		return 기타영업손익_기타영업수익_배당금수익;
	}
	public void set기타영업손익_기타영업수익_배당금수익(Double 기타영업손익_기타영업수익_배당금수익) {
		this.기타영업손익_기타영업수익_배당금수익 = 기타영업손익_기타영업수익_배당금수익;
	}
	public Double get기타영업손익_기타영업수익_외환거래이익() {
		return 기타영업손익_기타영업수익_외환거래이익;
	}
	public void set기타영업손익_기타영업수익_외환거래이익(Double 기타영업손익_기타영업수익_외환거래이익) {
		this.기타영업손익_기타영업수익_외환거래이익 = 기타영업손익_기타영업수익_외환거래이익;
	}
	public Double get기타영업손익_기타영업수익_외환거래이익_외환차익() {
		return 기타영업손익_기타영업수익_외환거래이익_외환차익;
	}
	public void set기타영업손익_기타영업수익_외환거래이익_외환차익(Double 기타영업손익_기타영업수익_외환거래이익_외환차익) {
		this.기타영업손익_기타영업수익_외환거래이익_외환차익 = 기타영업손익_기타영업수익_외환거래이익_외환차익;
	}
	public Double get기타영업손익_기타영업수익_외환거래이익_외화환산이익() {
		return 기타영업손익_기타영업수익_외환거래이익_외화환산이익;
	}
	public void set기타영업손익_기타영업수익_외환거래이익_외화환산이익(Double 기타영업손익_기타영업수익_외환거래이익_외화환산이익) {
		this.기타영업손익_기타영업수익_외환거래이익_외화환산이익 = 기타영업손익_기타영업수익_외환거래이익_외화환산이익;
	}
	public Double get기타영업손익_기타영업수익_외환거래이익_기타외화거래이익() {
		return 기타영업손익_기타영업수익_외환거래이익_기타외화거래이익;
	}
	public void set기타영업손익_기타영업수익_외환거래이익_기타외화거래이익(
			Double 기타영업손익_기타영업수익_외환거래이익_기타외화거래이익) {
		this.기타영업손익_기타영업수익_외환거래이익_기타외화거래이익 = 기타영업손익_기타영업수익_외환거래이익_기타외화거래이익;
	}
	public Double get기타영업손익_기타영업수익_임대료() {
		return 기타영업손익_기타영업수익_임대료;
	}
	public void set기타영업손익_기타영업수익_임대료(Double 기타영업손익_기타영업수익_임대료) {
		this.기타영업손익_기타영업수익_임대료 = 기타영업손익_기타영업수익_임대료;
	}
	public Double get기타영업손익_기타영업수익_대손충당금환입() {
		return 기타영업손익_기타영업수익_대손충당금환입;
	}
	public void set기타영업손익_기타영업수익_대손충당금환입(Double 기타영업손익_기타영업수익_대손충당금환입) {
		this.기타영업손익_기타영업수익_대손충당금환입 = 기타영업손익_기타영업수익_대손충당금환입;
	}
	public Double get기타영업손익_기타영업수익_자산처분이익() {
		return 기타영업손익_기타영업수익_자산처분이익;
	}
	public void set기타영업손익_기타영업수익_자산처분이익(Double 기타영업손익_기타영업수익_자산처분이익) {
		this.기타영업손익_기타영업수익_자산처분이익 = 기타영업손익_기타영업수익_자산처분이익;
	}
	public Double get기타영업손익_기타영업수익_자산처분이익_투자부동산처분이익() {
		return 기타영업손익_기타영업수익_자산처분이익_투자부동산처분이익;
	}
	public void set기타영업손익_기타영업수익_자산처분이익_투자부동산처분이익(
			Double 기타영업손익_기타영업수익_자산처분이익_투자부동산처분이익) {
		this.기타영업손익_기타영업수익_자산처분이익_투자부동산처분이익 = 기타영업손익_기타영업수익_자산처분이익_투자부동산처분이익;
	}
	public Double get기타영업손익_기타영업수익_자산처분이익_투자자산처분이익() {
		return 기타영업손익_기타영업수익_자산처분이익_투자자산처분이익;
	}
	public void set기타영업손익_기타영업수익_자산처분이익_투자자산처분이익(
			Double 기타영업손익_기타영업수익_자산처분이익_투자자산처분이익) {
		this.기타영업손익_기타영업수익_자산처분이익_투자자산처분이익 = 기타영업손익_기타영업수익_자산처분이익_투자자산처분이익;
	}
	public Double get기타영업손익_기타영업수익_자산처분이익_유형리스자산처분이익() {
		return 기타영업손익_기타영업수익_자산처분이익_유형리스자산처분이익;
	}
	public void set기타영업손익_기타영업수익_자산처분이익_유형리스자산처분이익(
			Double 기타영업손익_기타영업수익_자산처분이익_유형리스자산처분이익) {
		this.기타영업손익_기타영업수익_자산처분이익_유형리스자산처분이익 = 기타영업손익_기타영업수익_자산처분이익_유형리스자산처분이익;
	}
	public Double get기타영업손익_기타영업수익_자산처분이익_기타() {
		return 기타영업손익_기타영업수익_자산처분이익_기타;
	}
	public void set기타영업손익_기타영업수익_자산처분이익_기타(Double 기타영업손익_기타영업수익_자산처분이익_기타) {
		this.기타영업손익_기타영업수익_자산처분이익_기타 = 기타영업손익_기타영업수익_자산처분이익_기타;
	}
	public Double get기타영업손익_기타영업수익_투자자산평가이익() {
		return 기타영업손익_기타영업수익_투자자산평가이익;
	}
	public void set기타영업손익_기타영업수익_투자자산평가이익(Double 기타영업손익_기타영업수익_투자자산평가이익) {
		this.기타영업손익_기타영업수익_투자자산평가이익 = 기타영업손익_기타영업수익_투자자산평가이익;
	}
	public Double get기타영업손익_기타영업수익_자산재평가이익() {
		return 기타영업손익_기타영업수익_자산재평가이익;
	}
	public void set기타영업손익_기타영업수익_자산재평가이익(Double 기타영업손익_기타영업수익_자산재평가이익) {
		this.기타영업손익_기타영업수익_자산재평가이익 = 기타영업손익_기타영업수익_자산재평가이익;
	}
	public Double get기타영업손익_기타영업수익_파생상품이익() {
		return 기타영업손익_기타영업수익_파생상품이익;
	}
	public void set기타영업손익_기타영업수익_파생상품이익(Double 기타영업손익_기타영업수익_파생상품이익) {
		this.기타영업손익_기타영업수익_파생상품이익 = 기타영업손익_기타영업수익_파생상품이익;
	}
	public Double get기타영업손익_기타영업수익_자산손상차손환입() {
		return 기타영업손익_기타영업수익_자산손상차손환입;
	}
	public void set기타영업손익_기타영업수익_자산손상차손환입(Double 기타영업손익_기타영업수익_자산손상차손환입) {
		this.기타영업손익_기타영업수익_자산손상차손환입 = 기타영업손익_기타영업수익_자산손상차손환입;
	}
	public Double get기타영업손익_기타영업수익_지분법관련이익() {
		return 기타영업손익_기타영업수익_지분법관련이익;
	}
	public void set기타영업손익_기타영업수익_지분법관련이익(Double 기타영업손익_기타영업수익_지분법관련이익) {
		this.기타영업손익_기타영업수익_지분법관련이익 = 기타영업손익_기타영업수익_지분법관련이익;
	}
	public Double get기타영업손익_기타영업수익_기타() {
		return 기타영업손익_기타영업수익_기타;
	}
	public void set기타영업손익_기타영업수익_기타(Double 기타영업손익_기타영업수익_기타) {
		this.기타영업손익_기타영업수익_기타 = 기타영업손익_기타영업수익_기타;
	}
	public Double get기타영업손익_기타영업비용() {
		return 기타영업손익_기타영업비용;
	}
	public void set기타영업손익_기타영업비용(Double 기타영업손익_기타영업비용) {
		this.기타영업손익_기타영업비용 = 기타영업손익_기타영업비용;
	}
	public Double get기타영업손익_기타영업비용_이자비용() {
		return 기타영업손익_기타영업비용_이자비용;
	}
	public void set기타영업손익_기타영업비용_이자비용(Double 기타영업손익_기타영업비용_이자비용) {
		this.기타영업손익_기타영업비용_이자비용 = 기타영업손익_기타영업비용_이자비용;
	}
	public Double get기타영업손익_기타영업비용_외환거래손실() {
		return 기타영업손익_기타영업비용_외환거래손실;
	}
	public void set기타영업손익_기타영업비용_외환거래손실(Double 기타영업손익_기타영업비용_외환거래손실) {
		this.기타영업손익_기타영업비용_외환거래손실 = 기타영업손익_기타영업비용_외환거래손실;
	}
	public Double get기타영업손익_기타영업비용_외환거래손실_외환차손() {
		return 기타영업손익_기타영업비용_외환거래손실_외환차손;
	}
	public void set기타영업손익_기타영업비용_외환거래손실_외환차손(Double 기타영업손익_기타영업비용_외환거래손실_외환차손) {
		this.기타영업손익_기타영업비용_외환거래손실_외환차손 = 기타영업손익_기타영업비용_외환거래손실_외환차손;
	}
	public Double get기타영업손익_기타영업비용_외환거래손실_외화환산손실() {
		return 기타영업손익_기타영업비용_외환거래손실_외화환산손실;
	}
	public void set기타영업손익_기타영업비용_외환거래손실_외화환산손실(Double 기타영업손익_기타영업비용_외환거래손실_외화환산손실) {
		this.기타영업손익_기타영업비용_외환거래손실_외화환산손실 = 기타영업손익_기타영업비용_외환거래손실_외화환산손실;
	}
	public Double get기타영업손익_기타영업비용_외환거래손실_기타외환거래손실() {
		return 기타영업손익_기타영업비용_외환거래손실_기타외환거래손실;
	}
	public void set기타영업손익_기타영업비용_외환거래손실_기타외환거래손실(
			Double 기타영업손익_기타영업비용_외환거래손실_기타외환거래손실) {
		this.기타영업손익_기타영업비용_외환거래손실_기타외환거래손실 = 기타영업손익_기타영업비용_외환거래손실_기타외환거래손실;
	}
	public Double get기타영업손익_기타영업비용_기타대손상각비() {
		return 기타영업손익_기타영업비용_기타대손상각비;
	}
	public void set기타영업손익_기타영업비용_기타대손상각비(Double 기타영업손익_기타영업비용_기타대손상각비) {
		this.기타영업손익_기타영업비용_기타대손상각비 = 기타영업손익_기타영업비용_기타대손상각비;
	}
	public Double get기타영업손익_기타영업비용_충당부채전입액() {
		return 기타영업손익_기타영업비용_충당부채전입액;
	}
	public void set기타영업손익_기타영업비용_충당부채전입액(Double 기타영업손익_기타영업비용_충당부채전입액) {
		this.기타영업손익_기타영업비용_충당부채전입액 = 기타영업손익_기타영업비용_충당부채전입액;
	}
	public Double get기타영업손익_기타영업비용_자산처분손실() {
		return 기타영업손익_기타영업비용_자산처분손실;
	}
	public void set기타영업손익_기타영업비용_자산처분손실(Double 기타영업손익_기타영업비용_자산처분손실) {
		this.기타영업손익_기타영업비용_자산처분손실 = 기타영업손익_기타영업비용_자산처분손실;
	}
	public Double get기타영업손익_기타영업비용_자산처분손실_투자부동산처분손실() {
		return 기타영업손익_기타영업비용_자산처분손실_투자부동산처분손실;
	}
	public void set기타영업손익_기타영업비용_자산처분손실_투자부동산처분손실(
			Double 기타영업손익_기타영업비용_자산처분손실_투자부동산처분손실) {
		this.기타영업손익_기타영업비용_자산처분손실_투자부동산처분손실 = 기타영업손익_기타영업비용_자산처분손실_투자부동산처분손실;
	}
	public Double get기타영업손익_기타영업비용_자산처분손실_투자자산처분손실() {
		return 기타영업손익_기타영업비용_자산처분손실_투자자산처분손실;
	}
	public void set기타영업손익_기타영업비용_자산처분손실_투자자산처분손실(
			Double 기타영업손익_기타영업비용_자산처분손실_투자자산처분손실) {
		this.기타영업손익_기타영업비용_자산처분손실_투자자산처분손실 = 기타영업손익_기타영업비용_자산처분손실_투자자산처분손실;
	}
	public Double get기타영업손익_기타영업비용_자산처분손실_유형리스자산처분손실() {
		return 기타영업손익_기타영업비용_자산처분손실_유형리스자산처분손실;
	}
	public void set기타영업손익_기타영업비용_자산처분손실_유형리스자산처분손실(
			Double 기타영업손익_기타영업비용_자산처분손실_유형리스자산처분손실) {
		this.기타영업손익_기타영업비용_자산처분손실_유형리스자산처분손실 = 기타영업손익_기타영업비용_자산처분손실_유형리스자산처분손실;
	}
	public Double get기타영업손익_기타영업비용_자산처분손실_기타() {
		return 기타영업손익_기타영업비용_자산처분손실_기타;
	}
	public void set기타영업손익_기타영업비용_자산처분손실_기타(Double 기타영업손익_기타영업비용_자산처분손실_기타) {
		this.기타영업손익_기타영업비용_자산처분손실_기타 = 기타영업손익_기타영업비용_자산처분손실_기타;
	}
	public Double get기타영업손익_기타영업비용_투자자산평가손실() {
		return 기타영업손익_기타영업비용_투자자산평가손실;
	}
	public void set기타영업손익_기타영업비용_투자자산평가손실(Double 기타영업손익_기타영업비용_투자자산평가손실) {
		this.기타영업손익_기타영업비용_투자자산평가손실 = 기타영업손익_기타영업비용_투자자산평가손실;
	}
	public Double get기타영업손익_기타영업비용_자산재평가손실() {
		return 기타영업손익_기타영업비용_자산재평가손실;
	}
	public void set기타영업손익_기타영업비용_자산재평가손실(Double 기타영업손익_기타영업비용_자산재평가손실) {
		this.기타영업손익_기타영업비용_자산재평가손실 = 기타영업손익_기타영업비용_자산재평가손실;
	}
	public Double get기타영업손익_기타영업비용_파생상품손실() {
		return 기타영업손익_기타영업비용_파생상품손실;
	}
	public void set기타영업손익_기타영업비용_파생상품손실(Double 기타영업손익_기타영업비용_파생상품손실) {
		this.기타영업손익_기타영업비용_파생상품손실 = 기타영업손익_기타영업비용_파생상품손실;
	}
	public Double get기타영업손익_기타영업비용_자산손상차손() {
		return 기타영업손익_기타영업비용_자산손상차손;
	}
	public void set기타영업손익_기타영업비용_자산손상차손(Double 기타영업손익_기타영업비용_자산손상차손) {
		this.기타영업손익_기타영업비용_자산손상차손 = 기타영업손익_기타영업비용_자산손상차손;
	}
	public Double get기타영업손익_기타영업비용_지분법관련손실() {
		return 기타영업손익_기타영업비용_지분법관련손실;
	}
	public void set기타영업손익_기타영업비용_지분법관련손실(Double 기타영업손익_기타영업비용_지분법관련손실) {
		this.기타영업손익_기타영업비용_지분법관련손실 = 기타영업손익_기타영업비용_지분법관련손실;
	}
	public Double get기타영업손익_기타영업비용_기타() {
		return 기타영업손익_기타영업비용_기타;
	}
	public void set기타영업손익_기타영업비용_기타(Double 기타영업손익_기타영업비용_기타) {
		this.기타영업손익_기타영업비용_기타 = 기타영업손익_기타영업비용_기타;
	}
	public Double get지분법손익() {
		return 지분법손익;
	}
	public void set지분법손익(Double 지분법손익) {
		this.지분법손익 = 지분법손익;
	}
	public Double get지분법손익_지분법이익() {
		return 지분법손익_지분법이익;
	}
	public void set지분법손익_지분법이익(Double 지분법손익_지분법이익) {
		this.지분법손익_지분법이익 = 지분법손익_지분법이익;
	}
	public Double get지분법손익_지분법손실() {
		return 지분법손익_지분법손실;
	}
	public void set지분법손익_지분법손실(Double 지분법손익_지분법손실) {
		this.지분법손익_지분법손실 = 지분법손익_지분법손실;
	}
	public Double get지분법손익_기타() {
		return 지분법손익_기타;
	}
	public void set지분법손익_기타(Double 지분법손익_기타) {
		this.지분법손익_기타 = 지분법손익_기타;
	}
	public Double get지분법손익_구KIFRS영업이익() {
		return 지분법손익_구KIFRS영업이익;
	}
	public void set지분법손익_구KIFRS영업이익(Double 지분법손익_구kifrs영업이익) {
		지분법손익_구KIFRS영업이익 = 지분법손익_구kifrs영업이익;
	}
	public Double get금융수익() {
		return 금융수익;
	}
	public void set금융수익(Double 금융수익) {
		this.금융수익 = 금융수익;
	}
	public Double get금융수익_이자수익() {
		return 금융수익_이자수익;
	}
	public void set금융수익_이자수익(Double 금융수익_이자수익) {
		this.금융수익_이자수익 = 금융수익_이자수익;
	}
	public Double get금융수익_배당금수익() {
		return 금융수익_배당금수익;
	}
	public void set금융수익_배당금수익(Double 금융수익_배당금수익) {
		this.금융수익_배당금수익 = 금융수익_배당금수익;
	}
	public Double get금융수익_외환거래이익() {
		return 금융수익_외환거래이익;
	}
	public void set금융수익_외환거래이익(Double 금융수익_외환거래이익) {
		this.금융수익_외환거래이익 = 금융수익_외환거래이익;
	}
	public Double get금융수익_외환거래이익_외환차익() {
		return 금융수익_외환거래이익_외환차익;
	}
	public void set금융수익_외환거래이익_외환차익(Double 금융수익_외환거래이익_외환차익) {
		this.금융수익_외환거래이익_외환차익 = 금융수익_외환거래이익_외환차익;
	}
	public Double get금융수익_외환거래이익_외화환산이익() {
		return 금융수익_외환거래이익_외화환산이익;
	}
	public void set금융수익_외환거래이익_외화환산이익(Double 금융수익_외환거래이익_외화환산이익) {
		this.금융수익_외환거래이익_외화환산이익 = 금융수익_외환거래이익_외화환산이익;
	}
	public Double get금융수익_외환거래이익_기타외화거래이익() {
		return 금융수익_외환거래이익_기타외화거래이익;
	}
	public void set금융수익_외환거래이익_기타외화거래이익(Double 금융수익_외환거래이익_기타외화거래이익) {
		this.금융수익_외환거래이익_기타외화거래이익 = 금융수익_외환거래이익_기타외화거래이익;
	}
	public Double get금융수익_대손충당금환입액() {
		return 금융수익_대손충당금환입액;
	}
	public void set금융수익_대손충당금환입액(Double 금융수익_대손충당금환입액) {
		this.금융수익_대손충당금환입액 = 금융수익_대손충당금환입액;
	}
	public Double get금융수익_금융자산처분이익() {
		return 금융수익_금융자산처분이익;
	}
	public void set금융수익_금융자산처분이익(Double 금융수익_금융자산처분이익) {
		this.금융수익_금융자산처분이익 = 금융수익_금융자산처분이익;
	}
	public Double get금융수익_금융자산처분이익_단기금융자산처분이익() {
		return 금융수익_금융자산처분이익_단기금융자산처분이익;
	}
	public void set금융수익_금융자산처분이익_단기금융자산처분이익(Double 금융수익_금융자산처분이익_단기금융자산처분이익) {
		this.금융수익_금융자산처분이익_단기금융자산처분이익 = 금융수익_금융자산처분이익_단기금융자산처분이익;
	}
	public Double get금융수익_금융자산처분이익_장기금융자산처분이익() {
		return 금융수익_금융자산처분이익_장기금융자산처분이익;
	}
	public void set금융수익_금융자산처분이익_장기금융자산처분이익(Double 금융수익_금융자산처분이익_장기금융자산처분이익) {
		this.금융수익_금융자산처분이익_장기금융자산처분이익 = 금융수익_금융자산처분이익_장기금융자산처분이익;
	}
	public Double get금융수익_금융자산처분이익_기타금융자산처분이익() {
		return 금융수익_금융자산처분이익_기타금융자산처분이익;
	}
	public void set금융수익_금융자산처분이익_기타금융자산처분이익(Double 금융수익_금융자산처분이익_기타금융자산처분이익) {
		this.금융수익_금융자산처분이익_기타금융자산처분이익 = 금융수익_금융자산처분이익_기타금융자산처분이익;
	}
	public Double get금융수익_금융자산평가이익() {
		return 금융수익_금융자산평가이익;
	}
	public void set금융수익_금융자산평가이익(Double 금융수익_금융자산평가이익) {
		this.금융수익_금융자산평가이익 = 금융수익_금융자산평가이익;
	}
	public Double get금융수익_금융자산평가이익_단기금융자산평가이익() {
		return 금융수익_금융자산평가이익_단기금융자산평가이익;
	}
	public void set금융수익_금융자산평가이익_단기금융자산평가이익(Double 금융수익_금융자산평가이익_단기금융자산평가이익) {
		this.금융수익_금융자산평가이익_단기금융자산평가이익 = 금융수익_금융자산평가이익_단기금융자산평가이익;
	}
	public Double get금융수익_금융자산평가이익_장기금융자산평가이익() {
		return 금융수익_금융자산평가이익_장기금융자산평가이익;
	}
	public void set금융수익_금융자산평가이익_장기금융자산평가이익(Double 금융수익_금융자산평가이익_장기금융자산평가이익) {
		this.금융수익_금융자산평가이익_장기금융자산평가이익 = 금융수익_금융자산평가이익_장기금융자산평가이익;
	}
	public Double get금융수익_금융자산평가이익_기타금융자산평가이익() {
		return 금융수익_금융자산평가이익_기타금융자산평가이익;
	}
	public void set금융수익_금융자산평가이익_기타금융자산평가이익(Double 금융수익_금융자산평가이익_기타금융자산평가이익) {
		this.금융수익_금융자산평가이익_기타금융자산평가이익 = 금융수익_금융자산평가이익_기타금융자산평가이익;
	}
	public Double get금융수익_파생상품이익() {
		return 금융수익_파생상품이익;
	}
	public void set금융수익_파생상품이익(Double 금융수익_파생상품이익) {
		this.금융수익_파생상품이익 = 금융수익_파생상품이익;
	}
	public Double get금융수익_금융자산손상차손환입() {
		return 금융수익_금융자산손상차손환입;
	}
	public void set금융수익_금융자산손상차손환입(Double 금융수익_금융자산손상차손환입) {
		this.금융수익_금융자산손상차손환입 = 금융수익_금융자산손상차손환입;
	}
	public Double get금융수익_지분법관련이익() {
		return 금융수익_지분법관련이익;
	}
	public void set금융수익_지분법관련이익(Double 금융수익_지분법관련이익) {
		this.금융수익_지분법관련이익 = 금융수익_지분법관련이익;
	}
	public Double get금융수익_기타() {
		return 금융수익_기타;
	}
	public void set금융수익_기타(Double 금융수익_기타) {
		this.금융수익_기타 = 금융수익_기타;
	}
	public Double get금융비용() {
		return 금융비용;
	}
	public void set금융비용(Double 금융비용) {
		this.금융비용 = 금융비용;
	}
	public Double get금융비용_이자비용() {
		return 금융비용_이자비용;
	}
	public void set금융비용_이자비용(Double 금융비용_이자비용) {
		this.금융비용_이자비용 = 금융비용_이자비용;
	}
	public Double get금융비용_외환거래손실() {
		return 금융비용_외환거래손실;
	}
	public void set금융비용_외환거래손실(Double 금융비용_외환거래손실) {
		this.금융비용_외환거래손실 = 금융비용_외환거래손실;
	}
	public Double get금융비용_외환거래손실_외환차손() {
		return 금융비용_외환거래손실_외환차손;
	}
	public void set금융비용_외환거래손실_외환차손(Double 금융비용_외환거래손실_외환차손) {
		this.금융비용_외환거래손실_외환차손 = 금융비용_외환거래손실_외환차손;
	}
	public Double get금융비용_외환거래손실_외화환산손실() {
		return 금융비용_외환거래손실_외화환산손실;
	}
	public void set금융비용_외환거래손실_외화환산손실(Double 금융비용_외환거래손실_외화환산손실) {
		this.금융비용_외환거래손실_외화환산손실 = 금융비용_외환거래손실_외화환산손실;
	}
	public Double get금융비용_외환거래손실_기타외환거래손실() {
		return 금융비용_외환거래손실_기타외환거래손실;
	}
	public void set금융비용_외환거래손실_기타외환거래손실(Double 금융비용_외환거래손실_기타외환거래손실) {
		this.금융비용_외환거래손실_기타외환거래손실 = 금융비용_외환거래손실_기타외환거래손실;
	}
	public Double get금융비용_대손상각비() {
		return 금융비용_대손상각비;
	}
	public void set금융비용_대손상각비(Double 금융비용_대손상각비) {
		this.금융비용_대손상각비 = 금융비용_대손상각비;
	}
	public Double get금융비용_금융자산처분손실() {
		return 금융비용_금융자산처분손실;
	}
	public void set금융비용_금융자산처분손실(Double 금융비용_금융자산처분손실) {
		this.금융비용_금융자산처분손실 = 금융비용_금융자산처분손실;
	}
	public Double get금융비용_금융자산처분손실_단기금융자산처분손실() {
		return 금융비용_금융자산처분손실_단기금융자산처분손실;
	}
	public void set금융비용_금융자산처분손실_단기금융자산처분손실(Double 금융비용_금융자산처분손실_단기금융자산처분손실) {
		this.금융비용_금융자산처분손실_단기금융자산처분손실 = 금융비용_금융자산처분손실_단기금융자산처분손실;
	}
	public Double get금융비용_금융자산처분손실_장기금융자산처분손실() {
		return 금융비용_금융자산처분손실_장기금융자산처분손실;
	}
	public void set금융비용_금융자산처분손실_장기금융자산처분손실(Double 금융비용_금융자산처분손실_장기금융자산처분손실) {
		this.금융비용_금융자산처분손실_장기금융자산처분손실 = 금융비용_금융자산처분손실_장기금융자산처분손실;
	}
	public Double get금융비용_금융자산처분손실_기타금융자산처분손실() {
		return 금융비용_금융자산처분손실_기타금융자산처분손실;
	}
	public void set금융비용_금융자산처분손실_기타금융자산처분손실(Double 금융비용_금융자산처분손실_기타금융자산처분손실) {
		this.금융비용_금융자산처분손실_기타금융자산처분손실 = 금융비용_금융자산처분손실_기타금융자산처분손실;
	}
	public Double get금융비용_금융자산평가손실() {
		return 금융비용_금융자산평가손실;
	}
	public void set금융비용_금융자산평가손실(Double 금융비용_금융자산평가손실) {
		this.금융비용_금융자산평가손실 = 금융비용_금융자산평가손실;
	}
	public Double get금융비용_금융자산평가손실_단기금융자산평가손실() {
		return 금융비용_금융자산평가손실_단기금융자산평가손실;
	}
	public void set금융비용_금융자산평가손실_단기금융자산평가손실(Double 금융비용_금융자산평가손실_단기금융자산평가손실) {
		this.금융비용_금융자산평가손실_단기금융자산평가손실 = 금융비용_금융자산평가손실_단기금융자산평가손실;
	}
	public Double get금융비용_금융자산평가손실_장기금융자산평가손실() {
		return 금융비용_금융자산평가손실_장기금융자산평가손실;
	}
	public void set금융비용_금융자산평가손실_장기금융자산평가손실(Double 금융비용_금융자산평가손실_장기금융자산평가손실) {
		this.금융비용_금융자산평가손실_장기금융자산평가손실 = 금융비용_금융자산평가손실_장기금융자산평가손실;
	}
	public Double get금융비용_금융자산평가손실_기타금융자산평가손실() {
		return 금융비용_금융자산평가손실_기타금융자산평가손실;
	}
	public void set금융비용_금융자산평가손실_기타금융자산평가손실(Double 금융비용_금융자산평가손실_기타금융자산평가손실) {
		this.금융비용_금융자산평가손실_기타금융자산평가손실 = 금융비용_금융자산평가손실_기타금융자산평가손실;
	}
	public Double get금융비용_파생상품손실() {
		return 금융비용_파생상품손실;
	}
	public void set금융비용_파생상품손실(Double 금융비용_파생상품손실) {
		this.금융비용_파생상품손실 = 금융비용_파생상품손실;
	}
	public Double get금융비용_금융자산손상차손() {
		return 금융비용_금융자산손상차손;
	}
	public void set금융비용_금융자산손상차손(Double 금융비용_금융자산손상차손) {
		this.금융비용_금융자산손상차손 = 금융비용_금융자산손상차손;
	}
	public Double get금융비용_지분법관련손실() {
		return 금융비용_지분법관련손실;
	}
	public void set금융비용_지분법관련손실(Double 금융비용_지분법관련손실) {
		this.금융비용_지분법관련손실 = 금융비용_지분법관련손실;
	}
	public Double get금융비용_기타() {
		return 금융비용_기타;
	}
	public void set금융비용_기타(Double 금융비용_기타) {
		this.금융비용_기타 = 금융비용_기타;
	}
	public Double get기타영업외손익() {
		return 기타영업외손익;
	}
	public void set기타영업외손익(Double 기타영업외손익) {
		this.기타영업외손익 = 기타영업외손익;
	}
	public Double get기타영업외손익_기타영업외수익() {
		return 기타영업외손익_기타영업외수익;
	}
	public void set기타영업외손익_기타영업외수익(Double 기타영업외손익_기타영업외수익) {
		this.기타영업외손익_기타영업외수익 = 기타영업외손익_기타영업외수익;
	}
	public Double get기타영업외손익_기타영업외수익_외환거래이익() {
		return 기타영업외손익_기타영업외수익_외환거래이익;
	}
	public void set기타영업외손익_기타영업외수익_외환거래이익(Double 기타영업외손익_기타영업외수익_외환거래이익) {
		this.기타영업외손익_기타영업외수익_외환거래이익 = 기타영업외손익_기타영업외수익_외환거래이익;
	}
	public Double get기타영업외손익_기타영업외수익_외환거래이익_외환차익() {
		return 기타영업외손익_기타영업외수익_외환거래이익_외환차익;
	}
	public void set기타영업외손익_기타영업외수익_외환거래이익_외환차익(Double 기타영업외손익_기타영업외수익_외환거래이익_외환차익) {
		this.기타영업외손익_기타영업외수익_외환거래이익_외환차익 = 기타영업외손익_기타영업외수익_외환거래이익_외환차익;
	}
	public Double get기타영업외손익_기타영업외수익_외환거래이익_외화환산이익() {
		return 기타영업외손익_기타영업외수익_외환거래이익_외화환산이익;
	}
	public void set기타영업외손익_기타영업외수익_외환거래이익_외화환산이익(
			Double 기타영업외손익_기타영업외수익_외환거래이익_외화환산이익) {
		this.기타영업외손익_기타영업외수익_외환거래이익_외화환산이익 = 기타영업외손익_기타영업외수익_외환거래이익_외화환산이익;
	}
	public Double get기타영업외손익_기타영업외수익_외환거래이익_기타외화거래이익() {
		return 기타영업외손익_기타영업외수익_외환거래이익_기타외화거래이익;
	}
	public void set기타영업외손익_기타영업외수익_외환거래이익_기타외화거래이익(
			Double 기타영업외손익_기타영업외수익_외환거래이익_기타외화거래이익) {
		this.기타영업외손익_기타영업외수익_외환거래이익_기타외화거래이익 = 기타영업외손익_기타영업외수익_외환거래이익_기타외화거래이익;
	}
	public Double get기타영업외손익_기타영업외수익_임대료() {
		return 기타영업외손익_기타영업외수익_임대료;
	}
	public void set기타영업외손익_기타영업외수익_임대료(Double 기타영업외손익_기타영업외수익_임대료) {
		this.기타영업외손익_기타영업외수익_임대료 = 기타영업외손익_기타영업외수익_임대료;
	}
	public Double get기타영업외손익_기타영업외수익_대손충당금환입() {
		return 기타영업외손익_기타영업외수익_대손충당금환입;
	}
	public void set기타영업외손익_기타영업외수익_대손충당금환입(Double 기타영업외손익_기타영업외수익_대손충당금환입) {
		this.기타영업외손익_기타영업외수익_대손충당금환입 = 기타영업외손익_기타영업외수익_대손충당금환입;
	}
	public Double get기타영업외손익_기타영업외수익_자산처분이익() {
		return 기타영업외손익_기타영업외수익_자산처분이익;
	}
	public void set기타영업외손익_기타영업외수익_자산처분이익(Double 기타영업외손익_기타영업외수익_자산처분이익) {
		this.기타영업외손익_기타영업외수익_자산처분이익 = 기타영업외손익_기타영업외수익_자산처분이익;
	}
	public Double get기타영업외손익_기타영업외수익_자산처분이익_투자부동산처분이익() {
		return 기타영업외손익_기타영업외수익_자산처분이익_투자부동산처분이익;
	}
	public void set기타영업외손익_기타영업외수익_자산처분이익_투자부동산처분이익(
			Double 기타영업외손익_기타영업외수익_자산처분이익_투자부동산처분이익) {
		this.기타영업외손익_기타영업외수익_자산처분이익_투자부동산처분이익 = 기타영업외손익_기타영업외수익_자산처분이익_투자부동산처분이익;
	}
	public Double get기타영업외손익_기타영업외수익_자산처분이익_투자자산처분이익() {
		return 기타영업외손익_기타영업외수익_자산처분이익_투자자산처분이익;
	}
	public void set기타영업외손익_기타영업외수익_자산처분이익_투자자산처분이익(
			Double 기타영업외손익_기타영업외수익_자산처분이익_투자자산처분이익) {
		this.기타영업외손익_기타영업외수익_자산처분이익_투자자산처분이익 = 기타영업외손익_기타영업외수익_자산처분이익_투자자산처분이익;
	}
	public Double get기타영업외손익_기타영업외수익_자산처분이익_유형리스자산처분이익() {
		return 기타영업외손익_기타영업외수익_자산처분이익_유형리스자산처분이익;
	}
	public void set기타영업외손익_기타영업외수익_자산처분이익_유형리스자산처분이익(
			Double 기타영업외손익_기타영업외수익_자산처분이익_유형리스자산처분이익) {
		this.기타영업외손익_기타영업외수익_자산처분이익_유형리스자산처분이익 = 기타영업외손익_기타영업외수익_자산처분이익_유형리스자산처분이익;
	}
	public Double get기타영업외손익_기타영업외수익_자산처분이익_기타() {
		return 기타영업외손익_기타영업외수익_자산처분이익_기타;
	}
	public void set기타영업외손익_기타영업외수익_자산처분이익_기타(Double 기타영업외손익_기타영업외수익_자산처분이익_기타) {
		this.기타영업외손익_기타영업외수익_자산처분이익_기타 = 기타영업외손익_기타영업외수익_자산처분이익_기타;
	}
	public Double get기타영업외손익_기타영업외수익_투자자산평가이익() {
		return 기타영업외손익_기타영업외수익_투자자산평가이익;
	}
	public void set기타영업외손익_기타영업외수익_투자자산평가이익(Double 기타영업외손익_기타영업외수익_투자자산평가이익) {
		this.기타영업외손익_기타영업외수익_투자자산평가이익 = 기타영업외손익_기타영업외수익_투자자산평가이익;
	}
	public Double get기타영업외손익_기타영업외수익_자산재평가이익() {
		return 기타영업외손익_기타영업외수익_자산재평가이익;
	}
	public void set기타영업외손익_기타영업외수익_자산재평가이익(Double 기타영업외손익_기타영업외수익_자산재평가이익) {
		this.기타영업외손익_기타영업외수익_자산재평가이익 = 기타영업외손익_기타영업외수익_자산재평가이익;
	}
	public Double get기타영업외손익_기타영업외수익_자산손상차손환입() {
		return 기타영업외손익_기타영업외수익_자산손상차손환입;
	}
	public void set기타영업외손익_기타영업외수익_자산손상차손환입(Double 기타영업외손익_기타영업외수익_자산손상차손환입) {
		this.기타영업외손익_기타영업외수익_자산손상차손환입 = 기타영업외손익_기타영업외수익_자산손상차손환입;
	}
	public Double get기타영업외손익_기타영업외수익_지분법관련이익() {
		return 기타영업외손익_기타영업외수익_지분법관련이익;
	}
	public void set기타영업외손익_기타영업외수익_지분법관련이익(Double 기타영업외손익_기타영업외수익_지분법관련이익) {
		this.기타영업외손익_기타영업외수익_지분법관련이익 = 기타영업외손익_기타영업외수익_지분법관련이익;
	}
	public Double get기타영업외손익_기타영업외수익_기타() {
		return 기타영업외손익_기타영업외수익_기타;
	}
	public void set기타영업외손익_기타영업외수익_기타(Double 기타영업외손익_기타영업외수익_기타) {
		this.기타영업외손익_기타영업외수익_기타 = 기타영업외손익_기타영업외수익_기타;
	}
	public Double get기타영업외손익_기타영업외비용() {
		return 기타영업외손익_기타영업외비용;
	}
	public void set기타영업외손익_기타영업외비용(Double 기타영업외손익_기타영업외비용) {
		this.기타영업외손익_기타영업외비용 = 기타영업외손익_기타영업외비용;
	}
	public Double get기타영업외손익_기타영업외비용_외환거래손실() {
		return 기타영업외손익_기타영업외비용_외환거래손실;
	}
	public void set기타영업외손익_기타영업외비용_외환거래손실(Double 기타영업외손익_기타영업외비용_외환거래손실) {
		this.기타영업외손익_기타영업외비용_외환거래손실 = 기타영업외손익_기타영업외비용_외환거래손실;
	}
	public Double get기타영업외손익_기타영업외비용_외환거래손실_외환차손() {
		return 기타영업외손익_기타영업외비용_외환거래손실_외환차손;
	}
	public void set기타영업외손익_기타영업외비용_외환거래손실_외환차손(Double 기타영업외손익_기타영업외비용_외환거래손실_외환차손) {
		this.기타영업외손익_기타영업외비용_외환거래손실_외환차손 = 기타영업외손익_기타영업외비용_외환거래손실_외환차손;
	}
	public Double get기타영업외손익_기타영업외비용_외환거래손실_외화환산손실() {
		return 기타영업외손익_기타영업외비용_외환거래손실_외화환산손실;
	}
	public void set기타영업외손익_기타영업외비용_외환거래손실_외화환산손실(
			Double 기타영업외손익_기타영업외비용_외환거래손실_외화환산손실) {
		this.기타영업외손익_기타영업외비용_외환거래손실_외화환산손실 = 기타영업외손익_기타영업외비용_외환거래손실_외화환산손실;
	}
	public Double get기타영업외손익_기타영업외비용_외환거래손실_기타외환거래손실() {
		return 기타영업외손익_기타영업외비용_외환거래손실_기타외환거래손실;
	}
	public void set기타영업외손익_기타영업외비용_외환거래손실_기타외환거래손실(
			Double 기타영업외손익_기타영업외비용_외환거래손실_기타외환거래손실) {
		this.기타영업외손익_기타영업외비용_외환거래손실_기타외환거래손실 = 기타영업외손익_기타영업외비용_외환거래손실_기타외환거래손실;
	}
	public Double get기타영업외손익_기타영업외비용_기타대손상각비() {
		return 기타영업외손익_기타영업외비용_기타대손상각비;
	}
	public void set기타영업외손익_기타영업외비용_기타대손상각비(Double 기타영업외손익_기타영업외비용_기타대손상각비) {
		this.기타영업외손익_기타영업외비용_기타대손상각비 = 기타영업외손익_기타영업외비용_기타대손상각비;
	}
	public Double get기타영업외손익_기타영업외비용_충당부채전입액() {
		return 기타영업외손익_기타영업외비용_충당부채전입액;
	}
	public void set기타영업외손익_기타영업외비용_충당부채전입액(Double 기타영업외손익_기타영업외비용_충당부채전입액) {
		this.기타영업외손익_기타영업외비용_충당부채전입액 = 기타영업외손익_기타영업외비용_충당부채전입액;
	}
	public Double get기타영업외손익_기타영업외비용_자산처분손실() {
		return 기타영업외손익_기타영업외비용_자산처분손실;
	}
	public void set기타영업외손익_기타영업외비용_자산처분손실(Double 기타영업외손익_기타영업외비용_자산처분손실) {
		this.기타영업외손익_기타영업외비용_자산처분손실 = 기타영업외손익_기타영업외비용_자산처분손실;
	}
	public Double get기타영업외손익_기타영업외비용_자산처분손실_매출채권처분손실() {
		return 기타영업외손익_기타영업외비용_자산처분손실_매출채권처분손실;
	}
	public void set기타영업외손익_기타영업외비용_자산처분손실_매출채권처분손실(
			Double 기타영업외손익_기타영업외비용_자산처분손실_매출채권처분손실) {
		this.기타영업외손익_기타영업외비용_자산처분손실_매출채권처분손실 = 기타영업외손익_기타영업외비용_자산처분손실_매출채권처분손실;
	}
	public Double get기타영업외손익_기타영업외비용_자산처분손실_투자부동산처분손실() {
		return 기타영업외손익_기타영업외비용_자산처분손실_투자부동산처분손실;
	}
	public void set기타영업외손익_기타영업외비용_자산처분손실_투자부동산처분손실(
			Double 기타영업외손익_기타영업외비용_자산처분손실_투자부동산처분손실) {
		this.기타영업외손익_기타영업외비용_자산처분손실_투자부동산처분손실 = 기타영업외손익_기타영업외비용_자산처분손실_투자부동산처분손실;
	}
	public Double get기타영업외손익_기타영업외비용_자산처분손실_투자자산처분손실() {
		return 기타영업외손익_기타영업외비용_자산처분손실_투자자산처분손실;
	}
	public void set기타영업외손익_기타영업외비용_자산처분손실_투자자산처분손실(
			Double 기타영업외손익_기타영업외비용_자산처분손실_투자자산처분손실) {
		this.기타영업외손익_기타영업외비용_자산처분손실_투자자산처분손실 = 기타영업외손익_기타영업외비용_자산처분손실_투자자산처분손실;
	}
	public Double get기타영업외손익_기타영업외비용_자산처분손실_유형리스자산처분손실() {
		return 기타영업외손익_기타영업외비용_자산처분손실_유형리스자산처분손실;
	}
	public void set기타영업외손익_기타영업외비용_자산처분손실_유형리스자산처분손실(
			Double 기타영업외손익_기타영업외비용_자산처분손실_유형리스자산처분손실) {
		this.기타영업외손익_기타영업외비용_자산처분손실_유형리스자산처분손실 = 기타영업외손익_기타영업외비용_자산처분손실_유형리스자산처분손실;
	}
	public Double get기타영업외손익_기타영업외비용_자산처분손실_기타() {
		return 기타영업외손익_기타영업외비용_자산처분손실_기타;
	}
	public void set기타영업외손익_기타영업외비용_자산처분손실_기타(Double 기타영업외손익_기타영업외비용_자산처분손실_기타) {
		this.기타영업외손익_기타영업외비용_자산처분손실_기타 = 기타영업외손익_기타영업외비용_자산처분손실_기타;
	}
	public Double get기타영업외손익_기타영업외비용_투자자산평가손실() {
		return 기타영업외손익_기타영업외비용_투자자산평가손실;
	}
	public void set기타영업외손익_기타영업외비용_투자자산평가손실(Double 기타영업외손익_기타영업외비용_투자자산평가손실) {
		this.기타영업외손익_기타영업외비용_투자자산평가손실 = 기타영업외손익_기타영업외비용_투자자산평가손실;
	}
	public Double get기타영업외손익_기타영업외비용_자산재평가손실() {
		return 기타영업외손익_기타영업외비용_자산재평가손실;
	}
	public void set기타영업외손익_기타영업외비용_자산재평가손실(Double 기타영업외손익_기타영업외비용_자산재평가손실) {
		this.기타영업외손익_기타영업외비용_자산재평가손실 = 기타영업외손익_기타영업외비용_자산재평가손실;
	}
	public Double get기타영업외손익_기타영업외비용_자산손상차손() {
		return 기타영업외손익_기타영업외비용_자산손상차손;
	}
	public void set기타영업외손익_기타영업외비용_자산손상차손(Double 기타영업외손익_기타영업외비용_자산손상차손) {
		this.기타영업외손익_기타영업외비용_자산손상차손 = 기타영업외손익_기타영업외비용_자산손상차손;
	}
	public Double get기타영업외손익_기타영업외비용_지분법관련손실() {
		return 기타영업외손익_기타영업외비용_지분법관련손실;
	}
	public void set기타영업외손익_기타영업외비용_지분법관련손실(Double 기타영업외손익_기타영업외비용_지분법관련손실) {
		this.기타영업외손익_기타영업외비용_지분법관련손실 = 기타영업외손익_기타영업외비용_지분법관련손실;
	}
	public Double get기타영업외손익_기타영업외비용_기타() {
		return 기타영업외손익_기타영업외비용_기타;
	}
	public void set기타영업외손익_기타영업외비용_기타(Double 기타영업외손익_기타영업외비용_기타) {
		this.기타영업외손익_기타영업외비용_기타 = 기타영업외손익_기타영업외비용_기타;
	}
	public Double get기타영업외손익_기타영업외비용_종속기업공동지배기업및관계기업관련손익() {
		return 기타영업외손익_기타영업외비용_종속기업공동지배기업및관계기업관련손익;
	}
	public void set기타영업외손익_기타영업외비용_종속기업공동지배기업및관계기업관련손익(
			Double 기타영업외손익_기타영업외비용_종속기업공동지배기업및관계기업관련손익) {
		this.기타영업외손익_기타영업외비용_종속기업공동지배기업및관계기업관련손익 = 기타영업외손익_기타영업외비용_종속기업공동지배기업및관계기업관련손익;
	}
	public Double get기타영업외손익_지분법관련손익() {
		return 기타영업외손익_지분법관련손익;
	}
	public void set기타영업외손익_지분법관련손익(Double 기타영업외손익_지분법관련손익) {
		this.기타영업외손익_지분법관련손익 = 기타영업외손익_지분법관련손익;
	}
	public Double get기타영업외손익_지분법관련손익_지분법이익() {
		return 기타영업외손익_지분법관련손익_지분법이익;
	}
	public void set기타영업외손익_지분법관련손익_지분법이익(Double 기타영업외손익_지분법관련손익_지분법이익) {
		this.기타영업외손익_지분법관련손익_지분법이익 = 기타영업외손익_지분법관련손익_지분법이익;
	}
	public Double get기타영업외손익_지분법관련손익_지분법손실() {
		return 기타영업외손익_지분법관련손익_지분법손실;
	}
	public void set기타영업외손익_지분법관련손익_지분법손실(Double 기타영업외손익_지분법관련손익_지분법손실) {
		this.기타영업외손익_지분법관련손익_지분법손실 = 기타영업외손익_지분법관련손익_지분법손실;
	}
	public Double get기타영업외손익_지분법관련손익_기타() {
		return 기타영업외손익_지분법관련손익_기타;
	}
	public void set기타영업외손익_지분법관련손익_기타(Double 기타영업외손익_지분법관련손익_기타) {
		this.기타영업외손익_지분법관련손익_기타 = 기타영업외손익_지분법관련손익_기타;
	}
	public Double get기타영업외손익_관계기업처분손익() {
		return 기타영업외손익_관계기업처분손익;
	}
	public void set기타영업외손익_관계기업처분손익(Double 기타영업외손익_관계기업처분손익) {
		this.기타영업외손익_관계기업처분손익 = 기타영업외손익_관계기업처분손익;
	}
	public Double get기타영업외손익_관계기업처분손익_관계기업처분이익() {
		return 기타영업외손익_관계기업처분손익_관계기업처분이익;
	}
	public void set기타영업외손익_관계기업처분손익_관계기업처분이익(Double 기타영업외손익_관계기업처분손익_관계기업처분이익) {
		this.기타영업외손익_관계기업처분손익_관계기업처분이익 = 기타영업외손익_관계기업처분손익_관계기업처분이익;
	}
	public Double get기타영업외손익_관계기업처분손익_관계기업처분손실() {
		return 기타영업외손익_관계기업처분손익_관계기업처분손실;
	}
	public void set기타영업외손익_관계기업처분손익_관계기업처분손실(Double 기타영업외손익_관계기업처분손익_관계기업처분손실) {
		this.기타영업외손익_관계기업처분손익_관계기업처분손실 = 기타영업외손익_관계기업처분손익_관계기업처분손실;
	}
	public Double get기타영업외손익_관계기업처분손익_기타() {
		return 기타영업외손익_관계기업처분손익_기타;
	}
	public void set기타영업외손익_관계기업처분손익_기타(Double 기타영업외손익_관계기업처분손익_기타) {
		this.기타영업외손익_관계기업처분손익_기타 = 기타영업외손익_관계기업처분손익_기타;
	}
	public Double get기타영업외손익_종속기업관련손익() {
		return 기타영업외손익_종속기업관련손익;
	}
	public void set기타영업외손익_종속기업관련손익(Double 기타영업외손익_종속기업관련손익) {
		this.기타영업외손익_종속기업관련손익 = 기타영업외손익_종속기업관련손익;
	}
	public Double get기타영업외손익_종속기업관련손익_종속기업처분이익() {
		return 기타영업외손익_종속기업관련손익_종속기업처분이익;
	}
	public void set기타영업외손익_종속기업관련손익_종속기업처분이익(Double 기타영업외손익_종속기업관련손익_종속기업처분이익) {
		this.기타영업외손익_종속기업관련손익_종속기업처분이익 = 기타영업외손익_종속기업관련손익_종속기업처분이익;
	}
	public Double get기타영업외손익_종속기업관련손익_종속기업처분손실() {
		return 기타영업외손익_종속기업관련손익_종속기업처분손실;
	}
	public void set기타영업외손익_종속기업관련손익_종속기업처분손실(Double 기타영업외손익_종속기업관련손익_종속기업처분손실) {
		this.기타영업외손익_종속기업관련손익_종속기업처분손실 = 기타영업외손익_종속기업관련손익_종속기업처분손실;
	}
	public Double get기타영업외손익_종속기업관련손익_기타() {
		return 기타영업외손익_종속기업관련손익_기타;
	}
	public void set기타영업외손익_종속기업관련손익_기타(Double 기타영업외손익_종속기업관련손익_기타) {
		this.기타영업외손익_종속기업관련손익_기타 = 기타영업외손익_종속기업관련손익_기타;
	}
	public Double get기타영업외손익_기타() {
		return 기타영업외손익_기타;
	}
	public void set기타영업외손익_기타(Double 기타영업외손익_기타) {
		this.기타영업외손익_기타 = 기타영업외손익_기타;
	}
	public Double get법인세비용차감전계속사업이익() {
		return 법인세비용차감전계속사업이익;
	}
	public void set법인세비용차감전계속사업이익(Double 법인세비용차감전계속사업이익) {
		this.법인세비용차감전계속사업이익 = 법인세비용차감전계속사업이익;
	}
	public Double get법인세비용() {
		return 법인세비용;
	}
	public void set법인세비용(Double 법인세비용) {
		this.법인세비용 = 법인세비용;
	}
	public Double get기타() {
		return 기타;
	}
	public void set기타(Double 기타) {
		this.기타 = 기타;
	}
	public Double get계속사업이익() {
		return 계속사업이익;
	}
	public void set계속사업이익(Double 계속사업이익) {
		this.계속사업이익 = 계속사업이익;
	}
	public Double get중단사업이익() {
		return 중단사업이익;
	}
	public void set중단사업이익(Double 중단사업이익) {
		this.중단사업이익 = 중단사업이익;
	}
	public Double get중단사업법인세효과() {
		return 중단사업법인세효과;
	}
	public void set중단사업법인세효과(Double 중단사업법인세효과) {
		this.중단사업법인세효과 = 중단사업법인세효과;
	}
	public Double get당기순이익() {
		return 당기순이익;
	}
	public void set당기순이익(Double 당기순이익) {
		this.당기순이익 = 당기순이익;
	}
	public Double get당기순이익_지배주주지분당기순이익() {
		return 당기순이익_지배주주지분당기순이익;
	}
	public void set당기순이익_지배주주지분당기순이익(Double 당기순이익_지배주주지분당기순이익) {
		this.당기순이익_지배주주지분당기순이익 = 당기순이익_지배주주지분당기순이익;
	}
	public Double get당기순이익_비지배주주지분당기순이익() {
		return 당기순이익_비지배주주지분당기순이익;
	}
	public void set당기순이익_비지배주주지분당기순이익(Double 당기순이익_비지배주주지분당기순이익) {
		this.당기순이익_비지배주주지분당기순이익 = 당기순이익_비지배주주지분당기순이익;
	}
	public Double get당기순이익_지배주주지분연결당기순이익() {
		return 당기순이익_지배주주지분연결당기순이익;
	}
	public void set당기순이익_지배주주지분연결당기순이익(Double 당기순이익_지배주주지분연결당기순이익) {
		this.당기순이익_지배주주지분연결당기순이익 = 당기순이익_지배주주지분연결당기순이익;
	}
	public Double get기타포괄이익() {
		return 기타포괄이익;
	}
	public void set기타포괄이익(Double 기타포괄이익) {
		this.기타포괄이익 = 기타포괄이익;
	}
	public Double get기타포괄이익_금융자산평가손익() {
		return 기타포괄이익_금융자산평가손익;
	}
	public void set기타포괄이익_금융자산평가손익(Double 기타포괄이익_금융자산평가손익) {
		this.기타포괄이익_금융자산평가손익 = 기타포괄이익_금융자산평가손익;
	}
	public Double get기타포괄이익_매도가능금융자산평가손익() {
		return 기타포괄이익_매도가능금융자산평가손익;
	}
	public void set기타포괄이익_매도가능금융자산평가손익(Double 기타포괄이익_매도가능금융자산평가손익) {
		this.기타포괄이익_매도가능금융자산평가손익 = 기타포괄이익_매도가능금융자산평가손익;
	}
	public Double get기타포괄이익_관계기업등기타포괄이익() {
		return 기타포괄이익_관계기업등기타포괄이익;
	}
	public void set기타포괄이익_관계기업등기타포괄이익(Double 기타포괄이익_관계기업등기타포괄이익) {
		this.기타포괄이익_관계기업등기타포괄이익 = 기타포괄이익_관계기업등기타포괄이익;
	}
	public Double get기타포괄이익_환산관련외환차이환율변동차이() {
		return 기타포괄이익_환산관련외환차이환율변동차이;
	}
	public void set기타포괄이익_환산관련외환차이환율변동차이(Double 기타포괄이익_환산관련외환차이환율변동차이) {
		this.기타포괄이익_환산관련외환차이환율변동차이 = 기타포괄이익_환산관련외환차이환율변동차이;
	}
	public Double get기타포괄이익_현금흐름위험회피적립금() {
		return 기타포괄이익_현금흐름위험회피적립금;
	}
	public void set기타포괄이익_현금흐름위험회피적립금(Double 기타포괄이익_현금흐름위험회피적립금) {
		this.기타포괄이익_현금흐름위험회피적립금 = 기타포괄이익_현금흐름위험회피적립금;
	}
	public Double get기타포괄이익_재평가손익() {
		return 기타포괄이익_재평가손익;
	}
	public void set기타포괄이익_재평가손익(Double 기타포괄이익_재평가손익) {
		this.기타포괄이익_재평가손익 = 기타포괄이익_재평가손익;
	}
	public Double get기타포괄이익_기타포괄이익관련법인세() {
		return 기타포괄이익_기타포괄이익관련법인세;
	}
	public void set기타포괄이익_기타포괄이익관련법인세(Double 기타포괄이익_기타포괄이익관련법인세) {
		this.기타포괄이익_기타포괄이익관련법인세 = 기타포괄이익_기타포괄이익관련법인세;
	}
	public Double get기타포괄이익_기타() {
		return 기타포괄이익_기타;
	}
	public void set기타포괄이익_기타(Double 기타포괄이익_기타) {
		this.기타포괄이익_기타 = 기타포괄이익_기타;
	}
	public Double get총포괄이익() {
		return 총포괄이익;
	}
	public void set총포괄이익(Double 총포괄이익) {
		this.총포괄이익 = 총포괄이익;
	}
	public Double get총포괄이익_지배주주지분총포괄이익() {
		return 총포괄이익_지배주주지분총포괄이익;
	}
	public void set총포괄이익_지배주주지분총포괄이익(Double 총포괄이익_지배주주지분총포괄이익) {
		this.총포괄이익_지배주주지분총포괄이익 = 총포괄이익_지배주주지분총포괄이익;
	}
	public Double get총포괄이익_비지배주주지분총포괄이익() {
		return 총포괄이익_비지배주주지분총포괄이익;
	}
	public void set총포괄이익_비지배주주지분총포괄이익(Double 총포괄이익_비지배주주지분총포괄이익) {
		this.총포괄이익_비지배주주지분총포괄이익 = 총포괄이익_비지배주주지분총포괄이익;
	}
	public Double get주당계속사업이익() {
		return 주당계속사업이익;
	}
	public void set주당계속사업이익(Double 주당계속사업이익) {
		this.주당계속사업이익 = 주당계속사업이익;
	}
	public Double get주당순이익() {
		return 주당순이익;
	}
	public void set주당순이익(Double 주당순이익) {
		this.주당순이익 = 주당순이익;
	}
	public Double get희석주당계속사업이익() {
		return 희석주당계속사업이익;
	}
	public void set희석주당계속사업이익(Double 희석주당계속사업이익) {
		this.희석주당계속사업이익 = 희석주당계속사업이익;
	}
	public Double get희석주당순이익() {
		return 희석주당순이익;
	}
	public void set희석주당순이익(Double 희석주당순이익) {
		this.희석주당순이익 = 희석주당순이익;
	}
	public Double get지배주주지분주당계속사업이익() {
		return 지배주주지분주당계속사업이익;
	}
	public void set지배주주지분주당계속사업이익(Double 지배주주지분주당계속사업이익) {
		this.지배주주지분주당계속사업이익 = 지배주주지분주당계속사업이익;
	}
	public Double get지배주주지분주당순이익() {
		return 지배주주지분주당순이익;
	}
	public void set지배주주지분주당순이익(Double 지배주주지분주당순이익) {
		this.지배주주지분주당순이익 = 지배주주지분주당순이익;
	}
	public Double get지배주주지분희석주당계속사업이익() {
		return 지배주주지분희석주당계속사업이익;
	}
	public void set지배주주지분희석주당계속사업이익(Double 지배주주지분희석주당계속사업이익) {
		this.지배주주지분희석주당계속사업이익 = 지배주주지분희석주당계속사업이익;
	}
	public Double get지배주주지분희석주당순이익() {
		return 지배주주지분희석주당순이익;
	}
	public void set지배주주지분희석주당순이익(Double 지배주주지분희석주당순이익) {
		this.지배주주지분희석주당순이익 = 지배주주지분희석주당순이익;
	}
	public Double getEBITDA() {
		return EBITDA;
	}
	public void setEBITDA(Double eBITDA) {
		EBITDA = eBITDA;
	}
	public Double getEBIT() {
		return EBIT;
	}
	public void setEBIT(Double eBIT) {
		EBIT = eBIT;
	}
	public Double get구영업외수익KGAAP() {
		return 구영업외수익KGAAP;
	}
	public void set구영업외수익KGAAP(Double 구영업외수익kgaap) {
		구영업외수익KGAAP = 구영업외수익kgaap;
	}
	public Double get구영업외비용KGAAP() {
		return 구영업외비용KGAAP;
	}
	public void set구영업외비용KGAAP(Double 구영업외비용kgaap) {
		구영업외비용KGAAP = 구영업외비용kgaap;
	}
	public Double get구경상이익년이전발생() {
		return 구경상이익년이전발생;
	}
	public void set구경상이익년이전발생(Double 구경상이익년이전발생) {
		this.구경상이익년이전발생 = 구경상이익년이전발생;
	}
	public Double get구특별이익년이전발생() {
		return 구특별이익년이전발생;
	}
	public void set구특별이익년이전발생(Double 구특별이익년이전발생) {
		this.구특별이익년이전발생 = 구특별이익년이전발생;
	}
	public Double get구특별손실년이전발생() {
		return 구특별손실년이전발생;
	}
	public void set구특별손실년이전발생(Double 구특별손실년이전발생) {
		this.구특별손실년이전발생 = 구특별손실년이전발생;
	}
	
	public String toString() {
		return stockCode + "," + ymDay +"," + dataType + "," + dateType;		
	}

	
}
