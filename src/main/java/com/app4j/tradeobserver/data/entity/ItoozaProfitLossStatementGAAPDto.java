package com.app4j.tradeobserver.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.app4j.tradeobserver.data.entity.id.ItoozaProfitLossStatementGAAPDtoId;

@Entity
@Table (name = "ITOOZA_PROFIT_LOSS_STATEMENT_GAAP")
@IdClass(ItoozaProfitLossStatementGAAPDtoId.class)
public class ItoozaProfitLossStatementGAAPDto {
	
	@Id
    @Column (name = "STOCK_CODE")
    private String stockCode;	
	@Id
	@Column (name="YM_DAY")
	private String ymDay;
	@Id
	@Column (name="DATE_TYPE")
	private String dateType;		// 분기, 연간 (Q, Y)
	
	@Column (name="매출액")
	private Double 매출액;
	@Column (name="매출원가")
	private Double 매출원가;
	@Column (name="매출총이익")
	private Double 매출총이익;
	@Column (name="판매비와관리비")
	private Double 판매비와관리비;
	@Column (name="판매비와관리비_급여")
	private Double 판매비와관리비_급여;
	@Column (name="판매비와관리비_퇴직급여")
	private Double 판매비와관리비_퇴직급여;
	@Column (name="판매비와관리비_복리후생비")
	private Double 판매비와관리비_복리후생비;
	@Column (name="판매비와관리비_지급수수료")
	private Double 판매비와관리비_지급수수료;
	@Column (name="판매비와관리비_판매수수료")
	private Double 판매비와관리비_판매수수료;
	@Column (name="판매비와관리비_판매촉진비")
	private Double 판매비와관리비_판매촉진비;
	@Column (name="판매비와관리비_광고선전비")
	private Double 판매비와관리비_광고선전비;
	@Column (name="판매비와관리비_운반비")
	private Double 판매비와관리비_운반비;
	@Column (name="판매비와관리비_연구비")
	private Double 판매비와관리비_연구비;
	@Column (name="판매비와관리비_경상개발비")
	private Double 판매비와관리비_경상개발비;
	@Column (name="판매비와관리비_대손상각비")
	private Double 판매비와관리비_대손상각비;
	@Column (name="판매비와관리비_감가상각비")
	private Double 판매비와관리비_감가상각비;
	@Column (name="판매비와관리비_무형자산상각비")
	private Double 판매비와관리비_무형자산상각비;
	@Column (name="판매비와관리비_기타")
	private Double 판매비와관리비_기타;
	@Column (name="영업이익")
	private Double 영업이익;
	@Column (name="영업외손익")
	private Double 영업외손익;
	@Column (name="영업외수익")
	private Double 영업외수익;
	@Column (name="영업외수익_이자수익")
	private Double 영업외수익_이자수익;
	@Column (name="영업외수익_배당금수익")
	private Double 영업외수익_배당금수익;
	@Column (name="영업외수익_단기투자자산처분이익")
	private Double 영업외수익_단기투자자산처분이익;
	@Column (name="영업외수익_단기투자자산평가이익")
	private Double 영업외수익_단기투자자산평가이익;
	@Column (name="영업외수익_장기투자증권처분이익")
	private Double 영업외수익_장기투자증권처분이익;
	@Column (name="영업외수익_재고자산평가손실환입")
	private Double 영업외수익_재고자산평가손실환입;
	@Column (name="영업외수익_외화환산이익")
	private Double 영업외수익_외화환산이익;
	@Column (name="영업외수익_외환차익")
	private Double 영업외수익_외환차익;
	@Column (name="영업외수익_지분법이익")
	private Double 영업외수익_지분법이익;
	@Column (name="영업외수익_파생상품이익")
	private Double 영업외수익_파생상품이익;
	@Column (name="영업외수익_기타")
	private Double 영업외수익_기타;
	@Column (name="영업외비용")
	private Double 영업외비용;
	@Column (name="영업외비용_이자비용")
	private Double 영업외비용_이자비용;
	@Column (name="영업외비용_단기투자자산처분손실")
	private Double 영업외비용_단기투자자산처분손실;
	@Column (name="영업외비용_단기투자자산평가손실")
	private Double 영업외비용_단기투자자산평가손실;
	@Column (name="영업외비용_장기투자증권처분손실")
	private Double 영업외비용_장기투자증권처분손실;
	@Column (name="영업외비용_재고자산평가손실")
	private Double 영업외비용_재고자산평가손실;
	@Column (name="영업외비용_외화환산손실")
	private Double 영업외비용_외화환산손실;
	@Column (name="영업외비용_외환차손")
	private Double 영업외비용_외환차손;
	@Column (name="영업외비용_지분법손실")
	private Double 영업외비용_지분법손실;
	@Column (name="영업외비용_파생상품손실")
	private Double 영업외비용_파생상품손실;
	@Column (name="영업외비용_기타")
	private Double 영업외비용_기타;
	@Column (name="법인세차감전계속사업이익")
	private Double 법인세차감전계속사업이익;
	@Column (name="법인세비용")
	private Double 법인세비용;
	@Column (name="당기순이익")
	private Double 당기순이익;
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getYmDay() {
		return ymDay;
	}
	public void setYmDay(String ymDay) {
		this.ymDay = ymDay;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public Double get매출액() {
		return 매출액;
	}
	public void set매출액(Double 매출액) {
		this.매출액 = 매출액;
	}
	public Double get매출원가() {
		return 매출원가;
	}
	public void set매출원가(Double 매출원가) {
		this.매출원가 = 매출원가;
	}
	public Double get매출총이익() {
		return 매출총이익;
	}
	public void set매출총이익(Double 매출총이익) {
		this.매출총이익 = 매출총이익;
	}
	public Double get판매비와관리비() {
		return 판매비와관리비;
	}
	public void set판매비와관리비(Double 판매비와관리비) {
		this.판매비와관리비 = 판매비와관리비;
	}
	public Double get판매비와관리비_급여() {
		return 판매비와관리비_급여;
	}
	public void set판매비와관리비_급여(Double 판매비와관리비_급여) {
		this.판매비와관리비_급여 = 판매비와관리비_급여;
	}
	public Double get판매비와관리비_퇴직급여() {
		return 판매비와관리비_퇴직급여;
	}
	public void set판매비와관리비_퇴직급여(Double 판매비와관리비_퇴직급여) {
		this.판매비와관리비_퇴직급여 = 판매비와관리비_퇴직급여;
	}
	public Double get판매비와관리비_복리후생비() {
		return 판매비와관리비_복리후생비;
	}
	public void set판매비와관리비_복리후생비(Double 판매비와관리비_복리후생비) {
		this.판매비와관리비_복리후생비 = 판매비와관리비_복리후생비;
	}
	public Double get판매비와관리비_지급수수료() {
		return 판매비와관리비_지급수수료;
	}
	public void set판매비와관리비_지급수수료(Double 판매비와관리비_지급수수료) {
		this.판매비와관리비_지급수수료 = 판매비와관리비_지급수수료;
	}
	public Double get판매비와관리비_판매수수료() {
		return 판매비와관리비_판매수수료;
	}
	public void set판매비와관리비_판매수수료(Double 판매비와관리비_판매수수료) {
		this.판매비와관리비_판매수수료 = 판매비와관리비_판매수수료;
	}
	public Double get판매비와관리비_판매촉진비() {
		return 판매비와관리비_판매촉진비;
	}
	public void set판매비와관리비_판매촉진비(Double 판매비와관리비_판매촉진비) {
		this.판매비와관리비_판매촉진비 = 판매비와관리비_판매촉진비;
	}
	public Double get판매비와관리비_광고선전비() {
		return 판매비와관리비_광고선전비;
	}
	public void set판매비와관리비_광고선전비(Double 판매비와관리비_광고선전비) {
		this.판매비와관리비_광고선전비 = 판매비와관리비_광고선전비;
	}
	public Double get판매비와관리비_운반비() {
		return 판매비와관리비_운반비;
	}
	public void set판매비와관리비_운반비(Double 판매비와관리비_운반비) {
		this.판매비와관리비_운반비 = 판매비와관리비_운반비;
	}
	public Double get판매비와관리비_연구비() {
		return 판매비와관리비_연구비;
	}
	public void set판매비와관리비_연구비(Double 판매비와관리비_연구비) {
		this.판매비와관리비_연구비 = 판매비와관리비_연구비;
	}
	public Double get판매비와관리비_경상개발비() {
		return 판매비와관리비_경상개발비;
	}
	public void set판매비와관리비_경상개발비(Double 판매비와관리비_경상개발비) {
		this.판매비와관리비_경상개발비 = 판매비와관리비_경상개발비;
	}
	public Double get판매비와관리비_대손상각비() {
		return 판매비와관리비_대손상각비;
	}
	public void set판매비와관리비_대손상각비(Double 판매비와관리비_대손상각비) {
		this.판매비와관리비_대손상각비 = 판매비와관리비_대손상각비;
	}
	public Double get판매비와관리비_감가상각비() {
		return 판매비와관리비_감가상각비;
	}
	public void set판매비와관리비_감가상각비(Double 판매비와관리비_감가상각비) {
		this.판매비와관리비_감가상각비 = 판매비와관리비_감가상각비;
	}
	public Double get판매비와관리비_무형자산상각비() {
		return 판매비와관리비_무형자산상각비;
	}
	public void set판매비와관리비_무형자산상각비(Double 판매비와관리비_무형자산상각비) {
		this.판매비와관리비_무형자산상각비 = 판매비와관리비_무형자산상각비;
	}
	public Double get판매비와관리비_기타() {
		return 판매비와관리비_기타;
	}
	public void set판매비와관리비_기타(Double 판매비와관리비_기타) {
		this.판매비와관리비_기타 = 판매비와관리비_기타;
	}
	public Double get영업이익() {
		return 영업이익;
	}
	public void set영업이익(Double 영업이익) {
		this.영업이익 = 영업이익;
	}
	public Double get영업외손익() {
		return 영업외손익;
	}
	public void set영업외손익(Double 영업외손익) {
		this.영업외손익 = 영업외손익;
	}
	public Double get영업외수익() {
		return 영업외수익;
	}
	public void set영업외수익(Double 영업외수익) {
		this.영업외수익 = 영업외수익;
	}
	public Double get영업외수익_이자수익() {
		return 영업외수익_이자수익;
	}
	public void set영업외수익_이자수익(Double 영업외수익_이자수익) {
		this.영업외수익_이자수익 = 영업외수익_이자수익;
	}
	public Double get영업외수익_배당금수익() {
		return 영업외수익_배당금수익;
	}
	public void set영업외수익_배당금수익(Double 영업외수익_배당금수익) {
		this.영업외수익_배당금수익 = 영업외수익_배당금수익;
	}
	public Double get영업외수익_단기투자자산처분이익() {
		return 영업외수익_단기투자자산처분이익;
	}
	public void set영업외수익_단기투자자산처분이익(Double 영업외수익_단기투자자산처분이익) {
		this.영업외수익_단기투자자산처분이익 = 영업외수익_단기투자자산처분이익;
	}
	public Double get영업외수익_단기투자자산평가이익() {
		return 영업외수익_단기투자자산평가이익;
	}
	public void set영업외수익_단기투자자산평가이익(Double 영업외수익_단기투자자산평가이익) {
		this.영업외수익_단기투자자산평가이익 = 영업외수익_단기투자자산평가이익;
	}
	public Double get영업외수익_장기투자증권처분이익() {
		return 영업외수익_장기투자증권처분이익;
	}
	public void set영업외수익_장기투자증권처분이익(Double 영업외수익_장기투자증권처분이익) {
		this.영업외수익_장기투자증권처분이익 = 영업외수익_장기투자증권처분이익;
	}
	public Double get영업외수익_재고자산평가손실환입() {
		return 영업외수익_재고자산평가손실환입;
	}
	public void set영업외수익_재고자산평가손실환입(Double 영업외수익_재고자산평가손실환입) {
		this.영업외수익_재고자산평가손실환입 = 영업외수익_재고자산평가손실환입;
	}
	public Double get영업외수익_외화환산이익() {
		return 영업외수익_외화환산이익;
	}
	public void set영업외수익_외화환산이익(Double 영업외수익_외화환산이익) {
		this.영업외수익_외화환산이익 = 영업외수익_외화환산이익;
	}
	public Double get영업외수익_외환차익() {
		return 영업외수익_외환차익;
	}
	public void set영업외수익_외환차익(Double 영업외수익_외환차익) {
		this.영업외수익_외환차익 = 영업외수익_외환차익;
	}
	public Double get영업외수익_지분법이익() {
		return 영업외수익_지분법이익;
	}
	public void set영업외수익_지분법이익(Double 영업외수익_지분법이익) {
		this.영업외수익_지분법이익 = 영업외수익_지분법이익;
	}
	public Double get영업외수익_파생상품이익() {
		return 영업외수익_파생상품이익;
	}
	public void set영업외수익_파생상품이익(Double 영업외수익_파생상품이익) {
		this.영업외수익_파생상품이익 = 영업외수익_파생상품이익;
	}
	public Double get영업외수익_기타() {
		return 영업외수익_기타;
	}
	public void set영업외수익_기타(Double 영업외수익_기타) {
		this.영업외수익_기타 = 영업외수익_기타;
	}
	public Double get영업외비용() {
		return 영업외비용;
	}
	public void set영업외비용(Double 영업외비용) {
		this.영업외비용 = 영업외비용;
	}
	public Double get영업외비용_이자비용() {
		return 영업외비용_이자비용;
	}
	public void set영업외비용_이자비용(Double 영업외비용_이자비용) {
		this.영업외비용_이자비용 = 영업외비용_이자비용;
	}
	public Double get영업외비용_단기투자자산처분손실() {
		return 영업외비용_단기투자자산처분손실;
	}
	public void set영업외비용_단기투자자산처분손실(Double 영업외비용_단기투자자산처분손실) {
		this.영업외비용_단기투자자산처분손실 = 영업외비용_단기투자자산처분손실;
	}
	public Double get영업외비용_단기투자자산평가손실() {
		return 영업외비용_단기투자자산평가손실;
	}
	public void set영업외비용_단기투자자산평가손실(Double 영업외비용_단기투자자산평가손실) {
		this.영업외비용_단기투자자산평가손실 = 영업외비용_단기투자자산평가손실;
	}
	public Double get영업외비용_장기투자증권처분손실() {
		return 영업외비용_장기투자증권처분손실;
	}
	public void set영업외비용_장기투자증권처분손실(Double 영업외비용_장기투자증권처분손실) {
		this.영업외비용_장기투자증권처분손실 = 영업외비용_장기투자증권처분손실;
	}
	public Double get영업외비용_재고자산평가손실() {
		return 영업외비용_재고자산평가손실;
	}
	public void set영업외비용_재고자산평가손실(Double 영업외비용_재고자산평가손실) {
		this.영업외비용_재고자산평가손실 = 영업외비용_재고자산평가손실;
	}
	public Double get영업외비용_외화환산손실() {
		return 영업외비용_외화환산손실;
	}
	public void set영업외비용_외화환산손실(Double 영업외비용_외화환산손실) {
		this.영업외비용_외화환산손실 = 영업외비용_외화환산손실;
	}
	public Double get영업외비용_외환차손() {
		return 영업외비용_외환차손;
	}
	public void set영업외비용_외환차손(Double 영업외비용_외환차손) {
		this.영업외비용_외환차손 = 영업외비용_외환차손;
	}
	public Double get영업외비용_지분법손실() {
		return 영업외비용_지분법손실;
	}
	public void set영업외비용_지분법손실(Double 영업외비용_지분법손실) {
		this.영업외비용_지분법손실 = 영업외비용_지분법손실;
	}
	public Double get영업외비용_파생상품손실() {
		return 영업외비용_파생상품손실;
	}
	public void set영업외비용_파생상품손실(Double 영업외비용_파생상품손실) {
		this.영업외비용_파생상품손실 = 영업외비용_파생상품손실;
	}
	public Double get영업외비용_기타() {
		return 영업외비용_기타;
	}
	public void set영업외비용_기타(Double 영업외비용_기타) {
		this.영업외비용_기타 = 영업외비용_기타;
	}
	public Double get법인세차감전계속사업이익() {
		return 법인세차감전계속사업이익;
	}
	public void set법인세차감전계속사업이익(Double 법인세차감전계속사업이익) {
		this.법인세차감전계속사업이익 = 법인세차감전계속사업이익;
	}
	public Double get법인세비용() {
		return 법인세비용;
	}
	public void set법인세비용(Double 법인세비용) {
		this.법인세비용 = 법인세비용;
	}
	public Double get당기순이익() {
		return 당기순이익;
	}
	public void set당기순이익(Double 당기순이익) {
		this.당기순이익 = 당기순이익;
	}
	
	


	
}
