package com.app4j.tradeobserver.data.entity.id;

import java.io.Serializable;

import com.app4j.tradeobserver.data.entity.ItoozaCashFlowStatementGAAPDto;

public class ItoozaCashFlowStatementGAAPDtoId implements Serializable {
	
	protected String stockCode;
	protected String ymDay;
	protected String dateType;
	
	public ItoozaCashFlowStatementGAAPDtoId() {
	}

	public ItoozaCashFlowStatementGAAPDtoId(String stockCode, String ymDay, String dateType, String dataType) {
		this.ymDay = ymDay;
		this.stockCode = stockCode;
		this.dateType = dateType;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ItoozaCashFlowStatementGAAPDto itoozaCashFlowStatementGAAPDto = (ItoozaCashFlowStatementGAAPDto) o;

		return stockCode.equals(itoozaCashFlowStatementGAAPDto.getStockCode())
				&& ymDay.equals(itoozaCashFlowStatementGAAPDto.getYmDay())
			    && dateType.equals(itoozaCashFlowStatementGAAPDto.getDateType());
	}

	public int hashCode() {
		int result;
		result = 13 * dateType.hashCode();
		result = 17 * result + ymDay.hashCode();
		result = 29 * result + stockCode.hashCode();
		return result;
	}

}
