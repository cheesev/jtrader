package com.app4j.tradeobserver.data.entity.id;

import java.io.Serializable;

import com.app4j.tradeobserver.data.entity.ItoozaProfitLossStatementGAAPDto;

public class ItoozaProfitLossStatementGAAPDtoId implements Serializable {
	
	protected String stockCode;
	protected String ymDay;
	protected String dateType;
	
	public ItoozaProfitLossStatementGAAPDtoId() {
	}

	public ItoozaProfitLossStatementGAAPDtoId(String stockCode, String ymDay, String dateType) {
		this.ymDay = ymDay;
		this.stockCode = stockCode;
		this.dateType = dateType;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ItoozaProfitLossStatementGAAPDto itoozaProfitLossStatementGAAPDto = (ItoozaProfitLossStatementGAAPDto) o;

		return stockCode.equals(itoozaProfitLossStatementGAAPDto.getStockCode())
				&& ymDay.equals(itoozaProfitLossStatementGAAPDto.getYmDay())
			    && dateType.equals(itoozaProfitLossStatementGAAPDto.getDateType());
	}

	public int hashCode() {
		int result;
		result = 13 * dateType.hashCode();
		result = 17 * result + ymDay.hashCode();
		result = 29 * result + stockCode.hashCode();
		return result;
	}

}
