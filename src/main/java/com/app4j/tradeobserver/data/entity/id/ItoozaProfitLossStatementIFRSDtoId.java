package com.app4j.tradeobserver.data.entity.id;

import java.io.Serializable;

import com.app4j.tradeobserver.data.entity.ItoozaProfitLossStatementIFRSDto;

public class ItoozaProfitLossStatementIFRSDtoId implements Serializable {
	
	protected String stockCode;
	protected String ymDay;
	protected String dateType;
	
	public ItoozaProfitLossStatementIFRSDtoId() {
	}

	public ItoozaProfitLossStatementIFRSDtoId(String stockCode, String ymDay, String dateType) {
		this.ymDay = ymDay;
		this.stockCode = stockCode;
		this.dateType = dateType;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ItoozaProfitLossStatementIFRSDto itoozaProfitLossStatementIFRSDto = (ItoozaProfitLossStatementIFRSDto) o;

		return stockCode.equals(itoozaProfitLossStatementIFRSDto.getStockCode())
				&& ymDay.equals(itoozaProfitLossStatementIFRSDto.getYmDay())
			    && dateType.equals(itoozaProfitLossStatementIFRSDto.getDateType());
	}

	public int hashCode() {
		int result;
		result = 13 * dateType.hashCode();
		result = 17 * result + ymDay.hashCode();
		result = 29 * result + stockCode.hashCode();
		return result;
	}

}
