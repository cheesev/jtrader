package com.app4j.tradeobserver.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.app4j.tradeobserver.data.entity.id.CashFlowStatementGAAPDtoId;

@Entity
@Table (name = "FINANCIAL_POSITION_STATEMENT_GAAP")
@IdClass(CashFlowStatementGAAPDtoId.class)
public class CashFlowStatementGAAPDto {
	
	@Id
    @Column (name = "STOCK_CODE")
    private String stockCode;	
	@Id
	@Column (name="YM_DAY")
	private String ymDay;
	@Id
	@Column (name="DATE_TYPE")
	private String dateType;		// 분기, 연간 (Q, Y)
	@Id
	@Column (name="DATA_TYPE")
	private String dataType;		// 연결, 별도 (L, S)
	
	@Column (name="자산총계")
	private Double 자산총계;
	@Column (name="유동자산")
	private Double 유동자산;
	@Column (name="유동자산_당좌자산")
	private Double 유동자산_당좌자산;
	@Column (name="유동자산_당좌자산_현금및현금성자산")
	private Double 유동자산_당좌자산_현금및현금성자산;
	@Column (name="유동자산_당좌자산_단기투자자산")
	private Double 유동자산_당좌자산_단기투자자산;
	@Column (name="유동자산_당좌자산_단기투자자산_단기예금")
	private Double 유동자산_당좌자산_단기투자자산_단기예금;
	@Column (name="유동자산_당좌자산_단기투자자산_단기매매증권")
	private Double 유동자산_당좌자산_단기투자자산_단기매매증권;
	@Column (name="유동자산_당좌자산_단기투자자산_매도가능증권")
	private Double 유동자산_당좌자산_단기투자자산_매도가능증권;
	@Column (name="유동자산_당좌자산_단기투자자산_만기보유증권")
	private Double 유동자산_당좌자산_단기투자자산_만기보유증권;
	@Column (name="유동자산_당좌자산_단기투자자산_단기대여금")
	private Double 유동자산_당좌자산_단기투자자산_단기대여금;
	@Column (name="유동자산_당좌자산_단기투자자산_기타")
	private Double 유동자산_당좌자산_단기투자자산_기타;
	@Column (name="유동자산_당좌자산_매출채권")
	private Double 유동자산_당좌자산_매출채권;
	@Column (name="유동자산_당좌자산_매출채권_매출채권")
	private Double 유동자산_당좌자산_매출채권_매출채권;
	@Column (name="유동자산_당좌자산_매출채권_외화매출채권")
	private Double 유동자산_당좌자산_매출채권_외화매출채권;
	@Column (name="유동자산_당좌자산_매출채권_공사미수금")
	private Double 유동자산_당좌자산_매출채권_공사미수금;
	@Column (name="유동자산_당좌자산_매출채권_분양미수금")
	private Double 유동자산_당좌자산_매출채권_분양미수금;
	@Column (name="유동자산_당좌자산_매출채권_기타")
	private Double 유동자산_당좌자산_매출채권_기타;
	@Column (name="유동자산_당좌자산_단기대출채권")
	private Double 유동자산_당좌자산_단기대출채권;
	@Column (name="유동자산_당좌자산_미수금")
	private Double 유동자산_당좌자산_미수금;
	@Column (name="유동자산_당좌자산_미수수익")
	private Double 유동자산_당좌자산_미수수익;
	@Column (name="유동자산_당좌자산_선급금")
	private Double 유동자산_당좌자산_선급금;
	@Column (name="유동자산_당좌자산_선급비용")
	private Double 유동자산_당좌자산_선급비용;
	@Column (name="유동자산_당좌자산_보증금")
	private Double 유동자산_당좌자산_보증금;
	@Column (name="유동자산_당좌자산_파생상품자산")
	private Double 유동자산_당좌자산_파생상품자산;
	@Column (name="유동자산_당좌자산_이연법인세자산")
	private Double 유동자산_당좌자산_이연법인세자산;
	@Column (name="유동자산_당좌자산_금융리스채권")
	private Double 유동자산_당좌자산_금융리스채권;
	@Column (name="유동자산_당좌자산_기타당좌자산")
	private Double 유동자산_당좌자산_기타당좌자산;
	@Column (name="유동자산_재고자산")
	private Double 유동자산_재고자산;
	@Column (name="유동자산_재고자산_상품")
	private Double 유동자산_재고자산_상품;
	@Column (name="유동자산_재고자산_제품")
	private Double 유동자산_재고자산_제품;
	@Column (name="유동자산_재고자산_반제품")
	private Double 유동자산_재고자산_반제품;
	@Column (name="유동자산_재고자산_재공품")
	private Double 유동자산_재고자산_재공품;
	@Column (name="유동자산_재고자산_원재료")
	private Double 유동자산_재고자산_원재료;
	@Column (name="유동자산_재고자산_미착품")
	private Double 유동자산_재고자산_미착품;
	@Column (name="유동자산_재고자산_저장품")
	private Double 유동자산_재고자산_저장품;
	@Column (name="유동자산_재고자산_기타재고자산")
	private Double 유동자산_재고자산_기타재고자산;
	@Column (name="임대주택자산")
	private Double 임대주택자산;
	@Column (name="비유동자산")
	private Double 비유동자산;
	@Column (name="비유동자산_투자자산")
	private Double 비유동자산_투자자산;
	@Column (name="비유동자산_투자자산_장기금융상품")
	private Double 비유동자산_투자자산_장기금융상품;
	@Column (name="비유동자산_투자자산_매도가능증권")
	private Double 비유동자산_투자자산_매도가능증권;
	@Column (name="비유동자산_투자자산_만기보유증권")
	private Double 비유동자산_투자자산_만기보유증권;
	@Column (name="비유동자산_투자자산_지분법적용투자주식")
	private Double 비유동자산_투자자산_지분법적용투자주식;
	@Column (name="비유동자산_투자자산_신주인수권")
	private Double 비유동자산_투자자산_신주인수권;
	@Column (name="비유동자산_투자자산_투자부동산")
	private Double 비유동자산_투자자산_투자부동산;
	@Column (name="비유동자산_투자자산_장기대여금")
	private Double 비유동자산_투자자산_장기대여금;
	@Column (name="비유동자산_투자자산_장기대출채권")
	private Double 비유동자산_투자자산_장기대출채권;
	@Column (name="비유동자산_투자자산_퇴직보험예치금")
	private Double 비유동자산_투자자산_퇴직보험예치금;
	@Column (name="비유동자산_투자자산_퇴직연금운용자산")
	private Double 비유동자산_투자자산_퇴직연금운용자산;
	@Column (name="비유동자산_투자자산_파생상품자산")
	private Double 비유동자산_투자자산_파생상품자산;
	@Column (name="비유동자산_투자자산_기타투자자산")
	private Double 비유동자산_투자자산_기타투자자산;
	@Column (name="비유동자산_유형자산")
	private Double 비유동자산_유형자산;
	@Column (name="비유동자산_유형자산_토지")
	private Double 비유동자산_유형자산_토지;
	@Column (name="비유동자산_유형자산_입목")
	private Double 비유동자산_유형자산_입목;
	@Column (name="비유동자산_유형자산_건설중인자산")
	private Double 비유동자산_유형자산_건설중인자산;
	@Column (name="비유동자산_유형자산_설비자산")
	private Double 비유동자산_유형자산_설비자산;
	@Column (name="비유동자산_유형자산_설비자산_건물및부속설비")
	private Double 비유동자산_유형자산_설비자산_건물및부속설비;
	@Column (name="비유동자산_유형자산_설비자산_구축물")
	private Double 비유동자산_유형자산_설비자산_구축물;
	@Column (name="비유동자산_유형자산_설비자산_기계장치")
	private Double 비유동자산_유형자산_설비자산_기계장치;
	@Column (name="비유동자산_유형자산_설비자산_중장비")
	private Double 비유동자산_유형자산_설비자산_중장비;
	@Column (name="비유동자산_유형자산_설비자산_제설비")
	private Double 비유동자산_유형자산_설비자산_제설비;
	@Column (name="비유동자산_유형자산_설비자산_미착자산")
	private Double 비유동자산_유형자산_설비자산_미착자산;
	@Column (name="비유동자산_유형자산_설비자산_차량운반구")
	private Double 비유동자산_유형자산_설비자산_차량운반구;
	@Column (name="비유동자산_유형자산_설비자산_선박")
	private Double 비유동자산_유형자산_설비자산_선박;
	@Column (name="비유동자산_유형자산_설비자산_항공기")
	private Double 비유동자산_유형자산_설비자산_항공기;
	@Column (name="비유동자산_유형자산_설비자산_공구기구비품")
	private Double 비유동자산_유형자산_설비자산_공구기구비품;
	@Column (name="비유동자산_유형자산_설비자산_리스자산")
	private Double 비유동자산_유형자산_설비자산_리스자산;
	@Column (name="비유동자산_유형자산_설비자산_리스개량자산")
	private Double 비유동자산_유형자산_설비자산_리스개량자산;
	@Column (name="비유동자산_유형자산_설비자산_임대자산")
	private Double 비유동자산_유형자산_설비자산_임대자산;
	@Column (name="비유동자산_유형자산_설비자산_임차개량자산")
	private Double 비유동자산_유형자산_설비자산_임차개량자산;
	@Column (name="비유동자산_유형자산_설비자산_기타")
	private Double 비유동자산_유형자산_설비자산_기타;
	@Column (name="비유동자산_유형자산_기타유형자산")
	private Double 비유동자산_유형자산_기타유형자산;
	@Column (name="비유동자산_무형자산")
	private Double 비유동자산_무형자산;
	@Column (name="비유동자산_무형자산_영업권")
	private Double 비유동자산_무형자산_영업권;
	@Column (name="비유동자산_무형자산_부의영업권")
	private Double 비유동자산_무형자산_부의영업권;
	@Column (name="비유동자산_무형자산_산업재산권")
	private Double 비유동자산_무형자산_산업재산권;
	@Column (name="비유동자산_무형자산_개발비")
	private Double 비유동자산_무형자산_개발비;
	@Column (name="비유동자산_무형자산_제이용권")
	private Double 비유동자산_무형자산_제이용권;
	@Column (name="비유동자산_무형자산_라이센스와프랜차이즈")
	private Double 비유동자산_무형자산_라이센스와프랜차이즈;
	@Column (name="비유동자산_무형자산_저작권")
	private Double 비유동자산_무형자산_저작권;
	@Column (name="비유동자산_무형자산_기타무형자산")
	private Double 비유동자산_무형자산_기타무형자산;
	@Column (name="비유동자산_기타비유동자산")
	private Double 비유동자산_기타비유동자산;
	@Column (name="비유동자산_기타비유동자산_장기성매출채권")
	private Double 비유동자산_기타비유동자산_장기성매출채권;
	@Column (name="비유동자산_기타비유동자산_장기미수금")
	private Double 비유동자산_기타비유동자산_장기미수금;
	@Column (name="비유동자산_기타비유동자산_장기미수수익")
	private Double 비유동자산_기타비유동자산_장기미수수익;
	@Column (name="비유동자산_기타비유동자산_보증금")
	private Double 비유동자산_기타비유동자산_보증금;
	@Column (name="비유동자산_기타비유동자산_이연법인세자산")
	private Double 비유동자산_기타비유동자산_이연법인세자산;
	@Column (name="비유동자산_기타비유동자산_금융리스채권")
	private Double 비유동자산_기타비유동자산_금융리스채권;
	@Column (name="비유동자산_기타비유동자산_파생상품자산")
	private Double 비유동자산_기타비유동자산_파생상품자산;
	@Column (name="비유동자산_기타비유동자산_기타")
	private Double 비유동자산_기타비유동자산_기타;
	@Column (name="비유동자산_이연자산")
	private Double 비유동자산_이연자산;
	@Column (name="부채총계")
	private Double 부채총계;
	@Column (name="부채총계_유동부채")
	private Double 부채총계_유동부채;
	@Column (name="부채총계_유동부채_매입채무")
	private Double 부채총계_유동부채_매입채무;
	@Column (name="부채총계_유동부채_단기차입금")
	private Double 부채총계_유동부채_단기차입금;
	@Column (name="부채총계_유동부채_미지급금")
	private Double 부채총계_유동부채_미지급금;
	@Column (name="부채총계_유동부채_선수금")
	private Double 부채총계_유동부채_선수금;
	@Column (name="부채총계_유동부채_예수금")
	private Double 부채총계_유동부채_예수금;
	@Column (name="부채총계_유동부채_미지급비용")
	private Double 부채총계_유동부채_미지급비용;
	@Column (name="부채총계_유동부채_단기유동화채무")
	private Double 부채총계_유동부채_단기유동화채무;
	@Column (name="부채총계_유동부채_단기사채")
	private Double 부채총계_유동부채_단기사채;
	@Column (name="부채총계_유동부채_유동성장기부채")
	private Double 부채총계_유동부채_유동성장기부채;
	@Column (name="부채총계_유동부채_유동성장기부채_유동성사채")
	private Double 부채총계_유동부채_유동성장기부채_유동성사채;
	@Column (name="부채총계_유동부채_유동성장기부채_유동성장기차입금")
	private Double 부채총계_유동부채_유동성장기부채_유동성장기차입금;
	@Column (name="부채총계_유동부채_유동성장기부채_유동성외화장기차입금")
	private Double 부채총계_유동부채_유동성장기부채_유동성외화장기차입금;
	@Column (name="부채총계_유동부채_유동성장기부채_유동성유동화채무")
	private Double 부채총계_유동부채_유동성장기부채_유동성유동화채무;
	@Column (name="부채총계_유동부채_유동성장기부채_유동성연불매입채무")
	private Double 부채총계_유동부채_유동성장기부채_유동성연불매입채무;
	@Column (name="부채총계_유동부채_유동성장기부채_기타유동성장기부채")
	private Double 부채총계_유동부채_유동성장기부채_기타유동성장기부채;
	@Column (name="부채총계_유동부채_단기금융예수금")
	private Double 부채총계_유동부채_단기금융예수금;
	@Column (name="부채총계_유동부채_선수수익")
	private Double 부채총계_유동부채_선수수익;
	@Column (name="부채총계_유동부채_금융리스부채")
	private Double 부채총계_유동부채_금융리스부채;
	@Column (name="부채총계_유동부채_단기부채성충당부채")
	private Double 부채총계_유동부채_단기부채성충당부채;
	@Column (name="부채총계_유동부채_보증금")
	private Double 부채총계_유동부채_보증금;
	@Column (name="부채총계_유동부채_파생상품부채")
	private Double 부채총계_유동부채_파생상품부채;
	@Column (name="부채총계_유동부채_이연법인세부채")
	private Double 부채총계_유동부채_이연법인세부채;
	@Column (name="부채총계_유동부채_기타유동부채")
	private Double 부채총계_유동부채_기타유동부채;
	@Column (name="부채총계_비유동부채")
	private Double 부채총계_비유동부채;
	@Column (name="부채총계_비유동부채_사채")
	private Double 부채총계_비유동부채_사채;
	@Column (name="부채총계_비유동부채_사채_사채")
	private Double 부채총계_비유동부채_사채_사채;
	@Column (name="부채총계_비유동부채_사채_전환사채")
	private Double 부채총계_비유동부채_사채_전환사채;
	@Column (name="부채총계_비유동부채_사채_신주인수권부사채")
	private Double 부채총계_비유동부채_사채_신주인수권부사채;
	@Column (name="부채총계_비유동부채_사채_교환사채")
	private Double 부채총계_비유동부채_사채_교환사채;
	@Column (name="부채총계_비유동부채_장기차입금")
	private Double 부채총계_비유동부채_장기차입금;
	@Column (name="부채총계_비유동부채_장기차입금_장기차입금")
	private Double 부채총계_비유동부채_장기차입금_장기차입금;
	@Column (name="부채총계_비유동부채_장기차입금_외화장기차입금")
	private Double 부채총계_비유동부채_장기차입금_외화장기차입금;
	@Column (name="부채총계_비유동부채_장기차입금_국민주택기금차입금")
	private Double 부채총계_비유동부채_장기차입금_국민주택기금차입금;
	@Column (name="부채총계_비유동부채_장기차입금_기타")
	private Double 부채총계_비유동부채_장기차입금_기타;
	@Column (name="부채총계_비유동부채_금융리스부채")
	private Double 부채총계_비유동부채_금융리스부채;
	@Column (name="부채총계_비유동부채_장기성매입채무")
	private Double 부채총계_비유동부채_장기성매입채무;
	@Column (name="부채총계_비유동부채_장기선수금")
	private Double 부채총계_비유동부채_장기선수금;
	@Column (name="부채총계_비유동부채_장기미지급금")
	private Double 부채총계_비유동부채_장기미지급금;
	@Column (name="부채총계_비유동부채_장기미지급비용")
	private Double 부채총계_비유동부채_장기미지급비용;
	@Column (name="부채총계_비유동부채_장기성예수금")
	private Double 부채총계_비유동부채_장기성예수금;
	@Column (name="부채총계_비유동부채_장기선수수익")
	private Double 부채총계_비유동부채_장기선수수익;
	@Column (name="부채총계_비유동부채_장기유동화채무")
	private Double 부채총계_비유동부채_장기유동화채무;
	@Column (name="부채총계_비유동부채_기타장기채무")
	private Double 부채총계_비유동부채_기타장기채무;
	@Column (name="부채총계_비유동부채_보증금")
	private Double 부채총계_비유동부채_보증금;
	@Column (name="부채총계_비유동부채_퇴직급여충당부채")
	private Double 부채총계_비유동부채_퇴직급여충당부채;
	@Column (name="부채총계_비유동부채_장기금융예수금")
	private Double 부채총계_비유동부채_장기금융예수금;
	@Column (name="부채총계_비유동부채_책임준비금")
	private Double 부채총계_비유동부채_책임준비금;
	@Column (name="부채총계_비유동부채_퇴직연금미지급금")
	private Double 부채총계_비유동부채_퇴직연금미지급금;
	@Column (name="부채총계_비유동부채_장기부채성충당부채")
	private Double 부채총계_비유동부채_장기부채성충당부채;
	@Column (name="부채총계_비유동부채_기타세법상준비금")
	private Double 부채총계_비유동부채_기타세법상준비금;
	@Column (name="부채총계_비유동부채_파생상품부채")
	private Double 부채총계_비유동부채_파생상품부채;
	@Column (name="부채총계_비유동부채_이연법인세부채")
	private Double 부채총계_비유동부채_이연법인세부채;
	@Column (name="부채총계_비유동부채_기타비유동부채")
	private Double 부채총계_비유동부채_기타비유동부채;
	@Column (name="부채총계_이연부채")
	private Double 부채총계_이연부채;
	@Column (name="자본총계")
	private Double 자본총계;
	@Column (name="자본총계_자본금")
	private Double 자본총계_자본금;
	@Column (name="자본총계_자본금_보통주자본금")
	private Double 자본총계_자본금_보통주자본금;
	@Column (name="자본총계_자본금_우선주자본금")
	private Double 자본총계_자본금_우선주자본금;
	@Column (name="자본총계_자본잉여금")
	private Double 자본총계_자본잉여금;
	@Column (name="자본총계_자본잉여금_자본준비금")
	private Double 자본총계_자본잉여금_자본준비금;
	@Column (name="자본총계_자본잉여금_자본준비금_주식발행초과금")
	private Double 자본총계_자본잉여금_자본준비금_주식발행초과금;
	@Column (name="자본총계_자본잉여금_자본준비금_기타자본잉여금")
	private Double 자본총계_자본잉여금_자본준비금_기타자본잉여금;
	@Column (name="자본총계_자본잉여금_재평가적립금")
	private Double 자본총계_자본잉여금_재평가적립금;
	@Column (name="자본총계_자본조정")
	private Double 자본총계_자본조정;
	@Column (name="자본총계_자본조정_주식할인발행차금")
	private Double 자본총계_자본조정_주식할인발행차금;
	@Column (name="자본총계_자본조정_종속회사소유지배회사주식")
	private Double 자본총계_자본조정_종속회사소유지배회사주식;
	@Column (name="자본총계_자본조정_종속회사소유지배회사주식_보통주")
	private Double 자본총계_자본조정_종속회사소유지배회사주식_보통주;
	@Column (name="자본총계_자본조정_종속회사소유지배회사주식_우선주")
	private Double 자본총계_자본조정_종속회사소유지배회사주식_우선주;
	@Column (name="자본총계_자본조정_소액투자제거차액")
	private Double 자본총계_자본조정_소액투자제거차액;
	@Column (name="자본총계_자본조정_배당건설이자")
	private Double 자본총계_자본조정_배당건설이자;
	@Column (name="자본총계_자본조정_자기주식")
	private Double 자본총계_자본조정_자기주식;
	@Column (name="자본총계_자본조정_자기주식처분손실")
	private Double 자본총계_자본조정_자기주식처분손실;
	@Column (name="자본총계_자본조정_전환권대가")
	private Double 자본총계_자본조정_전환권대가;
	@Column (name="자본총계_자본조정_신주인수권대가")
	private Double 자본총계_자본조정_신주인수권대가;
	@Column (name="자본총계_자본조정_미교부주식배당금")
	private Double 자본총계_자본조정_미교부주식배당금;
	@Column (name="자본총계_자본조정_기타자본조정")
	private Double 자본총계_자본조정_기타자본조정;
	@Column (name="자본총계_기타포괄손익누계액")
	private Double 자본총계_기타포괄손익누계액;
	@Column (name="자본총계_기타포괄손익누계액_장기투자증권평가이익")
	private Double 자본총계_기타포괄손익누계액_장기투자증권평가이익;
	@Column (name="자본총계_기타포괄손익누계액_장기투자증권평가손실")
	private Double 자본총계_기타포괄손익누계액_장기투자증권평가손실;
	@Column (name="자본총계_기타포괄손익누계액_해외사업환산이익")
	private Double 자본총계_기타포괄손익누계액_해외사업환산이익;
	@Column (name="자본총계_기타포괄손익누계액_해외사업환산손실")
	private Double 자본총계_기타포괄손익누계액_해외사업환산손실;
	@Column (name="자본총계_기타포괄손익누계액_파생상품평가이익")
	private Double 자본총계_기타포괄손익누계액_파생상품평가이익;
	@Column (name="자본총계_기타포괄손익누계액_파생상품평가손실")
	private Double 자본총계_기타포괄손익누계액_파생상품평가손실;
	@Column (name="자본총계_기타포괄손익누계액_외화환산대")
	private Double 자본총계_기타포괄손익누계액_외화환산대;
	@Column (name="자본총계_기타포괄손익누계액_외화환산차")
	private Double 자본총계_기타포괄손익누계액_외화환산차;
	@Column (name="자본총계_기타포괄손익누계액_국고보조금공사부담금등")
	private Double 자본총계_기타포괄손익누계액_국고보조금공사부담금등;
	@Column (name="자본총계_기타포괄손익누계액_기타")
	private Double 자본총계_기타포괄손익누계액_기타;
	@Column (name="자본총계_이익잉여금")
	private Double 자본총계_이익잉여금;
	@Column (name="자본총계_이익잉여금_이익준비금")
	private Double 자본총계_이익잉여금_이익준비금;
	@Column (name="자본총계_이익잉여금_재무구조개선적립금")
	private Double 자본총계_이익잉여금_재무구조개선적립금;
	@Column (name="자본총계_이익잉여금_기타법정적립금")
	private Double 자본총계_이익잉여금_기타법정적립금;
	@Column (name="자본총계_이익잉여금_기타이익잉여금")
	private Double 자본총계_이익잉여금_기타이익잉여금;
	@Column (name="자본총계_이익잉여금_미처분이익잉여금")
	private Double 자본총계_이익잉여금_미처분이익잉여금;
	@Column (name="자본총계_이익잉여금_차기이월미처분이익잉여금")
	private Double 자본총계_이익잉여금_차기이월미처분이익잉여금;
	@Column (name="자본총계_이익잉여금_소수주주지분초과손실액")
	private Double 자본총계_이익잉여금_소수주주지분초과손실액;
	@Column (name="자본총계_연결조정대")
	private Double 자본총계_연결조정대;
	@Column (name="자본총계_외부주주지분")
	private Double 자본총계_외부주주지분;
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getYmDay() {
		return ymDay;
	}
	public void setYmDay(String ymDay) {
		this.ymDay = ymDay;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public Double get자산총계() {
		return 자산총계;
	}
	public void set자산총계(Double 자산총계) {
		this.자산총계 = 자산총계;
	}
	public Double get유동자산() {
		return 유동자산;
	}
	public void set유동자산(Double 유동자산) {
		this.유동자산 = 유동자산;
	}
	public Double get유동자산_당좌자산() {
		return 유동자산_당좌자산;
	}
	public void set유동자산_당좌자산(Double 유동자산_당좌자산) {
		this.유동자산_당좌자산 = 유동자산_당좌자산;
	}
	public Double get유동자산_당좌자산_현금및현금성자산() {
		return 유동자산_당좌자산_현금및현금성자산;
	}
	public void set유동자산_당좌자산_현금및현금성자산(Double 유동자산_당좌자산_현금및현금성자산) {
		this.유동자산_당좌자산_현금및현금성자산 = 유동자산_당좌자산_현금및현금성자산;
	}
	public Double get유동자산_당좌자산_단기투자자산() {
		return 유동자산_당좌자산_단기투자자산;
	}
	public void set유동자산_당좌자산_단기투자자산(Double 유동자산_당좌자산_단기투자자산) {
		this.유동자산_당좌자산_단기투자자산 = 유동자산_당좌자산_단기투자자산;
	}
	public Double get유동자산_당좌자산_단기투자자산_단기예금() {
		return 유동자산_당좌자산_단기투자자산_단기예금;
	}
	public void set유동자산_당좌자산_단기투자자산_단기예금(Double 유동자산_당좌자산_단기투자자산_단기예금) {
		this.유동자산_당좌자산_단기투자자산_단기예금 = 유동자산_당좌자산_단기투자자산_단기예금;
	}
	public Double get유동자산_당좌자산_단기투자자산_단기매매증권() {
		return 유동자산_당좌자산_단기투자자산_단기매매증권;
	}
	public void set유동자산_당좌자산_단기투자자산_단기매매증권(Double 유동자산_당좌자산_단기투자자산_단기매매증권) {
		this.유동자산_당좌자산_단기투자자산_단기매매증권 = 유동자산_당좌자산_단기투자자산_단기매매증권;
	}
	public Double get유동자산_당좌자산_단기투자자산_매도가능증권() {
		return 유동자산_당좌자산_단기투자자산_매도가능증권;
	}
	public void set유동자산_당좌자산_단기투자자산_매도가능증권(Double 유동자산_당좌자산_단기투자자산_매도가능증권) {
		this.유동자산_당좌자산_단기투자자산_매도가능증권 = 유동자산_당좌자산_단기투자자산_매도가능증권;
	}
	public Double get유동자산_당좌자산_단기투자자산_만기보유증권() {
		return 유동자산_당좌자산_단기투자자산_만기보유증권;
	}
	public void set유동자산_당좌자산_단기투자자산_만기보유증권(Double 유동자산_당좌자산_단기투자자산_만기보유증권) {
		this.유동자산_당좌자산_단기투자자산_만기보유증권 = 유동자산_당좌자산_단기투자자산_만기보유증권;
	}
	public Double get유동자산_당좌자산_단기투자자산_단기대여금() {
		return 유동자산_당좌자산_단기투자자산_단기대여금;
	}
	public void set유동자산_당좌자산_단기투자자산_단기대여금(Double 유동자산_당좌자산_단기투자자산_단기대여금) {
		this.유동자산_당좌자산_단기투자자산_단기대여금 = 유동자산_당좌자산_단기투자자산_단기대여금;
	}
	public Double get유동자산_당좌자산_단기투자자산_기타() {
		return 유동자산_당좌자산_단기투자자산_기타;
	}
	public void set유동자산_당좌자산_단기투자자산_기타(Double 유동자산_당좌자산_단기투자자산_기타) {
		this.유동자산_당좌자산_단기투자자산_기타 = 유동자산_당좌자산_단기투자자산_기타;
	}
	public Double get유동자산_당좌자산_매출채권() {
		return 유동자산_당좌자산_매출채권;
	}
	public void set유동자산_당좌자산_매출채권(Double 유동자산_당좌자산_매출채권) {
		this.유동자산_당좌자산_매출채권 = 유동자산_당좌자산_매출채권;
	}
	public Double get유동자산_당좌자산_매출채권_매출채권() {
		return 유동자산_당좌자산_매출채권_매출채권;
	}
	public void set유동자산_당좌자산_매출채권_매출채권(Double 유동자산_당좌자산_매출채권_매출채권) {
		this.유동자산_당좌자산_매출채권_매출채권 = 유동자산_당좌자산_매출채권_매출채권;
	}
	public Double get유동자산_당좌자산_매출채권_외화매출채권() {
		return 유동자산_당좌자산_매출채권_외화매출채권;
	}
	public void set유동자산_당좌자산_매출채권_외화매출채권(Double 유동자산_당좌자산_매출채권_외화매출채권) {
		this.유동자산_당좌자산_매출채권_외화매출채권 = 유동자산_당좌자산_매출채권_외화매출채권;
	}
	public Double get유동자산_당좌자산_매출채권_공사미수금() {
		return 유동자산_당좌자산_매출채권_공사미수금;
	}
	public void set유동자산_당좌자산_매출채권_공사미수금(Double 유동자산_당좌자산_매출채권_공사미수금) {
		this.유동자산_당좌자산_매출채권_공사미수금 = 유동자산_당좌자산_매출채권_공사미수금;
	}
	public Double get유동자산_당좌자산_매출채권_분양미수금() {
		return 유동자산_당좌자산_매출채권_분양미수금;
	}
	public void set유동자산_당좌자산_매출채권_분양미수금(Double 유동자산_당좌자산_매출채권_분양미수금) {
		this.유동자산_당좌자산_매출채권_분양미수금 = 유동자산_당좌자산_매출채권_분양미수금;
	}
	public Double get유동자산_당좌자산_매출채권_기타() {
		return 유동자산_당좌자산_매출채권_기타;
	}
	public void set유동자산_당좌자산_매출채권_기타(Double 유동자산_당좌자산_매출채권_기타) {
		this.유동자산_당좌자산_매출채권_기타 = 유동자산_당좌자산_매출채권_기타;
	}
	public Double get유동자산_당좌자산_단기대출채권() {
		return 유동자산_당좌자산_단기대출채권;
	}
	public void set유동자산_당좌자산_단기대출채권(Double 유동자산_당좌자산_단기대출채권) {
		this.유동자산_당좌자산_단기대출채권 = 유동자산_당좌자산_단기대출채권;
	}
	public Double get유동자산_당좌자산_미수금() {
		return 유동자산_당좌자산_미수금;
	}
	public void set유동자산_당좌자산_미수금(Double 유동자산_당좌자산_미수금) {
		this.유동자산_당좌자산_미수금 = 유동자산_당좌자산_미수금;
	}
	public Double get유동자산_당좌자산_미수수익() {
		return 유동자산_당좌자산_미수수익;
	}
	public void set유동자산_당좌자산_미수수익(Double 유동자산_당좌자산_미수수익) {
		this.유동자산_당좌자산_미수수익 = 유동자산_당좌자산_미수수익;
	}
	public Double get유동자산_당좌자산_선급금() {
		return 유동자산_당좌자산_선급금;
	}
	public void set유동자산_당좌자산_선급금(Double 유동자산_당좌자산_선급금) {
		this.유동자산_당좌자산_선급금 = 유동자산_당좌자산_선급금;
	}
	public Double get유동자산_당좌자산_선급비용() {
		return 유동자산_당좌자산_선급비용;
	}
	public void set유동자산_당좌자산_선급비용(Double 유동자산_당좌자산_선급비용) {
		this.유동자산_당좌자산_선급비용 = 유동자산_당좌자산_선급비용;
	}
	public Double get유동자산_당좌자산_보증금() {
		return 유동자산_당좌자산_보증금;
	}
	public void set유동자산_당좌자산_보증금(Double 유동자산_당좌자산_보증금) {
		this.유동자산_당좌자산_보증금 = 유동자산_당좌자산_보증금;
	}
	public Double get유동자산_당좌자산_파생상품자산() {
		return 유동자산_당좌자산_파생상품자산;
	}
	public void set유동자산_당좌자산_파생상품자산(Double 유동자산_당좌자산_파생상품자산) {
		this.유동자산_당좌자산_파생상품자산 = 유동자산_당좌자산_파생상품자산;
	}
	public Double get유동자산_당좌자산_이연법인세자산() {
		return 유동자산_당좌자산_이연법인세자산;
	}
	public void set유동자산_당좌자산_이연법인세자산(Double 유동자산_당좌자산_이연법인세자산) {
		this.유동자산_당좌자산_이연법인세자산 = 유동자산_당좌자산_이연법인세자산;
	}
	public Double get유동자산_당좌자산_금융리스채권() {
		return 유동자산_당좌자산_금융리스채권;
	}
	public void set유동자산_당좌자산_금융리스채권(Double 유동자산_당좌자산_금융리스채권) {
		this.유동자산_당좌자산_금융리스채권 = 유동자산_당좌자산_금융리스채권;
	}
	public Double get유동자산_당좌자산_기타당좌자산() {
		return 유동자산_당좌자산_기타당좌자산;
	}
	public void set유동자산_당좌자산_기타당좌자산(Double 유동자산_당좌자산_기타당좌자산) {
		this.유동자산_당좌자산_기타당좌자산 = 유동자산_당좌자산_기타당좌자산;
	}
	public Double get유동자산_재고자산() {
		return 유동자산_재고자산;
	}
	public void set유동자산_재고자산(Double 유동자산_재고자산) {
		this.유동자산_재고자산 = 유동자산_재고자산;
	}
	public Double get유동자산_재고자산_상품() {
		return 유동자산_재고자산_상품;
	}
	public void set유동자산_재고자산_상품(Double 유동자산_재고자산_상품) {
		this.유동자산_재고자산_상품 = 유동자산_재고자산_상품;
	}
	public Double get유동자산_재고자산_제품() {
		return 유동자산_재고자산_제품;
	}
	public void set유동자산_재고자산_제품(Double 유동자산_재고자산_제품) {
		this.유동자산_재고자산_제품 = 유동자산_재고자산_제품;
	}
	public Double get유동자산_재고자산_반제품() {
		return 유동자산_재고자산_반제품;
	}
	public void set유동자산_재고자산_반제품(Double 유동자산_재고자산_반제품) {
		this.유동자산_재고자산_반제품 = 유동자산_재고자산_반제품;
	}
	public Double get유동자산_재고자산_재공품() {
		return 유동자산_재고자산_재공품;
	}
	public void set유동자산_재고자산_재공품(Double 유동자산_재고자산_재공품) {
		this.유동자산_재고자산_재공품 = 유동자산_재고자산_재공품;
	}
	public Double get유동자산_재고자산_원재료() {
		return 유동자산_재고자산_원재료;
	}
	public void set유동자산_재고자산_원재료(Double 유동자산_재고자산_원재료) {
		this.유동자산_재고자산_원재료 = 유동자산_재고자산_원재료;
	}
	public Double get유동자산_재고자산_미착품() {
		return 유동자산_재고자산_미착품;
	}
	public void set유동자산_재고자산_미착품(Double 유동자산_재고자산_미착품) {
		this.유동자산_재고자산_미착품 = 유동자산_재고자산_미착품;
	}
	public Double get유동자산_재고자산_저장품() {
		return 유동자산_재고자산_저장품;
	}
	public void set유동자산_재고자산_저장품(Double 유동자산_재고자산_저장품) {
		this.유동자산_재고자산_저장품 = 유동자산_재고자산_저장품;
	}
	public Double get유동자산_재고자산_기타재고자산() {
		return 유동자산_재고자산_기타재고자산;
	}
	public void set유동자산_재고자산_기타재고자산(Double 유동자산_재고자산_기타재고자산) {
		this.유동자산_재고자산_기타재고자산 = 유동자산_재고자산_기타재고자산;
	}
	public Double get임대주택자산() {
		return 임대주택자산;
	}
	public void set임대주택자산(Double 임대주택자산) {
		this.임대주택자산 = 임대주택자산;
	}
	public Double get비유동자산() {
		return 비유동자산;
	}
	public void set비유동자산(Double 비유동자산) {
		this.비유동자산 = 비유동자산;
	}
	public Double get비유동자산_투자자산() {
		return 비유동자산_투자자산;
	}
	public void set비유동자산_투자자산(Double 비유동자산_투자자산) {
		this.비유동자산_투자자산 = 비유동자산_투자자산;
	}
	public Double get비유동자산_투자자산_장기금융상품() {
		return 비유동자산_투자자산_장기금융상품;
	}
	public void set비유동자산_투자자산_장기금융상품(Double 비유동자산_투자자산_장기금융상품) {
		this.비유동자산_투자자산_장기금융상품 = 비유동자산_투자자산_장기금융상품;
	}
	public Double get비유동자산_투자자산_매도가능증권() {
		return 비유동자산_투자자산_매도가능증권;
	}
	public void set비유동자산_투자자산_매도가능증권(Double 비유동자산_투자자산_매도가능증권) {
		this.비유동자산_투자자산_매도가능증권 = 비유동자산_투자자산_매도가능증권;
	}
	public Double get비유동자산_투자자산_만기보유증권() {
		return 비유동자산_투자자산_만기보유증권;
	}
	public void set비유동자산_투자자산_만기보유증권(Double 비유동자산_투자자산_만기보유증권) {
		this.비유동자산_투자자산_만기보유증권 = 비유동자산_투자자산_만기보유증권;
	}
	public Double get비유동자산_투자자산_지분법적용투자주식() {
		return 비유동자산_투자자산_지분법적용투자주식;
	}
	public void set비유동자산_투자자산_지분법적용투자주식(Double 비유동자산_투자자산_지분법적용투자주식) {
		this.비유동자산_투자자산_지분법적용투자주식 = 비유동자산_투자자산_지분법적용투자주식;
	}
	public Double get비유동자산_투자자산_신주인수권() {
		return 비유동자산_투자자산_신주인수권;
	}
	public void set비유동자산_투자자산_신주인수권(Double 비유동자산_투자자산_신주인수권) {
		this.비유동자산_투자자산_신주인수권 = 비유동자산_투자자산_신주인수권;
	}
	public Double get비유동자산_투자자산_투자부동산() {
		return 비유동자산_투자자산_투자부동산;
	}
	public void set비유동자산_투자자산_투자부동산(Double 비유동자산_투자자산_투자부동산) {
		this.비유동자산_투자자산_투자부동산 = 비유동자산_투자자산_투자부동산;
	}
	public Double get비유동자산_투자자산_장기대여금() {
		return 비유동자산_투자자산_장기대여금;
	}
	public void set비유동자산_투자자산_장기대여금(Double 비유동자산_투자자산_장기대여금) {
		this.비유동자산_투자자산_장기대여금 = 비유동자산_투자자산_장기대여금;
	}
	public Double get비유동자산_투자자산_장기대출채권() {
		return 비유동자산_투자자산_장기대출채권;
	}
	public void set비유동자산_투자자산_장기대출채권(Double 비유동자산_투자자산_장기대출채권) {
		this.비유동자산_투자자산_장기대출채권 = 비유동자산_투자자산_장기대출채권;
	}
	public Double get비유동자산_투자자산_퇴직보험예치금() {
		return 비유동자산_투자자산_퇴직보험예치금;
	}
	public void set비유동자산_투자자산_퇴직보험예치금(Double 비유동자산_투자자산_퇴직보험예치금) {
		this.비유동자산_투자자산_퇴직보험예치금 = 비유동자산_투자자산_퇴직보험예치금;
	}
	public Double get비유동자산_투자자산_퇴직연금운용자산() {
		return 비유동자산_투자자산_퇴직연금운용자산;
	}
	public void set비유동자산_투자자산_퇴직연금운용자산(Double 비유동자산_투자자산_퇴직연금운용자산) {
		this.비유동자산_투자자산_퇴직연금운용자산 = 비유동자산_투자자산_퇴직연금운용자산;
	}
	public Double get비유동자산_투자자산_파생상품자산() {
		return 비유동자산_투자자산_파생상품자산;
	}
	public void set비유동자산_투자자산_파생상품자산(Double 비유동자산_투자자산_파생상품자산) {
		this.비유동자산_투자자산_파생상품자산 = 비유동자산_투자자산_파생상품자산;
	}
	public Double get비유동자산_투자자산_기타투자자산() {
		return 비유동자산_투자자산_기타투자자산;
	}
	public void set비유동자산_투자자산_기타투자자산(Double 비유동자산_투자자산_기타투자자산) {
		this.비유동자산_투자자산_기타투자자산 = 비유동자산_투자자산_기타투자자산;
	}
	public Double get비유동자산_유형자산() {
		return 비유동자산_유형자산;
	}
	public void set비유동자산_유형자산(Double 비유동자산_유형자산) {
		this.비유동자산_유형자산 = 비유동자산_유형자산;
	}
	public Double get비유동자산_유형자산_토지() {
		return 비유동자산_유형자산_토지;
	}
	public void set비유동자산_유형자산_토지(Double 비유동자산_유형자산_토지) {
		this.비유동자산_유형자산_토지 = 비유동자산_유형자산_토지;
	}
	public Double get비유동자산_유형자산_입목() {
		return 비유동자산_유형자산_입목;
	}
	public void set비유동자산_유형자산_입목(Double 비유동자산_유형자산_입목) {
		this.비유동자산_유형자산_입목 = 비유동자산_유형자산_입목;
	}
	public Double get비유동자산_유형자산_건설중인자산() {
		return 비유동자산_유형자산_건설중인자산;
	}
	public void set비유동자산_유형자산_건설중인자산(Double 비유동자산_유형자산_건설중인자산) {
		this.비유동자산_유형자산_건설중인자산 = 비유동자산_유형자산_건설중인자산;
	}
	public Double get비유동자산_유형자산_설비자산() {
		return 비유동자산_유형자산_설비자산;
	}
	public void set비유동자산_유형자산_설비자산(Double 비유동자산_유형자산_설비자산) {
		this.비유동자산_유형자산_설비자산 = 비유동자산_유형자산_설비자산;
	}
	public Double get비유동자산_유형자산_설비자산_건물및부속설비() {
		return 비유동자산_유형자산_설비자산_건물및부속설비;
	}
	public void set비유동자산_유형자산_설비자산_건물및부속설비(Double 비유동자산_유형자산_설비자산_건물및부속설비) {
		this.비유동자산_유형자산_설비자산_건물및부속설비 = 비유동자산_유형자산_설비자산_건물및부속설비;
	}
	public Double get비유동자산_유형자산_설비자산_구축물() {
		return 비유동자산_유형자산_설비자산_구축물;
	}
	public void set비유동자산_유형자산_설비자산_구축물(Double 비유동자산_유형자산_설비자산_구축물) {
		this.비유동자산_유형자산_설비자산_구축물 = 비유동자산_유형자산_설비자산_구축물;
	}
	public Double get비유동자산_유형자산_설비자산_기계장치() {
		return 비유동자산_유형자산_설비자산_기계장치;
	}
	public void set비유동자산_유형자산_설비자산_기계장치(Double 비유동자산_유형자산_설비자산_기계장치) {
		this.비유동자산_유형자산_설비자산_기계장치 = 비유동자산_유형자산_설비자산_기계장치;
	}
	public Double get비유동자산_유형자산_설비자산_중장비() {
		return 비유동자산_유형자산_설비자산_중장비;
	}
	public void set비유동자산_유형자산_설비자산_중장비(Double 비유동자산_유형자산_설비자산_중장비) {
		this.비유동자산_유형자산_설비자산_중장비 = 비유동자산_유형자산_설비자산_중장비;
	}
	public Double get비유동자산_유형자산_설비자산_제설비() {
		return 비유동자산_유형자산_설비자산_제설비;
	}
	public void set비유동자산_유형자산_설비자산_제설비(Double 비유동자산_유형자산_설비자산_제설비) {
		this.비유동자산_유형자산_설비자산_제설비 = 비유동자산_유형자산_설비자산_제설비;
	}
	public Double get비유동자산_유형자산_설비자산_미착자산() {
		return 비유동자산_유형자산_설비자산_미착자산;
	}
	public void set비유동자산_유형자산_설비자산_미착자산(Double 비유동자산_유형자산_설비자산_미착자산) {
		this.비유동자산_유형자산_설비자산_미착자산 = 비유동자산_유형자산_설비자산_미착자산;
	}
	public Double get비유동자산_유형자산_설비자산_차량운반구() {
		return 비유동자산_유형자산_설비자산_차량운반구;
	}
	public void set비유동자산_유형자산_설비자산_차량운반구(Double 비유동자산_유형자산_설비자산_차량운반구) {
		this.비유동자산_유형자산_설비자산_차량운반구 = 비유동자산_유형자산_설비자산_차량운반구;
	}
	public Double get비유동자산_유형자산_설비자산_선박() {
		return 비유동자산_유형자산_설비자산_선박;
	}
	public void set비유동자산_유형자산_설비자산_선박(Double 비유동자산_유형자산_설비자산_선박) {
		this.비유동자산_유형자산_설비자산_선박 = 비유동자산_유형자산_설비자산_선박;
	}
	public Double get비유동자산_유형자산_설비자산_항공기() {
		return 비유동자산_유형자산_설비자산_항공기;
	}
	public void set비유동자산_유형자산_설비자산_항공기(Double 비유동자산_유형자산_설비자산_항공기) {
		this.비유동자산_유형자산_설비자산_항공기 = 비유동자산_유형자산_설비자산_항공기;
	}
	public Double get비유동자산_유형자산_설비자산_공구기구비품() {
		return 비유동자산_유형자산_설비자산_공구기구비품;
	}
	public void set비유동자산_유형자산_설비자산_공구기구비품(Double 비유동자산_유형자산_설비자산_공구기구비품) {
		this.비유동자산_유형자산_설비자산_공구기구비품 = 비유동자산_유형자산_설비자산_공구기구비품;
	}
	public Double get비유동자산_유형자산_설비자산_리스자산() {
		return 비유동자산_유형자산_설비자산_리스자산;
	}
	public void set비유동자산_유형자산_설비자산_리스자산(Double 비유동자산_유형자산_설비자산_리스자산) {
		this.비유동자산_유형자산_설비자산_리스자산 = 비유동자산_유형자산_설비자산_리스자산;
	}
	public Double get비유동자산_유형자산_설비자산_리스개량자산() {
		return 비유동자산_유형자산_설비자산_리스개량자산;
	}
	public void set비유동자산_유형자산_설비자산_리스개량자산(Double 비유동자산_유형자산_설비자산_리스개량자산) {
		this.비유동자산_유형자산_설비자산_리스개량자산 = 비유동자산_유형자산_설비자산_리스개량자산;
	}
	public Double get비유동자산_유형자산_설비자산_임대자산() {
		return 비유동자산_유형자산_설비자산_임대자산;
	}
	public void set비유동자산_유형자산_설비자산_임대자산(Double 비유동자산_유형자산_설비자산_임대자산) {
		this.비유동자산_유형자산_설비자산_임대자산 = 비유동자산_유형자산_설비자산_임대자산;
	}
	public Double get비유동자산_유형자산_설비자산_임차개량자산() {
		return 비유동자산_유형자산_설비자산_임차개량자산;
	}
	public void set비유동자산_유형자산_설비자산_임차개량자산(Double 비유동자산_유형자산_설비자산_임차개량자산) {
		this.비유동자산_유형자산_설비자산_임차개량자산 = 비유동자산_유형자산_설비자산_임차개량자산;
	}
	public Double get비유동자산_유형자산_설비자산_기타() {
		return 비유동자산_유형자산_설비자산_기타;
	}
	public void set비유동자산_유형자산_설비자산_기타(Double 비유동자산_유형자산_설비자산_기타) {
		this.비유동자산_유형자산_설비자산_기타 = 비유동자산_유형자산_설비자산_기타;
	}
	public Double get비유동자산_유형자산_기타유형자산() {
		return 비유동자산_유형자산_기타유형자산;
	}
	public void set비유동자산_유형자산_기타유형자산(Double 비유동자산_유형자산_기타유형자산) {
		this.비유동자산_유형자산_기타유형자산 = 비유동자산_유형자산_기타유형자산;
	}
	public Double get비유동자산_무형자산() {
		return 비유동자산_무형자산;
	}
	public void set비유동자산_무형자산(Double 비유동자산_무형자산) {
		this.비유동자산_무형자산 = 비유동자산_무형자산;
	}
	public Double get비유동자산_무형자산_영업권() {
		return 비유동자산_무형자산_영업권;
	}
	public void set비유동자산_무형자산_영업권(Double 비유동자산_무형자산_영업권) {
		this.비유동자산_무형자산_영업권 = 비유동자산_무형자산_영업권;
	}
	public Double get비유동자산_무형자산_부의영업권() {
		return 비유동자산_무형자산_부의영업권;
	}
	public void set비유동자산_무형자산_부의영업권(Double 비유동자산_무형자산_부의영업권) {
		this.비유동자산_무형자산_부의영업권 = 비유동자산_무형자산_부의영업권;
	}
	public Double get비유동자산_무형자산_산업재산권() {
		return 비유동자산_무형자산_산업재산권;
	}
	public void set비유동자산_무형자산_산업재산권(Double 비유동자산_무형자산_산업재산권) {
		this.비유동자산_무형자산_산업재산권 = 비유동자산_무형자산_산업재산권;
	}
	public Double get비유동자산_무형자산_개발비() {
		return 비유동자산_무형자산_개발비;
	}
	public void set비유동자산_무형자산_개발비(Double 비유동자산_무형자산_개발비) {
		this.비유동자산_무형자산_개발비 = 비유동자산_무형자산_개발비;
	}
	public Double get비유동자산_무형자산_제이용권() {
		return 비유동자산_무형자산_제이용권;
	}
	public void set비유동자산_무형자산_제이용권(Double 비유동자산_무형자산_제이용권) {
		this.비유동자산_무형자산_제이용권 = 비유동자산_무형자산_제이용권;
	}
	public Double get비유동자산_무형자산_라이센스와프랜차이즈() {
		return 비유동자산_무형자산_라이센스와프랜차이즈;
	}
	public void set비유동자산_무형자산_라이센스와프랜차이즈(Double 비유동자산_무형자산_라이센스와프랜차이즈) {
		this.비유동자산_무형자산_라이센스와프랜차이즈 = 비유동자산_무형자산_라이센스와프랜차이즈;
	}
	public Double get비유동자산_무형자산_저작권() {
		return 비유동자산_무형자산_저작권;
	}
	public void set비유동자산_무형자산_저작권(Double 비유동자산_무형자산_저작권) {
		this.비유동자산_무형자산_저작권 = 비유동자산_무형자산_저작권;
	}
	public Double get비유동자산_무형자산_기타무형자산() {
		return 비유동자산_무형자산_기타무형자산;
	}
	public void set비유동자산_무형자산_기타무형자산(Double 비유동자산_무형자산_기타무형자산) {
		this.비유동자산_무형자산_기타무형자산 = 비유동자산_무형자산_기타무형자산;
	}
	public Double get비유동자산_기타비유동자산() {
		return 비유동자산_기타비유동자산;
	}
	public void set비유동자산_기타비유동자산(Double 비유동자산_기타비유동자산) {
		this.비유동자산_기타비유동자산 = 비유동자산_기타비유동자산;
	}
	public Double get비유동자산_기타비유동자산_장기성매출채권() {
		return 비유동자산_기타비유동자산_장기성매출채권;
	}
	public void set비유동자산_기타비유동자산_장기성매출채권(Double 비유동자산_기타비유동자산_장기성매출채권) {
		this.비유동자산_기타비유동자산_장기성매출채권 = 비유동자산_기타비유동자산_장기성매출채권;
	}
	public Double get비유동자산_기타비유동자산_장기미수금() {
		return 비유동자산_기타비유동자산_장기미수금;
	}
	public void set비유동자산_기타비유동자산_장기미수금(Double 비유동자산_기타비유동자산_장기미수금) {
		this.비유동자산_기타비유동자산_장기미수금 = 비유동자산_기타비유동자산_장기미수금;
	}
	public Double get비유동자산_기타비유동자산_장기미수수익() {
		return 비유동자산_기타비유동자산_장기미수수익;
	}
	public void set비유동자산_기타비유동자산_장기미수수익(Double 비유동자산_기타비유동자산_장기미수수익) {
		this.비유동자산_기타비유동자산_장기미수수익 = 비유동자산_기타비유동자산_장기미수수익;
	}
	public Double get비유동자산_기타비유동자산_보증금() {
		return 비유동자산_기타비유동자산_보증금;
	}
	public void set비유동자산_기타비유동자산_보증금(Double 비유동자산_기타비유동자산_보증금) {
		this.비유동자산_기타비유동자산_보증금 = 비유동자산_기타비유동자산_보증금;
	}
	public Double get비유동자산_기타비유동자산_이연법인세자산() {
		return 비유동자산_기타비유동자산_이연법인세자산;
	}
	public void set비유동자산_기타비유동자산_이연법인세자산(Double 비유동자산_기타비유동자산_이연법인세자산) {
		this.비유동자산_기타비유동자산_이연법인세자산 = 비유동자산_기타비유동자산_이연법인세자산;
	}
	public Double get비유동자산_기타비유동자산_금융리스채권() {
		return 비유동자산_기타비유동자산_금융리스채권;
	}
	public void set비유동자산_기타비유동자산_금융리스채권(Double 비유동자산_기타비유동자산_금융리스채권) {
		this.비유동자산_기타비유동자산_금융리스채권 = 비유동자산_기타비유동자산_금융리스채권;
	}
	public Double get비유동자산_기타비유동자산_파생상품자산() {
		return 비유동자산_기타비유동자산_파생상품자산;
	}
	public void set비유동자산_기타비유동자산_파생상품자산(Double 비유동자산_기타비유동자산_파생상품자산) {
		this.비유동자산_기타비유동자산_파생상품자산 = 비유동자산_기타비유동자산_파생상품자산;
	}
	public Double get비유동자산_기타비유동자산_기타() {
		return 비유동자산_기타비유동자산_기타;
	}
	public void set비유동자산_기타비유동자산_기타(Double 비유동자산_기타비유동자산_기타) {
		this.비유동자산_기타비유동자산_기타 = 비유동자산_기타비유동자산_기타;
	}
	public Double get비유동자산_이연자산() {
		return 비유동자산_이연자산;
	}
	public void set비유동자산_이연자산(Double 비유동자산_이연자산) {
		this.비유동자산_이연자산 = 비유동자산_이연자산;
	}
	public Double get부채총계() {
		return 부채총계;
	}
	public void set부채총계(Double 부채총계) {
		this.부채총계 = 부채총계;
	}
	public Double get부채총계_유동부채() {
		return 부채총계_유동부채;
	}
	public void set부채총계_유동부채(Double 부채총계_유동부채) {
		this.부채총계_유동부채 = 부채총계_유동부채;
	}
	public Double get부채총계_유동부채_매입채무() {
		return 부채총계_유동부채_매입채무;
	}
	public void set부채총계_유동부채_매입채무(Double 부채총계_유동부채_매입채무) {
		this.부채총계_유동부채_매입채무 = 부채총계_유동부채_매입채무;
	}
	public Double get부채총계_유동부채_단기차입금() {
		return 부채총계_유동부채_단기차입금;
	}
	public void set부채총계_유동부채_단기차입금(Double 부채총계_유동부채_단기차입금) {
		this.부채총계_유동부채_단기차입금 = 부채총계_유동부채_단기차입금;
	}
	public Double get부채총계_유동부채_미지급금() {
		return 부채총계_유동부채_미지급금;
	}
	public void set부채총계_유동부채_미지급금(Double 부채총계_유동부채_미지급금) {
		this.부채총계_유동부채_미지급금 = 부채총계_유동부채_미지급금;
	}
	public Double get부채총계_유동부채_선수금() {
		return 부채총계_유동부채_선수금;
	}
	public void set부채총계_유동부채_선수금(Double 부채총계_유동부채_선수금) {
		this.부채총계_유동부채_선수금 = 부채총계_유동부채_선수금;
	}
	public Double get부채총계_유동부채_예수금() {
		return 부채총계_유동부채_예수금;
	}
	public void set부채총계_유동부채_예수금(Double 부채총계_유동부채_예수금) {
		this.부채총계_유동부채_예수금 = 부채총계_유동부채_예수금;
	}
	public Double get부채총계_유동부채_미지급비용() {
		return 부채총계_유동부채_미지급비용;
	}
	public void set부채총계_유동부채_미지급비용(Double 부채총계_유동부채_미지급비용) {
		this.부채총계_유동부채_미지급비용 = 부채총계_유동부채_미지급비용;
	}
	public Double get부채총계_유동부채_단기유동화채무() {
		return 부채총계_유동부채_단기유동화채무;
	}
	public void set부채총계_유동부채_단기유동화채무(Double 부채총계_유동부채_단기유동화채무) {
		this.부채총계_유동부채_단기유동화채무 = 부채총계_유동부채_단기유동화채무;
	}
	public Double get부채총계_유동부채_단기사채() {
		return 부채총계_유동부채_단기사채;
	}
	public void set부채총계_유동부채_단기사채(Double 부채총계_유동부채_단기사채) {
		this.부채총계_유동부채_단기사채 = 부채총계_유동부채_단기사채;
	}
	public Double get부채총계_유동부채_유동성장기부채() {
		return 부채총계_유동부채_유동성장기부채;
	}
	public void set부채총계_유동부채_유동성장기부채(Double 부채총계_유동부채_유동성장기부채) {
		this.부채총계_유동부채_유동성장기부채 = 부채총계_유동부채_유동성장기부채;
	}
	public Double get부채총계_유동부채_유동성장기부채_유동성사채() {
		return 부채총계_유동부채_유동성장기부채_유동성사채;
	}
	public void set부채총계_유동부채_유동성장기부채_유동성사채(Double 부채총계_유동부채_유동성장기부채_유동성사채) {
		this.부채총계_유동부채_유동성장기부채_유동성사채 = 부채총계_유동부채_유동성장기부채_유동성사채;
	}
	public Double get부채총계_유동부채_유동성장기부채_유동성장기차입금() {
		return 부채총계_유동부채_유동성장기부채_유동성장기차입금;
	}
	public void set부채총계_유동부채_유동성장기부채_유동성장기차입금(Double 부채총계_유동부채_유동성장기부채_유동성장기차입금) {
		this.부채총계_유동부채_유동성장기부채_유동성장기차입금 = 부채총계_유동부채_유동성장기부채_유동성장기차입금;
	}
	public Double get부채총계_유동부채_유동성장기부채_유동성외화장기차입금() {
		return 부채총계_유동부채_유동성장기부채_유동성외화장기차입금;
	}
	public void set부채총계_유동부채_유동성장기부채_유동성외화장기차입금(Double 부채총계_유동부채_유동성장기부채_유동성외화장기차입금) {
		this.부채총계_유동부채_유동성장기부채_유동성외화장기차입금 = 부채총계_유동부채_유동성장기부채_유동성외화장기차입금;
	}
	public Double get부채총계_유동부채_유동성장기부채_유동성유동화채무() {
		return 부채총계_유동부채_유동성장기부채_유동성유동화채무;
	}
	public void set부채총계_유동부채_유동성장기부채_유동성유동화채무(Double 부채총계_유동부채_유동성장기부채_유동성유동화채무) {
		this.부채총계_유동부채_유동성장기부채_유동성유동화채무 = 부채총계_유동부채_유동성장기부채_유동성유동화채무;
	}
	public Double get부채총계_유동부채_유동성장기부채_유동성연불매입채무() {
		return 부채총계_유동부채_유동성장기부채_유동성연불매입채무;
	}
	public void set부채총계_유동부채_유동성장기부채_유동성연불매입채무(Double 부채총계_유동부채_유동성장기부채_유동성연불매입채무) {
		this.부채총계_유동부채_유동성장기부채_유동성연불매입채무 = 부채총계_유동부채_유동성장기부채_유동성연불매입채무;
	}
	public Double get부채총계_유동부채_유동성장기부채_기타유동성장기부채() {
		return 부채총계_유동부채_유동성장기부채_기타유동성장기부채;
	}
	public void set부채총계_유동부채_유동성장기부채_기타유동성장기부채(Double 부채총계_유동부채_유동성장기부채_기타유동성장기부채) {
		this.부채총계_유동부채_유동성장기부채_기타유동성장기부채 = 부채총계_유동부채_유동성장기부채_기타유동성장기부채;
	}
	public Double get부채총계_유동부채_단기금융예수금() {
		return 부채총계_유동부채_단기금융예수금;
	}
	public void set부채총계_유동부채_단기금융예수금(Double 부채총계_유동부채_단기금융예수금) {
		this.부채총계_유동부채_단기금융예수금 = 부채총계_유동부채_단기금융예수금;
	}
	public Double get부채총계_유동부채_선수수익() {
		return 부채총계_유동부채_선수수익;
	}
	public void set부채총계_유동부채_선수수익(Double 부채총계_유동부채_선수수익) {
		this.부채총계_유동부채_선수수익 = 부채총계_유동부채_선수수익;
	}
	public Double get부채총계_유동부채_금융리스부채() {
		return 부채총계_유동부채_금융리스부채;
	}
	public void set부채총계_유동부채_금융리스부채(Double 부채총계_유동부채_금융리스부채) {
		this.부채총계_유동부채_금융리스부채 = 부채총계_유동부채_금융리스부채;
	}
	public Double get부채총계_유동부채_단기부채성충당부채() {
		return 부채총계_유동부채_단기부채성충당부채;
	}
	public void set부채총계_유동부채_단기부채성충당부채(Double 부채총계_유동부채_단기부채성충당부채) {
		this.부채총계_유동부채_단기부채성충당부채 = 부채총계_유동부채_단기부채성충당부채;
	}
	public Double get부채총계_유동부채_보증금() {
		return 부채총계_유동부채_보증금;
	}
	public void set부채총계_유동부채_보증금(Double 부채총계_유동부채_보증금) {
		this.부채총계_유동부채_보증금 = 부채총계_유동부채_보증금;
	}
	public Double get부채총계_유동부채_파생상품부채() {
		return 부채총계_유동부채_파생상품부채;
	}
	public void set부채총계_유동부채_파생상품부채(Double 부채총계_유동부채_파생상품부채) {
		this.부채총계_유동부채_파생상품부채 = 부채총계_유동부채_파생상품부채;
	}
	public Double get부채총계_유동부채_이연법인세부채() {
		return 부채총계_유동부채_이연법인세부채;
	}
	public void set부채총계_유동부채_이연법인세부채(Double 부채총계_유동부채_이연법인세부채) {
		this.부채총계_유동부채_이연법인세부채 = 부채총계_유동부채_이연법인세부채;
	}
	public Double get부채총계_유동부채_기타유동부채() {
		return 부채총계_유동부채_기타유동부채;
	}
	public void set부채총계_유동부채_기타유동부채(Double 부채총계_유동부채_기타유동부채) {
		this.부채총계_유동부채_기타유동부채 = 부채총계_유동부채_기타유동부채;
	}
	public Double get부채총계_비유동부채() {
		return 부채총계_비유동부채;
	}
	public void set부채총계_비유동부채(Double 부채총계_비유동부채) {
		this.부채총계_비유동부채 = 부채총계_비유동부채;
	}
	public Double get부채총계_비유동부채_사채() {
		return 부채총계_비유동부채_사채;
	}
	public void set부채총계_비유동부채_사채(Double 부채총계_비유동부채_사채) {
		this.부채총계_비유동부채_사채 = 부채총계_비유동부채_사채;
	}
	public Double get부채총계_비유동부채_사채_사채() {
		return 부채총계_비유동부채_사채_사채;
	}
	public void set부채총계_비유동부채_사채_사채(Double 부채총계_비유동부채_사채_사채) {
		this.부채총계_비유동부채_사채_사채 = 부채총계_비유동부채_사채_사채;
	}
	public Double get부채총계_비유동부채_사채_전환사채() {
		return 부채총계_비유동부채_사채_전환사채;
	}
	public void set부채총계_비유동부채_사채_전환사채(Double 부채총계_비유동부채_사채_전환사채) {
		this.부채총계_비유동부채_사채_전환사채 = 부채총계_비유동부채_사채_전환사채;
	}
	public Double get부채총계_비유동부채_사채_신주인수권부사채() {
		return 부채총계_비유동부채_사채_신주인수권부사채;
	}
	public void set부채총계_비유동부채_사채_신주인수권부사채(Double 부채총계_비유동부채_사채_신주인수권부사채) {
		this.부채총계_비유동부채_사채_신주인수권부사채 = 부채총계_비유동부채_사채_신주인수권부사채;
	}
	public Double get부채총계_비유동부채_사채_교환사채() {
		return 부채총계_비유동부채_사채_교환사채;
	}
	public void set부채총계_비유동부채_사채_교환사채(Double 부채총계_비유동부채_사채_교환사채) {
		this.부채총계_비유동부채_사채_교환사채 = 부채총계_비유동부채_사채_교환사채;
	}
	public Double get부채총계_비유동부채_장기차입금() {
		return 부채총계_비유동부채_장기차입금;
	}
	public void set부채총계_비유동부채_장기차입금(Double 부채총계_비유동부채_장기차입금) {
		this.부채총계_비유동부채_장기차입금 = 부채총계_비유동부채_장기차입금;
	}
	public Double get부채총계_비유동부채_장기차입금_장기차입금() {
		return 부채총계_비유동부채_장기차입금_장기차입금;
	}
	public void set부채총계_비유동부채_장기차입금_장기차입금(Double 부채총계_비유동부채_장기차입금_장기차입금) {
		this.부채총계_비유동부채_장기차입금_장기차입금 = 부채총계_비유동부채_장기차입금_장기차입금;
	}
	public Double get부채총계_비유동부채_장기차입금_외화장기차입금() {
		return 부채총계_비유동부채_장기차입금_외화장기차입금;
	}
	public void set부채총계_비유동부채_장기차입금_외화장기차입금(Double 부채총계_비유동부채_장기차입금_외화장기차입금) {
		this.부채총계_비유동부채_장기차입금_외화장기차입금 = 부채총계_비유동부채_장기차입금_외화장기차입금;
	}
	public Double get부채총계_비유동부채_장기차입금_국민주택기금차입금() {
		return 부채총계_비유동부채_장기차입금_국민주택기금차입금;
	}
	public void set부채총계_비유동부채_장기차입금_국민주택기금차입금(Double 부채총계_비유동부채_장기차입금_국민주택기금차입금) {
		this.부채총계_비유동부채_장기차입금_국민주택기금차입금 = 부채총계_비유동부채_장기차입금_국민주택기금차입금;
	}
	public Double get부채총계_비유동부채_장기차입금_기타() {
		return 부채총계_비유동부채_장기차입금_기타;
	}
	public void set부채총계_비유동부채_장기차입금_기타(Double 부채총계_비유동부채_장기차입금_기타) {
		this.부채총계_비유동부채_장기차입금_기타 = 부채총계_비유동부채_장기차입금_기타;
	}
	public Double get부채총계_비유동부채_금융리스부채() {
		return 부채총계_비유동부채_금융리스부채;
	}
	public void set부채총계_비유동부채_금융리스부채(Double 부채총계_비유동부채_금융리스부채) {
		this.부채총계_비유동부채_금융리스부채 = 부채총계_비유동부채_금융리스부채;
	}
	public Double get부채총계_비유동부채_장기성매입채무() {
		return 부채총계_비유동부채_장기성매입채무;
	}
	public void set부채총계_비유동부채_장기성매입채무(Double 부채총계_비유동부채_장기성매입채무) {
		this.부채총계_비유동부채_장기성매입채무 = 부채총계_비유동부채_장기성매입채무;
	}
	public Double get부채총계_비유동부채_장기선수금() {
		return 부채총계_비유동부채_장기선수금;
	}
	public void set부채총계_비유동부채_장기선수금(Double 부채총계_비유동부채_장기선수금) {
		this.부채총계_비유동부채_장기선수금 = 부채총계_비유동부채_장기선수금;
	}
	public Double get부채총계_비유동부채_장기미지급금() {
		return 부채총계_비유동부채_장기미지급금;
	}
	public void set부채총계_비유동부채_장기미지급금(Double 부채총계_비유동부채_장기미지급금) {
		this.부채총계_비유동부채_장기미지급금 = 부채총계_비유동부채_장기미지급금;
	}
	public Double get부채총계_비유동부채_장기미지급비용() {
		return 부채총계_비유동부채_장기미지급비용;
	}
	public void set부채총계_비유동부채_장기미지급비용(Double 부채총계_비유동부채_장기미지급비용) {
		this.부채총계_비유동부채_장기미지급비용 = 부채총계_비유동부채_장기미지급비용;
	}
	public Double get부채총계_비유동부채_장기성예수금() {
		return 부채총계_비유동부채_장기성예수금;
	}
	public void set부채총계_비유동부채_장기성예수금(Double 부채총계_비유동부채_장기성예수금) {
		this.부채총계_비유동부채_장기성예수금 = 부채총계_비유동부채_장기성예수금;
	}
	public Double get부채총계_비유동부채_장기선수수익() {
		return 부채총계_비유동부채_장기선수수익;
	}
	public void set부채총계_비유동부채_장기선수수익(Double 부채총계_비유동부채_장기선수수익) {
		this.부채총계_비유동부채_장기선수수익 = 부채총계_비유동부채_장기선수수익;
	}
	public Double get부채총계_비유동부채_장기유동화채무() {
		return 부채총계_비유동부채_장기유동화채무;
	}
	public void set부채총계_비유동부채_장기유동화채무(Double 부채총계_비유동부채_장기유동화채무) {
		this.부채총계_비유동부채_장기유동화채무 = 부채총계_비유동부채_장기유동화채무;
	}
	public Double get부채총계_비유동부채_기타장기채무() {
		return 부채총계_비유동부채_기타장기채무;
	}
	public void set부채총계_비유동부채_기타장기채무(Double 부채총계_비유동부채_기타장기채무) {
		this.부채총계_비유동부채_기타장기채무 = 부채총계_비유동부채_기타장기채무;
	}
	public Double get부채총계_비유동부채_보증금() {
		return 부채총계_비유동부채_보증금;
	}
	public void set부채총계_비유동부채_보증금(Double 부채총계_비유동부채_보증금) {
		this.부채총계_비유동부채_보증금 = 부채총계_비유동부채_보증금;
	}
	public Double get부채총계_비유동부채_퇴직급여충당부채() {
		return 부채총계_비유동부채_퇴직급여충당부채;
	}
	public void set부채총계_비유동부채_퇴직급여충당부채(Double 부채총계_비유동부채_퇴직급여충당부채) {
		this.부채총계_비유동부채_퇴직급여충당부채 = 부채총계_비유동부채_퇴직급여충당부채;
	}
	public Double get부채총계_비유동부채_장기금융예수금() {
		return 부채총계_비유동부채_장기금융예수금;
	}
	public void set부채총계_비유동부채_장기금융예수금(Double 부채총계_비유동부채_장기금융예수금) {
		this.부채총계_비유동부채_장기금융예수금 = 부채총계_비유동부채_장기금융예수금;
	}
	public Double get부채총계_비유동부채_책임준비금() {
		return 부채총계_비유동부채_책임준비금;
	}
	public void set부채총계_비유동부채_책임준비금(Double 부채총계_비유동부채_책임준비금) {
		this.부채총계_비유동부채_책임준비금 = 부채총계_비유동부채_책임준비금;
	}
	public Double get부채총계_비유동부채_퇴직연금미지급금() {
		return 부채총계_비유동부채_퇴직연금미지급금;
	}
	public void set부채총계_비유동부채_퇴직연금미지급금(Double 부채총계_비유동부채_퇴직연금미지급금) {
		this.부채총계_비유동부채_퇴직연금미지급금 = 부채총계_비유동부채_퇴직연금미지급금;
	}
	public Double get부채총계_비유동부채_장기부채성충당부채() {
		return 부채총계_비유동부채_장기부채성충당부채;
	}
	public void set부채총계_비유동부채_장기부채성충당부채(Double 부채총계_비유동부채_장기부채성충당부채) {
		this.부채총계_비유동부채_장기부채성충당부채 = 부채총계_비유동부채_장기부채성충당부채;
	}
	public Double get부채총계_비유동부채_기타세법상준비금() {
		return 부채총계_비유동부채_기타세법상준비금;
	}
	public void set부채총계_비유동부채_기타세법상준비금(Double 부채총계_비유동부채_기타세법상준비금) {
		this.부채총계_비유동부채_기타세법상준비금 = 부채총계_비유동부채_기타세법상준비금;
	}
	public Double get부채총계_비유동부채_파생상품부채() {
		return 부채총계_비유동부채_파생상품부채;
	}
	public void set부채총계_비유동부채_파생상품부채(Double 부채총계_비유동부채_파생상품부채) {
		this.부채총계_비유동부채_파생상품부채 = 부채총계_비유동부채_파생상품부채;
	}
	public Double get부채총계_비유동부채_이연법인세부채() {
		return 부채총계_비유동부채_이연법인세부채;
	}
	public void set부채총계_비유동부채_이연법인세부채(Double 부채총계_비유동부채_이연법인세부채) {
		this.부채총계_비유동부채_이연법인세부채 = 부채총계_비유동부채_이연법인세부채;
	}
	public Double get부채총계_비유동부채_기타비유동부채() {
		return 부채총계_비유동부채_기타비유동부채;
	}
	public void set부채총계_비유동부채_기타비유동부채(Double 부채총계_비유동부채_기타비유동부채) {
		this.부채총계_비유동부채_기타비유동부채 = 부채총계_비유동부채_기타비유동부채;
	}
	public Double get부채총계_이연부채() {
		return 부채총계_이연부채;
	}
	public void set부채총계_이연부채(Double 부채총계_이연부채) {
		this.부채총계_이연부채 = 부채총계_이연부채;
	}
	public Double get자본총계() {
		return 자본총계;
	}
	public void set자본총계(Double 자본총계) {
		this.자본총계 = 자본총계;
	}
	public Double get자본총계_자본금() {
		return 자본총계_자본금;
	}
	public void set자본총계_자본금(Double 자본총계_자본금) {
		this.자본총계_자본금 = 자본총계_자본금;
	}
	public Double get자본총계_자본금_보통주자본금() {
		return 자본총계_자본금_보통주자본금;
	}
	public void set자본총계_자본금_보통주자본금(Double 자본총계_자본금_보통주자본금) {
		this.자본총계_자본금_보통주자본금 = 자본총계_자본금_보통주자본금;
	}
	public Double get자본총계_자본금_우선주자본금() {
		return 자본총계_자본금_우선주자본금;
	}
	public void set자본총계_자본금_우선주자본금(Double 자본총계_자본금_우선주자본금) {
		this.자본총계_자본금_우선주자본금 = 자본총계_자본금_우선주자본금;
	}
	public Double get자본총계_자본잉여금() {
		return 자본총계_자본잉여금;
	}
	public void set자본총계_자본잉여금(Double 자본총계_자본잉여금) {
		this.자본총계_자본잉여금 = 자본총계_자본잉여금;
	}
	public Double get자본총계_자본잉여금_자본준비금() {
		return 자본총계_자본잉여금_자본준비금;
	}
	public void set자본총계_자본잉여금_자본준비금(Double 자본총계_자본잉여금_자본준비금) {
		this.자본총계_자본잉여금_자본준비금 = 자본총계_자본잉여금_자본준비금;
	}
	public Double get자본총계_자본잉여금_자본준비금_주식발행초과금() {
		return 자본총계_자본잉여금_자본준비금_주식발행초과금;
	}
	public void set자본총계_자본잉여금_자본준비금_주식발행초과금(Double 자본총계_자본잉여금_자본준비금_주식발행초과금) {
		this.자본총계_자본잉여금_자본준비금_주식발행초과금 = 자본총계_자본잉여금_자본준비금_주식발행초과금;
	}
	public Double get자본총계_자본잉여금_자본준비금_기타자본잉여금() {
		return 자본총계_자본잉여금_자본준비금_기타자본잉여금;
	}
	public void set자본총계_자본잉여금_자본준비금_기타자본잉여금(Double 자본총계_자본잉여금_자본준비금_기타자본잉여금) {
		this.자본총계_자본잉여금_자본준비금_기타자본잉여금 = 자본총계_자본잉여금_자본준비금_기타자본잉여금;
	}
	public Double get자본총계_자본잉여금_재평가적립금() {
		return 자본총계_자본잉여금_재평가적립금;
	}
	public void set자본총계_자본잉여금_재평가적립금(Double 자본총계_자본잉여금_재평가적립금) {
		this.자본총계_자본잉여금_재평가적립금 = 자본총계_자본잉여금_재평가적립금;
	}
	public Double get자본총계_자본조정() {
		return 자본총계_자본조정;
	}
	public void set자본총계_자본조정(Double 자본총계_자본조정) {
		this.자본총계_자본조정 = 자본총계_자본조정;
	}
	public Double get자본총계_자본조정_주식할인발행차금() {
		return 자본총계_자본조정_주식할인발행차금;
	}
	public void set자본총계_자본조정_주식할인발행차금(Double 자본총계_자본조정_주식할인발행차금) {
		this.자본총계_자본조정_주식할인발행차금 = 자본총계_자본조정_주식할인발행차금;
	}
	public Double get자본총계_자본조정_종속회사소유지배회사주식() {
		return 자본총계_자본조정_종속회사소유지배회사주식;
	}
	public void set자본총계_자본조정_종속회사소유지배회사주식(Double 자본총계_자본조정_종속회사소유지배회사주식) {
		this.자본총계_자본조정_종속회사소유지배회사주식 = 자본총계_자본조정_종속회사소유지배회사주식;
	}
	public Double get자본총계_자본조정_종속회사소유지배회사주식_보통주() {
		return 자본총계_자본조정_종속회사소유지배회사주식_보통주;
	}
	public void set자본총계_자본조정_종속회사소유지배회사주식_보통주(Double 자본총계_자본조정_종속회사소유지배회사주식_보통주) {
		this.자본총계_자본조정_종속회사소유지배회사주식_보통주 = 자본총계_자본조정_종속회사소유지배회사주식_보통주;
	}
	public Double get자본총계_자본조정_종속회사소유지배회사주식_우선주() {
		return 자본총계_자본조정_종속회사소유지배회사주식_우선주;
	}
	public void set자본총계_자본조정_종속회사소유지배회사주식_우선주(Double 자본총계_자본조정_종속회사소유지배회사주식_우선주) {
		this.자본총계_자본조정_종속회사소유지배회사주식_우선주 = 자본총계_자본조정_종속회사소유지배회사주식_우선주;
	}
	public Double get자본총계_자본조정_소액투자제거차액() {
		return 자본총계_자본조정_소액투자제거차액;
	}
	public void set자본총계_자본조정_소액투자제거차액(Double 자본총계_자본조정_소액투자제거차액) {
		this.자본총계_자본조정_소액투자제거차액 = 자본총계_자본조정_소액투자제거차액;
	}
	public Double get자본총계_자본조정_배당건설이자() {
		return 자본총계_자본조정_배당건설이자;
	}
	public void set자본총계_자본조정_배당건설이자(Double 자본총계_자본조정_배당건설이자) {
		this.자본총계_자본조정_배당건설이자 = 자본총계_자본조정_배당건설이자;
	}
	public Double get자본총계_자본조정_자기주식() {
		return 자본총계_자본조정_자기주식;
	}
	public void set자본총계_자본조정_자기주식(Double 자본총계_자본조정_자기주식) {
		this.자본총계_자본조정_자기주식 = 자본총계_자본조정_자기주식;
	}
	public Double get자본총계_자본조정_자기주식처분손실() {
		return 자본총계_자본조정_자기주식처분손실;
	}
	public void set자본총계_자본조정_자기주식처분손실(Double 자본총계_자본조정_자기주식처분손실) {
		this.자본총계_자본조정_자기주식처분손실 = 자본총계_자본조정_자기주식처분손실;
	}
	public Double get자본총계_자본조정_전환권대가() {
		return 자본총계_자본조정_전환권대가;
	}
	public void set자본총계_자본조정_전환권대가(Double 자본총계_자본조정_전환권대가) {
		this.자본총계_자본조정_전환권대가 = 자본총계_자본조정_전환권대가;
	}
	public Double get자본총계_자본조정_신주인수권대가() {
		return 자본총계_자본조정_신주인수권대가;
	}
	public void set자본총계_자본조정_신주인수권대가(Double 자본총계_자본조정_신주인수권대가) {
		this.자본총계_자본조정_신주인수권대가 = 자본총계_자본조정_신주인수권대가;
	}
	public Double get자본총계_자본조정_미교부주식배당금() {
		return 자본총계_자본조정_미교부주식배당금;
	}
	public void set자본총계_자본조정_미교부주식배당금(Double 자본총계_자본조정_미교부주식배당금) {
		this.자본총계_자본조정_미교부주식배당금 = 자본총계_자본조정_미교부주식배당금;
	}
	public Double get자본총계_자본조정_기타자본조정() {
		return 자본총계_자본조정_기타자본조정;
	}
	public void set자본총계_자본조정_기타자본조정(Double 자본총계_자본조정_기타자본조정) {
		this.자본총계_자본조정_기타자본조정 = 자본총계_자본조정_기타자본조정;
	}
	public Double get자본총계_기타포괄손익누계액() {
		return 자본총계_기타포괄손익누계액;
	}
	public void set자본총계_기타포괄손익누계액(Double 자본총계_기타포괄손익누계액) {
		this.자본총계_기타포괄손익누계액 = 자본총계_기타포괄손익누계액;
	}
	public Double get자본총계_기타포괄손익누계액_장기투자증권평가이익() {
		return 자본총계_기타포괄손익누계액_장기투자증권평가이익;
	}
	public void set자본총계_기타포괄손익누계액_장기투자증권평가이익(Double 자본총계_기타포괄손익누계액_장기투자증권평가이익) {
		this.자본총계_기타포괄손익누계액_장기투자증권평가이익 = 자본총계_기타포괄손익누계액_장기투자증권평가이익;
	}
	public Double get자본총계_기타포괄손익누계액_장기투자증권평가손실() {
		return 자본총계_기타포괄손익누계액_장기투자증권평가손실;
	}
	public void set자본총계_기타포괄손익누계액_장기투자증권평가손실(Double 자본총계_기타포괄손익누계액_장기투자증권평가손실) {
		this.자본총계_기타포괄손익누계액_장기투자증권평가손실 = 자본총계_기타포괄손익누계액_장기투자증권평가손실;
	}
	public Double get자본총계_기타포괄손익누계액_해외사업환산이익() {
		return 자본총계_기타포괄손익누계액_해외사업환산이익;
	}
	public void set자본총계_기타포괄손익누계액_해외사업환산이익(Double 자본총계_기타포괄손익누계액_해외사업환산이익) {
		this.자본총계_기타포괄손익누계액_해외사업환산이익 = 자본총계_기타포괄손익누계액_해외사업환산이익;
	}
	public Double get자본총계_기타포괄손익누계액_해외사업환산손실() {
		return 자본총계_기타포괄손익누계액_해외사업환산손실;
	}
	public void set자본총계_기타포괄손익누계액_해외사업환산손실(Double 자본총계_기타포괄손익누계액_해외사업환산손실) {
		this.자본총계_기타포괄손익누계액_해외사업환산손실 = 자본총계_기타포괄손익누계액_해외사업환산손실;
	}
	public Double get자본총계_기타포괄손익누계액_파생상품평가이익() {
		return 자본총계_기타포괄손익누계액_파생상품평가이익;
	}
	public void set자본총계_기타포괄손익누계액_파생상품평가이익(Double 자본총계_기타포괄손익누계액_파생상품평가이익) {
		this.자본총계_기타포괄손익누계액_파생상품평가이익 = 자본총계_기타포괄손익누계액_파생상품평가이익;
	}
	public Double get자본총계_기타포괄손익누계액_파생상품평가손실() {
		return 자본총계_기타포괄손익누계액_파생상품평가손실;
	}
	public void set자본총계_기타포괄손익누계액_파생상품평가손실(Double 자본총계_기타포괄손익누계액_파생상품평가손실) {
		this.자본총계_기타포괄손익누계액_파생상품평가손실 = 자본총계_기타포괄손익누계액_파생상품평가손실;
	}
	public Double get자본총계_기타포괄손익누계액_외화환산대() {
		return 자본총계_기타포괄손익누계액_외화환산대;
	}
	public void set자본총계_기타포괄손익누계액_외화환산대(Double 자본총계_기타포괄손익누계액_외화환산대) {
		this.자본총계_기타포괄손익누계액_외화환산대 = 자본총계_기타포괄손익누계액_외화환산대;
	}
	public Double get자본총계_기타포괄손익누계액_외화환산차() {
		return 자본총계_기타포괄손익누계액_외화환산차;
	}
	public void set자본총계_기타포괄손익누계액_외화환산차(Double 자본총계_기타포괄손익누계액_외화환산차) {
		this.자본총계_기타포괄손익누계액_외화환산차 = 자본총계_기타포괄손익누계액_외화환산차;
	}
	public Double get자본총계_기타포괄손익누계액_국고보조금공사부담금등() {
		return 자본총계_기타포괄손익누계액_국고보조금공사부담금등;
	}
	public void set자본총계_기타포괄손익누계액_국고보조금공사부담금등(Double 자본총계_기타포괄손익누계액_국고보조금공사부담금등) {
		this.자본총계_기타포괄손익누계액_국고보조금공사부담금등 = 자본총계_기타포괄손익누계액_국고보조금공사부담금등;
	}
	public Double get자본총계_기타포괄손익누계액_기타() {
		return 자본총계_기타포괄손익누계액_기타;
	}
	public void set자본총계_기타포괄손익누계액_기타(Double 자본총계_기타포괄손익누계액_기타) {
		this.자본총계_기타포괄손익누계액_기타 = 자본총계_기타포괄손익누계액_기타;
	}
	public Double get자본총계_이익잉여금() {
		return 자본총계_이익잉여금;
	}
	public void set자본총계_이익잉여금(Double 자본총계_이익잉여금) {
		this.자본총계_이익잉여금 = 자본총계_이익잉여금;
	}
	public Double get자본총계_이익잉여금_이익준비금() {
		return 자본총계_이익잉여금_이익준비금;
	}
	public void set자본총계_이익잉여금_이익준비금(Double 자본총계_이익잉여금_이익준비금) {
		this.자본총계_이익잉여금_이익준비금 = 자본총계_이익잉여금_이익준비금;
	}
	public Double get자본총계_이익잉여금_재무구조개선적립금() {
		return 자본총계_이익잉여금_재무구조개선적립금;
	}
	public void set자본총계_이익잉여금_재무구조개선적립금(Double 자본총계_이익잉여금_재무구조개선적립금) {
		this.자본총계_이익잉여금_재무구조개선적립금 = 자본총계_이익잉여금_재무구조개선적립금;
	}
	public Double get자본총계_이익잉여금_기타법정적립금() {
		return 자본총계_이익잉여금_기타법정적립금;
	}
	public void set자본총계_이익잉여금_기타법정적립금(Double 자본총계_이익잉여금_기타법정적립금) {
		this.자본총계_이익잉여금_기타법정적립금 = 자본총계_이익잉여금_기타법정적립금;
	}
	public Double get자본총계_이익잉여금_기타이익잉여금() {
		return 자본총계_이익잉여금_기타이익잉여금;
	}
	public void set자본총계_이익잉여금_기타이익잉여금(Double 자본총계_이익잉여금_기타이익잉여금) {
		this.자본총계_이익잉여금_기타이익잉여금 = 자본총계_이익잉여금_기타이익잉여금;
	}
	public Double get자본총계_이익잉여금_미처분이익잉여금() {
		return 자본총계_이익잉여금_미처분이익잉여금;
	}
	public void set자본총계_이익잉여금_미처분이익잉여금(Double 자본총계_이익잉여금_미처분이익잉여금) {
		this.자본총계_이익잉여금_미처분이익잉여금 = 자본총계_이익잉여금_미처분이익잉여금;
	}
	public Double get자본총계_이익잉여금_차기이월미처분이익잉여금() {
		return 자본총계_이익잉여금_차기이월미처분이익잉여금;
	}
	public void set자본총계_이익잉여금_차기이월미처분이익잉여금(Double 자본총계_이익잉여금_차기이월미처분이익잉여금) {
		this.자본총계_이익잉여금_차기이월미처분이익잉여금 = 자본총계_이익잉여금_차기이월미처분이익잉여금;
	}
	public Double get자본총계_이익잉여금_소수주주지분초과손실액() {
		return 자본총계_이익잉여금_소수주주지분초과손실액;
	}
	public void set자본총계_이익잉여금_소수주주지분초과손실액(Double 자본총계_이익잉여금_소수주주지분초과손실액) {
		this.자본총계_이익잉여금_소수주주지분초과손실액 = 자본총계_이익잉여금_소수주주지분초과손실액;
	}
	public Double get자본총계_연결조정대() {
		return 자본총계_연결조정대;
	}
	public void set자본총계_연결조정대(Double 자본총계_연결조정대) {
		this.자본총계_연결조정대 = 자본총계_연결조정대;
	}
	public Double get자본총계_외부주주지분() {
		return 자본총계_외부주주지분;
	}
	public void set자본총계_외부주주지분(Double 자본총계_외부주주지분) {
		this.자본총계_외부주주지분 = 자본총계_외부주주지분;
	}	

}
