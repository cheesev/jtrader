package com.app4j.tradeobserver.data.entity.id;

import java.io.Serializable;

import com.app4j.tradeobserver.data.entity.CashFlowStatementGAAPDto;

public class CashFlowStatementGAAPDtoId implements Serializable {
	
	protected String stockCode;
	protected String ymDay;
	protected String dateType;
	protected String dataType;
	
	public CashFlowStatementGAAPDtoId() {
	}

	public CashFlowStatementGAAPDtoId(String stockCode, String ymDay, String dateType, String dataType) {
		this.ymDay = ymDay;
		this.stockCode = stockCode;
		this.dateType = dateType;
		this.dataType = dataType;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		CashFlowStatementGAAPDto cashFlowStatementGAAPDto = (CashFlowStatementGAAPDto) o;

		return stockCode.equals(cashFlowStatementGAAPDto.getStockCode())
				&& ymDay.equals(cashFlowStatementGAAPDto.getYmDay())
			    && dateType.equals(cashFlowStatementGAAPDto.getDateType())
			    && dataType.equals(cashFlowStatementGAAPDto.getDataType());
	}

	public int hashCode() {
		int result;
		result = dataType.hashCode();
		result = 13 * dateType.hashCode();
		result = 17 * result + ymDay.hashCode();
		result = 29 * result + stockCode.hashCode();
		return result;
	}

}
