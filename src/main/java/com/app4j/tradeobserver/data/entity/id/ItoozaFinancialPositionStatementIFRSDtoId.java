package com.app4j.tradeobserver.data.entity.id;

import java.io.Serializable;

import com.app4j.tradeobserver.data.entity.ItoozaFinancialPositionStatementIFRSDto;

public class ItoozaFinancialPositionStatementIFRSDtoId implements Serializable {
	
	protected String stockCode;
	protected String ymDay;
	protected String dateType;
	
	public ItoozaFinancialPositionStatementIFRSDtoId() {
	}

	public ItoozaFinancialPositionStatementIFRSDtoId(String stockCode, String ymDay, String dateType) {
		this.ymDay = ymDay;
		this.stockCode = stockCode;
		this.dateType = dateType;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ItoozaFinancialPositionStatementIFRSDto itoozaFinancialPositionStatementIFRSDto = (ItoozaFinancialPositionStatementIFRSDto) o;

		return stockCode.equals(itoozaFinancialPositionStatementIFRSDto.getStockCode())
				&& ymDay.equals(itoozaFinancialPositionStatementIFRSDto.getYmDay())
			    && dateType.equals(itoozaFinancialPositionStatementIFRSDto.getDateType());
	}

	public int hashCode() {
		int result;
		result = 13 * dateType.hashCode();
		result = 17 * result + ymDay.hashCode();
		result = 29 * result + stockCode.hashCode();
		return result;
	}

}
