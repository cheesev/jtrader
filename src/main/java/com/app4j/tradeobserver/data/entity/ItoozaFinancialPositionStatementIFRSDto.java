package com.app4j.tradeobserver.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.app4j.tradeobserver.data.entity.id.ItoozaFinancialPositionStatementIFRSDtoId;

@Entity
@Table (name = "ITOOZA_FINANCIAL_POSITION_STATEMENT_IFRS")
@IdClass(ItoozaFinancialPositionStatementIFRSDtoId.class)
public class ItoozaFinancialPositionStatementIFRSDto {
	
	@Id
    @Column (name = "STOCK_CODE")
    private String stockCode;	
	@Id
	@Column (name="YM_DAY")
	private String ymDay;
	@Id
	@Column (name="DATE_TYPE")
	private String dateType;		// 분기, 연간 (Q, Y)
	
	@Column(name="유동자산")
	private Double 유동자산;
	@Column(name="당좌자산")
	private Double 당좌자산;
	@Column(name="당좌자산_현금및현금성자산")
	private Double 당좌자산_현금및현금성자산;
	@Column(name="당좌자산_단기금융자산")
	private Double 당좌자산_단기금융자산;
	@Column(name="당좌자산_단기금융자산_단기금융상품")
	private Double 당좌자산_단기금융자산_단기금융상품;
	@Column(name="당좌자산_단기금융자산_단기매매금융자산")
	private Double 당좌자산_단기금융자산_단기매매금융자산;
	@Column(name="당좌자산_단기금융자산_단기매도가능금융자산")
	private Double 당좌자산_단기금융자산_단기매도가능금융자산;
	@Column(name="당좌자산_단기금융자산_기타단기금융자산")
	private Double 당좌자산_단기금융자산_기타단기금융자산;
	@Column(name="당좌자산_매출채권및기타채권")
	private Double 당좌자산_매출채권및기타채권;
	@Column(name="당좌자산_매출채권및기타채권_매출채권")
	private Double 당좌자산_매출채권및기타채권_매출채권;
	@Column(name="당좌자산_매출채권및기타채권_미수금")
	private Double 당좌자산_매출채권및기타채권_미수금;
	@Column(name="당좌자산_매출채권및기타채권_단기미청구공사")
	private Double 당좌자산_매출채권및기타채권_단기미청구공사;
	@Column(name="당좌자산_매출채권및기타채권_기타")
	private Double 당좌자산_매출채권및기타채권_기타;
	@Column(name="재고자산")
	private Double 재고자산;
	@Column(name="재고자산_원재료")
	private Double 재고자산_원재료;
	@Column(name="재고자산_재공품")
	private Double 재고자산_재공품;
	@Column(name="재고자산_반제품")
	private Double 재고자산_반제품;
	@Column(name="재고자산_제품")
	private Double 재고자산_제품;
	@Column(name="재고자산_상품")
	private Double 재고자산_상품;
	@Column(name="재고자산_저장품")
	private Double 재고자산_저장품;
	@Column(name="재고자산_용지")
	private Double 재고자산_용지;
	@Column(name="재고자산_완성건물")
	private Double 재고자산_완성건물;
	@Column(name="재고자산_미완성건물공사")
	private Double 재고자산_미완성건물공사;
	@Column(name="재고자산_기타재고자산")
	private Double 재고자산_기타재고자산;
	@Column(name="선급법인세")
	private Double 선급법인세;
	@Column(name="기타유동자산")
	private Double 기타유동자산;
	@Column(name="기타유동자산_선급금등")
	private Double 기타유동자산_선급금등;
	@Column(name="기타유동자산_선급비용")
	private Double 기타유동자산_선급비용;
	@Column(name="기타유동자산_기타")
	private Double 기타유동자산_기타;
	@Column(name="비유동자산")
	private Double 비유동자산;
	@Column(name="투자자산")
	private Double 투자자산;
	@Column(name="투자자산_관계기업등지분관련투자자산")
	private Double 투자자산_관계기업등지분관련투자자산;
	@Column(name="투자자산_관계기업등지분관련투자자산_관계기업투자지분법")
	private Double 투자자산_관계기업등지분관련투자자산_관계기업투자지분법;
	@Column(name="투자자산_관계기업등지분관련투자자산_종속기업투자")
	private Double 투자자산_관계기업등지분관련투자자산_종속기업투자;
	@Column(name="투자자산_관계기업등지분관련투자자산_기타")
	private Double 투자자산_관계기업등지분관련투자자산_기타;
	@Column(name="투자자산_장기매도가능금융자산")
	private Double 투자자산_장기매도가능금융자산;
	@Column(name="투자자산_장기매도가능금융자산_주식")
	private Double 투자자산_장기매도가능금융자산_주식;
	@Column(name="투자자산_장기매도가능금융자산_수익증권")
	private Double 투자자산_장기매도가능금융자산_수익증권;
	@Column(name="투자자산_장기매도가능금융자산_채권")
	private Double 투자자산_장기매도가능금융자산_채권;
	@Column(name="투자자산_장기매도가능금융자산_기타")
	private Double 투자자산_장기매도가능금융자산_기타;
	@Column(name="투자자산_장기만기보유금융자산")
	private Double 투자자산_장기만기보유금융자산;
	@Column(name="투자자산_당기손익인식금융자산")
	private Double 투자자산_당기손익인식금융자산;
	@Column(name="투자자산_기타투자자산")
	private Double 투자자산_기타투자자산;
	@Column(name="유형자산")
	private Double 유형자산;
	@Column(name="유형자산_토지")
	private Double 유형자산_토지;
	@Column(name="유형자산_건물")
	private Double 유형자산_건물;
	@Column(name="유형자산_구축물")
	private Double 유형자산_구축물;
	@Column(name="유형자산_기계장치")
	private Double 유형자산_기계장치;
	@Column(name="유형자산_중장비")
	private Double 유형자산_중장비;
	@Column(name="유형자산_선박")
	private Double 유형자산_선박;
	@Column(name="유형자산_항공기")
	private Double 유형자산_항공기;
	@Column(name="유형자산_건설중인자산")
	private Double 유형자산_건설중인자산;
	@Column(name="유형자산_기타유형자산")
	private Double 유형자산_기타유형자산;
	@Column(name="무형자산")
	private Double 무형자산;
	@Column(name="무형자산_영업권")
	private Double 무형자산_영업권;
	@Column(name="무형자산_특허권")
	private Double 무형자산_특허권;
	@Column(name="무형자산_개발비")
	private Double 무형자산_개발비;
	@Column(name="무형자산_산업재산권")
	private Double 무형자산_산업재산권;
	@Column(name="무형자산_컴퓨터소프트웨어")
	private Double 무형자산_컴퓨터소프트웨어;
	@Column(name="무형자산_브랜드")
	private Double 무형자산_브랜드;
	@Column(name="무형자산_기타무형자산")
	private Double 무형자산_기타무형자산;
	@Column(name="생물자산")
	private Double 생물자산;
	@Column(name="투자부동산")
	private Double 투자부동산;
	@Column(name="투자부동산_토지")
	private Double 투자부동산_토지;
	@Column(name="투자부동산_건물")
	private Double 투자부동산_건물;
	@Column(name="투자부동산_기타투자부동산")
	private Double 투자부동산_기타투자부동산;
	@Column(name="매각예정자산")
	private Double 매각예정자산;
	@Column(name="이연법인세자산")
	private Double 이연법인세자산;
	@Column(name="기타비유동자산")
	private Double 기타비유동자산;
	@Column(name="자산총계")
	private Double 자산총계;
	@Column(name="자산총계지분법적용주석")
	private Double 자산총계지분법적용주석;
	@Column(name="유동부채")
	private Double 유동부채;
	@Column(name="유동부채_매입채무및기타채무")
	private Double 유동부채_매입채무및기타채무;
	@Column(name="유동부채_매입채무및기타채무_매입채무")
	private Double 유동부채_매입채무및기타채무_매입채무;
	@Column(name="유동부채_매입채무및기타채무_미지급금")
	private Double 유동부채_매입채무및기타채무_미지급금;
	@Column(name="유동부채_매입채무및기타채무_미지급비용")
	private Double 유동부채_매입채무및기타채무_미지급비용;
	@Column(name="유동부채_매입채무및기타채무_미지급배당금")
	private Double 유동부채_매입채무및기타채무_미지급배당금;
	@Column(name="유동부채_매입채무및기타채무_예수금")
	private Double 유동부채_매입채무및기타채무_예수금;
	@Column(name="유동부채_매입채무및기타채무_부가세예수금")
	private Double 유동부채_매입채무및기타채무_부가세예수금;
	@Column(name="유동부채_매입채무및기타채무_초과청구공사")
	private Double 유동부채_매입채무및기타채무_초과청구공사;
	@Column(name="유동부채_매입채무및기타채무_기타")
	private Double 유동부채_매입채무및기타채무_기타;
	@Column(name="유동부채_단기차입금")
	private Double 유동부채_단기차입금;
	@Column(name="유동부채_단기사채")
	private Double 유동부채_단기사채;
	@Column(name="유동부채_유동성장기부채")
	private Double 유동부채_유동성장기부채;
	@Column(name="유동부채_유동성장기부채_유동성사채")
	private Double 유동부채_유동성장기부채_유동성사채;
	@Column(name="유동부채_유동성장기부채_유동성장기차입금")
	private Double 유동부채_유동성장기부채_유동성장기차입금;
	@Column(name="유동부채_유동성장기부채_유동성외화장기차입금")
	private Double 유동부채_유동성장기부채_유동성외화장기차입금;
	@Column(name="유동부채_유동성장기부채_기타")
	private Double 유동부채_유동성장기부채_기타;
	@Column(name="유동부채_단기금융부채")
	private Double 유동부채_단기금융부채;
	@Column(name="유동부채_단기충당부채")
	private Double 유동부채_단기충당부채;
	@Column(name="유동부채_당기법인세부채")
	private Double 유동부채_당기법인세부채;
	@Column(name="유동부채_기타유동부채")
	private Double 유동부채_기타유동부채;
	@Column(name="유동부채_기타유동부채_선수금")
	private Double 유동부채_기타유동부채_선수금;
	@Column(name="유동부채_기타유동부채_선수수익")
	private Double 유동부채_기타유동부채_선수수익;
	@Column(name="유동부채_기타유동부채_기타")
	private Double 유동부채_기타유동부채_기타;
	@Column(name="유동부채_기타")
	private Double 유동부채_기타;
	@Column(name="비유동부채")
	private Double 비유동부채;
	@Column(name="비유동부채_사채")
	private Double 비유동부채_사채;
	@Column(name="비유동부채_사채_사채")
	private Double 비유동부채_사채_사채;
	@Column(name="비유동부채_사채_CB")
	private Double 비유동부채_사채_CB;
	@Column(name="비유동부채_사채_BW")
	private Double 비유동부채_사채_BW;
	@Column(name="비유동부채_사채_EB")
	private Double 비유동부채_사채_EB;
	@Column(name="비유동부채_장기차입금")
	private Double 비유동부채_장기차입금;
	@Column(name="비유동부채_장기금융부채")
	private Double 비유동부채_장기금융부채;
	@Column(name="비유동부채_장기충당부채")
	private Double 비유동부채_장기충당부채;
	@Column(name="비유동부채_장기매입채무및기타채무")
	private Double 비유동부채_장기매입채무및기타채무;
	@Column(name="비유동부채_장기매입채무및기타채무_장기매입채무")
	private Double 비유동부채_장기매입채무및기타채무_장기매입채무;
	@Column(name="비유동부채_장기매입채무및기타채무_장기미지급금")
	private Double 비유동부채_장기매입채무및기타채무_장기미지급금;
	@Column(name="비유동부채_장기매입채무및기타채무_기타")
	private Double 비유동부채_장기매입채무및기타채무_기타;
	@Column(name="비유동부채_기타비유동부채")
	private Double 비유동부채_기타비유동부채;
	@Column(name="비유동부채_기타비유동부채_장기선수금")
	private Double 비유동부채_기타비유동부채_장기선수금;
	@Column(name="비유동부채_기타비유동부채_장기선수수익")
	private Double 비유동부채_기타비유동부채_장기선수수익;
	@Column(name="비유동부채_기타비유동부채_장기미지급비용")
	private Double 비유동부채_기타비유동부채_장기미지급비용;
	@Column(name="비유동부채_기타비유동부채_기타")
	private Double 비유동부채_기타비유동부채_기타;
	@Column(name="비유동부채_매각예정부채")
	private Double 비유동부채_매각예정부채;
	@Column(name="비유동부채_이연법인세부채")
	private Double 비유동부채_이연법인세부채;
	@Column(name="비유동부채_퇴직급여충당부채")
	private Double 비유동부채_퇴직급여충당부채;
	@Column(name="비유동부채_기타")
	private Double 비유동부채_기타;
	@Column(name="부채총계")
	private Double 부채총계;
	@Column(name="부채총계지분법적용주석")
	private Double 부채총계지분법적용주석;
	@Column(name="지배주주지분")
	private Double 지배주주지분;
	@Column(name="자본금")
	private Double 자본금;
	@Column(name="자본금_보통주자본금")
	private Double 자본금_보통주자본금;
	@Column(name="자본금_우선주자본금")
	private Double 자본금_우선주자본금;
	@Column(name="자본잉여금")
	private Double 자본잉여금;
	@Column(name="자본잉여금_주식발행초과금")
	private Double 자본잉여금_주식발행초과금;
	@Column(name="자본잉여금_자기주식처분이익")
	private Double 자본잉여금_자기주식처분이익;
	@Column(name="자본잉여금_기타")
	private Double 자본잉여금_기타;
	@Column(name="기타자본")
	private Double 기타자본;
	@Column(name="기타자본_자기주식")
	private Double 기타자본_자기주식;
	@Column(name="기타자본_자기주식처분손실")
	private Double 기타자본_자기주식처분손실;
	@Column(name="기타자본_기타")
	private Double 기타자본_기타;
	@Column(name="기타포괄이익누계액")
	private Double 기타포괄이익누계액;
	@Column(name="기타포괄이익누계액_금융자산평가손익")
	private Double 기타포괄이익누계액_금융자산평가손익;
	@Column(name="기타포괄이익누계액_매도가능금융자산평가손익")
	private Double 기타포괄이익누계액_매도가능금융자산평가손익;
	@Column(name="기타포괄이익누계액_관계기업등기타포괄이익")
	private Double 기타포괄이익누계액_관계기업등기타포괄이익;
	@Column(name="기타포괄이익누계액_해외사업환산손익")
	private Double 기타포괄이익누계액_해외사업환산손익;
	@Column(name="기타포괄이익누계액_보험수리적손익")
	private Double 기타포괄이익누계액_보험수리적손익;
	@Column(name="기타포괄이익누계액_현금흐름위험회피평가손익")
	private Double 기타포괄이익누계액_현금흐름위험회피평가손익;
	@Column(name="기타포괄이익누계액_재평가잉여금")
	private Double 기타포괄이익누계액_재평가잉여금;
	@Column(name="기타포괄이익누계액_기타")
	private Double 기타포괄이익누계액_기타;
	@Column(name="이익잉여금")
	private Double 이익잉여금;
	@Column(name="비지배주주지분")
	private Double 비지배주주지분;
	@Column(name="자본총계")
	private Double 자본총계;
	@Column(name="자본총계지분법적용주석")
	private Double 자본총계지분법적용주석;
	@Column(name="BPSAdj지분법적용주석")
	private Double BPSAdj지분법적용주석;
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getYmDay() {
		return ymDay;
	}
	public void setYmDay(String ymDay) {
		this.ymDay = ymDay;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public Double get유동자산() {
		return 유동자산;
	}
	public void set유동자산(Double 유동자산) {
		this.유동자산 = 유동자산;
	}
	public Double get당좌자산() {
		return 당좌자산;
	}
	public void set당좌자산(Double 당좌자산) {
		this.당좌자산 = 당좌자산;
	}
	public Double get당좌자산_현금및현금성자산() {
		return 당좌자산_현금및현금성자산;
	}
	public void set당좌자산_현금및현금성자산(Double 당좌자산_현금및현금성자산) {
		this.당좌자산_현금및현금성자산 = 당좌자산_현금및현금성자산;
	}
	public Double get당좌자산_단기금융자산() {
		return 당좌자산_단기금융자산;
	}
	public void set당좌자산_단기금융자산(Double 당좌자산_단기금융자산) {
		this.당좌자산_단기금융자산 = 당좌자산_단기금융자산;
	}
	public Double get당좌자산_단기금융자산_단기금융상품() {
		return 당좌자산_단기금융자산_단기금융상품;
	}
	public void set당좌자산_단기금융자산_단기금융상품(Double 당좌자산_단기금융자산_단기금융상품) {
		this.당좌자산_단기금융자산_단기금융상품 = 당좌자산_단기금융자산_단기금융상품;
	}
	public Double get당좌자산_단기금융자산_단기매매금융자산() {
		return 당좌자산_단기금융자산_단기매매금융자산;
	}
	public void set당좌자산_단기금융자산_단기매매금융자산(Double 당좌자산_단기금융자산_단기매매금융자산) {
		this.당좌자산_단기금융자산_단기매매금융자산 = 당좌자산_단기금융자산_단기매매금융자산;
	}
	public Double get당좌자산_단기금융자산_단기매도가능금융자산() {
		return 당좌자산_단기금융자산_단기매도가능금융자산;
	}
	public void set당좌자산_단기금융자산_단기매도가능금융자산(Double 당좌자산_단기금융자산_단기매도가능금융자산) {
		this.당좌자산_단기금융자산_단기매도가능금융자산 = 당좌자산_단기금융자산_단기매도가능금융자산;
	}
	public Double get당좌자산_단기금융자산_기타단기금융자산() {
		return 당좌자산_단기금융자산_기타단기금융자산;
	}
	public void set당좌자산_단기금융자산_기타단기금융자산(Double 당좌자산_단기금융자산_기타단기금융자산) {
		this.당좌자산_단기금융자산_기타단기금융자산 = 당좌자산_단기금융자산_기타단기금융자산;
	}
	public Double get당좌자산_매출채권및기타채권() {
		return 당좌자산_매출채권및기타채권;
	}
	public void set당좌자산_매출채권및기타채권(Double 당좌자산_매출채권및기타채권) {
		this.당좌자산_매출채권및기타채권 = 당좌자산_매출채권및기타채권;
	}
	public Double get당좌자산_매출채권및기타채권_매출채권() {
		return 당좌자산_매출채권및기타채권_매출채권;
	}
	public void set당좌자산_매출채권및기타채권_매출채권(Double 당좌자산_매출채권및기타채권_매출채권) {
		this.당좌자산_매출채권및기타채권_매출채권 = 당좌자산_매출채권및기타채권_매출채권;
	}
	public Double get당좌자산_매출채권및기타채권_미수금() {
		return 당좌자산_매출채권및기타채권_미수금;
	}
	public void set당좌자산_매출채권및기타채권_미수금(Double 당좌자산_매출채권및기타채권_미수금) {
		this.당좌자산_매출채권및기타채권_미수금 = 당좌자산_매출채권및기타채권_미수금;
	}
	public Double get당좌자산_매출채권및기타채권_단기미청구공사() {
		return 당좌자산_매출채권및기타채권_단기미청구공사;
	}
	public void set당좌자산_매출채권및기타채권_단기미청구공사(Double 당좌자산_매출채권및기타채권_단기미청구공사) {
		this.당좌자산_매출채권및기타채권_단기미청구공사 = 당좌자산_매출채권및기타채권_단기미청구공사;
	}
	public Double get당좌자산_매출채권및기타채권_기타() {
		return 당좌자산_매출채권및기타채권_기타;
	}
	public void set당좌자산_매출채권및기타채권_기타(Double 당좌자산_매출채권및기타채권_기타) {
		this.당좌자산_매출채권및기타채권_기타 = 당좌자산_매출채권및기타채권_기타;
	}
	public Double get재고자산() {
		return 재고자산;
	}
	public void set재고자산(Double 재고자산) {
		this.재고자산 = 재고자산;
	}
	public Double get재고자산_원재료() {
		return 재고자산_원재료;
	}
	public void set재고자산_원재료(Double 재고자산_원재료) {
		this.재고자산_원재료 = 재고자산_원재료;
	}
	public Double get재고자산_재공품() {
		return 재고자산_재공품;
	}
	public void set재고자산_재공품(Double 재고자산_재공품) {
		this.재고자산_재공품 = 재고자산_재공품;
	}
	public Double get재고자산_반제품() {
		return 재고자산_반제품;
	}
	public void set재고자산_반제품(Double 재고자산_반제품) {
		this.재고자산_반제품 = 재고자산_반제품;
	}
	public Double get재고자산_제품() {
		return 재고자산_제품;
	}
	public void set재고자산_제품(Double 재고자산_제품) {
		this.재고자산_제품 = 재고자산_제품;
	}
	public Double get재고자산_상품() {
		return 재고자산_상품;
	}
	public void set재고자산_상품(Double 재고자산_상품) {
		this.재고자산_상품 = 재고자산_상품;
	}
	public Double get재고자산_저장품() {
		return 재고자산_저장품;
	}
	public void set재고자산_저장품(Double 재고자산_저장품) {
		this.재고자산_저장품 = 재고자산_저장품;
	}
	public Double get재고자산_용지() {
		return 재고자산_용지;
	}
	public void set재고자산_용지(Double 재고자산_용지) {
		this.재고자산_용지 = 재고자산_용지;
	}
	public Double get재고자산_완성건물() {
		return 재고자산_완성건물;
	}
	public void set재고자산_완성건물(Double 재고자산_완성건물) {
		this.재고자산_완성건물 = 재고자산_완성건물;
	}
	public Double get재고자산_미완성건물공사() {
		return 재고자산_미완성건물공사;
	}
	public void set재고자산_미완성건물공사(Double 재고자산_미완성건물공사) {
		this.재고자산_미완성건물공사 = 재고자산_미완성건물공사;
	}
	public Double get재고자산_기타재고자산() {
		return 재고자산_기타재고자산;
	}
	public void set재고자산_기타재고자산(Double 재고자산_기타재고자산) {
		this.재고자산_기타재고자산 = 재고자산_기타재고자산;
	}
	public Double get선급법인세() {
		return 선급법인세;
	}
	public void set선급법인세(Double 선급법인세) {
		this.선급법인세 = 선급법인세;
	}
	public Double get기타유동자산() {
		return 기타유동자산;
	}
	public void set기타유동자산(Double 기타유동자산) {
		this.기타유동자산 = 기타유동자산;
	}
	public Double get기타유동자산_선급금등() {
		return 기타유동자산_선급금등;
	}
	public void set기타유동자산_선급금등(Double 기타유동자산_선급금등) {
		this.기타유동자산_선급금등 = 기타유동자산_선급금등;
	}
	public Double get기타유동자산_선급비용() {
		return 기타유동자산_선급비용;
	}
	public void set기타유동자산_선급비용(Double 기타유동자산_선급비용) {
		this.기타유동자산_선급비용 = 기타유동자산_선급비용;
	}
	public Double get기타유동자산_기타() {
		return 기타유동자산_기타;
	}
	public void set기타유동자산_기타(Double 기타유동자산_기타) {
		this.기타유동자산_기타 = 기타유동자산_기타;
	}
	public Double get비유동자산() {
		return 비유동자산;
	}
	public void set비유동자산(Double 비유동자산) {
		this.비유동자산 = 비유동자산;
	}
	public Double get투자자산() {
		return 투자자산;
	}
	public void set투자자산(Double 투자자산) {
		this.투자자산 = 투자자산;
	}
	public Double get투자자산_관계기업등지분관련투자자산() {
		return 투자자산_관계기업등지분관련투자자산;
	}
	public void set투자자산_관계기업등지분관련투자자산(Double 투자자산_관계기업등지분관련투자자산) {
		this.투자자산_관계기업등지분관련투자자산 = 투자자산_관계기업등지분관련투자자산;
	}
	public Double get투자자산_관계기업등지분관련투자자산_관계기업투자지분법() {
		return 투자자산_관계기업등지분관련투자자산_관계기업투자지분법;
	}
	public void set투자자산_관계기업등지분관련투자자산_관계기업투자지분법(Double 투자자산_관계기업등지분관련투자자산_관계기업투자지분법) {
		this.투자자산_관계기업등지분관련투자자산_관계기업투자지분법 = 투자자산_관계기업등지분관련투자자산_관계기업투자지분법;
	}
	public Double get투자자산_관계기업등지분관련투자자산_종속기업투자() {
		return 투자자산_관계기업등지분관련투자자산_종속기업투자;
	}
	public void set투자자산_관계기업등지분관련투자자산_종속기업투자(Double 투자자산_관계기업등지분관련투자자산_종속기업투자) {
		this.투자자산_관계기업등지분관련투자자산_종속기업투자 = 투자자산_관계기업등지분관련투자자산_종속기업투자;
	}
	public Double get투자자산_관계기업등지분관련투자자산_기타() {
		return 투자자산_관계기업등지분관련투자자산_기타;
	}
	public void set투자자산_관계기업등지분관련투자자산_기타(Double 투자자산_관계기업등지분관련투자자산_기타) {
		this.투자자산_관계기업등지분관련투자자산_기타 = 투자자산_관계기업등지분관련투자자산_기타;
	}
	public Double get투자자산_장기매도가능금융자산() {
		return 투자자산_장기매도가능금융자산;
	}
	public void set투자자산_장기매도가능금융자산(Double 투자자산_장기매도가능금융자산) {
		this.투자자산_장기매도가능금융자산 = 투자자산_장기매도가능금융자산;
	}
	public Double get투자자산_장기매도가능금융자산_주식() {
		return 투자자산_장기매도가능금융자산_주식;
	}
	public void set투자자산_장기매도가능금융자산_주식(Double 투자자산_장기매도가능금융자산_주식) {
		this.투자자산_장기매도가능금융자산_주식 = 투자자산_장기매도가능금융자산_주식;
	}
	public Double get투자자산_장기매도가능금융자산_수익증권() {
		return 투자자산_장기매도가능금융자산_수익증권;
	}
	public void set투자자산_장기매도가능금융자산_수익증권(Double 투자자산_장기매도가능금융자산_수익증권) {
		this.투자자산_장기매도가능금융자산_수익증권 = 투자자산_장기매도가능금융자산_수익증권;
	}
	public Double get투자자산_장기매도가능금융자산_채권() {
		return 투자자산_장기매도가능금융자산_채권;
	}
	public void set투자자산_장기매도가능금융자산_채권(Double 투자자산_장기매도가능금융자산_채권) {
		this.투자자산_장기매도가능금융자산_채권 = 투자자산_장기매도가능금융자산_채권;
	}
	public Double get투자자산_장기매도가능금융자산_기타() {
		return 투자자산_장기매도가능금융자산_기타;
	}
	public void set투자자산_장기매도가능금융자산_기타(Double 투자자산_장기매도가능금융자산_기타) {
		this.투자자산_장기매도가능금융자산_기타 = 투자자산_장기매도가능금융자산_기타;
	}
	public Double get투자자산_장기만기보유금융자산() {
		return 투자자산_장기만기보유금융자산;
	}
	public void set투자자산_장기만기보유금융자산(Double 투자자산_장기만기보유금융자산) {
		this.투자자산_장기만기보유금융자산 = 투자자산_장기만기보유금융자산;
	}
	public Double get투자자산_당기손익인식금융자산() {
		return 투자자산_당기손익인식금융자산;
	}
	public void set투자자산_당기손익인식금융자산(Double 투자자산_당기손익인식금융자산) {
		this.투자자산_당기손익인식금융자산 = 투자자산_당기손익인식금융자산;
	}
	public Double get투자자산_기타투자자산() {
		return 투자자산_기타투자자산;
	}
	public void set투자자산_기타투자자산(Double 투자자산_기타투자자산) {
		this.투자자산_기타투자자산 = 투자자산_기타투자자산;
	}
	public Double get유형자산() {
		return 유형자산;
	}
	public void set유형자산(Double 유형자산) {
		this.유형자산 = 유형자산;
	}
	public Double get유형자산_토지() {
		return 유형자산_토지;
	}
	public void set유형자산_토지(Double 유형자산_토지) {
		this.유형자산_토지 = 유형자산_토지;
	}
	public Double get유형자산_건물() {
		return 유형자산_건물;
	}
	public void set유형자산_건물(Double 유형자산_건물) {
		this.유형자산_건물 = 유형자산_건물;
	}
	public Double get유형자산_구축물() {
		return 유형자산_구축물;
	}
	public void set유형자산_구축물(Double 유형자산_구축물) {
		this.유형자산_구축물 = 유형자산_구축물;
	}
	public Double get유형자산_기계장치() {
		return 유형자산_기계장치;
	}
	public void set유형자산_기계장치(Double 유형자산_기계장치) {
		this.유형자산_기계장치 = 유형자산_기계장치;
	}
	public Double get유형자산_중장비() {
		return 유형자산_중장비;
	}
	public void set유형자산_중장비(Double 유형자산_중장비) {
		this.유형자산_중장비 = 유형자산_중장비;
	}
	public Double get유형자산_선박() {
		return 유형자산_선박;
	}
	public void set유형자산_선박(Double 유형자산_선박) {
		this.유형자산_선박 = 유형자산_선박;
	}
	public Double get유형자산_항공기() {
		return 유형자산_항공기;
	}
	public void set유형자산_항공기(Double 유형자산_항공기) {
		this.유형자산_항공기 = 유형자산_항공기;
	}
	public Double get유형자산_건설중인자산() {
		return 유형자산_건설중인자산;
	}
	public void set유형자산_건설중인자산(Double 유형자산_건설중인자산) {
		this.유형자산_건설중인자산 = 유형자산_건설중인자산;
	}
	public Double get유형자산_기타유형자산() {
		return 유형자산_기타유형자산;
	}
	public void set유형자산_기타유형자산(Double 유형자산_기타유형자산) {
		this.유형자산_기타유형자산 = 유형자산_기타유형자산;
	}
	public Double get무형자산() {
		return 무형자산;
	}
	public void set무형자산(Double 무형자산) {
		this.무형자산 = 무형자산;
	}
	public Double get무형자산_영업권() {
		return 무형자산_영업권;
	}
	public void set무형자산_영업권(Double 무형자산_영업권) {
		this.무형자산_영업권 = 무형자산_영업권;
	}
	public Double get무형자산_특허권() {
		return 무형자산_특허권;
	}
	public void set무형자산_특허권(Double 무형자산_특허권) {
		this.무형자산_특허권 = 무형자산_특허권;
	}
	public Double get무형자산_개발비() {
		return 무형자산_개발비;
	}
	public void set무형자산_개발비(Double 무형자산_개발비) {
		this.무형자산_개발비 = 무형자산_개발비;
	}
	public Double get무형자산_산업재산권() {
		return 무형자산_산업재산권;
	}
	public void set무형자산_산업재산권(Double 무형자산_산업재산권) {
		this.무형자산_산업재산권 = 무형자산_산업재산권;
	}
	public Double get무형자산_컴퓨터소프트웨어() {
		return 무형자산_컴퓨터소프트웨어;
	}
	public void set무형자산_컴퓨터소프트웨어(Double 무형자산_컴퓨터소프트웨어) {
		this.무형자산_컴퓨터소프트웨어 = 무형자산_컴퓨터소프트웨어;
	}
	public Double get무형자산_브랜드() {
		return 무형자산_브랜드;
	}
	public void set무형자산_브랜드(Double 무형자산_브랜드) {
		this.무형자산_브랜드 = 무형자산_브랜드;
	}
	public Double get무형자산_기타무형자산() {
		return 무형자산_기타무형자산;
	}
	public void set무형자산_기타무형자산(Double 무형자산_기타무형자산) {
		this.무형자산_기타무형자산 = 무형자산_기타무형자산;
	}
	public Double get생물자산() {
		return 생물자산;
	}
	public void set생물자산(Double 생물자산) {
		this.생물자산 = 생물자산;
	}
	public Double get투자부동산() {
		return 투자부동산;
	}
	public void set투자부동산(Double 투자부동산) {
		this.투자부동산 = 투자부동산;
	}
	public Double get투자부동산_토지() {
		return 투자부동산_토지;
	}
	public void set투자부동산_토지(Double 투자부동산_토지) {
		this.투자부동산_토지 = 투자부동산_토지;
	}
	public Double get투자부동산_건물() {
		return 투자부동산_건물;
	}
	public void set투자부동산_건물(Double 투자부동산_건물) {
		this.투자부동산_건물 = 투자부동산_건물;
	}
	public Double get투자부동산_기타투자부동산() {
		return 투자부동산_기타투자부동산;
	}
	public void set투자부동산_기타투자부동산(Double 투자부동산_기타투자부동산) {
		this.투자부동산_기타투자부동산 = 투자부동산_기타투자부동산;
	}
	public Double get매각예정자산() {
		return 매각예정자산;
	}
	public void set매각예정자산(Double 매각예정자산) {
		this.매각예정자산 = 매각예정자산;
	}
	public Double get이연법인세자산() {
		return 이연법인세자산;
	}
	public void set이연법인세자산(Double 이연법인세자산) {
		this.이연법인세자산 = 이연법인세자산;
	}
	public Double get기타비유동자산() {
		return 기타비유동자산;
	}
	public void set기타비유동자산(Double 기타비유동자산) {
		this.기타비유동자산 = 기타비유동자산;
	}
	public Double get자산총계() {
		return 자산총계;
	}
	public void set자산총계(Double 자산총계) {
		this.자산총계 = 자산총계;
	}
	public Double get자산총계지분법적용주석() {
		return 자산총계지분법적용주석;
	}
	public void set자산총계지분법적용주석(Double 자산총계지분법적용주석) {
		this.자산총계지분법적용주석 = 자산총계지분법적용주석;
	}
	public Double get유동부채() {
		return 유동부채;
	}
	public void set유동부채(Double 유동부채) {
		this.유동부채 = 유동부채;
	}
	public Double get유동부채_매입채무및기타채무() {
		return 유동부채_매입채무및기타채무;
	}
	public void set유동부채_매입채무및기타채무(Double 유동부채_매입채무및기타채무) {
		this.유동부채_매입채무및기타채무 = 유동부채_매입채무및기타채무;
	}
	public Double get유동부채_매입채무및기타채무_매입채무() {
		return 유동부채_매입채무및기타채무_매입채무;
	}
	public void set유동부채_매입채무및기타채무_매입채무(Double 유동부채_매입채무및기타채무_매입채무) {
		this.유동부채_매입채무및기타채무_매입채무 = 유동부채_매입채무및기타채무_매입채무;
	}
	public Double get유동부채_매입채무및기타채무_미지급금() {
		return 유동부채_매입채무및기타채무_미지급금;
	}
	public void set유동부채_매입채무및기타채무_미지급금(Double 유동부채_매입채무및기타채무_미지급금) {
		this.유동부채_매입채무및기타채무_미지급금 = 유동부채_매입채무및기타채무_미지급금;
	}
	public Double get유동부채_매입채무및기타채무_미지급비용() {
		return 유동부채_매입채무및기타채무_미지급비용;
	}
	public void set유동부채_매입채무및기타채무_미지급비용(Double 유동부채_매입채무및기타채무_미지급비용) {
		this.유동부채_매입채무및기타채무_미지급비용 = 유동부채_매입채무및기타채무_미지급비용;
	}
	public Double get유동부채_매입채무및기타채무_미지급배당금() {
		return 유동부채_매입채무및기타채무_미지급배당금;
	}
	public void set유동부채_매입채무및기타채무_미지급배당금(Double 유동부채_매입채무및기타채무_미지급배당금) {
		this.유동부채_매입채무및기타채무_미지급배당금 = 유동부채_매입채무및기타채무_미지급배당금;
	}
	public Double get유동부채_매입채무및기타채무_예수금() {
		return 유동부채_매입채무및기타채무_예수금;
	}
	public void set유동부채_매입채무및기타채무_예수금(Double 유동부채_매입채무및기타채무_예수금) {
		this.유동부채_매입채무및기타채무_예수금 = 유동부채_매입채무및기타채무_예수금;
	}
	public Double get유동부채_매입채무및기타채무_부가세예수금() {
		return 유동부채_매입채무및기타채무_부가세예수금;
	}
	public void set유동부채_매입채무및기타채무_부가세예수금(Double 유동부채_매입채무및기타채무_부가세예수금) {
		this.유동부채_매입채무및기타채무_부가세예수금 = 유동부채_매입채무및기타채무_부가세예수금;
	}
	public Double get유동부채_매입채무및기타채무_초과청구공사() {
		return 유동부채_매입채무및기타채무_초과청구공사;
	}
	public void set유동부채_매입채무및기타채무_초과청구공사(Double 유동부채_매입채무및기타채무_초과청구공사) {
		this.유동부채_매입채무및기타채무_초과청구공사 = 유동부채_매입채무및기타채무_초과청구공사;
	}
	public Double get유동부채_매입채무및기타채무_기타() {
		return 유동부채_매입채무및기타채무_기타;
	}
	public void set유동부채_매입채무및기타채무_기타(Double 유동부채_매입채무및기타채무_기타) {
		this.유동부채_매입채무및기타채무_기타 = 유동부채_매입채무및기타채무_기타;
	}
	public Double get유동부채_단기차입금() {
		return 유동부채_단기차입금;
	}
	public void set유동부채_단기차입금(Double 유동부채_단기차입금) {
		this.유동부채_단기차입금 = 유동부채_단기차입금;
	}
	public Double get유동부채_단기사채() {
		return 유동부채_단기사채;
	}
	public void set유동부채_단기사채(Double 유동부채_단기사채) {
		this.유동부채_단기사채 = 유동부채_단기사채;
	}
	public Double get유동부채_유동성장기부채() {
		return 유동부채_유동성장기부채;
	}
	public void set유동부채_유동성장기부채(Double 유동부채_유동성장기부채) {
		this.유동부채_유동성장기부채 = 유동부채_유동성장기부채;
	}
	public Double get유동부채_유동성장기부채_유동성사채() {
		return 유동부채_유동성장기부채_유동성사채;
	}
	public void set유동부채_유동성장기부채_유동성사채(Double 유동부채_유동성장기부채_유동성사채) {
		this.유동부채_유동성장기부채_유동성사채 = 유동부채_유동성장기부채_유동성사채;
	}
	public Double get유동부채_유동성장기부채_유동성장기차입금() {
		return 유동부채_유동성장기부채_유동성장기차입금;
	}
	public void set유동부채_유동성장기부채_유동성장기차입금(Double 유동부채_유동성장기부채_유동성장기차입금) {
		this.유동부채_유동성장기부채_유동성장기차입금 = 유동부채_유동성장기부채_유동성장기차입금;
	}
	public Double get유동부채_유동성장기부채_유동성외화장기차입금() {
		return 유동부채_유동성장기부채_유동성외화장기차입금;
	}
	public void set유동부채_유동성장기부채_유동성외화장기차입금(Double 유동부채_유동성장기부채_유동성외화장기차입금) {
		this.유동부채_유동성장기부채_유동성외화장기차입금 = 유동부채_유동성장기부채_유동성외화장기차입금;
	}
	public Double get유동부채_유동성장기부채_기타() {
		return 유동부채_유동성장기부채_기타;
	}
	public void set유동부채_유동성장기부채_기타(Double 유동부채_유동성장기부채_기타) {
		this.유동부채_유동성장기부채_기타 = 유동부채_유동성장기부채_기타;
	}
	public Double get유동부채_단기금융부채() {
		return 유동부채_단기금융부채;
	}
	public void set유동부채_단기금융부채(Double 유동부채_단기금융부채) {
		this.유동부채_단기금융부채 = 유동부채_단기금융부채;
	}
	public Double get유동부채_단기충당부채() {
		return 유동부채_단기충당부채;
	}
	public void set유동부채_단기충당부채(Double 유동부채_단기충당부채) {
		this.유동부채_단기충당부채 = 유동부채_단기충당부채;
	}
	public Double get유동부채_당기법인세부채() {
		return 유동부채_당기법인세부채;
	}
	public void set유동부채_당기법인세부채(Double 유동부채_당기법인세부채) {
		this.유동부채_당기법인세부채 = 유동부채_당기법인세부채;
	}
	public Double get유동부채_기타유동부채() {
		return 유동부채_기타유동부채;
	}
	public void set유동부채_기타유동부채(Double 유동부채_기타유동부채) {
		this.유동부채_기타유동부채 = 유동부채_기타유동부채;
	}
	public Double get유동부채_기타유동부채_선수금() {
		return 유동부채_기타유동부채_선수금;
	}
	public void set유동부채_기타유동부채_선수금(Double 유동부채_기타유동부채_선수금) {
		this.유동부채_기타유동부채_선수금 = 유동부채_기타유동부채_선수금;
	}
	public Double get유동부채_기타유동부채_선수수익() {
		return 유동부채_기타유동부채_선수수익;
	}
	public void set유동부채_기타유동부채_선수수익(Double 유동부채_기타유동부채_선수수익) {
		this.유동부채_기타유동부채_선수수익 = 유동부채_기타유동부채_선수수익;
	}
	public Double get유동부채_기타유동부채_기타() {
		return 유동부채_기타유동부채_기타;
	}
	public void set유동부채_기타유동부채_기타(Double 유동부채_기타유동부채_기타) {
		this.유동부채_기타유동부채_기타 = 유동부채_기타유동부채_기타;
	}
	public Double get유동부채_기타() {
		return 유동부채_기타;
	}
	public void set유동부채_기타(Double 유동부채_기타) {
		this.유동부채_기타 = 유동부채_기타;
	}
	public Double get비유동부채() {
		return 비유동부채;
	}
	public void set비유동부채(Double 비유동부채) {
		this.비유동부채 = 비유동부채;
	}
	public Double get비유동부채_사채() {
		return 비유동부채_사채;
	}
	public void set비유동부채_사채(Double 비유동부채_사채) {
		this.비유동부채_사채 = 비유동부채_사채;
	}
	public Double get비유동부채_사채_사채() {
		return 비유동부채_사채_사채;
	}
	public void set비유동부채_사채_사채(Double 비유동부채_사채_사채) {
		this.비유동부채_사채_사채 = 비유동부채_사채_사채;
	}
	public Double get비유동부채_사채_CB() {
		return 비유동부채_사채_CB;
	}
	public void set비유동부채_사채_CB(Double 비유동부채_사채_CB) {
		this.비유동부채_사채_CB = 비유동부채_사채_CB;
	}
	public Double get비유동부채_사채_BW() {
		return 비유동부채_사채_BW;
	}
	public void set비유동부채_사채_BW(Double 비유동부채_사채_BW) {
		this.비유동부채_사채_BW = 비유동부채_사채_BW;
	}
	public Double get비유동부채_사채_EB() {
		return 비유동부채_사채_EB;
	}
	public void set비유동부채_사채_EB(Double 비유동부채_사채_EB) {
		this.비유동부채_사채_EB = 비유동부채_사채_EB;
	}
	public Double get비유동부채_장기차입금() {
		return 비유동부채_장기차입금;
	}
	public void set비유동부채_장기차입금(Double 비유동부채_장기차입금) {
		this.비유동부채_장기차입금 = 비유동부채_장기차입금;
	}
	public Double get비유동부채_장기금융부채() {
		return 비유동부채_장기금융부채;
	}
	public void set비유동부채_장기금융부채(Double 비유동부채_장기금융부채) {
		this.비유동부채_장기금융부채 = 비유동부채_장기금융부채;
	}
	public Double get비유동부채_장기충당부채() {
		return 비유동부채_장기충당부채;
	}
	public void set비유동부채_장기충당부채(Double 비유동부채_장기충당부채) {
		this.비유동부채_장기충당부채 = 비유동부채_장기충당부채;
	}
	public Double get비유동부채_장기매입채무및기타채무() {
		return 비유동부채_장기매입채무및기타채무;
	}
	public void set비유동부채_장기매입채무및기타채무(Double 비유동부채_장기매입채무및기타채무) {
		this.비유동부채_장기매입채무및기타채무 = 비유동부채_장기매입채무및기타채무;
	}
	public Double get비유동부채_장기매입채무및기타채무_장기매입채무() {
		return 비유동부채_장기매입채무및기타채무_장기매입채무;
	}
	public void set비유동부채_장기매입채무및기타채무_장기매입채무(Double 비유동부채_장기매입채무및기타채무_장기매입채무) {
		this.비유동부채_장기매입채무및기타채무_장기매입채무 = 비유동부채_장기매입채무및기타채무_장기매입채무;
	}
	public Double get비유동부채_장기매입채무및기타채무_장기미지급금() {
		return 비유동부채_장기매입채무및기타채무_장기미지급금;
	}
	public void set비유동부채_장기매입채무및기타채무_장기미지급금(Double 비유동부채_장기매입채무및기타채무_장기미지급금) {
		this.비유동부채_장기매입채무및기타채무_장기미지급금 = 비유동부채_장기매입채무및기타채무_장기미지급금;
	}
	public Double get비유동부채_장기매입채무및기타채무_기타() {
		return 비유동부채_장기매입채무및기타채무_기타;
	}
	public void set비유동부채_장기매입채무및기타채무_기타(Double 비유동부채_장기매입채무및기타채무_기타) {
		this.비유동부채_장기매입채무및기타채무_기타 = 비유동부채_장기매입채무및기타채무_기타;
	}
	public Double get비유동부채_기타비유동부채() {
		return 비유동부채_기타비유동부채;
	}
	public void set비유동부채_기타비유동부채(Double 비유동부채_기타비유동부채) {
		this.비유동부채_기타비유동부채 = 비유동부채_기타비유동부채;
	}
	public Double get비유동부채_기타비유동부채_장기선수금() {
		return 비유동부채_기타비유동부채_장기선수금;
	}
	public void set비유동부채_기타비유동부채_장기선수금(Double 비유동부채_기타비유동부채_장기선수금) {
		this.비유동부채_기타비유동부채_장기선수금 = 비유동부채_기타비유동부채_장기선수금;
	}
	public Double get비유동부채_기타비유동부채_장기선수수익() {
		return 비유동부채_기타비유동부채_장기선수수익;
	}
	public void set비유동부채_기타비유동부채_장기선수수익(Double 비유동부채_기타비유동부채_장기선수수익) {
		this.비유동부채_기타비유동부채_장기선수수익 = 비유동부채_기타비유동부채_장기선수수익;
	}
	public Double get비유동부채_기타비유동부채_장기미지급비용() {
		return 비유동부채_기타비유동부채_장기미지급비용;
	}
	public void set비유동부채_기타비유동부채_장기미지급비용(Double 비유동부채_기타비유동부채_장기미지급비용) {
		this.비유동부채_기타비유동부채_장기미지급비용 = 비유동부채_기타비유동부채_장기미지급비용;
	}
	public Double get비유동부채_기타비유동부채_기타() {
		return 비유동부채_기타비유동부채_기타;
	}
	public void set비유동부채_기타비유동부채_기타(Double 비유동부채_기타비유동부채_기타) {
		this.비유동부채_기타비유동부채_기타 = 비유동부채_기타비유동부채_기타;
	}
	public Double get비유동부채_매각예정부채() {
		return 비유동부채_매각예정부채;
	}
	public void set비유동부채_매각예정부채(Double 비유동부채_매각예정부채) {
		this.비유동부채_매각예정부채 = 비유동부채_매각예정부채;
	}
	public Double get비유동부채_이연법인세부채() {
		return 비유동부채_이연법인세부채;
	}
	public void set비유동부채_이연법인세부채(Double 비유동부채_이연법인세부채) {
		this.비유동부채_이연법인세부채 = 비유동부채_이연법인세부채;
	}
	public Double get비유동부채_퇴직급여충당부채() {
		return 비유동부채_퇴직급여충당부채;
	}
	public void set비유동부채_퇴직급여충당부채(Double 비유동부채_퇴직급여충당부채) {
		this.비유동부채_퇴직급여충당부채 = 비유동부채_퇴직급여충당부채;
	}
	public Double get비유동부채_기타() {
		return 비유동부채_기타;
	}
	public void set비유동부채_기타(Double 비유동부채_기타) {
		this.비유동부채_기타 = 비유동부채_기타;
	}
	public Double get부채총계() {
		return 부채총계;
	}
	public void set부채총계(Double 부채총계) {
		this.부채총계 = 부채총계;
	}
	public Double get부채총계지분법적용주석() {
		return 부채총계지분법적용주석;
	}
	public void set부채총계지분법적용주석(Double 부채총계지분법적용주석) {
		this.부채총계지분법적용주석 = 부채총계지분법적용주석;
	}
	public Double get지배주주지분() {
		return 지배주주지분;
	}
	public void set지배주주지분(Double 지배주주지분) {
		this.지배주주지분 = 지배주주지분;
	}
	public Double get자본금() {
		return 자본금;
	}
	public void set자본금(Double 자본금) {
		this.자본금 = 자본금;
	}
	public Double get자본금_보통주자본금() {
		return 자본금_보통주자본금;
	}
	public void set자본금_보통주자본금(Double 자본금_보통주자본금) {
		this.자본금_보통주자본금 = 자본금_보통주자본금;
	}
	public Double get자본금_우선주자본금() {
		return 자본금_우선주자본금;
	}
	public void set자본금_우선주자본금(Double 자본금_우선주자본금) {
		this.자본금_우선주자본금 = 자본금_우선주자본금;
	}
	public Double get자본잉여금() {
		return 자본잉여금;
	}
	public void set자본잉여금(Double 자본잉여금) {
		this.자본잉여금 = 자본잉여금;
	}
	public Double get자본잉여금_주식발행초과금() {
		return 자본잉여금_주식발행초과금;
	}
	public void set자본잉여금_주식발행초과금(Double 자본잉여금_주식발행초과금) {
		this.자본잉여금_주식발행초과금 = 자본잉여금_주식발행초과금;
	}
	public Double get자본잉여금_자기주식처분이익() {
		return 자본잉여금_자기주식처분이익;
	}
	public void set자본잉여금_자기주식처분이익(Double 자본잉여금_자기주식처분이익) {
		this.자본잉여금_자기주식처분이익 = 자본잉여금_자기주식처분이익;
	}
	public Double get자본잉여금_기타() {
		return 자본잉여금_기타;
	}
	public void set자본잉여금_기타(Double 자본잉여금_기타) {
		this.자본잉여금_기타 = 자본잉여금_기타;
	}
	public Double get기타자본() {
		return 기타자본;
	}
	public void set기타자본(Double 기타자본) {
		this.기타자본 = 기타자본;
	}
	public Double get기타자본_자기주식() {
		return 기타자본_자기주식;
	}
	public void set기타자본_자기주식(Double 기타자본_자기주식) {
		this.기타자본_자기주식 = 기타자본_자기주식;
	}
	public Double get기타자본_자기주식처분손실() {
		return 기타자본_자기주식처분손실;
	}
	public void set기타자본_자기주식처분손실(Double 기타자본_자기주식처분손실) {
		this.기타자본_자기주식처분손실 = 기타자본_자기주식처분손실;
	}
	public Double get기타자본_기타() {
		return 기타자본_기타;
	}
	public void set기타자본_기타(Double 기타자본_기타) {
		this.기타자본_기타 = 기타자본_기타;
	}
	public Double get기타포괄이익누계액() {
		return 기타포괄이익누계액;
	}
	public void set기타포괄이익누계액(Double 기타포괄이익누계액) {
		this.기타포괄이익누계액 = 기타포괄이익누계액;
	}
	public Double get기타포괄이익누계액_금융자산평가손익() {
		return 기타포괄이익누계액_금융자산평가손익;
	}
	public void set기타포괄이익누계액_금융자산평가손익(Double 기타포괄이익누계액_금융자산평가손익) {
		this.기타포괄이익누계액_금융자산평가손익 = 기타포괄이익누계액_금융자산평가손익;
	}
	public Double get기타포괄이익누계액_매도가능금융자산평가손익() {
		return 기타포괄이익누계액_매도가능금융자산평가손익;
	}
	public void set기타포괄이익누계액_매도가능금융자산평가손익(Double 기타포괄이익누계액_매도가능금융자산평가손익) {
		this.기타포괄이익누계액_매도가능금융자산평가손익 = 기타포괄이익누계액_매도가능금융자산평가손익;
	}
	public Double get기타포괄이익누계액_관계기업등기타포괄이익() {
		return 기타포괄이익누계액_관계기업등기타포괄이익;
	}
	public void set기타포괄이익누계액_관계기업등기타포괄이익(Double 기타포괄이익누계액_관계기업등기타포괄이익) {
		this.기타포괄이익누계액_관계기업등기타포괄이익 = 기타포괄이익누계액_관계기업등기타포괄이익;
	}
	public Double get기타포괄이익누계액_해외사업환산손익() {
		return 기타포괄이익누계액_해외사업환산손익;
	}
	public void set기타포괄이익누계액_해외사업환산손익(Double 기타포괄이익누계액_해외사업환산손익) {
		this.기타포괄이익누계액_해외사업환산손익 = 기타포괄이익누계액_해외사업환산손익;
	}
	public Double get기타포괄이익누계액_보험수리적손익() {
		return 기타포괄이익누계액_보험수리적손익;
	}
	public void set기타포괄이익누계액_보험수리적손익(Double 기타포괄이익누계액_보험수리적손익) {
		this.기타포괄이익누계액_보험수리적손익 = 기타포괄이익누계액_보험수리적손익;
	}
	public Double get기타포괄이익누계액_현금흐름위험회피평가손익() {
		return 기타포괄이익누계액_현금흐름위험회피평가손익;
	}
	public void set기타포괄이익누계액_현금흐름위험회피평가손익(Double 기타포괄이익누계액_현금흐름위험회피평가손익) {
		this.기타포괄이익누계액_현금흐름위험회피평가손익 = 기타포괄이익누계액_현금흐름위험회피평가손익;
	}
	public Double get기타포괄이익누계액_재평가잉여금() {
		return 기타포괄이익누계액_재평가잉여금;
	}
	public void set기타포괄이익누계액_재평가잉여금(Double 기타포괄이익누계액_재평가잉여금) {
		this.기타포괄이익누계액_재평가잉여금 = 기타포괄이익누계액_재평가잉여금;
	}
	public Double get기타포괄이익누계액_기타() {
		return 기타포괄이익누계액_기타;
	}
	public void set기타포괄이익누계액_기타(Double 기타포괄이익누계액_기타) {
		this.기타포괄이익누계액_기타 = 기타포괄이익누계액_기타;
	}
	public Double get이익잉여금() {
		return 이익잉여금;
	}
	public void set이익잉여금(Double 이익잉여금) {
		this.이익잉여금 = 이익잉여금;
	}
	public Double get비지배주주지분() {
		return 비지배주주지분;
	}
	public void set비지배주주지분(Double 비지배주주지분) {
		this.비지배주주지분 = 비지배주주지분;
	}
	public Double get자본총계() {
		return 자본총계;
	}
	public void set자본총계(Double 자본총계) {
		this.자본총계 = 자본총계;
	}
	public Double get자본총계지분법적용주석() {
		return 자본총계지분법적용주석;
	}
	public void set자본총계지분법적용주석(Double 자본총계지분법적용주석) {
		this.자본총계지분법적용주석 = 자본총계지분법적용주석;
	}
	public Double getBPSAdj지분법적용주석() {
		return BPSAdj지분법적용주석;
	}
	public void setBPSAdj지분법적용주석(Double bPSAdj지분법적용주석) {
		BPSAdj지분법적용주석 = bPSAdj지분법적용주석;
	}

	
	

	
	
	

	

}
