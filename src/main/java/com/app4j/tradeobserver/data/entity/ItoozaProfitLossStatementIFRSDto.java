package com.app4j.tradeobserver.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.app4j.tradeobserver.data.entity.id.ItoozaProfitLossStatementIFRSDtoId;

@Entity
@Table (name = "ITOOZA_PROFIT_LOSS_STATEMENT_IFRS")
@IdClass(ItoozaProfitLossStatementIFRSDtoId.class)
public class ItoozaProfitLossStatementIFRSDto {
	
	@Id
    @Column (name = "STOCK_CODE")
    private String stockCode;	
	@Id
	@Column (name="YM_DAY")
	private String ymDay;
	@Id
	@Column (name="DATE_TYPE")
	private String dateType;		// 분기, 연간 (Q, Y)
	
	@Column(name="매출액수익")
	private Double 매출액수익;
	@Column(name="매출액수익_제품상품매출액")
	private Double 매출액수익_제품상품매출액;
	@Column(name="매출액수익_공사수익")
	private Double 매출액수익_공사수익;
	@Column(name="매출액수익_분양수익")
	private Double 매출액수익_분양수익;
	@Column(name="매출액수익_기타매출액")
	private Double 매출액수익_기타매출액;
	@Column(name="매출원가")
	private Double 매출원가;
	@Column(name="매출원가_제품상품매출원가")
	private Double 매출원가_제품상품매출원가;
	@Column(name="매출원가_공사원가")
	private Double 매출원가_공사원가;
	@Column(name="매출원가_분양원가")
	private Double 매출원가_분양원가;
	@Column(name="매출원가_기타매출원가")
	private Double 매출원가_기타매출원가;
	@Column(name="매출총이익")
	private Double 매출총이익;
	@Column(name="판매비와관리비")
	private Double 판매비와관리비;
	@Column(name="판매비와관리비_급여")
	private Double 판매비와관리비_급여;
	@Column(name="판매비와관리비_퇴직급여")
	private Double 판매비와관리비_퇴직급여;
	@Column(name="판매비와관리비_복리후생비")
	private Double 판매비와관리비_복리후생비;
	@Column(name="판매비와관리비_임차료")
	private Double 판매비와관리비_임차료;
	@Column(name="판매비와관리비_수도광열비")
	private Double 판매비와관리비_수도광열비;
	@Column(name="판매비와관리비_전산처리비")
	private Double 판매비와관리비_전산처리비;
	@Column(name="판매비와관리비_지급수수료")
	private Double 판매비와관리비_지급수수료;
	@Column(name="판매비와관리비_용역비")
	private Double 판매비와관리비_용역비;
	@Column(name="판매비와관리비_수출비용")
	private Double 판매비와관리비_수출비용;
	@Column(name="판매비와관리비_판매수수료")
	private Double 판매비와관리비_판매수수료;
	@Column(name="판매비와관리비_판매촉진비")
	private Double 판매비와관리비_판매촉진비;
	@Column(name="판매비와관리비_광고선전비")
	private Double 판매비와관리비_광고선전비;
	@Column(name="판매비와관리비_애프터서비스비")
	private Double 판매비와관리비_애프터서비스비;
	@Column(name="판매비와관리비_운반비")
	private Double 판매비와관리비_운반비;
	@Column(name="판매비와관리비_포장비")
	private Double 판매비와관리비_포장비;
	@Column(name="판매비와관리비_연구개발비")
	private Double 판매비와관리비_연구개발비;
	@Column(name="판매비와관리비_개발비상각")
	private Double 판매비와관리비_개발비상각;
	@Column(name="판매비와관리비_대손상각비")
	private Double 판매비와관리비_대손상각비;
	@Column(name="판매비와관리비_감가상각비")
	private Double 판매비와관리비_감가상각비;
	@Column(name="판매비와관리비_무형자산상각비")
	private Double 판매비와관리비_무형자산상각비;
	@Column(name="판매비와관리비_기타")
	private Double 판매비와관리비_기타;
	@Column(name="영업이익")
	private Double 영업이익;
	@Column(name="금융손익")
	private Double 금융손익;
	@Column(name="금융수익")
	private Double 금융수익;
	@Column(name="금융수익_이자수익")
	private Double 금융수익_이자수익;
	@Column(name="금융수익_외환거래이익")
	private Double 금융수익_외환거래이익;
	@Column(name="금융수익_외환거래이익_외환차익")
	private Double 금융수익_외환거래이익_외환차익;
	@Column(name="금융수익_외환거래이익_외화환산이익")
	private Double 금융수익_외환거래이익_외화환산이익;
	@Column(name="금융수익_외환거래이익_기타외화거래이익")
	private Double 금융수익_외환거래이익_기타외화거래이익;
	@Column(name="금융수익_배당금수익")
	private Double 금융수익_배당금수익;
	@Column(name="금융수익_금융자산처분이익")
	private Double 금융수익_금융자산처분이익;
	@Column(name="금융수익_금융자산평가이익")
	private Double 금융수익_금융자산평가이익;
	@Column(name="금융수익_파생상품이익")
	private Double 금융수익_파생상품이익;
	@Column(name="금융수익_기타금융수익")
	private Double 금융수익_기타금융수익;
	@Column(name="금융비용")
	private Double 금융비용;
	@Column(name="금융비용_이자비용")
	private Double 금융비용_이자비용;
	@Column(name="금융비용_외환거래손실")
	private Double 금융비용_외환거래손실;
	@Column(name="금융비용_외환거래손실_외환차손")
	private Double 금융비용_외환거래손실_외환차손;
	@Column(name="금융비용_외환거래손실_외화환산손실")
	private Double 금융비용_외환거래손실_외화환산손실;
	@Column(name="금융비용_외환거래손실_기타외환거래손실")
	private Double 금융비용_외환거래손실_기타외환거래손실;
	@Column(name="금융비용_대손상각비")
	private Double 금융비용_대손상각비;
	@Column(name="금융비용_금융자산처분손실")
	private Double 금융비용_금융자산처분손실;
	@Column(name="금융비용_금융자산평가손실")
	private Double 금융비용_금융자산평가손실;
	@Column(name="금융비용_파생상품손실")
	private Double 금융비용_파생상품손실;
	@Column(name="금융비용_기타금융비용")
	private Double 금융비용_기타금융비용;
	@Column(name="기타영업외손익")
	private Double 기타영업외손익;
	@Column(name="기타영업외수익")
	private Double 기타영업외수익;
	@Column(name="기타영업외수익_외환거래이익")
	private Double 기타영업외수익_외환거래이익;
	@Column(name="기타영업외수익_외환거래이익_외환차익")
	private Double 기타영업외수익_외환거래이익_외환차익;
	@Column(name="기타영업외수익_외환거래이익_외화환산이익")
	private Double 기타영업외수익_외환거래이익_외화환산이익;
	@Column(name="기타영업외수익_외환거래이익_기타외화거래이익")
	private Double 기타영업외수익_외환거래이익_기타외화거래이익;
	@Column(name="기타영업외수익_수수료수익")
	private Double 기타영업외수익_수수료수익;
	@Column(name="기타영업외수익_임대료")
	private Double 기타영업외수익_임대료;
	@Column(name="기타영업외수익_로열티수익")
	private Double 기타영업외수익_로열티수익;
	@Column(name="기타영업외수익_투자부동산처분이익")
	private Double 기타영업외수익_투자부동산처분이익;
	@Column(name="기타영업외수익_투자자산처분이익")
	private Double 기타영업외수익_투자자산처분이익;
	@Column(name="기타영업외수익_유형리스자산처분이익")
	private Double 기타영업외수익_유형리스자산처분이익;
	@Column(name="기타영업외수익_기타자산처분이익")
	private Double 기타영업외수익_기타자산처분이익;
	@Column(name="기타영업외수익_파생상품이익")
	private Double 기타영업외수익_파생상품이익;
	@Column(name="기타영업외수익_자산손상차손환입")
	private Double 기타영업외수익_자산손상차손환입;
	@Column(name="기타영업외수익_대손충당금환입")
	private Double 기타영업외수익_대손충당금환입;
	@Column(name="기타영업외수익_투자자산평가이익")
	private Double 기타영업외수익_투자자산평가이익;
	@Column(name="기타영업외수익_기타")
	private Double 기타영업외수익_기타;
	@Column(name="기타영업외비용")
	private Double 기타영업외비용;
	@Column(name="기타영업외비용_외환거래손실")
	private Double 기타영업외비용_외환거래손실;
	@Column(name="기타영업외비용_외환거래손실_외환차손")
	private Double 기타영업외비용_외환거래손실_외환차손;
	@Column(name="기타영업외비용_외환거래손실_외화환산손실")
	private Double 기타영업외비용_외환거래손실_외화환산손실;
	@Column(name="기타영업외비용_외환거래손실_기타외환거래손실")
	private Double 기타영업외비용_외환거래손실_기타외환거래손실;
	@Column(name="기타영업외비용_기타대손상각비")
	private Double 기타영업외비용_기타대손상각비;
	@Column(name="기타영업외비용_기부금")
	private Double 기타영업외비용_기부금;
	@Column(name="기타영업외비용_투자부동산처분손실")
	private Double 기타영업외비용_투자부동산처분손실;
	@Column(name="기타영업외비용_투자자산처분손실")
	private Double 기타영업외비용_투자자산처분손실;
	@Column(name="기타영업외비용_유형리스자산처분손실")
	private Double 기타영업외비용_유형리스자산처분손실;
	@Column(name="기타영업외비용_기타자산처분손실")
	private Double 기타영업외비용_기타자산처분손실;
	@Column(name="기타영업외비용_파생상품손실")
	private Double 기타영업외비용_파생상품손실;
	@Column(name="기타영업외비용_자산손상차손")
	private Double 기타영업외비용_자산손상차손;
	@Column(name="기타영업외비용_투자자산평가손실")
	private Double 기타영업외비용_투자자산평가손실;
	@Column(name="기타영업외비용_기타")
	private Double 기타영업외비용_기타;
	@Column(name="종속기업관련손익")
	private Double 종속기업관련손익;
	@Column(name="종속기업관련손익_지분법손익")
	private Double 종속기업관련손익_지분법손익;
	@Column(name="종속기업관련손익_지분법손익_지분법이익")
	private Double 종속기업관련손익_지분법손익_지분법이익;
	@Column(name="종속기업관련손익_지분법손익_지분법이익지분법적용주석")
	private Double 종속기업관련손익_지분법손익_지분법이익지분법적용주석;
	@Column(name="종속기업관련손익_지분법손익_지분법손실")
	private Double 종속기업관련손익_지분법손익_지분법손실;
	@Column(name="종속기업관련손익_지분법손익_지분법손실지분법적용주석")
	private Double 종속기업관련손익_지분법손익_지분법손실지분법적용주석;
	@Column(name="종속기업관련손익_지분법손익_기타")
	private Double 종속기업관련손익_지분법손익_기타;
	@Column(name="종속기업관련손익_관계기업처분손익")
	private Double 종속기업관련손익_관계기업처분손익;
	@Column(name="종속기업관련손익_관계기업처분손익_관계기업처분이익")
	private Double 종속기업관련손익_관계기업처분손익_관계기업처분이익;
	@Column(name="종속기업관련손익_관계기업처분손익_관계기업처분손실")
	private Double 종속기업관련손익_관계기업처분손익_관계기업처분손실;
	@Column(name="종속기업관련손익_관계기업처분손익_기타")
	private Double 종속기업관련손익_관계기업처분손익_기타;
	@Column(name="종속기업관련손익_종속기업관련손익")
	private Double 종속기업관련손익_종속기업관련손익;
	@Column(name="종속기업관련손익_종속기업관련손익_종속기업처분이익")
	private Double 종속기업관련손익_종속기업관련손익_종속기업처분이익;
	@Column(name="종속기업관련손익_종속기업관련손익_종속기업처분손실")
	private Double 종속기업관련손익_종속기업관련손익_종속기업처분손실;
	@Column(name="종속기업관련손익_종속기업관련손익_기타")
	private Double 종속기업관련손익_종속기업관련손익_기타;
	@Column(name="종속기업관련손익_기타")
	private Double 종속기업관련손익_기타;
	@Column(name="법인세비용차감전계속사업이익")
	private Double 법인세비용차감전계속사업이익;
	@Column(name="법인세비용")
	private Double 법인세비용;
	@Column(name="당기순이익")
	private Double 당기순이익;
	@Column(name="지배지분순이익")
	private Double 지배지분순이익;
	@Column(name="비지배지분순이익")
	private Double 비지배지분순이익;
	@Column(name="지배지분연결순이익")
	private Double 지배지분연결순이익;
	@Column(name="당기순이익지분법적용주석")
	private Double 당기순이익지분법적용주석;
	@Column(name="기타포괄이익")
	private Double 기타포괄이익;
	@Column(name="기타포괄이익_금융자산평가손익")
	private Double 기타포괄이익_금융자산평가손익;
	@Column(name="기타포괄이익_매도가능금융자산평가손익")
	private Double 기타포괄이익_매도가능금융자산평가손익;
	@Column(name="기타포괄이익_관계기업등기타포괄이익")
	private Double 기타포괄이익_관계기업등기타포괄이익;
	@Column(name="기타포괄이익_해외사업환산손익")
	private Double 기타포괄이익_해외사업환산손익;
	@Column(name="기타포괄이익_현금흐름위험회피평가손익")
	private Double 기타포괄이익_현금흐름위험회피평가손익;
	@Column(name="기타포괄이익_재평가손익")
	private Double 기타포괄이익_재평가손익;
	@Column(name="기타포괄이익_기타포괄이익관련법인세")
	private Double 기타포괄이익_기타포괄이익관련법인세;
	@Column(name="기타포괄이익_기타")
	private Double 기타포괄이익_기타;
	@Column(name="총포괄이익")
	private Double 총포괄이익;
	@Column(name="총포괄이익_지배지분총포괄이익")
	private Double 총포괄이익_지배지분총포괄이익;
	@Column(name="총포괄이익_비지배지분총포괄이익")
	private Double 총포괄이익_비지배지분총포괄이익;
	@Column(name="총포괄이익_EPSAdj지분법적용주석")
	private Double 총포괄이익_EPSAdj지분법적용주석;
	@Column(name="순이익지분법적용아이투자")
	private Double 순이익지분법적용아이투자;
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getYmDay() {
		return ymDay;
	}
	public void setYmDay(String ymDay) {
		this.ymDay = ymDay;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public Double get매출액수익() {
		return 매출액수익;
	}
	public void set매출액수익(Double 매출액수익) {
		this.매출액수익 = 매출액수익;
	}
	public Double get매출액수익_제품상품매출액() {
		return 매출액수익_제품상품매출액;
	}
	public void set매출액수익_제품상품매출액(Double 매출액수익_제품상품매출액) {
		this.매출액수익_제품상품매출액 = 매출액수익_제품상품매출액;
	}
	public Double get매출액수익_공사수익() {
		return 매출액수익_공사수익;
	}
	public void set매출액수익_공사수익(Double 매출액수익_공사수익) {
		this.매출액수익_공사수익 = 매출액수익_공사수익;
	}
	public Double get매출액수익_분양수익() {
		return 매출액수익_분양수익;
	}
	public void set매출액수익_분양수익(Double 매출액수익_분양수익) {
		this.매출액수익_분양수익 = 매출액수익_분양수익;
	}
	public Double get매출액수익_기타매출액() {
		return 매출액수익_기타매출액;
	}
	public void set매출액수익_기타매출액(Double 매출액수익_기타매출액) {
		this.매출액수익_기타매출액 = 매출액수익_기타매출액;
	}
	public Double get매출원가() {
		return 매출원가;
	}
	public void set매출원가(Double 매출원가) {
		this.매출원가 = 매출원가;
	}
	public Double get매출원가_제품상품매출원가() {
		return 매출원가_제품상품매출원가;
	}
	public void set매출원가_제품상품매출원가(Double 매출원가_제품상품매출원가) {
		this.매출원가_제품상품매출원가 = 매출원가_제품상품매출원가;
	}
	public Double get매출원가_공사원가() {
		return 매출원가_공사원가;
	}
	public void set매출원가_공사원가(Double 매출원가_공사원가) {
		this.매출원가_공사원가 = 매출원가_공사원가;
	}
	public Double get매출원가_분양원가() {
		return 매출원가_분양원가;
	}
	public void set매출원가_분양원가(Double 매출원가_분양원가) {
		this.매출원가_분양원가 = 매출원가_분양원가;
	}
	public Double get매출원가_기타매출원가() {
		return 매출원가_기타매출원가;
	}
	public void set매출원가_기타매출원가(Double 매출원가_기타매출원가) {
		this.매출원가_기타매출원가 = 매출원가_기타매출원가;
	}
	public Double get매출총이익() {
		return 매출총이익;
	}
	public void set매출총이익(Double 매출총이익) {
		this.매출총이익 = 매출총이익;
	}
	public Double get판매비와관리비() {
		return 판매비와관리비;
	}
	public void set판매비와관리비(Double 판매비와관리비) {
		this.판매비와관리비 = 판매비와관리비;
	}
	public Double get판매비와관리비_급여() {
		return 판매비와관리비_급여;
	}
	public void set판매비와관리비_급여(Double 판매비와관리비_급여) {
		this.판매비와관리비_급여 = 판매비와관리비_급여;
	}
	public Double get판매비와관리비_퇴직급여() {
		return 판매비와관리비_퇴직급여;
	}
	public void set판매비와관리비_퇴직급여(Double 판매비와관리비_퇴직급여) {
		this.판매비와관리비_퇴직급여 = 판매비와관리비_퇴직급여;
	}
	public Double get판매비와관리비_복리후생비() {
		return 판매비와관리비_복리후생비;
	}
	public void set판매비와관리비_복리후생비(Double 판매비와관리비_복리후생비) {
		this.판매비와관리비_복리후생비 = 판매비와관리비_복리후생비;
	}
	public Double get판매비와관리비_임차료() {
		return 판매비와관리비_임차료;
	}
	public void set판매비와관리비_임차료(Double 판매비와관리비_임차료) {
		this.판매비와관리비_임차료 = 판매비와관리비_임차료;
	}
	public Double get판매비와관리비_수도광열비() {
		return 판매비와관리비_수도광열비;
	}
	public void set판매비와관리비_수도광열비(Double 판매비와관리비_수도광열비) {
		this.판매비와관리비_수도광열비 = 판매비와관리비_수도광열비;
	}
	public Double get판매비와관리비_전산처리비() {
		return 판매비와관리비_전산처리비;
	}
	public void set판매비와관리비_전산처리비(Double 판매비와관리비_전산처리비) {
		this.판매비와관리비_전산처리비 = 판매비와관리비_전산처리비;
	}
	public Double get판매비와관리비_지급수수료() {
		return 판매비와관리비_지급수수료;
	}
	public void set판매비와관리비_지급수수료(Double 판매비와관리비_지급수수료) {
		this.판매비와관리비_지급수수료 = 판매비와관리비_지급수수료;
	}
	public Double get판매비와관리비_용역비() {
		return 판매비와관리비_용역비;
	}
	public void set판매비와관리비_용역비(Double 판매비와관리비_용역비) {
		this.판매비와관리비_용역비 = 판매비와관리비_용역비;
	}
	public Double get판매비와관리비_수출비용() {
		return 판매비와관리비_수출비용;
	}
	public void set판매비와관리비_수출비용(Double 판매비와관리비_수출비용) {
		this.판매비와관리비_수출비용 = 판매비와관리비_수출비용;
	}
	public Double get판매비와관리비_판매수수료() {
		return 판매비와관리비_판매수수료;
	}
	public void set판매비와관리비_판매수수료(Double 판매비와관리비_판매수수료) {
		this.판매비와관리비_판매수수료 = 판매비와관리비_판매수수료;
	}
	public Double get판매비와관리비_판매촉진비() {
		return 판매비와관리비_판매촉진비;
	}
	public void set판매비와관리비_판매촉진비(Double 판매비와관리비_판매촉진비) {
		this.판매비와관리비_판매촉진비 = 판매비와관리비_판매촉진비;
	}
	public Double get판매비와관리비_광고선전비() {
		return 판매비와관리비_광고선전비;
	}
	public void set판매비와관리비_광고선전비(Double 판매비와관리비_광고선전비) {
		this.판매비와관리비_광고선전비 = 판매비와관리비_광고선전비;
	}
	public Double get판매비와관리비_애프터서비스비() {
		return 판매비와관리비_애프터서비스비;
	}
	public void set판매비와관리비_애프터서비스비(Double 판매비와관리비_애프터서비스비) {
		this.판매비와관리비_애프터서비스비 = 판매비와관리비_애프터서비스비;
	}
	public Double get판매비와관리비_운반비() {
		return 판매비와관리비_운반비;
	}
	public void set판매비와관리비_운반비(Double 판매비와관리비_운반비) {
		this.판매비와관리비_운반비 = 판매비와관리비_운반비;
	}
	public Double get판매비와관리비_포장비() {
		return 판매비와관리비_포장비;
	}
	public void set판매비와관리비_포장비(Double 판매비와관리비_포장비) {
		this.판매비와관리비_포장비 = 판매비와관리비_포장비;
	}
	public Double get판매비와관리비_연구개발비() {
		return 판매비와관리비_연구개발비;
	}
	public void set판매비와관리비_연구개발비(Double 판매비와관리비_연구개발비) {
		this.판매비와관리비_연구개발비 = 판매비와관리비_연구개발비;
	}
	public Double get판매비와관리비_개발비상각() {
		return 판매비와관리비_개발비상각;
	}
	public void set판매비와관리비_개발비상각(Double 판매비와관리비_개발비상각) {
		this.판매비와관리비_개발비상각 = 판매비와관리비_개발비상각;
	}
	public Double get판매비와관리비_대손상각비() {
		return 판매비와관리비_대손상각비;
	}
	public void set판매비와관리비_대손상각비(Double 판매비와관리비_대손상각비) {
		this.판매비와관리비_대손상각비 = 판매비와관리비_대손상각비;
	}
	public Double get판매비와관리비_감가상각비() {
		return 판매비와관리비_감가상각비;
	}
	public void set판매비와관리비_감가상각비(Double 판매비와관리비_감가상각비) {
		this.판매비와관리비_감가상각비 = 판매비와관리비_감가상각비;
	}
	public Double get판매비와관리비_무형자산상각비() {
		return 판매비와관리비_무형자산상각비;
	}
	public void set판매비와관리비_무형자산상각비(Double 판매비와관리비_무형자산상각비) {
		this.판매비와관리비_무형자산상각비 = 판매비와관리비_무형자산상각비;
	}
	public Double get판매비와관리비_기타() {
		return 판매비와관리비_기타;
	}
	public void set판매비와관리비_기타(Double 판매비와관리비_기타) {
		this.판매비와관리비_기타 = 판매비와관리비_기타;
	}
	public Double get영업이익() {
		return 영업이익;
	}
	public void set영업이익(Double 영업이익) {
		this.영업이익 = 영업이익;
	}
	public Double get금융손익() {
		return 금융손익;
	}
	public void set금융손익(Double 금융손익) {
		this.금융손익 = 금융손익;
	}
	public Double get금융수익() {
		return 금융수익;
	}
	public void set금융수익(Double 금융수익) {
		this.금융수익 = 금융수익;
	}
	public Double get금융수익_이자수익() {
		return 금융수익_이자수익;
	}
	public void set금융수익_이자수익(Double 금융수익_이자수익) {
		this.금융수익_이자수익 = 금융수익_이자수익;
	}
	public Double get금융수익_외환거래이익() {
		return 금융수익_외환거래이익;
	}
	public void set금융수익_외환거래이익(Double 금융수익_외환거래이익) {
		this.금융수익_외환거래이익 = 금융수익_외환거래이익;
	}
	public Double get금융수익_외환거래이익_외환차익() {
		return 금융수익_외환거래이익_외환차익;
	}
	public void set금융수익_외환거래이익_외환차익(Double 금융수익_외환거래이익_외환차익) {
		this.금융수익_외환거래이익_외환차익 = 금융수익_외환거래이익_외환차익;
	}
	public Double get금융수익_외환거래이익_외화환산이익() {
		return 금융수익_외환거래이익_외화환산이익;
	}
	public void set금융수익_외환거래이익_외화환산이익(Double 금융수익_외환거래이익_외화환산이익) {
		this.금융수익_외환거래이익_외화환산이익 = 금융수익_외환거래이익_외화환산이익;
	}
	public Double get금융수익_외환거래이익_기타외화거래이익() {
		return 금융수익_외환거래이익_기타외화거래이익;
	}
	public void set금융수익_외환거래이익_기타외화거래이익(Double 금융수익_외환거래이익_기타외화거래이익) {
		this.금융수익_외환거래이익_기타외화거래이익 = 금융수익_외환거래이익_기타외화거래이익;
	}
	public Double get금융수익_배당금수익() {
		return 금융수익_배당금수익;
	}
	public void set금융수익_배당금수익(Double 금융수익_배당금수익) {
		this.금융수익_배당금수익 = 금융수익_배당금수익;
	}
	public Double get금융수익_금융자산처분이익() {
		return 금융수익_금융자산처분이익;
	}
	public void set금융수익_금융자산처분이익(Double 금융수익_금융자산처분이익) {
		this.금융수익_금융자산처분이익 = 금융수익_금융자산처분이익;
	}
	public Double get금융수익_금융자산평가이익() {
		return 금융수익_금융자산평가이익;
	}
	public void set금융수익_금융자산평가이익(Double 금융수익_금융자산평가이익) {
		this.금융수익_금융자산평가이익 = 금융수익_금융자산평가이익;
	}
	public Double get금융수익_파생상품이익() {
		return 금융수익_파생상품이익;
	}
	public void set금융수익_파생상품이익(Double 금융수익_파생상품이익) {
		this.금융수익_파생상품이익 = 금융수익_파생상품이익;
	}
	public Double get금융수익_기타금융수익() {
		return 금융수익_기타금융수익;
	}
	public void set금융수익_기타금융수익(Double 금융수익_기타금융수익) {
		this.금융수익_기타금융수익 = 금융수익_기타금융수익;
	}
	public Double get금융비용() {
		return 금융비용;
	}
	public void set금융비용(Double 금융비용) {
		this.금융비용 = 금융비용;
	}
	public Double get금융비용_이자비용() {
		return 금융비용_이자비용;
	}
	public void set금융비용_이자비용(Double 금융비용_이자비용) {
		this.금융비용_이자비용 = 금융비용_이자비용;
	}
	public Double get금융비용_외환거래손실() {
		return 금융비용_외환거래손실;
	}
	public void set금융비용_외환거래손실(Double 금융비용_외환거래손실) {
		this.금융비용_외환거래손실 = 금융비용_외환거래손실;
	}
	public Double get금융비용_외환거래손실_외환차손() {
		return 금융비용_외환거래손실_외환차손;
	}
	public void set금융비용_외환거래손실_외환차손(Double 금융비용_외환거래손실_외환차손) {
		this.금융비용_외환거래손실_외환차손 = 금융비용_외환거래손실_외환차손;
	}
	public Double get금융비용_외환거래손실_외화환산손실() {
		return 금융비용_외환거래손실_외화환산손실;
	}
	public void set금융비용_외환거래손실_외화환산손실(Double 금융비용_외환거래손실_외화환산손실) {
		this.금융비용_외환거래손실_외화환산손실 = 금융비용_외환거래손실_외화환산손실;
	}
	public Double get금융비용_외환거래손실_기타외환거래손실() {
		return 금융비용_외환거래손실_기타외환거래손실;
	}
	public void set금융비용_외환거래손실_기타외환거래손실(Double 금융비용_외환거래손실_기타외환거래손실) {
		this.금융비용_외환거래손실_기타외환거래손실 = 금융비용_외환거래손실_기타외환거래손실;
	}
	public Double get금융비용_대손상각비() {
		return 금융비용_대손상각비;
	}
	public void set금융비용_대손상각비(Double 금융비용_대손상각비) {
		this.금융비용_대손상각비 = 금융비용_대손상각비;
	}
	public Double get금융비용_금융자산처분손실() {
		return 금융비용_금융자산처분손실;
	}
	public void set금융비용_금융자산처분손실(Double 금융비용_금융자산처분손실) {
		this.금융비용_금융자산처분손실 = 금융비용_금융자산처분손실;
	}
	public Double get금융비용_금융자산평가손실() {
		return 금융비용_금융자산평가손실;
	}
	public void set금융비용_금융자산평가손실(Double 금융비용_금융자산평가손실) {
		this.금융비용_금융자산평가손실 = 금융비용_금융자산평가손실;
	}
	public Double get금융비용_파생상품손실() {
		return 금융비용_파생상품손실;
	}
	public void set금융비용_파생상품손실(Double 금융비용_파생상품손실) {
		this.금융비용_파생상품손실 = 금융비용_파생상품손실;
	}
	public Double get금융비용_기타금융비용() {
		return 금융비용_기타금융비용;
	}
	public void set금융비용_기타금융비용(Double 금융비용_기타금융비용) {
		this.금융비용_기타금융비용 = 금융비용_기타금융비용;
	}
	public Double get기타영업외손익() {
		return 기타영업외손익;
	}
	public void set기타영업외손익(Double 기타영업외손익) {
		this.기타영업외손익 = 기타영업외손익;
	}
	public Double get기타영업외수익() {
		return 기타영업외수익;
	}
	public void set기타영업외수익(Double 기타영업외수익) {
		this.기타영업외수익 = 기타영업외수익;
	}
	public Double get기타영업외수익_외환거래이익() {
		return 기타영업외수익_외환거래이익;
	}
	public void set기타영업외수익_외환거래이익(Double 기타영업외수익_외환거래이익) {
		this.기타영업외수익_외환거래이익 = 기타영업외수익_외환거래이익;
	}
	public Double get기타영업외수익_외환거래이익_외환차익() {
		return 기타영업외수익_외환거래이익_외환차익;
	}
	public void set기타영업외수익_외환거래이익_외환차익(Double 기타영업외수익_외환거래이익_외환차익) {
		this.기타영업외수익_외환거래이익_외환차익 = 기타영업외수익_외환거래이익_외환차익;
	}
	public Double get기타영업외수익_외환거래이익_외화환산이익() {
		return 기타영업외수익_외환거래이익_외화환산이익;
	}
	public void set기타영업외수익_외환거래이익_외화환산이익(Double 기타영업외수익_외환거래이익_외화환산이익) {
		this.기타영업외수익_외환거래이익_외화환산이익 = 기타영업외수익_외환거래이익_외화환산이익;
	}
	public Double get기타영업외수익_외환거래이익_기타외화거래이익() {
		return 기타영업외수익_외환거래이익_기타외화거래이익;
	}
	public void set기타영업외수익_외환거래이익_기타외화거래이익(Double 기타영업외수익_외환거래이익_기타외화거래이익) {
		this.기타영업외수익_외환거래이익_기타외화거래이익 = 기타영업외수익_외환거래이익_기타외화거래이익;
	}
	public Double get기타영업외수익_수수료수익() {
		return 기타영업외수익_수수료수익;
	}
	public void set기타영업외수익_수수료수익(Double 기타영업외수익_수수료수익) {
		this.기타영업외수익_수수료수익 = 기타영업외수익_수수료수익;
	}
	public Double get기타영업외수익_임대료() {
		return 기타영업외수익_임대료;
	}
	public void set기타영업외수익_임대료(Double 기타영업외수익_임대료) {
		this.기타영업외수익_임대료 = 기타영업외수익_임대료;
	}
	public Double get기타영업외수익_로열티수익() {
		return 기타영업외수익_로열티수익;
	}
	public void set기타영업외수익_로열티수익(Double 기타영업외수익_로열티수익) {
		this.기타영업외수익_로열티수익 = 기타영업외수익_로열티수익;
	}
	public Double get기타영업외수익_투자부동산처분이익() {
		return 기타영업외수익_투자부동산처분이익;
	}
	public void set기타영업외수익_투자부동산처분이익(Double 기타영업외수익_투자부동산처분이익) {
		this.기타영업외수익_투자부동산처분이익 = 기타영업외수익_투자부동산처분이익;
	}
	public Double get기타영업외수익_투자자산처분이익() {
		return 기타영업외수익_투자자산처분이익;
	}
	public void set기타영업외수익_투자자산처분이익(Double 기타영업외수익_투자자산처분이익) {
		this.기타영업외수익_투자자산처분이익 = 기타영업외수익_투자자산처분이익;
	}
	public Double get기타영업외수익_유형리스자산처분이익() {
		return 기타영업외수익_유형리스자산처분이익;
	}
	public void set기타영업외수익_유형리스자산처분이익(Double 기타영업외수익_유형리스자산처분이익) {
		this.기타영업외수익_유형리스자산처분이익 = 기타영업외수익_유형리스자산처분이익;
	}
	public Double get기타영업외수익_기타자산처분이익() {
		return 기타영업외수익_기타자산처분이익;
	}
	public void set기타영업외수익_기타자산처분이익(Double 기타영업외수익_기타자산처분이익) {
		this.기타영업외수익_기타자산처분이익 = 기타영업외수익_기타자산처분이익;
	}
	public Double get기타영업외수익_파생상품이익() {
		return 기타영업외수익_파생상품이익;
	}
	public void set기타영업외수익_파생상품이익(Double 기타영업외수익_파생상품이익) {
		this.기타영업외수익_파생상품이익 = 기타영업외수익_파생상품이익;
	}
	public Double get기타영업외수익_자산손상차손환입() {
		return 기타영업외수익_자산손상차손환입;
	}
	public void set기타영업외수익_자산손상차손환입(Double 기타영업외수익_자산손상차손환입) {
		this.기타영업외수익_자산손상차손환입 = 기타영업외수익_자산손상차손환입;
	}
	public Double get기타영업외수익_대손충당금환입() {
		return 기타영업외수익_대손충당금환입;
	}
	public void set기타영업외수익_대손충당금환입(Double 기타영업외수익_대손충당금환입) {
		this.기타영업외수익_대손충당금환입 = 기타영업외수익_대손충당금환입;
	}
	public Double get기타영업외수익_투자자산평가이익() {
		return 기타영업외수익_투자자산평가이익;
	}
	public void set기타영업외수익_투자자산평가이익(Double 기타영업외수익_투자자산평가이익) {
		this.기타영업외수익_투자자산평가이익 = 기타영업외수익_투자자산평가이익;
	}
	public Double get기타영업외수익_기타() {
		return 기타영업외수익_기타;
	}
	public void set기타영업외수익_기타(Double 기타영업외수익_기타) {
		this.기타영업외수익_기타 = 기타영업외수익_기타;
	}
	public Double get기타영업외비용() {
		return 기타영업외비용;
	}
	public void set기타영업외비용(Double 기타영업외비용) {
		this.기타영업외비용 = 기타영업외비용;
	}
	public Double get기타영업외비용_외환거래손실() {
		return 기타영업외비용_외환거래손실;
	}
	public void set기타영업외비용_외환거래손실(Double 기타영업외비용_외환거래손실) {
		this.기타영업외비용_외환거래손실 = 기타영업외비용_외환거래손실;
	}
	public Double get기타영업외비용_외환거래손실_외환차손() {
		return 기타영업외비용_외환거래손실_외환차손;
	}
	public void set기타영업외비용_외환거래손실_외환차손(Double 기타영업외비용_외환거래손실_외환차손) {
		this.기타영업외비용_외환거래손실_외환차손 = 기타영업외비용_외환거래손실_외환차손;
	}
	public Double get기타영업외비용_외환거래손실_외화환산손실() {
		return 기타영업외비용_외환거래손실_외화환산손실;
	}
	public void set기타영업외비용_외환거래손실_외화환산손실(Double 기타영업외비용_외환거래손실_외화환산손실) {
		this.기타영업외비용_외환거래손실_외화환산손실 = 기타영업외비용_외환거래손실_외화환산손실;
	}
	public Double get기타영업외비용_외환거래손실_기타외환거래손실() {
		return 기타영업외비용_외환거래손실_기타외환거래손실;
	}
	public void set기타영업외비용_외환거래손실_기타외환거래손실(Double 기타영업외비용_외환거래손실_기타외환거래손실) {
		this.기타영업외비용_외환거래손실_기타외환거래손실 = 기타영업외비용_외환거래손실_기타외환거래손실;
	}
	public Double get기타영업외비용_기타대손상각비() {
		return 기타영업외비용_기타대손상각비;
	}
	public void set기타영업외비용_기타대손상각비(Double 기타영업외비용_기타대손상각비) {
		this.기타영업외비용_기타대손상각비 = 기타영업외비용_기타대손상각비;
	}
	public Double get기타영업외비용_기부금() {
		return 기타영업외비용_기부금;
	}
	public void set기타영업외비용_기부금(Double 기타영업외비용_기부금) {
		this.기타영업외비용_기부금 = 기타영업외비용_기부금;
	}
	public Double get기타영업외비용_투자부동산처분손실() {
		return 기타영업외비용_투자부동산처분손실;
	}
	public void set기타영업외비용_투자부동산처분손실(Double 기타영업외비용_투자부동산처분손실) {
		this.기타영업외비용_투자부동산처분손실 = 기타영업외비용_투자부동산처분손실;
	}
	public Double get기타영업외비용_투자자산처분손실() {
		return 기타영업외비용_투자자산처분손실;
	}
	public void set기타영업외비용_투자자산처분손실(Double 기타영업외비용_투자자산처분손실) {
		this.기타영업외비용_투자자산처분손실 = 기타영업외비용_투자자산처분손실;
	}
	public Double get기타영업외비용_유형리스자산처분손실() {
		return 기타영업외비용_유형리스자산처분손실;
	}
	public void set기타영업외비용_유형리스자산처분손실(Double 기타영업외비용_유형리스자산처분손실) {
		this.기타영업외비용_유형리스자산처분손실 = 기타영업외비용_유형리스자산처분손실;
	}
	public Double get기타영업외비용_기타자산처분손실() {
		return 기타영업외비용_기타자산처분손실;
	}
	public void set기타영업외비용_기타자산처분손실(Double 기타영업외비용_기타자산처분손실) {
		this.기타영업외비용_기타자산처분손실 = 기타영업외비용_기타자산처분손실;
	}
	public Double get기타영업외비용_파생상품손실() {
		return 기타영업외비용_파생상품손실;
	}
	public void set기타영업외비용_파생상품손실(Double 기타영업외비용_파생상품손실) {
		this.기타영업외비용_파생상품손실 = 기타영업외비용_파생상품손실;
	}
	public Double get기타영업외비용_자산손상차손() {
		return 기타영업외비용_자산손상차손;
	}
	public void set기타영업외비용_자산손상차손(Double 기타영업외비용_자산손상차손) {
		this.기타영업외비용_자산손상차손 = 기타영업외비용_자산손상차손;
	}
	public Double get기타영업외비용_투자자산평가손실() {
		return 기타영업외비용_투자자산평가손실;
	}
	public void set기타영업외비용_투자자산평가손실(Double 기타영업외비용_투자자산평가손실) {
		this.기타영업외비용_투자자산평가손실 = 기타영업외비용_투자자산평가손실;
	}
	public Double get기타영업외비용_기타() {
		return 기타영업외비용_기타;
	}
	public void set기타영업외비용_기타(Double 기타영업외비용_기타) {
		this.기타영업외비용_기타 = 기타영업외비용_기타;
	}
	public Double get종속기업관련손익() {
		return 종속기업관련손익;
	}
	public void set종속기업관련손익(Double 종속기업관련손익) {
		this.종속기업관련손익 = 종속기업관련손익;
	}
	public Double get종속기업관련손익_지분법손익() {
		return 종속기업관련손익_지분법손익;
	}
	public void set종속기업관련손익_지분법손익(Double 종속기업관련손익_지분법손익) {
		this.종속기업관련손익_지분법손익 = 종속기업관련손익_지분법손익;
	}
	public Double get종속기업관련손익_지분법손익_지분법이익() {
		return 종속기업관련손익_지분법손익_지분법이익;
	}
	public void set종속기업관련손익_지분법손익_지분법이익(Double 종속기업관련손익_지분법손익_지분법이익) {
		this.종속기업관련손익_지분법손익_지분법이익 = 종속기업관련손익_지분법손익_지분법이익;
	}
	public Double get종속기업관련손익_지분법손익_지분법이익지분법적용주석() {
		return 종속기업관련손익_지분법손익_지분법이익지분법적용주석;
	}
	public void set종속기업관련손익_지분법손익_지분법이익지분법적용주석(Double 종속기업관련손익_지분법손익_지분법이익지분법적용주석) {
		this.종속기업관련손익_지분법손익_지분법이익지분법적용주석 = 종속기업관련손익_지분법손익_지분법이익지분법적용주석;
	}
	public Double get종속기업관련손익_지분법손익_지분법손실() {
		return 종속기업관련손익_지분법손익_지분법손실;
	}
	public void set종속기업관련손익_지분법손익_지분법손실(Double 종속기업관련손익_지분법손익_지분법손실) {
		this.종속기업관련손익_지분법손익_지분법손실 = 종속기업관련손익_지분법손익_지분법손실;
	}
	public Double get종속기업관련손익_지분법손익_지분법손실지분법적용주석() {
		return 종속기업관련손익_지분법손익_지분법손실지분법적용주석;
	}
	public void set종속기업관련손익_지분법손익_지분법손실지분법적용주석(Double 종속기업관련손익_지분법손익_지분법손실지분법적용주석) {
		this.종속기업관련손익_지분법손익_지분법손실지분법적용주석 = 종속기업관련손익_지분법손익_지분법손실지분법적용주석;
	}
	public Double get종속기업관련손익_지분법손익_기타() {
		return 종속기업관련손익_지분법손익_기타;
	}
	public void set종속기업관련손익_지분법손익_기타(Double 종속기업관련손익_지분법손익_기타) {
		this.종속기업관련손익_지분법손익_기타 = 종속기업관련손익_지분법손익_기타;
	}
	public Double get종속기업관련손익_관계기업처분손익() {
		return 종속기업관련손익_관계기업처분손익;
	}
	public void set종속기업관련손익_관계기업처분손익(Double 종속기업관련손익_관계기업처분손익) {
		this.종속기업관련손익_관계기업처분손익 = 종속기업관련손익_관계기업처분손익;
	}
	public Double get종속기업관련손익_관계기업처분손익_관계기업처분이익() {
		return 종속기업관련손익_관계기업처분손익_관계기업처분이익;
	}
	public void set종속기업관련손익_관계기업처분손익_관계기업처분이익(Double 종속기업관련손익_관계기업처분손익_관계기업처분이익) {
		this.종속기업관련손익_관계기업처분손익_관계기업처분이익 = 종속기업관련손익_관계기업처분손익_관계기업처분이익;
	}
	public Double get종속기업관련손익_관계기업처분손익_관계기업처분손실() {
		return 종속기업관련손익_관계기업처분손익_관계기업처분손실;
	}
	public void set종속기업관련손익_관계기업처분손익_관계기업처분손실(Double 종속기업관련손익_관계기업처분손익_관계기업처분손실) {
		this.종속기업관련손익_관계기업처분손익_관계기업처분손실 = 종속기업관련손익_관계기업처분손익_관계기업처분손실;
	}
	public Double get종속기업관련손익_관계기업처분손익_기타() {
		return 종속기업관련손익_관계기업처분손익_기타;
	}
	public void set종속기업관련손익_관계기업처분손익_기타(Double 종속기업관련손익_관계기업처분손익_기타) {
		this.종속기업관련손익_관계기업처분손익_기타 = 종속기업관련손익_관계기업처분손익_기타;
	}
	public Double get종속기업관련손익_종속기업관련손익() {
		return 종속기업관련손익_종속기업관련손익;
	}
	public void set종속기업관련손익_종속기업관련손익(Double 종속기업관련손익_종속기업관련손익) {
		this.종속기업관련손익_종속기업관련손익 = 종속기업관련손익_종속기업관련손익;
	}
	public Double get종속기업관련손익_종속기업관련손익_종속기업처분이익() {
		return 종속기업관련손익_종속기업관련손익_종속기업처분이익;
	}
	public void set종속기업관련손익_종속기업관련손익_종속기업처분이익(Double 종속기업관련손익_종속기업관련손익_종속기업처분이익) {
		this.종속기업관련손익_종속기업관련손익_종속기업처분이익 = 종속기업관련손익_종속기업관련손익_종속기업처분이익;
	}
	public Double get종속기업관련손익_종속기업관련손익_종속기업처분손실() {
		return 종속기업관련손익_종속기업관련손익_종속기업처분손실;
	}
	public void set종속기업관련손익_종속기업관련손익_종속기업처분손실(Double 종속기업관련손익_종속기업관련손익_종속기업처분손실) {
		this.종속기업관련손익_종속기업관련손익_종속기업처분손실 = 종속기업관련손익_종속기업관련손익_종속기업처분손실;
	}
	public Double get종속기업관련손익_종속기업관련손익_기타() {
		return 종속기업관련손익_종속기업관련손익_기타;
	}
	public void set종속기업관련손익_종속기업관련손익_기타(Double 종속기업관련손익_종속기업관련손익_기타) {
		this.종속기업관련손익_종속기업관련손익_기타 = 종속기업관련손익_종속기업관련손익_기타;
	}
	public Double get종속기업관련손익_기타() {
		return 종속기업관련손익_기타;
	}
	public void set종속기업관련손익_기타(Double 종속기업관련손익_기타) {
		this.종속기업관련손익_기타 = 종속기업관련손익_기타;
	}
	public Double get법인세비용차감전계속사업이익() {
		return 법인세비용차감전계속사업이익;
	}
	public void set법인세비용차감전계속사업이익(Double 법인세비용차감전계속사업이익) {
		this.법인세비용차감전계속사업이익 = 법인세비용차감전계속사업이익;
	}
	public Double get법인세비용() {
		return 법인세비용;
	}
	public void set법인세비용(Double 법인세비용) {
		this.법인세비용 = 법인세비용;
	}
	public Double get당기순이익() {
		return 당기순이익;
	}
	public void set당기순이익(Double 당기순이익) {
		this.당기순이익 = 당기순이익;
	}
	public Double get지배지분순이익() {
		return 지배지분순이익;
	}
	public void set지배지분순이익(Double 지배지분순이익) {
		this.지배지분순이익 = 지배지분순이익;
	}
	public Double get비지배지분순이익() {
		return 비지배지분순이익;
	}
	public void set비지배지분순이익(Double 비지배지분순이익) {
		this.비지배지분순이익 = 비지배지분순이익;
	}
	public Double get지배지분연결순이익() {
		return 지배지분연결순이익;
	}
	public void set지배지분연결순이익(Double 지배지분연결순이익) {
		this.지배지분연결순이익 = 지배지분연결순이익;
	}
	public Double get당기순이익지분법적용주석() {
		return 당기순이익지분법적용주석;
	}
	public void set당기순이익지분법적용주석(Double 당기순이익지분법적용주석) {
		this.당기순이익지분법적용주석 = 당기순이익지분법적용주석;
	}
	public Double get기타포괄이익() {
		return 기타포괄이익;
	}
	public void set기타포괄이익(Double 기타포괄이익) {
		this.기타포괄이익 = 기타포괄이익;
	}
	public Double get기타포괄이익_금융자산평가손익() {
		return 기타포괄이익_금융자산평가손익;
	}
	public void set기타포괄이익_금융자산평가손익(Double 기타포괄이익_금융자산평가손익) {
		this.기타포괄이익_금융자산평가손익 = 기타포괄이익_금융자산평가손익;
	}
	public Double get기타포괄이익_매도가능금융자산평가손익() {
		return 기타포괄이익_매도가능금융자산평가손익;
	}
	public void set기타포괄이익_매도가능금융자산평가손익(Double 기타포괄이익_매도가능금융자산평가손익) {
		this.기타포괄이익_매도가능금융자산평가손익 = 기타포괄이익_매도가능금융자산평가손익;
	}
	public Double get기타포괄이익_관계기업등기타포괄이익() {
		return 기타포괄이익_관계기업등기타포괄이익;
	}
	public void set기타포괄이익_관계기업등기타포괄이익(Double 기타포괄이익_관계기업등기타포괄이익) {
		this.기타포괄이익_관계기업등기타포괄이익 = 기타포괄이익_관계기업등기타포괄이익;
	}
	public Double get기타포괄이익_해외사업환산손익() {
		return 기타포괄이익_해외사업환산손익;
	}
	public void set기타포괄이익_해외사업환산손익(Double 기타포괄이익_해외사업환산손익) {
		this.기타포괄이익_해외사업환산손익 = 기타포괄이익_해외사업환산손익;
	}
	public Double get기타포괄이익_현금흐름위험회피평가손익() {
		return 기타포괄이익_현금흐름위험회피평가손익;
	}
	public void set기타포괄이익_현금흐름위험회피평가손익(Double 기타포괄이익_현금흐름위험회피평가손익) {
		this.기타포괄이익_현금흐름위험회피평가손익 = 기타포괄이익_현금흐름위험회피평가손익;
	}
	public Double get기타포괄이익_재평가손익() {
		return 기타포괄이익_재평가손익;
	}
	public void set기타포괄이익_재평가손익(Double 기타포괄이익_재평가손익) {
		this.기타포괄이익_재평가손익 = 기타포괄이익_재평가손익;
	}
	public Double get기타포괄이익_기타포괄이익관련법인세() {
		return 기타포괄이익_기타포괄이익관련법인세;
	}
	public void set기타포괄이익_기타포괄이익관련법인세(Double 기타포괄이익_기타포괄이익관련법인세) {
		this.기타포괄이익_기타포괄이익관련법인세 = 기타포괄이익_기타포괄이익관련법인세;
	}
	public Double get기타포괄이익_기타() {
		return 기타포괄이익_기타;
	}
	public void set기타포괄이익_기타(Double 기타포괄이익_기타) {
		this.기타포괄이익_기타 = 기타포괄이익_기타;
	}
	public Double get총포괄이익() {
		return 총포괄이익;
	}
	public void set총포괄이익(Double 총포괄이익) {
		this.총포괄이익 = 총포괄이익;
	}
	public Double get총포괄이익_지배지분총포괄이익() {
		return 총포괄이익_지배지분총포괄이익;
	}
	public void set총포괄이익_지배지분총포괄이익(Double 총포괄이익_지배지분총포괄이익) {
		this.총포괄이익_지배지분총포괄이익 = 총포괄이익_지배지분총포괄이익;
	}
	public Double get총포괄이익_비지배지분총포괄이익() {
		return 총포괄이익_비지배지분총포괄이익;
	}
	public void set총포괄이익_비지배지분총포괄이익(Double 총포괄이익_비지배지분총포괄이익) {
		this.총포괄이익_비지배지분총포괄이익 = 총포괄이익_비지배지분총포괄이익;
	}
	public Double get총포괄이익_EPSAdj지분법적용주석() {
		return 총포괄이익_EPSAdj지분법적용주석;
	}
	public void set총포괄이익_EPSAdj지분법적용주석(Double 총포괄이익_EPSAdj지분법적용주석) {
		this.총포괄이익_EPSAdj지분법적용주석 = 총포괄이익_EPSAdj지분법적용주석;
	}
	public Double get순이익지분법적용아이투자() {
		return 순이익지분법적용아이투자;
	}
	public void set순이익지분법적용아이투자(Double 순이익지분법적용아이투자) {
		this.순이익지분법적용아이투자 = 순이익지분법적용아이투자;
	}
	
	


	
	
}
