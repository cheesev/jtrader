package com.app4j.tradeobserver.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.app4j.tradeobserver.data.entity.id.FinancialPositionStatementIFRSDtoId;

@Entity
@Table (name = "FINANCIAL_POSITION_STATEMENT_IFRS")
@IdClass(FinancialPositionStatementIFRSDtoId.class)
public class FinancialPositionStatementIFRSDto {
	
	@Id
    @Column (name = "STOCK_CODE")
    private String stockCode;	
	@Id
	@Column (name="YM_DAY")
	private String ymDay;
	@Id
	@Column (name="DATE_TYPE")
	private String dateType;		// 분기, 연간 (Q, Y)
	@Id
	@Column (name="DATA_TYPE")
	private String dataType;		// 연결, 별도 (L, S)
	
	
	@Column(name="자산총계")
	private Double 자산총계;
	@Column(name="유동자산")
	private Double 유동자산;
	@Column(name="유동자산_재고자산")
	private Double 유동자산_재고자산;
	@Column(name="유동자산_재고자산_상품")
	private Double 유동자산_재고자산_상품;
	@Column(name="유동자산_재고자산_제품")
	private Double 유동자산_재고자산_제품;
	@Column(name="유동자산_재고자산_완성건물")
	private Double 유동자산_재고자산_완성건물;
	@Column(name="유동자산_재고자산_원재료")
	private Double 유동자산_재고자산_원재료;
	@Column(name="유동자산_재고자산_용지")
	private Double 유동자산_재고자산_용지;
	@Column(name="유동자산_재고자산_재공품")
	private Double 유동자산_재고자산_재공품;
	@Column(name="유동자산_재고자산_미완성건물공사")
	private Double 유동자산_재고자산_미완성건물공사;
	@Column(name="유동자산_재고자산_반제품")
	private Double 유동자산_재고자산_반제품;
	@Column(name="유동자산_재고자산_미착품")
	private Double 유동자산_재고자산_미착품;
	@Column(name="유동자산_재고자산_저장품소모품")
	private Double 유동자산_재고자산_저장품소모품;
	@Column(name="유동자산_재고자산_기타재고자산")
	private Double 유동자산_재고자산_기타재고자산;
	@Column(name="유동자산_유동생물자산")
	private Double 유동자산_유동생물자산;
	@Column(name="유동자산_단기금융자산")
	private Double 유동자산_단기금융자산;
	@Column(name="유동자산_단기금융자산_단기매매금융자산")
	private Double 유동자산_단기금융자산_단기매매금융자산;
	@Column(name="유동자산_단기금융자산_당기손익인식지정금융자산")
	private Double 유동자산_단기금융자산_당기손익인식지정금융자산;
	@Column(name="유동자산_단기금융자산_단기금융상품")
	private Double 유동자산_단기금융자산_단기금융상품;
	@Column(name="유동자산_단기금융자산_단기대여금")
	private Double 유동자산_단기금융자산_단기대여금;
	@Column(name="유동자산_단기금융자산_단기매도가능금융자산")
	private Double 유동자산_단기금융자산_단기매도가능금융자산;
	@Column(name="유동자산_단기금융자산_단기만기보유금융자산")
	private Double 유동자산_단기금융자산_단기만기보유금융자산;
	@Column(name="유동자산_단기금융자산_단기파생상품자산")
	private Double 유동자산_단기금융자산_단기파생상품자산;
	@Column(name="유동자산_단기금융자산_금융리스채권")
	private Double 유동자산_단기금융자산_금융리스채권;
	@Column(name="유동자산_단기금융자산_기타단기금융자산")
	private Double 유동자산_단기금융자산_기타단기금융자산;
	@Column(name="유동자산_매출채권및기타채권")
	private Double 유동자산_매출채권및기타채권;
	@Column(name="유동자산_매출채권및기타채권_매출채권")
	private Double 유동자산_매출채권및기타채권_매출채권;
	@Column(name="유동자산_매출채권및기타채권_미수금등")
	private Double 유동자산_매출채권및기타채권_미수금등;
	@Column(name="유동자산_매출채권및기타채권_단기대출채권")
	private Double 유동자산_매출채권및기타채권_단기대출채권;
	@Column(name="유동자산_매출채권및기타채권_미수수익")
	private Double 유동자산_매출채권및기타채권_미수수익;
	@Column(name="유동자산_매출채권및기타채권_단기보증금")
	private Double 유동자산_매출채권및기타채권_단기보증금;
	@Column(name="유동자산_매출채권및기타채권_기타단기채권")
	private Double 유동자산_매출채권및기타채권_기타단기채권;
	@Column(name="유동자산_당기법인세자산선급법인세")
	private Double 유동자산_당기법인세자산선급법인세;
	@Column(name="유동자산_기타유동자산")
	private Double 유동자산_기타유동자산;
	@Column(name="유동자산_기타유동자산_선급금등")
	private Double 유동자산_기타유동자산_선급금등;
	@Column(name="유동자산_기타유동자산_선급제세")
	private Double 유동자산_기타유동자산_선급제세;
	@Column(name="유동자산_기타유동자산_선급비용")
	private Double 유동자산_기타유동자산_선급비용;
	@Column(name="유동자산_기타유동자산_기타")
	private Double 유동자산_기타유동자산_기타;
	@Column(name="유동자산_현금및현금성자산")
	private Double 유동자산_현금및현금성자산;
	@Column(name="유동자산_매각예정비유동자산및처분자산")
	private Double 유동자산_매각예정비유동자산및처분자산;
	@Column(name="유동자산_매각예정비유동자산및처분자산_비유동자산")
	private Double 유동자산_매각예정비유동자산및처분자산_비유동자산;
	@Column(name="유동자산_매각예정비유동자산및처분자산_유동자산")
	private Double 유동자산_매각예정비유동자산및처분자산_유동자산;
	@Column(name="유동자산_매각예정비유동자산및처분자산_기타")
	private Double 유동자산_매각예정비유동자산및처분자산_기타;
	@Column(name="비유동자산")
	private Double 비유동자산;
	@Column(name="비유동자산_유형자산")
	private Double 비유동자산_유형자산;
	@Column(name="비유동자산_유형자산_토지")
	private Double 비유동자산_유형자산_토지;
	@Column(name="비유동자산_유형자산_설비자산")
	private Double 비유동자산_유형자산_설비자산;
	@Column(name="비유동자산_유형자산_설비자산_건물및부속설비")
	private Double 비유동자산_유형자산_설비자산_건물및부속설비;
	@Column(name="비유동자산_유형자산_설비자산_구축물")
	private Double 비유동자산_유형자산_설비자산_구축물;
	@Column(name="비유동자산_유형자산_설비자산_기계장치")
	private Double 비유동자산_유형자산_설비자산_기계장치;
	@Column(name="비유동자산_유형자산_설비자산_중장비")
	private Double 비유동자산_유형자산_설비자산_중장비;
	@Column(name="비유동자산_유형자산_설비자산_시설장치")
	private Double 비유동자산_유형자산_설비자산_시설장치;
	@Column(name="비유동자산_유형자산_설비자산_미착자산")
	private Double 비유동자산_유형자산_설비자산_미착자산;
	@Column(name="비유동자산_유형자산_설비자산_차량운반구")
	private Double 비유동자산_유형자산_설비자산_차량운반구;
	@Column(name="비유동자산_유형자산_설비자산_선박")
	private Double 비유동자산_유형자산_설비자산_선박;
	@Column(name="비유동자산_유형자산_설비자산_항공기")
	private Double 비유동자산_유형자산_설비자산_항공기;
	@Column(name="비유동자산_유형자산_설비자산_공구기구비품")
	private Double 비유동자산_유형자산_설비자산_공구기구비품;
	@Column(name="비유동자산_유형자산_설비자산_리스자산")
	private Double 비유동자산_유형자산_설비자산_리스자산;
	@Column(name="비유동자산_유형자산_설비자산_리스개량자산")
	private Double 비유동자산_유형자산_설비자산_리스개량자산;
	@Column(name="비유동자산_유형자산_건설중인자산")
	private Double 비유동자산_유형자산_건설중인자산;
	@Column(name="비유동자산_유형자산_기타유형자산")
	private Double 비유동자산_유형자산_기타유형자산;
	@Column(name="비유동자산_무형자산")
	private Double 비유동자산_무형자산;
	@Column(name="비유동자산_무형자산_영업권")
	private Double 비유동자산_무형자산_영업권;
	@Column(name="비유동자산_무형자산_개발비")
	private Double 비유동자산_무형자산_개발비;
	@Column(name="비유동자산_무형자산_특허권")
	private Double 비유동자산_무형자산_특허권;
	@Column(name="비유동자산_무형자산_산업재산권")
	private Double 비유동자산_무형자산_산업재산권;
	@Column(name="비유동자산_무형자산_컴퓨터소프트웨어")
	private Double 비유동자산_무형자산_컴퓨터소프트웨어;
	@Column(name="비유동자산_무형자산_브랜드")
	private Double 비유동자산_무형자산_브랜드;
	@Column(name="비유동자산_무형자산_라이센스와프랜차이즈")
	private Double 비유동자산_무형자산_라이센스와프랜차이즈;
	@Column(name="비유동자산_무형자산_저작권")
	private Double 비유동자산_무형자산_저작권;
	@Column(name="비유동자산_무형자산_회원권")
	private Double 비유동자산_무형자산_회원권;
	@Column(name="비유동자산_무형자산_제이용권")
	private Double 비유동자산_무형자산_제이용권;
	@Column(name="비유동자산_무형자산_기타")
	private Double 비유동자산_무형자산_기타;
	@Column(name="비유동자산_생물자산")
	private Double 비유동자산_생물자산;
	@Column(name="비유동자산_투자부동산")
	private Double 비유동자산_투자부동산;
	@Column(name="비유동자산_투자부동산_토지")
	private Double 비유동자산_투자부동산_토지;
	@Column(name="비유동자산_투자부동산_건물")
	private Double 비유동자산_투자부동산_건물;
	@Column(name="비유동자산_투자부동산_기타투자부동산")
	private Double 비유동자산_투자부동산_기타투자부동산;
	@Column(name="비유동자산_투자자산")
	private Double 비유동자산_투자자산;
	@Column(name="비유동자산_투자자산_관계기업등지분관련투자자산")
	private Double 비유동자산_투자자산_관계기업등지분관련투자자산;
	@Column(name="비유동자산_투자자산_관계기업등지분관련투자자산_관계기업투자지분법적용투자")
	private Double 비유동자산_투자자산_관계기업등지분관련투자자산_관계기업투자지분법적용투자;
	@Column(name="비유동자산_투자자산_관계기업등지분관련투자자산_종속기업투자")
	private Double 비유동자산_투자자산_관계기업등지분관련투자자산_종속기업투자;
	@Column(name="비유동자산_투자자산_관계기업등지분관련투자자산_기타")
	private Double 비유동자산_투자자산_관계기업등지분관련투자자산_기타;
	@Column(name="비유동자산_투자자산_장기매도가능금융자산")
	private Double 비유동자산_투자자산_장기매도가능금융자산;
	@Column(name="비유동자산_투자자산_장기만기보유금융자산")
	private Double 비유동자산_투자자산_장기만기보유금융자산;
	@Column(name="비유동자산_투자자산_기타금융자산")
	private Double 비유동자산_투자자산_기타금융자산;
	@Column(name="비유동자산_투자자산_기타금융자산_당기손익인식금융자산")
	private Double 비유동자산_투자자산_기타금융자산_당기손익인식금융자산;
	@Column(name="비유동자산_투자자산_기타금융자산_장기금융상품")
	private Double 비유동자산_투자자산_기타금융자산_장기금융상품;
	@Column(name="비유동자산_투자자산_기타금융자산_장기대여금")
	private Double 비유동자산_투자자산_기타금융자산_장기대여금;
	@Column(name="비유동자산_투자자산_기타금융자산_장기파생상품자산")
	private Double 비유동자산_투자자산_기타금융자산_장기파생상품자산;
	@Column(name="비유동자산_투자자산_기타금융자산_신주인수권")
	private Double 비유동자산_투자자산_기타금융자산_신주인수권;
	@Column(name="비유동자산_투자자산_기타금융자산_기타")
	private Double 비유동자산_투자자산_기타금융자산_기타;
	@Column(name="비유동자산_장기매출채권및기타채권")
	private Double 비유동자산_장기매출채권및기타채권;
	@Column(name="비유동자산_장기매출채권및기타채권_장기매출채권")
	private Double 비유동자산_장기매출채권및기타채권_장기매출채권;
	@Column(name="비유동자산_장기매출채권및기타채권_장기미수금등")
	private Double 비유동자산_장기매출채권및기타채권_장기미수금등;
	@Column(name="비유동자산_장기매출채권및기타채권_장기대출채권")
	private Double 비유동자산_장기매출채권및기타채권_장기대출채권;
	@Column(name="비유동자산_장기매출채권및기타채권_장기미수수익")
	private Double 비유동자산_장기매출채권및기타채권_장기미수수익;
	@Column(name="비유동자산_장기매출채권및기타채권_기타")
	private Double 비유동자산_장기매출채권및기타채권_기타;
	@Column(name="비유동자산_이연법인세자산")
	private Double 비유동자산_이연법인세자산;
	@Column(name="비유동자산_장기당기법인세자산")
	private Double 비유동자산_장기당기법인세자산;
	@Column(name="비유동자산_기타비유동자산")
	private Double 비유동자산_기타비유동자산;
	@Column(name="비유동자산_기타비유동자산_리스자산")
	private Double 비유동자산_기타비유동자산_리스자산;
	@Column(name="비유동자산_기타비유동자산_리스채권")
	private Double 비유동자산_기타비유동자산_리스채권;
	@Column(name="비유동자산_기타비유동자산_퇴직보험연금등")
	private Double 비유동자산_기타비유동자산_퇴직보험연금등;
	@Column(name="비유동자산_기타비유동자산_기타")
	private Double 비유동자산_기타비유동자산_기타;
	@Column(name="기타금융업자산")
	private Double 기타금융업자산;
	@Column(name="순자산1")	/* 자산총계 - 부채총계 */
	private Double 순자산1;
	@Column(name="순자산2")
	private Double 순자산2;   /* 자산총계 - 부채총계 - 무형자산 */
	@Column(name="부채총계")
	private Double 부채총계;
	@Column(name="유동부채")
	private Double 유동부채;
	@Column(name="유동부채_단기사채")
	private Double 유동부채_단기사채;
	@Column(name="유동부채_단기차입금")
	private Double 유동부채_단기차입금;
	@Column(name="유동부채_유동성장기부채")
	private Double 유동부채_유동성장기부채;
	@Column(name="유동부채_유동성장기부채_유동성사채")
	private Double 유동부채_유동성장기부채_유동성사채;
	@Column(name="유동부채_유동성장기부채_유동성장기차입금")
	private Double 유동부채_유동성장기부채_유동성장기차입금;
	@Column(name="유동부채_유동성장기부채_유동성외화장기차입금")
	private Double 유동부채_유동성장기부채_유동성외화장기차입금;
	@Column(name="유동부채_유동성장기부채_유동성유동화채무")
	private Double 유동부채_유동성장기부채_유동성유동화채무;
	@Column(name="유동부채_유동성장기부채_유동성연불매입채무")
	private Double 유동부채_유동성장기부채_유동성연불매입채무;
	@Column(name="유동부채_유동성장기부채_유동성금융리스부채미지급금등")
	private Double 유동부채_유동성장기부채_유동성금융리스부채미지급금등;
	@Column(name="유동부채_유동성장기부채_기타유동성장기부채")
	private Double 유동부채_유동성장기부채_기타유동성장기부채;
	@Column(name="유동부채_단기금융부채")
	private Double 유동부채_단기금융부채;
	@Column(name="유동부채_단기금융부채_단기매매금융부채")
	private Double 유동부채_단기금융부채_단기매매금융부채;
	@Column(name="유동부채_단기금융부채_당기손익인식지정금융부채")
	private Double 유동부채_단기금융부채_당기손익인식지정금융부채;
	@Column(name="유동부채_단기금융부채_단기파생상품부채")
	private Double 유동부채_단기금융부채_단기파생상품부채;
	@Column(name="유동부채_단기금융부채_금융리스부채")
	private Double 유동부채_단기금융부채_금융리스부채;
	@Column(name="유동부채_단기금융부채_보증금")
	private Double 유동부채_단기금융부채_보증금;
	@Column(name="유동부채_단기금융부채_단기상환우선주부채")
	private Double 유동부채_단기금융부채_단기상환우선주부채;
	@Column(name="유동부채_단기금융부채_지급보증계약부채")
	private Double 유동부채_단기금융부채_지급보증계약부채;
	@Column(name="유동부채_단기금융부채_기타금융부채")
	private Double 유동부채_단기금융부채_기타금융부채;
	@Column(name="유동부채_매입채무및기타채무")
	private Double 유동부채_매입채무및기타채무;
	@Column(name="유동부채_매입채무및기타채무_매입채무")
	private Double 유동부채_매입채무및기타채무_매입채무;
	@Column(name="유동부채_매입채무및기타채무_미지급금")
	private Double 유동부채_매입채무및기타채무_미지급금;
	@Column(name="유동부채_매입채무및기타채무_예수금")
	private Double 유동부채_매입채무및기타채무_예수금;
	@Column(name="유동부채_매입채무및기타채무_단기금융예수금")
	private Double 유동부채_매입채무및기타채무_단기금융예수금;
	@Column(name="유동부채_매입채무및기타채무_미지급비용")
	private Double 유동부채_매입채무및기타채무_미지급비용;
	@Column(name="유동부채_매입채무및기타채무_미지급배당금")
	private Double 유동부채_매입채무및기타채무_미지급배당금;
	@Column(name="유동부채_매입채무및기타채무_보증금")
	private Double 유동부채_매입채무및기타채무_보증금;
	@Column(name="유동부채_매입채무및기타채무_기타")
	private Double 유동부채_매입채무및기타채무_기타;
	@Column(name="유동부채_유동종업원급여충당부채")
	private Double 유동부채_유동종업원급여충당부채;
	@Column(name="유동부채_단기충당부채")
	private Double 유동부채_단기충당부채;
	@Column(name="유동부채_당기법인세부채미지급법인세")
	private Double 유동부채_당기법인세부채미지급법인세;
	@Column(name="유동부채_기타유동부채")
	private Double 유동부채_기타유동부채;
	@Column(name="유동부채_기타유동부채_선수금")
	private Double 유동부채_기타유동부채_선수금;
	@Column(name="유동부채_기타유동부채_선수수익")
	private Double 유동부채_기타유동부채_선수수익;
	@Column(name="유동부채_기타유동부채_이연수익")
	private Double 유동부채_기타유동부채_이연수익;
	@Column(name="유동부채_기타유동부채_현금결제형주식기준보상거래")
	private Double 유동부채_기타유동부채_현금결제형주식기준보상거래;
	@Column(name="유동부채_기타유동부채_기타")
	private Double 유동부채_기타유동부채_기타;
	@Column(name="유동부채_매각예정으로분류된처분자산에포함된부채")
	private Double 유동부채_매각예정으로분류된처분자산에포함된부채;
	@Column(name="유동부채_매각예정으로분류된처분자산에포함된부채_비유동부채")
	private Double 유동부채_매각예정으로분류된처분자산에포함된부채_비유동부채;
	@Column(name="유동부채_매각예정으로분류된처분자산에포함된부채_유동부채")
	private Double 유동부채_매각예정으로분류된처분자산에포함된부채_유동부채;
	@Column(name="유동부채_매각예정으로분류된처분자산에포함된부채_기타")
	private Double 유동부채_매각예정으로분류된처분자산에포함된부채_기타;
	@Column(name="비유동부채")
	private Double 비유동부채;
	@Column(name="비유동부채_사채")
	private Double 비유동부채_사채;
	@Column(name="비유동부채_사채_사채")
	private Double 비유동부채_사채_사채;
	@Column(name="비유동부채_사채_CB")
	private Double 비유동부채_사채_CB;
	@Column(name="비유동부채_사채_BW")
	private Double 비유동부채_사채_BW;
	@Column(name="비유동부채_사채_EB")
	private Double 비유동부채_사채_EB;
	@Column(name="비유동부채_장기차입금")
	private Double 비유동부채_장기차입금;
	@Column(name="비유동부채_장기금융부채")
	private Double 비유동부채_장기금융부채;
	@Column(name="비유동부채_장기금융부채_당기손익인식금융부채")
	private Double 비유동부채_장기금융부채_당기손익인식금융부채;
	@Column(name="비유동부채_장기금융부채_장기파생상품부채")
	private Double 비유동부채_장기금융부채_장기파생상품부채;
	@Column(name="비유동부채_장기금융부채_금융리스부채")
	private Double 비유동부채_장기금융부채_금융리스부채;
	@Column(name="비유동부채_장기금융부채_장기상환우선주부채")
	private Double 비유동부채_장기금융부채_장기상환우선주부채;
	@Column(name="비유동부채_장기금융부채_기타비유동금융부채")
	private Double 비유동부채_장기금융부채_기타비유동금융부채;
	@Column(name="비유동부채_장기매입채무및기타채무")
	private Double 비유동부채_장기매입채무및기타채무;
	@Column(name="비유동부채_장기매입채무및기타채무_장기매입채무")
	private Double 비유동부채_장기매입채무및기타채무_장기매입채무;
	@Column(name="비유동부채_장기매입채무및기타채무_장기미지급금")
	private Double 비유동부채_장기매입채무및기타채무_장기미지급금;
	@Column(name="비유동부채_장기매입채무및기타채무_장기예수금")
	private Double 비유동부채_장기매입채무및기타채무_장기예수금;
	@Column(name="비유동부채_장기매입채무및기타채무_장기금융예수금")
	private Double 비유동부채_장기매입채무및기타채무_장기금융예수금;
	@Column(name="비유동부채_장기매입채무및기타채무_장기미지급비용")
	private Double 비유동부채_장기매입채무및기타채무_장기미지급비용;
	@Column(name="비유동부채_장기매입채무및기타채무_장기미지급배당금")
	private Double 비유동부채_장기매입채무및기타채무_장기미지급배당금;
	@Column(name="비유동부채_장기매입채무및기타채무_장기보증금")
	private Double 비유동부채_장기매입채무및기타채무_장기보증금;
	@Column(name="비유동부채_장기매입채무및기타채무_기타")
	private Double 비유동부채_장기매입채무및기타채무_기타;
	@Column(name="비유동부채_비유동종업원급여충당부채")
	private Double 비유동부채_비유동종업원급여충당부채;
	@Column(name="비유동부채_장기충당부채")
	private Double 비유동부채_장기충당부채;
	@Column(name="비유동부채_이연법인세부채")
	private Double 비유동부채_이연법인세부채;
	@Column(name="비유동부채_장기당기법인세부채미지급법인세")
	private Double 비유동부채_장기당기법인세부채미지급법인세;
	@Column(name="비유동부채_기타비유동부채")
	private Double 비유동부채_기타비유동부채;
	@Column(name="비유동부채_기타비유동부채_장기선수금")
	private Double 비유동부채_기타비유동부채_장기선수금;
	@Column(name="비유동부채_기타비유동부채_장기선수수익")
	private Double 비유동부채_기타비유동부채_장기선수수익;
	@Column(name="비유동부채_기타비유동부채_이연수익")
	private Double 비유동부채_기타비유동부채_이연수익;
	@Column(name="비유동부채_기타비유동부채_현금결제형주식기준보상거래")
	private Double 비유동부채_기타비유동부채_현금결제형주식기준보상거래;
	@Column(name="비유동부채_기타비유동부채_기타")
	private Double 비유동부채_기타비유동부채_기타;
	@Column(name="기타금융업부채")
	private Double 기타금융업부채;
	@Column(name="이자발생부채")
	private Double 이자발생부채;
	@Column(name="순부채")
	private Double 순부채;
	@Column(name="CAPEX")
	private Double CAPEX;
	@Column(name="자본총계")
	private Double 자본총계;
	@Column(name="지배주주지분")
	private Double 지배주주지분;
	@Column(name="지배주주지분_자본금")
	private Double 지배주주지분_자본금;
	@Column(name="지배주주지분_자본금_보통주자본금")
	private Double 지배주주지분_자본금_보통주자본금;
	@Column(name="지배주주지분_자본금_우선주자본금")
	private Double 지배주주지분_자본금_우선주자본금;
	@Column(name="지배주주지분_신종자본증권")
	private Double 지배주주지분_신종자본증권;
	@Column(name="지배주주지분_자본잉여금")
	private Double 지배주주지분_자본잉여금;
	@Column(name="지배주주지분_자본잉여금_주식발행초과금")
	private Double 지배주주지분_자본잉여금_주식발행초과금;
	@Column(name="지배주주지분_자본잉여금_전환권대가")
	private Double 지배주주지분_자본잉여금_전환권대가;
	@Column(name="지배주주지분_자본잉여금_신주인수권대가")
	private Double 지배주주지분_자본잉여금_신주인수권대가;
	@Column(name="지배주주지분_자본잉여금_교환권대가")
	private Double 지배주주지분_자본잉여금_교환권대가;
	@Column(name="지배주주지분_자본잉여금_기타")
	private Double 지배주주지분_자본잉여금_기타;
	@Column(name="지배주주지분_기타자본")
	private Double 지배주주지분_기타자본;
	@Column(name="지배주주지분_기타자본_자기주식")
	private Double 지배주주지분_기타자본_자기주식;
	@Column(name="지배주주지분_기타자본_자기주식처분손실")
	private Double 지배주주지분_기타자본_자기주식처분손실;
	@Column(name="지배주주지분_기타자본_기타")
	private Double 지배주주지분_기타자본_기타;
	@Column(name="지배주주지분_기타포괄이익누계액")
	private Double 지배주주지분_기타포괄이익누계액;
	@Column(name="지배주주지분_기타포괄이익누계액_금융자산평가손익")
	private Double 지배주주지분_기타포괄이익누계액_금융자산평가손익;
	@Column(name="지배주주지분_기타포괄이익누계액_매도가능금융자산평가손익")
	private Double 지배주주지분_기타포괄이익누계액_매도가능금융자산평가손익;
	@Column(name="지배주주지분_기타포괄이익누계액_관계기업등기타포괄이익")
	private Double 지배주주지분_기타포괄이익누계액_관계기업등기타포괄이익;
	@Column(name="지배주주지분_기타포괄이익누계액_환산관련외환차이적립금환율변동차이")
	private Double 지배주주지분_기타포괄이익누계액_환산관련외환차이적립금환율변동차이;
	@Column(name="지배주주지분_기타포괄이익누계액_현금흐름위험회피적립금")
	private Double 지배주주지분_기타포괄이익누계액_현금흐름위험회피적립금;
	@Column(name="지배주주지분_기타포괄이익누계액_재평가잉여금")
	private Double 지배주주지분_기타포괄이익누계액_재평가잉여금;
	@Column(name="지배주주지분_기타포괄이익누계액_기타")
	private Double 지배주주지분_기타포괄이익누계액_기타;
	@Column(name="지배주주지분_이익잉여금")
	private Double 지배주주지분_이익잉여금;
	@Column(name="지배주주지분_지배당기순이익")
	private Double 지배주주지분_지배당기순이익;
	@Column(name="비지배주주지분")
	private Double 비지배주주지분;
	@Column(name="비지배당기순이익")
	private Double 비지배당기순이익;
	@Column(name="수권주식수")
	private Double 수권주식수;
	@Column(name="주당금액")
	private Double 주당금액;
	@Column(name="발행주식수")
	private Double 발행주식수;
	@Column(name="보통주")
	private Double 보통주;
	@Column(name="우선주")
	private Double 우선주;
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getYmDay() {
		return ymDay;
	}
	public void setYmDay(String ymDay) {
		this.ymDay = ymDay;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public Double get자산총계() {
		return 자산총계;
	}
	public void set자산총계(Double 자산총계) {
		this.자산총계 = 자산총계;
	}
	public Double get유동자산() {
		return 유동자산;
	}
	public void set유동자산(Double 유동자산) {
		this.유동자산 = 유동자산;
	}
	public Double get유동자산_재고자산() {
		return 유동자산_재고자산;
	}
	public void set유동자산_재고자산(Double 유동자산_재고자산) {
		this.유동자산_재고자산 = 유동자산_재고자산;
	}
	public Double get유동자산_재고자산_상품() {
		return 유동자산_재고자산_상품;
	}
	public void set유동자산_재고자산_상품(Double 유동자산_재고자산_상품) {
		this.유동자산_재고자산_상품 = 유동자산_재고자산_상품;
	}
	public Double get유동자산_재고자산_제품() {
		return 유동자산_재고자산_제품;
	}
	public void set유동자산_재고자산_제품(Double 유동자산_재고자산_제품) {
		this.유동자산_재고자산_제품 = 유동자산_재고자산_제품;
	}
	public Double get유동자산_재고자산_완성건물() {
		return 유동자산_재고자산_완성건물;
	}
	public void set유동자산_재고자산_완성건물(Double 유동자산_재고자산_완성건물) {
		this.유동자산_재고자산_완성건물 = 유동자산_재고자산_완성건물;
	}
	public Double get유동자산_재고자산_원재료() {
		return 유동자산_재고자산_원재료;
	}
	public void set유동자산_재고자산_원재료(Double 유동자산_재고자산_원재료) {
		this.유동자산_재고자산_원재료 = 유동자산_재고자산_원재료;
	}
	public Double get유동자산_재고자산_용지() {
		return 유동자산_재고자산_용지;
	}
	public void set유동자산_재고자산_용지(Double 유동자산_재고자산_용지) {
		this.유동자산_재고자산_용지 = 유동자산_재고자산_용지;
	}
	public Double get유동자산_재고자산_재공품() {
		return 유동자산_재고자산_재공품;
	}
	public void set유동자산_재고자산_재공품(Double 유동자산_재고자산_재공품) {
		this.유동자산_재고자산_재공품 = 유동자산_재고자산_재공품;
	}
	public Double get유동자산_재고자산_미완성건물공사() {
		return 유동자산_재고자산_미완성건물공사;
	}
	public void set유동자산_재고자산_미완성건물공사(Double 유동자산_재고자산_미완성건물공사) {
		this.유동자산_재고자산_미완성건물공사 = 유동자산_재고자산_미완성건물공사;
	}
	public Double get유동자산_재고자산_반제품() {
		return 유동자산_재고자산_반제품;
	}
	public void set유동자산_재고자산_반제품(Double 유동자산_재고자산_반제품) {
		this.유동자산_재고자산_반제품 = 유동자산_재고자산_반제품;
	}
	public Double get유동자산_재고자산_미착품() {
		return 유동자산_재고자산_미착품;
	}
	public void set유동자산_재고자산_미착품(Double 유동자산_재고자산_미착품) {
		this.유동자산_재고자산_미착품 = 유동자산_재고자산_미착품;
	}
	public Double get유동자산_재고자산_저장품소모품() {
		return 유동자산_재고자산_저장품소모품;
	}
	public void set유동자산_재고자산_저장품소모품(Double 유동자산_재고자산_저장품소모품) {
		this.유동자산_재고자산_저장품소모품 = 유동자산_재고자산_저장품소모품;
	}
	public Double get유동자산_재고자산_기타재고자산() {
		return 유동자산_재고자산_기타재고자산;
	}
	public void set유동자산_재고자산_기타재고자산(Double 유동자산_재고자산_기타재고자산) {
		this.유동자산_재고자산_기타재고자산 = 유동자산_재고자산_기타재고자산;
	}
	public Double get유동자산_유동생물자산() {
		return 유동자산_유동생물자산;
	}
	public void set유동자산_유동생물자산(Double 유동자산_유동생물자산) {
		this.유동자산_유동생물자산 = 유동자산_유동생물자산;
	}
	public Double get유동자산_단기금융자산() {
		return 유동자산_단기금융자산;
	}
	public void set유동자산_단기금융자산(Double 유동자산_단기금융자산) {
		this.유동자산_단기금융자산 = 유동자산_단기금융자산;
	}
	public Double get유동자산_단기금융자산_단기매매금융자산() {
		return 유동자산_단기금융자산_단기매매금융자산;
	}
	public void set유동자산_단기금융자산_단기매매금융자산(Double 유동자산_단기금융자산_단기매매금융자산) {
		this.유동자산_단기금융자산_단기매매금융자산 = 유동자산_단기금융자산_단기매매금융자산;
	}
	public Double get유동자산_단기금융자산_당기손익인식지정금융자산() {
		return 유동자산_단기금융자산_당기손익인식지정금융자산;
	}
	public void set유동자산_단기금융자산_당기손익인식지정금융자산(Double 유동자산_단기금융자산_당기손익인식지정금융자산) {
		this.유동자산_단기금융자산_당기손익인식지정금융자산 = 유동자산_단기금융자산_당기손익인식지정금융자산;
	}
	public Double get유동자산_단기금융자산_단기금융상품() {
		return 유동자산_단기금융자산_단기금융상품;
	}
	public void set유동자산_단기금융자산_단기금융상품(Double 유동자산_단기금융자산_단기금융상품) {
		this.유동자산_단기금융자산_단기금융상품 = 유동자산_단기금융자산_단기금융상품;
	}
	public Double get유동자산_단기금융자산_단기대여금() {
		return 유동자산_단기금융자산_단기대여금;
	}
	public void set유동자산_단기금융자산_단기대여금(Double 유동자산_단기금융자산_단기대여금) {
		this.유동자산_단기금융자산_단기대여금 = 유동자산_단기금융자산_단기대여금;
	}
	public Double get유동자산_단기금융자산_단기매도가능금융자산() {
		return 유동자산_단기금융자산_단기매도가능금융자산;
	}
	public void set유동자산_단기금융자산_단기매도가능금융자산(Double 유동자산_단기금융자산_단기매도가능금융자산) {
		this.유동자산_단기금융자산_단기매도가능금융자산 = 유동자산_단기금융자산_단기매도가능금융자산;
	}
	public Double get유동자산_단기금융자산_단기만기보유금융자산() {
		return 유동자산_단기금융자산_단기만기보유금융자산;
	}
	public void set유동자산_단기금융자산_단기만기보유금융자산(Double 유동자산_단기금융자산_단기만기보유금융자산) {
		this.유동자산_단기금융자산_단기만기보유금융자산 = 유동자산_단기금융자산_단기만기보유금융자산;
	}
	public Double get유동자산_단기금융자산_단기파생상품자산() {
		return 유동자산_단기금융자산_단기파생상품자산;
	}
	public void set유동자산_단기금융자산_단기파생상품자산(Double 유동자산_단기금융자산_단기파생상품자산) {
		this.유동자산_단기금융자산_단기파생상품자산 = 유동자산_단기금융자산_단기파생상품자산;
	}
	public Double get유동자산_단기금융자산_금융리스채권() {
		return 유동자산_단기금융자산_금융리스채권;
	}
	public void set유동자산_단기금융자산_금융리스채권(Double 유동자산_단기금융자산_금융리스채권) {
		this.유동자산_단기금융자산_금융리스채권 = 유동자산_단기금융자산_금융리스채권;
	}
	public Double get유동자산_단기금융자산_기타단기금융자산() {
		return 유동자산_단기금융자산_기타단기금융자산;
	}
	public void set유동자산_단기금융자산_기타단기금융자산(Double 유동자산_단기금융자산_기타단기금융자산) {
		this.유동자산_단기금융자산_기타단기금융자산 = 유동자산_단기금융자산_기타단기금융자산;
	}
	public Double get유동자산_매출채권및기타채권() {
		return 유동자산_매출채권및기타채권;
	}
	public void set유동자산_매출채권및기타채권(Double 유동자산_매출채권및기타채권) {
		this.유동자산_매출채권및기타채권 = 유동자산_매출채권및기타채권;
	}
	public Double get유동자산_매출채권및기타채권_매출채권() {
		return 유동자산_매출채권및기타채권_매출채권;
	}
	public void set유동자산_매출채권및기타채권_매출채권(Double 유동자산_매출채권및기타채권_매출채권) {
		this.유동자산_매출채권및기타채권_매출채권 = 유동자산_매출채권및기타채권_매출채권;
	}
	public Double get유동자산_매출채권및기타채권_미수금등() {
		return 유동자산_매출채권및기타채권_미수금등;
	}
	public void set유동자산_매출채권및기타채권_미수금등(Double 유동자산_매출채권및기타채권_미수금등) {
		this.유동자산_매출채권및기타채권_미수금등 = 유동자산_매출채권및기타채권_미수금등;
	}
	public Double get유동자산_매출채권및기타채권_단기대출채권() {
		return 유동자산_매출채권및기타채권_단기대출채권;
	}
	public void set유동자산_매출채권및기타채권_단기대출채권(Double 유동자산_매출채권및기타채권_단기대출채권) {
		this.유동자산_매출채권및기타채권_단기대출채권 = 유동자산_매출채권및기타채권_단기대출채권;
	}
	public Double get유동자산_매출채권및기타채권_미수수익() {
		return 유동자산_매출채권및기타채권_미수수익;
	}
	public void set유동자산_매출채권및기타채권_미수수익(Double 유동자산_매출채권및기타채권_미수수익) {
		this.유동자산_매출채권및기타채권_미수수익 = 유동자산_매출채권및기타채권_미수수익;
	}
	public Double get유동자산_매출채권및기타채권_단기보증금() {
		return 유동자산_매출채권및기타채권_단기보증금;
	}
	public void set유동자산_매출채권및기타채권_단기보증금(Double 유동자산_매출채권및기타채권_단기보증금) {
		this.유동자산_매출채권및기타채권_단기보증금 = 유동자산_매출채권및기타채권_단기보증금;
	}
	public Double get유동자산_매출채권및기타채권_기타단기채권() {
		return 유동자산_매출채권및기타채권_기타단기채권;
	}
	public void set유동자산_매출채권및기타채권_기타단기채권(Double 유동자산_매출채권및기타채권_기타단기채권) {
		this.유동자산_매출채권및기타채권_기타단기채권 = 유동자산_매출채권및기타채권_기타단기채권;
	}
	public Double get유동자산_당기법인세자산선급법인세() {
		return 유동자산_당기법인세자산선급법인세;
	}
	public void set유동자산_당기법인세자산선급법인세(Double 유동자산_당기법인세자산선급법인세) {
		this.유동자산_당기법인세자산선급법인세 = 유동자산_당기법인세자산선급법인세;
	}
	public Double get유동자산_기타유동자산() {
		return 유동자산_기타유동자산;
	}
	public void set유동자산_기타유동자산(Double 유동자산_기타유동자산) {
		this.유동자산_기타유동자산 = 유동자산_기타유동자산;
	}
	public Double get유동자산_기타유동자산_선급금등() {
		return 유동자산_기타유동자산_선급금등;
	}
	public void set유동자산_기타유동자산_선급금등(Double 유동자산_기타유동자산_선급금등) {
		this.유동자산_기타유동자산_선급금등 = 유동자산_기타유동자산_선급금등;
	}
	public Double get유동자산_기타유동자산_선급제세() {
		return 유동자산_기타유동자산_선급제세;
	}
	public void set유동자산_기타유동자산_선급제세(Double 유동자산_기타유동자산_선급제세) {
		this.유동자산_기타유동자산_선급제세 = 유동자산_기타유동자산_선급제세;
	}
	public Double get유동자산_기타유동자산_선급비용() {
		return 유동자산_기타유동자산_선급비용;
	}
	public void set유동자산_기타유동자산_선급비용(Double 유동자산_기타유동자산_선급비용) {
		this.유동자산_기타유동자산_선급비용 = 유동자산_기타유동자산_선급비용;
	}
	public Double get유동자산_기타유동자산_기타() {
		return 유동자산_기타유동자산_기타;
	}
	public void set유동자산_기타유동자산_기타(Double 유동자산_기타유동자산_기타) {
		this.유동자산_기타유동자산_기타 = 유동자산_기타유동자산_기타;
	}
	public Double get유동자산_현금및현금성자산() {
		return 유동자산_현금및현금성자산;
	}
	public void set유동자산_현금및현금성자산(Double 유동자산_현금및현금성자산) {
		this.유동자산_현금및현금성자산 = 유동자산_현금및현금성자산;
	}
	public Double get유동자산_매각예정비유동자산및처분자산() {
		return 유동자산_매각예정비유동자산및처분자산;
	}
	public void set유동자산_매각예정비유동자산및처분자산(Double 유동자산_매각예정비유동자산및처분자산) {
		this.유동자산_매각예정비유동자산및처분자산 = 유동자산_매각예정비유동자산및처분자산;
	}
	public Double get유동자산_매각예정비유동자산및처분자산_비유동자산() {
		return 유동자산_매각예정비유동자산및처분자산_비유동자산;
	}
	public void set유동자산_매각예정비유동자산및처분자산_비유동자산(Double 유동자산_매각예정비유동자산및처분자산_비유동자산) {
		this.유동자산_매각예정비유동자산및처분자산_비유동자산 = 유동자산_매각예정비유동자산및처분자산_비유동자산;
	}
	public Double get유동자산_매각예정비유동자산및처분자산_유동자산() {
		return 유동자산_매각예정비유동자산및처분자산_유동자산;
	}
	public void set유동자산_매각예정비유동자산및처분자산_유동자산(Double 유동자산_매각예정비유동자산및처분자산_유동자산) {
		this.유동자산_매각예정비유동자산및처분자산_유동자산 = 유동자산_매각예정비유동자산및처분자산_유동자산;
	}
	public Double get유동자산_매각예정비유동자산및처분자산_기타() {
		return 유동자산_매각예정비유동자산및처분자산_기타;
	}
	public void set유동자산_매각예정비유동자산및처분자산_기타(Double 유동자산_매각예정비유동자산및처분자산_기타) {
		this.유동자산_매각예정비유동자산및처분자산_기타 = 유동자산_매각예정비유동자산및처분자산_기타;
	}
	public Double get비유동자산() {
		return 비유동자산;
	}
	public void set비유동자산(Double 비유동자산) {
		this.비유동자산 = 비유동자산;
	}
	public Double get비유동자산_유형자산() {
		return 비유동자산_유형자산;
	}
	public void set비유동자산_유형자산(Double 비유동자산_유형자산) {
		this.비유동자산_유형자산 = 비유동자산_유형자산;
	}
	public Double get비유동자산_유형자산_토지() {
		return 비유동자산_유형자산_토지;
	}
	public void set비유동자산_유형자산_토지(Double 비유동자산_유형자산_토지) {
		this.비유동자산_유형자산_토지 = 비유동자산_유형자산_토지;
	}
	public Double get비유동자산_유형자산_설비자산() {
		return 비유동자산_유형자산_설비자산;
	}
	public void set비유동자산_유형자산_설비자산(Double 비유동자산_유형자산_설비자산) {
		this.비유동자산_유형자산_설비자산 = 비유동자산_유형자산_설비자산;
	}
	public Double get비유동자산_유형자산_설비자산_건물및부속설비() {
		return 비유동자산_유형자산_설비자산_건물및부속설비;
	}
	public void set비유동자산_유형자산_설비자산_건물및부속설비(Double 비유동자산_유형자산_설비자산_건물및부속설비) {
		this.비유동자산_유형자산_설비자산_건물및부속설비 = 비유동자산_유형자산_설비자산_건물및부속설비;
	}
	public Double get비유동자산_유형자산_설비자산_구축물() {
		return 비유동자산_유형자산_설비자산_구축물;
	}
	public void set비유동자산_유형자산_설비자산_구축물(Double 비유동자산_유형자산_설비자산_구축물) {
		this.비유동자산_유형자산_설비자산_구축물 = 비유동자산_유형자산_설비자산_구축물;
	}
	public Double get비유동자산_유형자산_설비자산_기계장치() {
		return 비유동자산_유형자산_설비자산_기계장치;
	}
	public void set비유동자산_유형자산_설비자산_기계장치(Double 비유동자산_유형자산_설비자산_기계장치) {
		this.비유동자산_유형자산_설비자산_기계장치 = 비유동자산_유형자산_설비자산_기계장치;
	}
	public Double get비유동자산_유형자산_설비자산_중장비() {
		return 비유동자산_유형자산_설비자산_중장비;
	}
	public void set비유동자산_유형자산_설비자산_중장비(Double 비유동자산_유형자산_설비자산_중장비) {
		this.비유동자산_유형자산_설비자산_중장비 = 비유동자산_유형자산_설비자산_중장비;
	}
	public Double get비유동자산_유형자산_설비자산_시설장치() {
		return 비유동자산_유형자산_설비자산_시설장치;
	}
	public void set비유동자산_유형자산_설비자산_시설장치(Double 비유동자산_유형자산_설비자산_시설장치) {
		this.비유동자산_유형자산_설비자산_시설장치 = 비유동자산_유형자산_설비자산_시설장치;
	}
	public Double get비유동자산_유형자산_설비자산_미착자산() {
		return 비유동자산_유형자산_설비자산_미착자산;
	}
	public void set비유동자산_유형자산_설비자산_미착자산(Double 비유동자산_유형자산_설비자산_미착자산) {
		this.비유동자산_유형자산_설비자산_미착자산 = 비유동자산_유형자산_설비자산_미착자산;
	}
	public Double get비유동자산_유형자산_설비자산_차량운반구() {
		return 비유동자산_유형자산_설비자산_차량운반구;
	}
	public void set비유동자산_유형자산_설비자산_차량운반구(Double 비유동자산_유형자산_설비자산_차량운반구) {
		this.비유동자산_유형자산_설비자산_차량운반구 = 비유동자산_유형자산_설비자산_차량운반구;
	}
	public Double get비유동자산_유형자산_설비자산_선박() {
		return 비유동자산_유형자산_설비자산_선박;
	}
	public void set비유동자산_유형자산_설비자산_선박(Double 비유동자산_유형자산_설비자산_선박) {
		this.비유동자산_유형자산_설비자산_선박 = 비유동자산_유형자산_설비자산_선박;
	}
	public Double get비유동자산_유형자산_설비자산_항공기() {
		return 비유동자산_유형자산_설비자산_항공기;
	}
	public void set비유동자산_유형자산_설비자산_항공기(Double 비유동자산_유형자산_설비자산_항공기) {
		this.비유동자산_유형자산_설비자산_항공기 = 비유동자산_유형자산_설비자산_항공기;
	}
	public Double get비유동자산_유형자산_설비자산_공구기구비품() {
		return 비유동자산_유형자산_설비자산_공구기구비품;
	}
	public void set비유동자산_유형자산_설비자산_공구기구비품(Double 비유동자산_유형자산_설비자산_공구기구비품) {
		this.비유동자산_유형자산_설비자산_공구기구비품 = 비유동자산_유형자산_설비자산_공구기구비품;
	}
	public Double get비유동자산_유형자산_설비자산_리스자산() {
		return 비유동자산_유형자산_설비자산_리스자산;
	}
	public void set비유동자산_유형자산_설비자산_리스자산(Double 비유동자산_유형자산_설비자산_리스자산) {
		this.비유동자산_유형자산_설비자산_리스자산 = 비유동자산_유형자산_설비자산_리스자산;
	}
	public Double get비유동자산_유형자산_설비자산_리스개량자산() {
		return 비유동자산_유형자산_설비자산_리스개량자산;
	}
	public void set비유동자산_유형자산_설비자산_리스개량자산(Double 비유동자산_유형자산_설비자산_리스개량자산) {
		this.비유동자산_유형자산_설비자산_리스개량자산 = 비유동자산_유형자산_설비자산_리스개량자산;
	}
	public Double get비유동자산_유형자산_건설중인자산() {
		return 비유동자산_유형자산_건설중인자산;
	}
	public void set비유동자산_유형자산_건설중인자산(Double 비유동자산_유형자산_건설중인자산) {
		this.비유동자산_유형자산_건설중인자산 = 비유동자산_유형자산_건설중인자산;
	}
	public Double get비유동자산_유형자산_기타유형자산() {
		return 비유동자산_유형자산_기타유형자산;
	}
	public void set비유동자산_유형자산_기타유형자산(Double 비유동자산_유형자산_기타유형자산) {
		this.비유동자산_유형자산_기타유형자산 = 비유동자산_유형자산_기타유형자산;
	}
	public Double get비유동자산_무형자산() {
		return 비유동자산_무형자산;
	}
	public void set비유동자산_무형자산(Double 비유동자산_무형자산) {
		this.비유동자산_무형자산 = 비유동자산_무형자산;
	}
	public Double get비유동자산_무형자산_영업권() {
		return 비유동자산_무형자산_영업권;
	}
	public void set비유동자산_무형자산_영업권(Double 비유동자산_무형자산_영업권) {
		this.비유동자산_무형자산_영업권 = 비유동자산_무형자산_영업권;
	}
	public Double get비유동자산_무형자산_개발비() {
		return 비유동자산_무형자산_개발비;
	}
	public void set비유동자산_무형자산_개발비(Double 비유동자산_무형자산_개발비) {
		this.비유동자산_무형자산_개발비 = 비유동자산_무형자산_개발비;
	}
	public Double get비유동자산_무형자산_특허권() {
		return 비유동자산_무형자산_특허권;
	}
	public void set비유동자산_무형자산_특허권(Double 비유동자산_무형자산_특허권) {
		this.비유동자산_무형자산_특허권 = 비유동자산_무형자산_특허권;
	}
	public Double get비유동자산_무형자산_산업재산권() {
		return 비유동자산_무형자산_산업재산권;
	}
	public void set비유동자산_무형자산_산업재산권(Double 비유동자산_무형자산_산업재산권) {
		this.비유동자산_무형자산_산업재산권 = 비유동자산_무형자산_산업재산권;
	}
	public Double get비유동자산_무형자산_컴퓨터소프트웨어() {
		return 비유동자산_무형자산_컴퓨터소프트웨어;
	}
	public void set비유동자산_무형자산_컴퓨터소프트웨어(Double 비유동자산_무형자산_컴퓨터소프트웨어) {
		this.비유동자산_무형자산_컴퓨터소프트웨어 = 비유동자산_무형자산_컴퓨터소프트웨어;
	}
	public Double get비유동자산_무형자산_브랜드() {
		return 비유동자산_무형자산_브랜드;
	}
	public void set비유동자산_무형자산_브랜드(Double 비유동자산_무형자산_브랜드) {
		this.비유동자산_무형자산_브랜드 = 비유동자산_무형자산_브랜드;
	}
	public Double get비유동자산_무형자산_라이센스와프랜차이즈() {
		return 비유동자산_무형자산_라이센스와프랜차이즈;
	}
	public void set비유동자산_무형자산_라이센스와프랜차이즈(Double 비유동자산_무형자산_라이센스와프랜차이즈) {
		this.비유동자산_무형자산_라이센스와프랜차이즈 = 비유동자산_무형자산_라이센스와프랜차이즈;
	}
	public Double get비유동자산_무형자산_저작권() {
		return 비유동자산_무형자산_저작권;
	}
	public void set비유동자산_무형자산_저작권(Double 비유동자산_무형자산_저작권) {
		this.비유동자산_무형자산_저작권 = 비유동자산_무형자산_저작권;
	}
	public Double get비유동자산_무형자산_회원권() {
		return 비유동자산_무형자산_회원권;
	}
	public void set비유동자산_무형자산_회원권(Double 비유동자산_무형자산_회원권) {
		this.비유동자산_무형자산_회원권 = 비유동자산_무형자산_회원권;
	}
	public Double get비유동자산_무형자산_제이용권() {
		return 비유동자산_무형자산_제이용권;
	}
	public void set비유동자산_무형자산_제이용권(Double 비유동자산_무형자산_제이용권) {
		this.비유동자산_무형자산_제이용권 = 비유동자산_무형자산_제이용권;
	}
	public Double get비유동자산_무형자산_기타() {
		return 비유동자산_무형자산_기타;
	}
	public void set비유동자산_무형자산_기타(Double 비유동자산_무형자산_기타) {
		this.비유동자산_무형자산_기타 = 비유동자산_무형자산_기타;
	}
	public Double get비유동자산_생물자산() {
		return 비유동자산_생물자산;
	}
	public void set비유동자산_생물자산(Double 비유동자산_생물자산) {
		this.비유동자산_생물자산 = 비유동자산_생물자산;
	}
	public Double get비유동자산_투자부동산() {
		return 비유동자산_투자부동산;
	}
	public void set비유동자산_투자부동산(Double 비유동자산_투자부동산) {
		this.비유동자산_투자부동산 = 비유동자산_투자부동산;
	}
	public Double get비유동자산_투자부동산_토지() {
		return 비유동자산_투자부동산_토지;
	}
	public void set비유동자산_투자부동산_토지(Double 비유동자산_투자부동산_토지) {
		this.비유동자산_투자부동산_토지 = 비유동자산_투자부동산_토지;
	}
	public Double get비유동자산_투자부동산_건물() {
		return 비유동자산_투자부동산_건물;
	}
	public void set비유동자산_투자부동산_건물(Double 비유동자산_투자부동산_건물) {
		this.비유동자산_투자부동산_건물 = 비유동자산_투자부동산_건물;
	}
	public Double get비유동자산_투자부동산_기타투자부동산() {
		return 비유동자산_투자부동산_기타투자부동산;
	}
	public void set비유동자산_투자부동산_기타투자부동산(Double 비유동자산_투자부동산_기타투자부동산) {
		this.비유동자산_투자부동산_기타투자부동산 = 비유동자산_투자부동산_기타투자부동산;
	}
	public Double get비유동자산_투자자산() {
		return 비유동자산_투자자산;
	}
	public void set비유동자산_투자자산(Double 비유동자산_투자자산) {
		this.비유동자산_투자자산 = 비유동자산_투자자산;
	}
	public Double get비유동자산_투자자산_관계기업등지분관련투자자산() {
		return 비유동자산_투자자산_관계기업등지분관련투자자산;
	}
	public void set비유동자산_투자자산_관계기업등지분관련투자자산(Double 비유동자산_투자자산_관계기업등지분관련투자자산) {
		this.비유동자산_투자자산_관계기업등지분관련투자자산 = 비유동자산_투자자산_관계기업등지분관련투자자산;
	}
	public Double get비유동자산_투자자산_관계기업등지분관련투자자산_관계기업투자지분법적용투자() {
		return 비유동자산_투자자산_관계기업등지분관련투자자산_관계기업투자지분법적용투자;
	}
	public void set비유동자산_투자자산_관계기업등지분관련투자자산_관계기업투자지분법적용투자(
			Double 비유동자산_투자자산_관계기업등지분관련투자자산_관계기업투자지분법적용투자) {
		this.비유동자산_투자자산_관계기업등지분관련투자자산_관계기업투자지분법적용투자 = 비유동자산_투자자산_관계기업등지분관련투자자산_관계기업투자지분법적용투자;
	}
	public Double get비유동자산_투자자산_관계기업등지분관련투자자산_종속기업투자() {
		return 비유동자산_투자자산_관계기업등지분관련투자자산_종속기업투자;
	}
	public void set비유동자산_투자자산_관계기업등지분관련투자자산_종속기업투자(
			Double 비유동자산_투자자산_관계기업등지분관련투자자산_종속기업투자) {
		this.비유동자산_투자자산_관계기업등지분관련투자자산_종속기업투자 = 비유동자산_투자자산_관계기업등지분관련투자자산_종속기업투자;
	}
	public Double get비유동자산_투자자산_관계기업등지분관련투자자산_기타() {
		return 비유동자산_투자자산_관계기업등지분관련투자자산_기타;
	}
	public void set비유동자산_투자자산_관계기업등지분관련투자자산_기타(Double 비유동자산_투자자산_관계기업등지분관련투자자산_기타) {
		this.비유동자산_투자자산_관계기업등지분관련투자자산_기타 = 비유동자산_투자자산_관계기업등지분관련투자자산_기타;
	}
	public Double get비유동자산_투자자산_장기매도가능금융자산() {
		return 비유동자산_투자자산_장기매도가능금융자산;
	}
	public void set비유동자산_투자자산_장기매도가능금융자산(Double 비유동자산_투자자산_장기매도가능금융자산) {
		this.비유동자산_투자자산_장기매도가능금융자산 = 비유동자산_투자자산_장기매도가능금융자산;
	}
	public Double get비유동자산_투자자산_장기만기보유금융자산() {
		return 비유동자산_투자자산_장기만기보유금융자산;
	}
	public void set비유동자산_투자자산_장기만기보유금융자산(Double 비유동자산_투자자산_장기만기보유금융자산) {
		this.비유동자산_투자자산_장기만기보유금융자산 = 비유동자산_투자자산_장기만기보유금융자산;
	}
	public Double get비유동자산_투자자산_기타금융자산() {
		return 비유동자산_투자자산_기타금융자산;
	}
	public void set비유동자산_투자자산_기타금융자산(Double 비유동자산_투자자산_기타금융자산) {
		this.비유동자산_투자자산_기타금융자산 = 비유동자산_투자자산_기타금융자산;
	}
	public Double get비유동자산_투자자산_기타금융자산_당기손익인식금융자산() {
		return 비유동자산_투자자산_기타금융자산_당기손익인식금융자산;
	}
	public void set비유동자산_투자자산_기타금융자산_당기손익인식금융자산(Double 비유동자산_투자자산_기타금융자산_당기손익인식금융자산) {
		this.비유동자산_투자자산_기타금융자산_당기손익인식금융자산 = 비유동자산_투자자산_기타금융자산_당기손익인식금융자산;
	}
	public Double get비유동자산_투자자산_기타금융자산_장기금융상품() {
		return 비유동자산_투자자산_기타금융자산_장기금융상품;
	}
	public void set비유동자산_투자자산_기타금융자산_장기금융상품(Double 비유동자산_투자자산_기타금융자산_장기금융상품) {
		this.비유동자산_투자자산_기타금융자산_장기금융상품 = 비유동자산_투자자산_기타금융자산_장기금융상품;
	}
	public Double get비유동자산_투자자산_기타금융자산_장기대여금() {
		return 비유동자산_투자자산_기타금융자산_장기대여금;
	}
	public void set비유동자산_투자자산_기타금융자산_장기대여금(Double 비유동자산_투자자산_기타금융자산_장기대여금) {
		this.비유동자산_투자자산_기타금융자산_장기대여금 = 비유동자산_투자자산_기타금융자산_장기대여금;
	}
	public Double get비유동자산_투자자산_기타금융자산_장기파생상품자산() {
		return 비유동자산_투자자산_기타금융자산_장기파생상품자산;
	}
	public void set비유동자산_투자자산_기타금융자산_장기파생상품자산(Double 비유동자산_투자자산_기타금융자산_장기파생상품자산) {
		this.비유동자산_투자자산_기타금융자산_장기파생상품자산 = 비유동자산_투자자산_기타금융자산_장기파생상품자산;
	}
	public Double get비유동자산_투자자산_기타금융자산_신주인수권() {
		return 비유동자산_투자자산_기타금융자산_신주인수권;
	}
	public void set비유동자산_투자자산_기타금융자산_신주인수권(Double 비유동자산_투자자산_기타금융자산_신주인수권) {
		this.비유동자산_투자자산_기타금융자산_신주인수권 = 비유동자산_투자자산_기타금융자산_신주인수권;
	}
	public Double get비유동자산_투자자산_기타금융자산_기타() {
		return 비유동자산_투자자산_기타금융자산_기타;
	}
	public void set비유동자산_투자자산_기타금융자산_기타(Double 비유동자산_투자자산_기타금융자산_기타) {
		this.비유동자산_투자자산_기타금융자산_기타 = 비유동자산_투자자산_기타금융자산_기타;
	}
	public Double get비유동자산_장기매출채권및기타채권() {
		return 비유동자산_장기매출채권및기타채권;
	}
	public void set비유동자산_장기매출채권및기타채권(Double 비유동자산_장기매출채권및기타채권) {
		this.비유동자산_장기매출채권및기타채권 = 비유동자산_장기매출채권및기타채권;
	}
	public Double get비유동자산_장기매출채권및기타채권_장기매출채권() {
		return 비유동자산_장기매출채권및기타채권_장기매출채권;
	}
	public void set비유동자산_장기매출채권및기타채권_장기매출채권(Double 비유동자산_장기매출채권및기타채권_장기매출채권) {
		this.비유동자산_장기매출채권및기타채권_장기매출채권 = 비유동자산_장기매출채권및기타채권_장기매출채권;
	}
	public Double get비유동자산_장기매출채권및기타채권_장기미수금등() {
		return 비유동자산_장기매출채권및기타채권_장기미수금등;
	}
	public void set비유동자산_장기매출채권및기타채권_장기미수금등(Double 비유동자산_장기매출채권및기타채권_장기미수금등) {
		this.비유동자산_장기매출채권및기타채권_장기미수금등 = 비유동자산_장기매출채권및기타채권_장기미수금등;
	}
	public Double get비유동자산_장기매출채권및기타채권_장기대출채권() {
		return 비유동자산_장기매출채권및기타채권_장기대출채권;
	}
	public void set비유동자산_장기매출채권및기타채권_장기대출채권(Double 비유동자산_장기매출채권및기타채권_장기대출채권) {
		this.비유동자산_장기매출채권및기타채권_장기대출채권 = 비유동자산_장기매출채권및기타채권_장기대출채권;
	}
	public Double get비유동자산_장기매출채권및기타채권_장기미수수익() {
		return 비유동자산_장기매출채권및기타채권_장기미수수익;
	}
	public void set비유동자산_장기매출채권및기타채권_장기미수수익(Double 비유동자산_장기매출채권및기타채권_장기미수수익) {
		this.비유동자산_장기매출채권및기타채권_장기미수수익 = 비유동자산_장기매출채권및기타채권_장기미수수익;
	}
	public Double get비유동자산_장기매출채권및기타채권_기타() {
		return 비유동자산_장기매출채권및기타채권_기타;
	}
	public void set비유동자산_장기매출채권및기타채권_기타(Double 비유동자산_장기매출채권및기타채권_기타) {
		this.비유동자산_장기매출채권및기타채권_기타 = 비유동자산_장기매출채권및기타채권_기타;
	}
	public Double get비유동자산_이연법인세자산() {
		return 비유동자산_이연법인세자산;
	}
	public void set비유동자산_이연법인세자산(Double 비유동자산_이연법인세자산) {
		this.비유동자산_이연법인세자산 = 비유동자산_이연법인세자산;
	}
	public Double get비유동자산_장기당기법인세자산() {
		return 비유동자산_장기당기법인세자산;
	}
	public void set비유동자산_장기당기법인세자산(Double 비유동자산_장기당기법인세자산) {
		this.비유동자산_장기당기법인세자산 = 비유동자산_장기당기법인세자산;
	}
	public Double get비유동자산_기타비유동자산() {
		return 비유동자산_기타비유동자산;
	}
	public void set비유동자산_기타비유동자산(Double 비유동자산_기타비유동자산) {
		this.비유동자산_기타비유동자산 = 비유동자산_기타비유동자산;
	}
	public Double get비유동자산_기타비유동자산_리스자산() {
		return 비유동자산_기타비유동자산_리스자산;
	}
	public void set비유동자산_기타비유동자산_리스자산(Double 비유동자산_기타비유동자산_리스자산) {
		this.비유동자산_기타비유동자산_리스자산 = 비유동자산_기타비유동자산_리스자산;
	}
	public Double get비유동자산_기타비유동자산_리스채권() {
		return 비유동자산_기타비유동자산_리스채권;
	}
	public void set비유동자산_기타비유동자산_리스채권(Double 비유동자산_기타비유동자산_리스채권) {
		this.비유동자산_기타비유동자산_리스채권 = 비유동자산_기타비유동자산_리스채권;
	}
	public Double get비유동자산_기타비유동자산_퇴직보험연금등() {
		return 비유동자산_기타비유동자산_퇴직보험연금등;
	}
	public void set비유동자산_기타비유동자산_퇴직보험연금등(Double 비유동자산_기타비유동자산_퇴직보험연금등) {
		this.비유동자산_기타비유동자산_퇴직보험연금등 = 비유동자산_기타비유동자산_퇴직보험연금등;
	}
	public Double get비유동자산_기타비유동자산_기타() {
		return 비유동자산_기타비유동자산_기타;
	}
	public void set비유동자산_기타비유동자산_기타(Double 비유동자산_기타비유동자산_기타) {
		this.비유동자산_기타비유동자산_기타 = 비유동자산_기타비유동자산_기타;
	}
	public Double get기타금융업자산() {
		return 기타금융업자산;
	}
	public void set기타금융업자산(Double 기타금융업자산) {
		this.기타금융업자산 = 기타금융업자산;
	}
	public Double get순자산1() {
		return 순자산1;
	}
	public void set순자산1(Double 순자산1) {
		this.순자산1 = 순자산1;
	}
	public Double get순자산2() {
		return 순자산2;
	}
	public void set순자산2(Double 순자산2) {
		this.순자산2 = 순자산2;
	}
	public Double get부채총계() {
		return 부채총계;
	}
	public void set부채총계(Double 부채총계) {
		this.부채총계 = 부채총계;
	}
	public Double get유동부채() {
		return 유동부채;
	}
	public void set유동부채(Double 유동부채) {
		this.유동부채 = 유동부채;
	}
	public Double get유동부채_단기사채() {
		return 유동부채_단기사채;
	}
	public void set유동부채_단기사채(Double 유동부채_단기사채) {
		this.유동부채_단기사채 = 유동부채_단기사채;
	}
	public Double get유동부채_단기차입금() {
		return 유동부채_단기차입금;
	}
	public void set유동부채_단기차입금(Double 유동부채_단기차입금) {
		this.유동부채_단기차입금 = 유동부채_단기차입금;
	}
	public Double get유동부채_유동성장기부채() {
		return 유동부채_유동성장기부채;
	}
	public void set유동부채_유동성장기부채(Double 유동부채_유동성장기부채) {
		this.유동부채_유동성장기부채 = 유동부채_유동성장기부채;
	}
	public Double get유동부채_유동성장기부채_유동성사채() {
		return 유동부채_유동성장기부채_유동성사채;
	}
	public void set유동부채_유동성장기부채_유동성사채(Double 유동부채_유동성장기부채_유동성사채) {
		this.유동부채_유동성장기부채_유동성사채 = 유동부채_유동성장기부채_유동성사채;
	}
	public Double get유동부채_유동성장기부채_유동성장기차입금() {
		return 유동부채_유동성장기부채_유동성장기차입금;
	}
	public void set유동부채_유동성장기부채_유동성장기차입금(Double 유동부채_유동성장기부채_유동성장기차입금) {
		this.유동부채_유동성장기부채_유동성장기차입금 = 유동부채_유동성장기부채_유동성장기차입금;
	}
	public Double get유동부채_유동성장기부채_유동성외화장기차입금() {
		return 유동부채_유동성장기부채_유동성외화장기차입금;
	}
	public void set유동부채_유동성장기부채_유동성외화장기차입금(Double 유동부채_유동성장기부채_유동성외화장기차입금) {
		this.유동부채_유동성장기부채_유동성외화장기차입금 = 유동부채_유동성장기부채_유동성외화장기차입금;
	}
	public Double get유동부채_유동성장기부채_유동성유동화채무() {
		return 유동부채_유동성장기부채_유동성유동화채무;
	}
	public void set유동부채_유동성장기부채_유동성유동화채무(Double 유동부채_유동성장기부채_유동성유동화채무) {
		this.유동부채_유동성장기부채_유동성유동화채무 = 유동부채_유동성장기부채_유동성유동화채무;
	}
	public Double get유동부채_유동성장기부채_유동성연불매입채무() {
		return 유동부채_유동성장기부채_유동성연불매입채무;
	}
	public void set유동부채_유동성장기부채_유동성연불매입채무(Double 유동부채_유동성장기부채_유동성연불매입채무) {
		this.유동부채_유동성장기부채_유동성연불매입채무 = 유동부채_유동성장기부채_유동성연불매입채무;
	}
	public Double get유동부채_유동성장기부채_유동성금융리스부채미지급금등() {
		return 유동부채_유동성장기부채_유동성금융리스부채미지급금등;
	}
	public void set유동부채_유동성장기부채_유동성금융리스부채미지급금등(Double 유동부채_유동성장기부채_유동성금융리스부채미지급금등) {
		this.유동부채_유동성장기부채_유동성금융리스부채미지급금등 = 유동부채_유동성장기부채_유동성금융리스부채미지급금등;
	}
	public Double get유동부채_유동성장기부채_기타유동성장기부채() {
		return 유동부채_유동성장기부채_기타유동성장기부채;
	}
	public void set유동부채_유동성장기부채_기타유동성장기부채(Double 유동부채_유동성장기부채_기타유동성장기부채) {
		this.유동부채_유동성장기부채_기타유동성장기부채 = 유동부채_유동성장기부채_기타유동성장기부채;
	}
	public Double get유동부채_단기금융부채() {
		return 유동부채_단기금융부채;
	}
	public void set유동부채_단기금융부채(Double 유동부채_단기금융부채) {
		this.유동부채_단기금융부채 = 유동부채_단기금융부채;
	}
	public Double get유동부채_단기금융부채_단기매매금융부채() {
		return 유동부채_단기금융부채_단기매매금융부채;
	}
	public void set유동부채_단기금융부채_단기매매금융부채(Double 유동부채_단기금융부채_단기매매금융부채) {
		this.유동부채_단기금융부채_단기매매금융부채 = 유동부채_단기금융부채_단기매매금융부채;
	}
	public Double get유동부채_단기금융부채_당기손익인식지정금융부채() {
		return 유동부채_단기금융부채_당기손익인식지정금융부채;
	}
	public void set유동부채_단기금융부채_당기손익인식지정금융부채(Double 유동부채_단기금융부채_당기손익인식지정금융부채) {
		this.유동부채_단기금융부채_당기손익인식지정금융부채 = 유동부채_단기금융부채_당기손익인식지정금융부채;
	}
	public Double get유동부채_단기금융부채_단기파생상품부채() {
		return 유동부채_단기금융부채_단기파생상품부채;
	}
	public void set유동부채_단기금융부채_단기파생상품부채(Double 유동부채_단기금융부채_단기파생상품부채) {
		this.유동부채_단기금융부채_단기파생상품부채 = 유동부채_단기금융부채_단기파생상품부채;
	}
	public Double get유동부채_단기금융부채_금융리스부채() {
		return 유동부채_단기금융부채_금융리스부채;
	}
	public void set유동부채_단기금융부채_금융리스부채(Double 유동부채_단기금융부채_금융리스부채) {
		this.유동부채_단기금융부채_금융리스부채 = 유동부채_단기금융부채_금융리스부채;
	}
	public Double get유동부채_단기금융부채_보증금() {
		return 유동부채_단기금융부채_보증금;
	}
	public void set유동부채_단기금융부채_보증금(Double 유동부채_단기금융부채_보증금) {
		this.유동부채_단기금융부채_보증금 = 유동부채_단기금융부채_보증금;
	}
	public Double get유동부채_단기금융부채_단기상환우선주부채() {
		return 유동부채_단기금융부채_단기상환우선주부채;
	}
	public void set유동부채_단기금융부채_단기상환우선주부채(Double 유동부채_단기금융부채_단기상환우선주부채) {
		this.유동부채_단기금융부채_단기상환우선주부채 = 유동부채_단기금융부채_단기상환우선주부채;
	}
	public Double get유동부채_단기금융부채_지급보증계약부채() {
		return 유동부채_단기금융부채_지급보증계약부채;
	}
	public void set유동부채_단기금융부채_지급보증계약부채(Double 유동부채_단기금융부채_지급보증계약부채) {
		this.유동부채_단기금융부채_지급보증계약부채 = 유동부채_단기금융부채_지급보증계약부채;
	}
	public Double get유동부채_단기금융부채_기타금융부채() {
		return 유동부채_단기금융부채_기타금융부채;
	}
	public void set유동부채_단기금융부채_기타금융부채(Double 유동부채_단기금융부채_기타금융부채) {
		this.유동부채_단기금융부채_기타금융부채 = 유동부채_단기금융부채_기타금융부채;
	}
	public Double get유동부채_매입채무및기타채무() {
		return 유동부채_매입채무및기타채무;
	}
	public void set유동부채_매입채무및기타채무(Double 유동부채_매입채무및기타채무) {
		this.유동부채_매입채무및기타채무 = 유동부채_매입채무및기타채무;
	}
	public Double get유동부채_매입채무및기타채무_매입채무() {
		return 유동부채_매입채무및기타채무_매입채무;
	}
	public void set유동부채_매입채무및기타채무_매입채무(Double 유동부채_매입채무및기타채무_매입채무) {
		this.유동부채_매입채무및기타채무_매입채무 = 유동부채_매입채무및기타채무_매입채무;
	}
	public Double get유동부채_매입채무및기타채무_미지급금() {
		return 유동부채_매입채무및기타채무_미지급금;
	}
	public void set유동부채_매입채무및기타채무_미지급금(Double 유동부채_매입채무및기타채무_미지급금) {
		this.유동부채_매입채무및기타채무_미지급금 = 유동부채_매입채무및기타채무_미지급금;
	}
	public Double get유동부채_매입채무및기타채무_예수금() {
		return 유동부채_매입채무및기타채무_예수금;
	}
	public void set유동부채_매입채무및기타채무_예수금(Double 유동부채_매입채무및기타채무_예수금) {
		this.유동부채_매입채무및기타채무_예수금 = 유동부채_매입채무및기타채무_예수금;
	}
	public Double get유동부채_매입채무및기타채무_단기금융예수금() {
		return 유동부채_매입채무및기타채무_단기금융예수금;
	}
	public void set유동부채_매입채무및기타채무_단기금융예수금(Double 유동부채_매입채무및기타채무_단기금융예수금) {
		this.유동부채_매입채무및기타채무_단기금융예수금 = 유동부채_매입채무및기타채무_단기금융예수금;
	}
	public Double get유동부채_매입채무및기타채무_미지급비용() {
		return 유동부채_매입채무및기타채무_미지급비용;
	}
	public void set유동부채_매입채무및기타채무_미지급비용(Double 유동부채_매입채무및기타채무_미지급비용) {
		this.유동부채_매입채무및기타채무_미지급비용 = 유동부채_매입채무및기타채무_미지급비용;
	}
	public Double get유동부채_매입채무및기타채무_미지급배당금() {
		return 유동부채_매입채무및기타채무_미지급배당금;
	}
	public void set유동부채_매입채무및기타채무_미지급배당금(Double 유동부채_매입채무및기타채무_미지급배당금) {
		this.유동부채_매입채무및기타채무_미지급배당금 = 유동부채_매입채무및기타채무_미지급배당금;
	}
	public Double get유동부채_매입채무및기타채무_보증금() {
		return 유동부채_매입채무및기타채무_보증금;
	}
	public void set유동부채_매입채무및기타채무_보증금(Double 유동부채_매입채무및기타채무_보증금) {
		this.유동부채_매입채무및기타채무_보증금 = 유동부채_매입채무및기타채무_보증금;
	}
	public Double get유동부채_매입채무및기타채무_기타() {
		return 유동부채_매입채무및기타채무_기타;
	}
	public void set유동부채_매입채무및기타채무_기타(Double 유동부채_매입채무및기타채무_기타) {
		this.유동부채_매입채무및기타채무_기타 = 유동부채_매입채무및기타채무_기타;
	}
	public Double get유동부채_유동종업원급여충당부채() {
		return 유동부채_유동종업원급여충당부채;
	}
	public void set유동부채_유동종업원급여충당부채(Double 유동부채_유동종업원급여충당부채) {
		this.유동부채_유동종업원급여충당부채 = 유동부채_유동종업원급여충당부채;
	}
	public Double get유동부채_단기충당부채() {
		return 유동부채_단기충당부채;
	}
	public void set유동부채_단기충당부채(Double 유동부채_단기충당부채) {
		this.유동부채_단기충당부채 = 유동부채_단기충당부채;
	}
	public Double get유동부채_당기법인세부채미지급법인세() {
		return 유동부채_당기법인세부채미지급법인세;
	}
	public void set유동부채_당기법인세부채미지급법인세(Double 유동부채_당기법인세부채미지급법인세) {
		this.유동부채_당기법인세부채미지급법인세 = 유동부채_당기법인세부채미지급법인세;
	}
	public Double get유동부채_기타유동부채() {
		return 유동부채_기타유동부채;
	}
	public void set유동부채_기타유동부채(Double 유동부채_기타유동부채) {
		this.유동부채_기타유동부채 = 유동부채_기타유동부채;
	}
	public Double get유동부채_기타유동부채_선수금() {
		return 유동부채_기타유동부채_선수금;
	}
	public void set유동부채_기타유동부채_선수금(Double 유동부채_기타유동부채_선수금) {
		this.유동부채_기타유동부채_선수금 = 유동부채_기타유동부채_선수금;
	}
	public Double get유동부채_기타유동부채_선수수익() {
		return 유동부채_기타유동부채_선수수익;
	}
	public void set유동부채_기타유동부채_선수수익(Double 유동부채_기타유동부채_선수수익) {
		this.유동부채_기타유동부채_선수수익 = 유동부채_기타유동부채_선수수익;
	}
	public Double get유동부채_기타유동부채_이연수익() {
		return 유동부채_기타유동부채_이연수익;
	}
	public void set유동부채_기타유동부채_이연수익(Double 유동부채_기타유동부채_이연수익) {
		this.유동부채_기타유동부채_이연수익 = 유동부채_기타유동부채_이연수익;
	}
	public Double get유동부채_기타유동부채_현금결제형주식기준보상거래() {
		return 유동부채_기타유동부채_현금결제형주식기준보상거래;
	}
	public void set유동부채_기타유동부채_현금결제형주식기준보상거래(Double 유동부채_기타유동부채_현금결제형주식기준보상거래) {
		this.유동부채_기타유동부채_현금결제형주식기준보상거래 = 유동부채_기타유동부채_현금결제형주식기준보상거래;
	}
	public Double get유동부채_기타유동부채_기타() {
		return 유동부채_기타유동부채_기타;
	}
	public void set유동부채_기타유동부채_기타(Double 유동부채_기타유동부채_기타) {
		this.유동부채_기타유동부채_기타 = 유동부채_기타유동부채_기타;
	}
	public Double get유동부채_매각예정으로분류된처분자산에포함된부채() {
		return 유동부채_매각예정으로분류된처분자산에포함된부채;
	}
	public void set유동부채_매각예정으로분류된처분자산에포함된부채(Double 유동부채_매각예정으로분류된처분자산에포함된부채) {
		this.유동부채_매각예정으로분류된처분자산에포함된부채 = 유동부채_매각예정으로분류된처분자산에포함된부채;
	}
	public Double get유동부채_매각예정으로분류된처분자산에포함된부채_비유동부채() {
		return 유동부채_매각예정으로분류된처분자산에포함된부채_비유동부채;
	}
	public void set유동부채_매각예정으로분류된처분자산에포함된부채_비유동부채(
			Double 유동부채_매각예정으로분류된처분자산에포함된부채_비유동부채) {
		this.유동부채_매각예정으로분류된처분자산에포함된부채_비유동부채 = 유동부채_매각예정으로분류된처분자산에포함된부채_비유동부채;
	}
	public Double get유동부채_매각예정으로분류된처분자산에포함된부채_유동부채() {
		return 유동부채_매각예정으로분류된처분자산에포함된부채_유동부채;
	}
	public void set유동부채_매각예정으로분류된처분자산에포함된부채_유동부채(
			Double 유동부채_매각예정으로분류된처분자산에포함된부채_유동부채) {
		this.유동부채_매각예정으로분류된처분자산에포함된부채_유동부채 = 유동부채_매각예정으로분류된처분자산에포함된부채_유동부채;
	}
	public Double get유동부채_매각예정으로분류된처분자산에포함된부채_기타() {
		return 유동부채_매각예정으로분류된처분자산에포함된부채_기타;
	}
	public void set유동부채_매각예정으로분류된처분자산에포함된부채_기타(Double 유동부채_매각예정으로분류된처분자산에포함된부채_기타) {
		this.유동부채_매각예정으로분류된처분자산에포함된부채_기타 = 유동부채_매각예정으로분류된처분자산에포함된부채_기타;
	}
	public Double get비유동부채() {
		return 비유동부채;
	}
	public void set비유동부채(Double 비유동부채) {
		this.비유동부채 = 비유동부채;
	}
	public Double get비유동부채_사채() {
		return 비유동부채_사채;
	}
	public void set비유동부채_사채(Double 비유동부채_사채) {
		this.비유동부채_사채 = 비유동부채_사채;
	}
	public Double get비유동부채_사채_사채() {
		return 비유동부채_사채_사채;
	}
	public void set비유동부채_사채_사채(Double 비유동부채_사채_사채) {
		this.비유동부채_사채_사채 = 비유동부채_사채_사채;
	}
	public Double get비유동부채_사채_CB() {
		return 비유동부채_사채_CB;
	}
	public void set비유동부채_사채_CB(Double 비유동부채_사채_CB) {
		this.비유동부채_사채_CB = 비유동부채_사채_CB;
	}
	public Double get비유동부채_사채_BW() {
		return 비유동부채_사채_BW;
	}
	public void set비유동부채_사채_BW(Double 비유동부채_사채_BW) {
		this.비유동부채_사채_BW = 비유동부채_사채_BW;
	}
	public Double get비유동부채_사채_EB() {
		return 비유동부채_사채_EB;
	}
	public void set비유동부채_사채_EB(Double 비유동부채_사채_EB) {
		this.비유동부채_사채_EB = 비유동부채_사채_EB;
	}
	public Double get비유동부채_장기차입금() {
		return 비유동부채_장기차입금;
	}
	public void set비유동부채_장기차입금(Double 비유동부채_장기차입금) {
		this.비유동부채_장기차입금 = 비유동부채_장기차입금;
	}
	public Double get비유동부채_장기금융부채() {
		return 비유동부채_장기금융부채;
	}
	public void set비유동부채_장기금융부채(Double 비유동부채_장기금융부채) {
		this.비유동부채_장기금융부채 = 비유동부채_장기금융부채;
	}
	public Double get비유동부채_장기금융부채_당기손익인식금융부채() {
		return 비유동부채_장기금융부채_당기손익인식금융부채;
	}
	public void set비유동부채_장기금융부채_당기손익인식금융부채(Double 비유동부채_장기금융부채_당기손익인식금융부채) {
		this.비유동부채_장기금융부채_당기손익인식금융부채 = 비유동부채_장기금융부채_당기손익인식금융부채;
	}
	public Double get비유동부채_장기금융부채_장기파생상품부채() {
		return 비유동부채_장기금융부채_장기파생상품부채;
	}
	public void set비유동부채_장기금융부채_장기파생상품부채(Double 비유동부채_장기금융부채_장기파생상품부채) {
		this.비유동부채_장기금융부채_장기파생상품부채 = 비유동부채_장기금융부채_장기파생상품부채;
	}
	public Double get비유동부채_장기금융부채_금융리스부채() {
		return 비유동부채_장기금융부채_금융리스부채;
	}
	public void set비유동부채_장기금융부채_금융리스부채(Double 비유동부채_장기금융부채_금융리스부채) {
		this.비유동부채_장기금융부채_금융리스부채 = 비유동부채_장기금융부채_금융리스부채;
	}
	public Double get비유동부채_장기금융부채_장기상환우선주부채() {
		return 비유동부채_장기금융부채_장기상환우선주부채;
	}
	public void set비유동부채_장기금융부채_장기상환우선주부채(Double 비유동부채_장기금융부채_장기상환우선주부채) {
		this.비유동부채_장기금융부채_장기상환우선주부채 = 비유동부채_장기금융부채_장기상환우선주부채;
	}
	public Double get비유동부채_장기금융부채_기타비유동금융부채() {
		return 비유동부채_장기금융부채_기타비유동금융부채;
	}
	public void set비유동부채_장기금융부채_기타비유동금융부채(Double 비유동부채_장기금융부채_기타비유동금융부채) {
		this.비유동부채_장기금융부채_기타비유동금융부채 = 비유동부채_장기금융부채_기타비유동금융부채;
	}
	public Double get비유동부채_장기매입채무및기타채무() {
		return 비유동부채_장기매입채무및기타채무;
	}
	public void set비유동부채_장기매입채무및기타채무(Double 비유동부채_장기매입채무및기타채무) {
		this.비유동부채_장기매입채무및기타채무 = 비유동부채_장기매입채무및기타채무;
	}
	public Double get비유동부채_장기매입채무및기타채무_장기매입채무() {
		return 비유동부채_장기매입채무및기타채무_장기매입채무;
	}
	public void set비유동부채_장기매입채무및기타채무_장기매입채무(Double 비유동부채_장기매입채무및기타채무_장기매입채무) {
		this.비유동부채_장기매입채무및기타채무_장기매입채무 = 비유동부채_장기매입채무및기타채무_장기매입채무;
	}
	public Double get비유동부채_장기매입채무및기타채무_장기미지급금() {
		return 비유동부채_장기매입채무및기타채무_장기미지급금;
	}
	public void set비유동부채_장기매입채무및기타채무_장기미지급금(Double 비유동부채_장기매입채무및기타채무_장기미지급금) {
		this.비유동부채_장기매입채무및기타채무_장기미지급금 = 비유동부채_장기매입채무및기타채무_장기미지급금;
	}
	public Double get비유동부채_장기매입채무및기타채무_장기예수금() {
		return 비유동부채_장기매입채무및기타채무_장기예수금;
	}
	public void set비유동부채_장기매입채무및기타채무_장기예수금(Double 비유동부채_장기매입채무및기타채무_장기예수금) {
		this.비유동부채_장기매입채무및기타채무_장기예수금 = 비유동부채_장기매입채무및기타채무_장기예수금;
	}
	public Double get비유동부채_장기매입채무및기타채무_장기금융예수금() {
		return 비유동부채_장기매입채무및기타채무_장기금융예수금;
	}
	public void set비유동부채_장기매입채무및기타채무_장기금융예수금(Double 비유동부채_장기매입채무및기타채무_장기금융예수금) {
		this.비유동부채_장기매입채무및기타채무_장기금융예수금 = 비유동부채_장기매입채무및기타채무_장기금융예수금;
	}
	public Double get비유동부채_장기매입채무및기타채무_장기미지급비용() {
		return 비유동부채_장기매입채무및기타채무_장기미지급비용;
	}
	public void set비유동부채_장기매입채무및기타채무_장기미지급비용(Double 비유동부채_장기매입채무및기타채무_장기미지급비용) {
		this.비유동부채_장기매입채무및기타채무_장기미지급비용 = 비유동부채_장기매입채무및기타채무_장기미지급비용;
	}
	public Double get비유동부채_장기매입채무및기타채무_장기미지급배당금() {
		return 비유동부채_장기매입채무및기타채무_장기미지급배당금;
	}
	public void set비유동부채_장기매입채무및기타채무_장기미지급배당금(Double 비유동부채_장기매입채무및기타채무_장기미지급배당금) {
		this.비유동부채_장기매입채무및기타채무_장기미지급배당금 = 비유동부채_장기매입채무및기타채무_장기미지급배당금;
	}
	public Double get비유동부채_장기매입채무및기타채무_장기보증금() {
		return 비유동부채_장기매입채무및기타채무_장기보증금;
	}
	public void set비유동부채_장기매입채무및기타채무_장기보증금(Double 비유동부채_장기매입채무및기타채무_장기보증금) {
		this.비유동부채_장기매입채무및기타채무_장기보증금 = 비유동부채_장기매입채무및기타채무_장기보증금;
	}
	public Double get비유동부채_장기매입채무및기타채무_기타() {
		return 비유동부채_장기매입채무및기타채무_기타;
	}
	public void set비유동부채_장기매입채무및기타채무_기타(Double 비유동부채_장기매입채무및기타채무_기타) {
		this.비유동부채_장기매입채무및기타채무_기타 = 비유동부채_장기매입채무및기타채무_기타;
	}
	public Double get비유동부채_비유동종업원급여충당부채() {
		return 비유동부채_비유동종업원급여충당부채;
	}
	public void set비유동부채_비유동종업원급여충당부채(Double 비유동부채_비유동종업원급여충당부채) {
		this.비유동부채_비유동종업원급여충당부채 = 비유동부채_비유동종업원급여충당부채;
	}
	public Double get비유동부채_장기충당부채() {
		return 비유동부채_장기충당부채;
	}
	public void set비유동부채_장기충당부채(Double 비유동부채_장기충당부채) {
		this.비유동부채_장기충당부채 = 비유동부채_장기충당부채;
	}
	public Double get비유동부채_이연법인세부채() {
		return 비유동부채_이연법인세부채;
	}
	public void set비유동부채_이연법인세부채(Double 비유동부채_이연법인세부채) {
		this.비유동부채_이연법인세부채 = 비유동부채_이연법인세부채;
	}
	public Double get비유동부채_장기당기법인세부채미지급법인세() {
		return 비유동부채_장기당기법인세부채미지급법인세;
	}
	public void set비유동부채_장기당기법인세부채미지급법인세(Double 비유동부채_장기당기법인세부채미지급법인세) {
		this.비유동부채_장기당기법인세부채미지급법인세 = 비유동부채_장기당기법인세부채미지급법인세;
	}
	public Double get비유동부채_기타비유동부채() {
		return 비유동부채_기타비유동부채;
	}
	public void set비유동부채_기타비유동부채(Double 비유동부채_기타비유동부채) {
		this.비유동부채_기타비유동부채 = 비유동부채_기타비유동부채;
	}
	public Double get비유동부채_기타비유동부채_장기선수금() {
		return 비유동부채_기타비유동부채_장기선수금;
	}
	public void set비유동부채_기타비유동부채_장기선수금(Double 비유동부채_기타비유동부채_장기선수금) {
		this.비유동부채_기타비유동부채_장기선수금 = 비유동부채_기타비유동부채_장기선수금;
	}
	public Double get비유동부채_기타비유동부채_장기선수수익() {
		return 비유동부채_기타비유동부채_장기선수수익;
	}
	public void set비유동부채_기타비유동부채_장기선수수익(Double 비유동부채_기타비유동부채_장기선수수익) {
		this.비유동부채_기타비유동부채_장기선수수익 = 비유동부채_기타비유동부채_장기선수수익;
	}
	public Double get비유동부채_기타비유동부채_이연수익() {
		return 비유동부채_기타비유동부채_이연수익;
	}
	public void set비유동부채_기타비유동부채_이연수익(Double 비유동부채_기타비유동부채_이연수익) {
		this.비유동부채_기타비유동부채_이연수익 = 비유동부채_기타비유동부채_이연수익;
	}
	public Double get비유동부채_기타비유동부채_현금결제형주식기준보상거래() {
		return 비유동부채_기타비유동부채_현금결제형주식기준보상거래;
	}
	public void set비유동부채_기타비유동부채_현금결제형주식기준보상거래(Double 비유동부채_기타비유동부채_현금결제형주식기준보상거래) {
		this.비유동부채_기타비유동부채_현금결제형주식기준보상거래 = 비유동부채_기타비유동부채_현금결제형주식기준보상거래;
	}
	public Double get비유동부채_기타비유동부채_기타() {
		return 비유동부채_기타비유동부채_기타;
	}
	public void set비유동부채_기타비유동부채_기타(Double 비유동부채_기타비유동부채_기타) {
		this.비유동부채_기타비유동부채_기타 = 비유동부채_기타비유동부채_기타;
	}
	public Double get기타금융업부채() {
		return 기타금융업부채;
	}
	public void set기타금융업부채(Double 기타금융업부채) {
		this.기타금융업부채 = 기타금융업부채;
	}
	public Double get이자발생부채() {
		return 이자발생부채;
	}
	public void set이자발생부채(Double 이자발생부채) {
		this.이자발생부채 = 이자발생부채;
	}
	public Double get순부채() {
		return 순부채;
	}
	public void set순부채(Double 순부채) {
		this.순부채 = 순부채;
	}
	public Double getCAPEX() {
		return CAPEX;
	}
	public void setCAPEX(Double cAPEX) {
		CAPEX = cAPEX;
	}
	public Double get자본총계() {
		return 자본총계;
	}
	public void set자본총계(Double 자본총계) {
		this.자본총계 = 자본총계;
	}
	public Double get지배주주지분() {
		return 지배주주지분;
	}
	public void set지배주주지분(Double 지배주주지분) {
		this.지배주주지분 = 지배주주지분;
	}
	public Double get지배주주지분_자본금() {
		return 지배주주지분_자본금;
	}
	public void set지배주주지분_자본금(Double 지배주주지분_자본금) {
		this.지배주주지분_자본금 = 지배주주지분_자본금;
	}
	public Double get지배주주지분_자본금_보통주자본금() {
		return 지배주주지분_자본금_보통주자본금;
	}
	public void set지배주주지분_자본금_보통주자본금(Double 지배주주지분_자본금_보통주자본금) {
		this.지배주주지분_자본금_보통주자본금 = 지배주주지분_자본금_보통주자본금;
	}
	public Double get지배주주지분_자본금_우선주자본금() {
		return 지배주주지분_자본금_우선주자본금;
	}
	public void set지배주주지분_자본금_우선주자본금(Double 지배주주지분_자본금_우선주자본금) {
		this.지배주주지분_자본금_우선주자본금 = 지배주주지분_자본금_우선주자본금;
	}
	public Double get지배주주지분_신종자본증권() {
		return 지배주주지분_신종자본증권;
	}
	public void set지배주주지분_신종자본증권(Double 지배주주지분_신종자본증권) {
		this.지배주주지분_신종자본증권 = 지배주주지분_신종자본증권;
	}
	public Double get지배주주지분_자본잉여금() {
		return 지배주주지분_자본잉여금;
	}
	public void set지배주주지분_자본잉여금(Double 지배주주지분_자본잉여금) {
		this.지배주주지분_자본잉여금 = 지배주주지분_자본잉여금;
	}
	public Double get지배주주지분_자본잉여금_주식발행초과금() {
		return 지배주주지분_자본잉여금_주식발행초과금;
	}
	public void set지배주주지분_자본잉여금_주식발행초과금(Double 지배주주지분_자본잉여금_주식발행초과금) {
		this.지배주주지분_자본잉여금_주식발행초과금 = 지배주주지분_자본잉여금_주식발행초과금;
	}
	public Double get지배주주지분_자본잉여금_전환권대가() {
		return 지배주주지분_자본잉여금_전환권대가;
	}
	public void set지배주주지분_자본잉여금_전환권대가(Double 지배주주지분_자본잉여금_전환권대가) {
		this.지배주주지분_자본잉여금_전환권대가 = 지배주주지분_자본잉여금_전환권대가;
	}
	public Double get지배주주지분_자본잉여금_신주인수권대가() {
		return 지배주주지분_자본잉여금_신주인수권대가;
	}
	public void set지배주주지분_자본잉여금_신주인수권대가(Double 지배주주지분_자본잉여금_신주인수권대가) {
		this.지배주주지분_자본잉여금_신주인수권대가 = 지배주주지분_자본잉여금_신주인수권대가;
	}
	public Double get지배주주지분_자본잉여금_교환권대가() {
		return 지배주주지분_자본잉여금_교환권대가;
	}
	public void set지배주주지분_자본잉여금_교환권대가(Double 지배주주지분_자본잉여금_교환권대가) {
		this.지배주주지분_자본잉여금_교환권대가 = 지배주주지분_자본잉여금_교환권대가;
	}
	public Double get지배주주지분_자본잉여금_기타() {
		return 지배주주지분_자본잉여금_기타;
	}
	public void set지배주주지분_자본잉여금_기타(Double 지배주주지분_자본잉여금_기타) {
		this.지배주주지분_자본잉여금_기타 = 지배주주지분_자본잉여금_기타;
	}
	public Double get지배주주지분_기타자본() {
		return 지배주주지분_기타자본;
	}
	public void set지배주주지분_기타자본(Double 지배주주지분_기타자본) {
		this.지배주주지분_기타자본 = 지배주주지분_기타자본;
	}
	public Double get지배주주지분_기타자본_자기주식() {
		return 지배주주지분_기타자본_자기주식;
	}
	public void set지배주주지분_기타자본_자기주식(Double 지배주주지분_기타자본_자기주식) {
		this.지배주주지분_기타자본_자기주식 = 지배주주지분_기타자본_자기주식;
	}
	public Double get지배주주지분_기타자본_자기주식처분손실() {
		return 지배주주지분_기타자본_자기주식처분손실;
	}
	public void set지배주주지분_기타자본_자기주식처분손실(Double 지배주주지분_기타자본_자기주식처분손실) {
		this.지배주주지분_기타자본_자기주식처분손실 = 지배주주지분_기타자본_자기주식처분손실;
	}
	public Double get지배주주지분_기타자본_기타() {
		return 지배주주지분_기타자본_기타;
	}
	public void set지배주주지분_기타자본_기타(Double 지배주주지분_기타자본_기타) {
		this.지배주주지분_기타자본_기타 = 지배주주지분_기타자본_기타;
	}
	public Double get지배주주지분_기타포괄이익누계액() {
		return 지배주주지분_기타포괄이익누계액;
	}
	public void set지배주주지분_기타포괄이익누계액(Double 지배주주지분_기타포괄이익누계액) {
		this.지배주주지분_기타포괄이익누계액 = 지배주주지분_기타포괄이익누계액;
	}
	public Double get지배주주지분_기타포괄이익누계액_금융자산평가손익() {
		return 지배주주지분_기타포괄이익누계액_금융자산평가손익;
	}
	public void set지배주주지분_기타포괄이익누계액_금융자산평가손익(Double 지배주주지분_기타포괄이익누계액_금융자산평가손익) {
		this.지배주주지분_기타포괄이익누계액_금융자산평가손익 = 지배주주지분_기타포괄이익누계액_금융자산평가손익;
	}
	public Double get지배주주지분_기타포괄이익누계액_매도가능금융자산평가손익() {
		return 지배주주지분_기타포괄이익누계액_매도가능금융자산평가손익;
	}
	public void set지배주주지분_기타포괄이익누계액_매도가능금융자산평가손익(
			Double 지배주주지분_기타포괄이익누계액_매도가능금융자산평가손익) {
		this.지배주주지분_기타포괄이익누계액_매도가능금융자산평가손익 = 지배주주지분_기타포괄이익누계액_매도가능금융자산평가손익;
	}
	public Double get지배주주지분_기타포괄이익누계액_관계기업등기타포괄이익() {
		return 지배주주지분_기타포괄이익누계액_관계기업등기타포괄이익;
	}
	public void set지배주주지분_기타포괄이익누계액_관계기업등기타포괄이익(Double 지배주주지분_기타포괄이익누계액_관계기업등기타포괄이익) {
		this.지배주주지분_기타포괄이익누계액_관계기업등기타포괄이익 = 지배주주지분_기타포괄이익누계액_관계기업등기타포괄이익;
	}
	public Double get지배주주지분_기타포괄이익누계액_환산관련외환차이적립금환율변동차이() {
		return 지배주주지분_기타포괄이익누계액_환산관련외환차이적립금환율변동차이;
	}
	public void set지배주주지분_기타포괄이익누계액_환산관련외환차이적립금환율변동차이(
			Double 지배주주지분_기타포괄이익누계액_환산관련외환차이적립금환율변동차이) {
		this.지배주주지분_기타포괄이익누계액_환산관련외환차이적립금환율변동차이 = 지배주주지분_기타포괄이익누계액_환산관련외환차이적립금환율변동차이;
	}
	public Double get지배주주지분_기타포괄이익누계액_현금흐름위험회피적립금() {
		return 지배주주지분_기타포괄이익누계액_현금흐름위험회피적립금;
	}
	public void set지배주주지분_기타포괄이익누계액_현금흐름위험회피적립금(Double 지배주주지분_기타포괄이익누계액_현금흐름위험회피적립금) {
		this.지배주주지분_기타포괄이익누계액_현금흐름위험회피적립금 = 지배주주지분_기타포괄이익누계액_현금흐름위험회피적립금;
	}
	public Double get지배주주지분_기타포괄이익누계액_재평가잉여금() {
		return 지배주주지분_기타포괄이익누계액_재평가잉여금;
	}
	public void set지배주주지분_기타포괄이익누계액_재평가잉여금(Double 지배주주지분_기타포괄이익누계액_재평가잉여금) {
		this.지배주주지분_기타포괄이익누계액_재평가잉여금 = 지배주주지분_기타포괄이익누계액_재평가잉여금;
	}
	public Double get지배주주지분_기타포괄이익누계액_기타() {
		return 지배주주지분_기타포괄이익누계액_기타;
	}
	public void set지배주주지분_기타포괄이익누계액_기타(Double 지배주주지분_기타포괄이익누계액_기타) {
		this.지배주주지분_기타포괄이익누계액_기타 = 지배주주지분_기타포괄이익누계액_기타;
	}
	public Double get지배주주지분_이익잉여금() {
		return 지배주주지분_이익잉여금;
	}
	public void set지배주주지분_이익잉여금(Double 지배주주지분_이익잉여금) {
		this.지배주주지분_이익잉여금 = 지배주주지분_이익잉여금;
	}
	public Double get지배주주지분_지배당기순이익() {
		return 지배주주지분_지배당기순이익;
	}
	public void set지배주주지분_지배당기순이익(Double 지배주주지분_지배당기순이익) {
		this.지배주주지분_지배당기순이익 = 지배주주지분_지배당기순이익;
	}
	public Double get비지배주주지분() {
		return 비지배주주지분;
	}
	public void set비지배주주지분(Double 비지배주주지분) {
		this.비지배주주지분 = 비지배주주지분;
	}
	public Double get비지배당기순이익() {
		return 비지배당기순이익;
	}
	public void set비지배당기순이익(Double 비지배당기순이익) {
		this.비지배당기순이익 = 비지배당기순이익;
	}
	public Double get수권주식수() {
		return 수권주식수;
	}
	public void set수권주식수(Double 수권주식수) {
		this.수권주식수 = 수권주식수;
	}
	public Double get주당금액() {
		return 주당금액;
	}
	public void set주당금액(Double 주당금액) {
		this.주당금액 = 주당금액;
	}
	public Double get발행주식수() {
		return 발행주식수;
	}
	public void set발행주식수(Double 발행주식수) {
		this.발행주식수 = 발행주식수;
	}
	public Double get보통주() {
		return 보통주;
	}
	public void set보통주(Double 보통주) {
		this.보통주 = 보통주;
	}
	public Double get우선주() {
		return 우선주;
	}
	public void set우선주(Double 우선주) {
		this.우선주 = 우선주;
	}

	
	public String toString() {
		return stockCode + "," + ymDay +"," + dataType + "," + dateType;		
	}

	
	
	

}
