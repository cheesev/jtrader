package com.app4j.tradeobserver.data.entity.id;

import java.io.Serializable;

import com.app4j.tradeobserver.data.entity.FinancialPositionStatementIFRSDto;

public class FinancialPositionStatementIFRSDtoId implements Serializable {
	
	protected String stockCode;
	protected String ymDay;
	protected String dateType;
	protected String dataType;
	
	public FinancialPositionStatementIFRSDtoId() {
	}

	public FinancialPositionStatementIFRSDtoId(String stockCode, String ymDay, String dateType, String dataType) {
		this.ymDay = ymDay;
		this.stockCode = stockCode;
		this.dateType = dateType;
		this.dataType = dataType;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		FinancialPositionStatementIFRSDto financialPositionStatementIFRSDto = (FinancialPositionStatementIFRSDto) o;

		return stockCode.equals(financialPositionStatementIFRSDto.getStockCode())
				&& ymDay.equals(financialPositionStatementIFRSDto.getYmDay())
			    && dateType.equals(financialPositionStatementIFRSDto.getDateType())
			    && dataType.equals(financialPositionStatementIFRSDto.getDataType());
	}

	public int hashCode() {
		int result;
		result = dataType.hashCode();
		result = 13 * dateType.hashCode();
		result = 17 * result + ymDay.hashCode();
		result = 29 * result + stockCode.hashCode();
		return result;
	}

}
