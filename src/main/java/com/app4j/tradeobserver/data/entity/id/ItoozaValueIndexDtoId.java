package com.app4j.tradeobserver.data.entity.id;

import java.io.Serializable;

import com.app4j.tradeobserver.data.entity.ItoozaValueIndexDto;

public class ItoozaValueIndexDtoId implements Serializable {
	
	protected String stockCode;
	protected String ymDay;
	protected String dateType;
	
	public ItoozaValueIndexDtoId() {
	}

	public ItoozaValueIndexDtoId(String stockCode, String ymDay, String dateType) {
		this.ymDay = ymDay;
		this.stockCode = stockCode;
		this.dateType = dateType;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ItoozaValueIndexDto itoozaValueIndexDto = (ItoozaValueIndexDto) o;

		return stockCode.equals(itoozaValueIndexDto.getStockCode())
				&& ymDay.equals(itoozaValueIndexDto.getYmDay())
			    && dateType.equals(itoozaValueIndexDto.getDateType());
	}

	public int hashCode() {
		int result;
		result = 13 * dateType.hashCode();
		result = 17 * result + ymDay.hashCode();
		result = 29 * result + stockCode.hashCode();
		return result;
	}

}
