package com.app4j.tradeobserver.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.app4j.tradeobserver.data.entity.id.ItoozaFinancialPositionStatementGAAPDtoId;

@Entity
@Table (name = "ITOOZA_FINANCIAL_POSITION_STATEMENT_GAAP")
@IdClass(ItoozaFinancialPositionStatementGAAPDtoId.class)
public class ItoozaFinancialPositionStatementGAAPDto {
	// 개별 기준. DTO
	@Id
    @Column (name = "STOCK_CODE")
    private String stockCode;	
	@Id
	@Column (name="YM_DAY")
	private String ymDay;
	@Id
	@Column (name="DATE_TYPE")
	private String dateType;		// 분기, 연간 (Q, Y)
	
	@Column (name="유동자산")
	private Double 유동자산;
	@Column (name="당좌자산")
	private Double 당좌자산;
	@Column (name="당좌자산_현금및현금성자산")
	private Double 당좌자산_현금및현금성자산;
	@Column (name="당좌자산_단기투자자산")
	private Double 당좌자산_단기투자자산;
	@Column (name="당좌자산_단기투자자산_단기예금")
	private Double 당좌자산_단기투자자산_단기예금;
	@Column (name="당좌자산_단기투자자산_매도가능증권")
	private Double 당좌자산_단기투자자산_매도가능증권;
	@Column (name="당좌자산_단기투자자산_만기보유증권")
	private Double 당좌자산_단기투자자산_만기보유증권;
	@Column (name="당좌자산_단기투자자산_단기매매증권")
	private Double 당좌자산_단기투자자산_단기매매증권;
	@Column (name="당좌자산_단기투자자산_단기매매증권_주식")
	private Double 당좌자산_단기투자자산_단기매매증권_주식;
	@Column (name="당좌자산_단기투자자산_단기매매증권_수익증권")
	private Double 당좌자산_단기투자자산_단기매매증권_수익증권;
	@Column (name="당좌자산_단기투자자산_단기매매증권_채권")
	private Double 당좌자산_단기투자자산_단기매매증권_채권;
	@Column (name="당좌자산_단기투자자산_단기매매증권_기타")
	private Double 당좌자산_단기투자자산_단기매매증권_기타;
	@Column (name="당좌자산_단기투자자산_단기대여금")
	private Double 당좌자산_단기투자자산_단기대여금;
	@Column (name="당좌자산_단기투자자산_기타")
	private Double 당좌자산_단기투자자산_기타;
	@Column (name="당좌자산_매출채권")
	private Double 당좌자산_매출채권;
	@Column (name="당좌자산_매출채권_매출채권")
	private Double 당좌자산_매출채권_매출채권;
	@Column (name="당좌자산_매출채권_외화매출채권")
	private Double 당좌자산_매출채권_외화매출채권;
	@Column (name="당좌자산_매출채권_기타매출채권")
	private Double 당좌자산_매출채권_기타매출채권;
	@Column (name="당좌자산_기타")
	private Double 당좌자산_기타;
	@Column (name="재고자산")
	private Double 재고자산;
	@Column (name="재고자산_원재료")
	private Double 재고자산_원재료;
	@Column (name="재고자산_재공품")
	private Double 재고자산_재공품;
	@Column (name="재고자산_반제품")
	private Double 재고자산_반제품;
	@Column (name="재고자산_제품")
	private Double 재고자산_제품;
	@Column (name="재고자산_상품")
	private Double 재고자산_상품;
	@Column (name="재고자산_기타")
	private Double 재고자산_기타;
	@Column (name="비유동자산")
	private Double 비유동자산;
	@Column (name="투자자산")
	private Double 투자자산;
	@Column (name="투자자산_장기금융상품")
	private Double 투자자산_장기금융상품;
	@Column (name="투자자산_매도가능증권")
	private Double 투자자산_매도가능증권;
	@Column (name="투자자산_매도가능증권_주식")
	private Double 투자자산_매도가능증권_주식;
	@Column (name="투자자산_매도가능증권_수익증권")
	private Double 투자자산_매도가능증권_수익증권;
	@Column (name="투자자산_매도가능증권_채권")
	private Double 투자자산_매도가능증권_채권;
	@Column (name="투자자산_매도가능증권_기타")
	private Double 투자자산_매도가능증권_기타;
	@Column (name="투자자산_만기보유증권")
	private Double 투자자산_만기보유증권;
	@Column (name="투자자산_지분법적용투자주식")
	private Double 투자자산_지분법적용투자주식;
	@Column (name="투자자산_투자부동산")
	private Double 투자자산_투자부동산;
	@Column (name="투자자산_기타투자자산")
	private Double 투자자산_기타투자자산;
	@Column (name="유형자산")
	private Double 유형자산;
	@Column (name="유형자산_토지")
	private Double 유형자산_토지;
	@Column (name="유형자산_건물")
	private Double 유형자산_건물;
	@Column (name="유형자산_구축물")
	private Double 유형자산_구축물;
	@Column (name="유형자산_기계장치")
	private Double 유형자산_기계장치;
	@Column (name="유형자산_건설중인자산")
	private Double 유형자산_건설중인자산;
	@Column (name="유형자산_기타")
	private Double 유형자산_기타;
	@Column (name="무형자산")
	private Double 무형자산;
	@Column (name="무형자산_영업권")
	private Double 무형자산_영업권;
	@Column (name="무형자산_광업권")
	private Double 무형자산_광업권;
	@Column (name="무형자산_어업권")
	private Double 무형자산_어업권;
	@Column (name="무형자산_차지권")
	private Double 무형자산_차지권;
	@Column (name="무형자산_개발비")
	private Double 무형자산_개발비;
	@Column (name="무형자산_기타")
	private Double 무형자산_기타;
	@Column (name="기타비유동자산")
	private Double 기타비유동자산;
	@Column (name="이연자산")
	private Double 이연자산;
	@Column (name="자산총계")
	private Double 자산총계;
	@Column (name="유동부채")
	private Double 유동부채;
	@Column (name="유동부채_매입채무")
	private Double 유동부채_매입채무;
	@Column (name="유동부채_매입채무_매입채무")
	private Double 유동부채_매입채무_매입채무;
	@Column (name="유동부채_매입채무_외화매입채무")
	private Double 유동부채_매입채무_외화매입채무;
	@Column (name="유동부채_매입채무_기타")
	private Double 유동부채_매입채무_기타;
	@Column (name="유동부채_단기차입금")
	private Double 유동부채_단기차입금;
	@Column (name="유동부채_단기차입금_단기차입금")
	private Double 유동부채_단기차입금_단기차입금;
	@Column (name="유동부채_단기차입금_외화단기차입금")
	private Double 유동부채_단기차입금_외화단기차입금;
	@Column (name="유동부채_단기차입금_기타")
	private Double 유동부채_단기차입금_기타;
	@Column (name="유동부채_유동성장기부채")
	private Double 유동부채_유동성장기부채;
	@Column (name="유동부채_선수금")
	private Double 유동부채_선수금;
	@Column (name="유동부채_기타")
	private Double 유동부채_기타;
	@Column (name="비유동부채")
	private Double 비유동부채;
	@Column (name="비유동부채_사채")
	private Double 비유동부채_사채;
	@Column (name="비유동부채_장기차입금")
	private Double 비유동부채_장기차입금;
	@Column (name="비유동부채_장기차입금_장기차입금")
	private Double 비유동부채_장기차입금_장기차입금;
	@Column (name="비유동부채_장기차입금_외화장기차입금")
	private Double 비유동부채_장기차입금_외화장기차입금;
	@Column (name="비유동부채_장기차입금_기타")
	private Double 비유동부채_장기차입금_기타;
	@Column (name="비유동부채_장기선수금")
	private Double 비유동부채_장기선수금;
	@Column (name="비유동부채_파생상품부채")
	private Double 비유동부채_파생상품부채;
	@Column (name="비유동부채_기타")
	private Double 비유동부채_기타;
	@Column (name="이연부채")
	private Double 이연부채;
	@Column (name="부채총계")
	private Double 부채총계;
	@Column (name="자본금")
	private Double 자본금;
	@Column (name="자본금_보통주자본금")
	private Double 자본금_보통주자본금;
	@Column (name="자본금_우선주자본금")
	private Double 자본금_우선주자본금;
	@Column (name="자본잉여금")
	private Double 자본잉여금;
	@Column (name="이익잉여금")
	private Double 이익잉여금;
	@Column (name="자본조정")
	private Double 자본조정;
	@Column (name="자본조정_자기주식")
	private Double 자본조정_자기주식;
	@Column (name="자본조정_기타")
	private Double 자본조정_기타;
	@Column (name="기타포괄손익누계액")
	private Double 기타포괄손익누계액;
	@Column (name="기타포괄손익누계액_매도가능증권평가손익")
	private Double 기타포괄손익누계액_매도가능증권평가손익;
	@Column (name="기타포괄손익누계액_파생상품평가손익")
	private Double 기타포괄손익누계액_파생상품평가손익;
	@Column (name="기타포괄손익누계액_외화환산손익")
	private Double 기타포괄손익누계액_외화환산손익;
	@Column (name="기타포괄손익누계액_자산재평가손익")
	private Double 기타포괄손익누계액_자산재평가손익;
	@Column (name="기타포괄손익누계액_기타손익")
	private Double 기타포괄손익누계액_기타손익;
	@Column (name="자본총계")
	private Double 자본총계;
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getYmDay() {
		return ymDay;
	}
	public void setYmDay(String ymDay) {
		this.ymDay = ymDay;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	
	public Double get유동자산() {
		return 유동자산;
	}
	public void set유동자산(Double 유동자산) {
		this.유동자산 = 유동자산;
	}
	public Double get당좌자산() {
		return 당좌자산;
	}
	public void set당좌자산(Double 당좌자산) {
		this.당좌자산 = 당좌자산;
	}
	public Double get당좌자산_현금및현금성자산() {
		return 당좌자산_현금및현금성자산;
	}
	public void set당좌자산_현금및현금성자산(Double 당좌자산_현금및현금성자산) {
		this.당좌자산_현금및현금성자산 = 당좌자산_현금및현금성자산;
	}
	public Double get당좌자산_단기투자자산() {
		return 당좌자산_단기투자자산;
	}
	public void set당좌자산_단기투자자산(Double 당좌자산_단기투자자산) {
		this.당좌자산_단기투자자산 = 당좌자산_단기투자자산;
	}
	public Double get당좌자산_단기투자자산_단기예금() {
		return 당좌자산_단기투자자산_단기예금;
	}
	public void set당좌자산_단기투자자산_단기예금(Double 당좌자산_단기투자자산_단기예금) {
		this.당좌자산_단기투자자산_단기예금 = 당좌자산_단기투자자산_단기예금;
	}
	public Double get당좌자산_단기투자자산_매도가능증권() {
		return 당좌자산_단기투자자산_매도가능증권;
	}
	public void set당좌자산_단기투자자산_매도가능증권(Double 당좌자산_단기투자자산_매도가능증권) {
		this.당좌자산_단기투자자산_매도가능증권 = 당좌자산_단기투자자산_매도가능증권;
	}
	public Double get당좌자산_단기투자자산_만기보유증권() {
		return 당좌자산_단기투자자산_만기보유증권;
	}
	public void set당좌자산_단기투자자산_만기보유증권(Double 당좌자산_단기투자자산_만기보유증권) {
		this.당좌자산_단기투자자산_만기보유증권 = 당좌자산_단기투자자산_만기보유증권;
	}
	public Double get당좌자산_단기투자자산_단기매매증권() {
		return 당좌자산_단기투자자산_단기매매증권;
	}
	public void set당좌자산_단기투자자산_단기매매증권(Double 당좌자산_단기투자자산_단기매매증권) {
		this.당좌자산_단기투자자산_단기매매증권 = 당좌자산_단기투자자산_단기매매증권;
	}
	public Double get당좌자산_단기투자자산_단기매매증권_주식() {
		return 당좌자산_단기투자자산_단기매매증권_주식;
	}
	public void set당좌자산_단기투자자산_단기매매증권_주식(Double 당좌자산_단기투자자산_단기매매증권_주식) {
		this.당좌자산_단기투자자산_단기매매증권_주식 = 당좌자산_단기투자자산_단기매매증권_주식;
	}
	public Double get당좌자산_단기투자자산_단기매매증권_수익증권() {
		return 당좌자산_단기투자자산_단기매매증권_수익증권;
	}
	public void set당좌자산_단기투자자산_단기매매증권_수익증권(Double 당좌자산_단기투자자산_단기매매증권_수익증권) {
		this.당좌자산_단기투자자산_단기매매증권_수익증권 = 당좌자산_단기투자자산_단기매매증권_수익증권;
	}
	public Double get당좌자산_단기투자자산_단기매매증권_채권() {
		return 당좌자산_단기투자자산_단기매매증권_채권;
	}
	public void set당좌자산_단기투자자산_단기매매증권_채권(Double 당좌자산_단기투자자산_단기매매증권_채권) {
		this.당좌자산_단기투자자산_단기매매증권_채권 = 당좌자산_단기투자자산_단기매매증권_채권;
	}
	public Double get당좌자산_단기투자자산_단기매매증권_기타() {
		return 당좌자산_단기투자자산_단기매매증권_기타;
	}
	public void set당좌자산_단기투자자산_단기매매증권_기타(Double 당좌자산_단기투자자산_단기매매증권_기타) {
		this.당좌자산_단기투자자산_단기매매증권_기타 = 당좌자산_단기투자자산_단기매매증권_기타;
	}
	public Double get당좌자산_단기투자자산_단기대여금() {
		return 당좌자산_단기투자자산_단기대여금;
	}
	public void set당좌자산_단기투자자산_단기대여금(Double 당좌자산_단기투자자산_단기대여금) {
		this.당좌자산_단기투자자산_단기대여금 = 당좌자산_단기투자자산_단기대여금;
	}
	public Double get당좌자산_단기투자자산_기타() {
		return 당좌자산_단기투자자산_기타;
	}
	public void set당좌자산_단기투자자산_기타(Double 당좌자산_단기투자자산_기타) {
		this.당좌자산_단기투자자산_기타 = 당좌자산_단기투자자산_기타;
	}
	public Double get당좌자산_매출채권() {
		return 당좌자산_매출채권;
	}
	public void set당좌자산_매출채권(Double 당좌자산_매출채권) {
		this.당좌자산_매출채권 = 당좌자산_매출채권;
	}
	public Double get당좌자산_매출채권_매출채권() {
		return 당좌자산_매출채권_매출채권;
	}
	public void set당좌자산_매출채권_매출채권(Double 당좌자산_매출채권_매출채권) {
		this.당좌자산_매출채권_매출채권 = 당좌자산_매출채권_매출채권;
	}
	public Double get당좌자산_매출채권_외화매출채권() {
		return 당좌자산_매출채권_외화매출채권;
	}
	public void set당좌자산_매출채권_외화매출채권(Double 당좌자산_매출채권_외화매출채권) {
		this.당좌자산_매출채권_외화매출채권 = 당좌자산_매출채권_외화매출채권;
	}
	public Double get당좌자산_매출채권_기타매출채권() {
		return 당좌자산_매출채권_기타매출채권;
	}
	public void set당좌자산_매출채권_기타매출채권(Double 당좌자산_매출채권_기타매출채권) {
		this.당좌자산_매출채권_기타매출채권 = 당좌자산_매출채권_기타매출채권;
	}
	public Double get당좌자산_기타() {
		return 당좌자산_기타;
	}
	public void set당좌자산_기타(Double 당좌자산_기타) {
		this.당좌자산_기타 = 당좌자산_기타;
	}
	public Double get재고자산() {
		return 재고자산;
	}
	public void set재고자산(Double 재고자산) {
		this.재고자산 = 재고자산;
	}
	public Double get재고자산_원재료() {
		return 재고자산_원재료;
	}
	public void set재고자산_원재료(Double 재고자산_원재료) {
		this.재고자산_원재료 = 재고자산_원재료;
	}
	public Double get재고자산_재공품() {
		return 재고자산_재공품;
	}
	public void set재고자산_재공품(Double 재고자산_재공품) {
		this.재고자산_재공품 = 재고자산_재공품;
	}
	public Double get재고자산_반제품() {
		return 재고자산_반제품;
	}
	public void set재고자산_반제품(Double 재고자산_반제품) {
		this.재고자산_반제품 = 재고자산_반제품;
	}
	public Double get재고자산_제품() {
		return 재고자산_제품;
	}
	public void set재고자산_제품(Double 재고자산_제품) {
		this.재고자산_제품 = 재고자산_제품;
	}
	public Double get재고자산_상품() {
		return 재고자산_상품;
	}
	public void set재고자산_상품(Double 재고자산_상품) {
		this.재고자산_상품 = 재고자산_상품;
	}
	public Double get재고자산_기타() {
		return 재고자산_기타;
	}
	public void set재고자산_기타(Double 재고자산_기타) {
		this.재고자산_기타 = 재고자산_기타;
	}
	public Double get비유동자산() {
		return 비유동자산;
	}
	public void set비유동자산(Double 비유동자산) {
		this.비유동자산 = 비유동자산;
	}
	public Double get투자자산() {
		return 투자자산;
	}
	public void set투자자산(Double 투자자산) {
		this.투자자산 = 투자자산;
	}
	public Double get투자자산_장기금융상품() {
		return 투자자산_장기금융상품;
	}
	public void set투자자산_장기금융상품(Double 투자자산_장기금융상품) {
		this.투자자산_장기금융상품 = 투자자산_장기금융상품;
	}
	public Double get투자자산_매도가능증권() {
		return 투자자산_매도가능증권;
	}
	public void set투자자산_매도가능증권(Double 투자자산_매도가능증권) {
		this.투자자산_매도가능증권 = 투자자산_매도가능증권;
	}
	public Double get투자자산_매도가능증권_주식() {
		return 투자자산_매도가능증권_주식;
	}
	public void set투자자산_매도가능증권_주식(Double 투자자산_매도가능증권_주식) {
		this.투자자산_매도가능증권_주식 = 투자자산_매도가능증권_주식;
	}
	public Double get투자자산_매도가능증권_수익증권() {
		return 투자자산_매도가능증권_수익증권;
	}
	public void set투자자산_매도가능증권_수익증권(Double 투자자산_매도가능증권_수익증권) {
		this.투자자산_매도가능증권_수익증권 = 투자자산_매도가능증권_수익증권;
	}
	public Double get투자자산_매도가능증권_채권() {
		return 투자자산_매도가능증권_채권;
	}
	public void set투자자산_매도가능증권_채권(Double 투자자산_매도가능증권_채권) {
		this.투자자산_매도가능증권_채권 = 투자자산_매도가능증권_채권;
	}
	public Double get투자자산_매도가능증권_기타() {
		return 투자자산_매도가능증권_기타;
	}
	public void set투자자산_매도가능증권_기타(Double 투자자산_매도가능증권_기타) {
		this.투자자산_매도가능증권_기타 = 투자자산_매도가능증권_기타;
	}
	public Double get투자자산_만기보유증권() {
		return 투자자산_만기보유증권;
	}
	public void set투자자산_만기보유증권(Double 투자자산_만기보유증권) {
		this.투자자산_만기보유증권 = 투자자산_만기보유증권;
	}
	public Double get투자자산_지분법적용투자주식() {
		return 투자자산_지분법적용투자주식;
	}
	public void set투자자산_지분법적용투자주식(Double 투자자산_지분법적용투자주식) {
		this.투자자산_지분법적용투자주식 = 투자자산_지분법적용투자주식;
	}
	public Double get투자자산_투자부동산() {
		return 투자자산_투자부동산;
	}
	public void set투자자산_투자부동산(Double 투자자산_투자부동산) {
		this.투자자산_투자부동산 = 투자자산_투자부동산;
	}
	public Double get투자자산_기타투자자산() {
		return 투자자산_기타투자자산;
	}
	public void set투자자산_기타투자자산(Double 투자자산_기타투자자산) {
		this.투자자산_기타투자자산 = 투자자산_기타투자자산;
	}
	public Double get유형자산() {
		return 유형자산;
	}
	public void set유형자산(Double 유형자산) {
		this.유형자산 = 유형자산;
	}
	public Double get유형자산_토지() {
		return 유형자산_토지;
	}
	public void set유형자산_토지(Double 유형자산_토지) {
		this.유형자산_토지 = 유형자산_토지;
	}
	public Double get유형자산_건물() {
		return 유형자산_건물;
	}
	public void set유형자산_건물(Double 유형자산_건물) {
		this.유형자산_건물 = 유형자산_건물;
	}
	public Double get유형자산_구축물() {
		return 유형자산_구축물;
	}
	public void set유형자산_구축물(Double 유형자산_구축물) {
		this.유형자산_구축물 = 유형자산_구축물;
	}
	public Double get유형자산_기계장치() {
		return 유형자산_기계장치;
	}
	public void set유형자산_기계장치(Double 유형자산_기계장치) {
		this.유형자산_기계장치 = 유형자산_기계장치;
	}
	public Double get유형자산_건설중인자산() {
		return 유형자산_건설중인자산;
	}
	public void set유형자산_건설중인자산(Double 유형자산_건설중인자산) {
		this.유형자산_건설중인자산 = 유형자산_건설중인자산;
	}
	public Double get유형자산_기타() {
		return 유형자산_기타;
	}
	public void set유형자산_기타(Double 유형자산_기타) {
		this.유형자산_기타 = 유형자산_기타;
	}
	public Double get무형자산() {
		return 무형자산;
	}
	public void set무형자산(Double 무형자산) {
		this.무형자산 = 무형자산;
	}
	public Double get무형자산_영업권() {
		return 무형자산_영업권;
	}
	public void set무형자산_영업권(Double 무형자산_영업권) {
		this.무형자산_영업권 = 무형자산_영업권;
	}
	public Double get무형자산_광업권() {
		return 무형자산_광업권;
	}
	public void set무형자산_광업권(Double 무형자산_광업권) {
		this.무형자산_광업권 = 무형자산_광업권;
	}
	public Double get무형자산_어업권() {
		return 무형자산_어업권;
	}
	public void set무형자산_어업권(Double 무형자산_어업권) {
		this.무형자산_어업권 = 무형자산_어업권;
	}
	public Double get무형자산_차지권() {
		return 무형자산_차지권;
	}
	public void set무형자산_차지권(Double 무형자산_차지권) {
		this.무형자산_차지권 = 무형자산_차지권;
	}
	public Double get무형자산_개발비() {
		return 무형자산_개발비;
	}
	public void set무형자산_개발비(Double 무형자산_개발비) {
		this.무형자산_개발비 = 무형자산_개발비;
	}
	public Double get무형자산_기타() {
		return 무형자산_기타;
	}
	public void set무형자산_기타(Double 무형자산_기타) {
		this.무형자산_기타 = 무형자산_기타;
	}
	public Double get기타비유동자산() {
		return 기타비유동자산;
	}
	public void set기타비유동자산(Double 기타비유동자산) {
		this.기타비유동자산 = 기타비유동자산;
	}
	public Double get이연자산() {
		return 이연자산;
	}
	public void set이연자산(Double 이연자산) {
		this.이연자산 = 이연자산;
	}
	public Double get자산총계() {
		return 자산총계;
	}
	public void set자산총계(Double 자산총계) {
		this.자산총계 = 자산총계;
	}
	public Double get유동부채() {
		return 유동부채;
	}
	public void set유동부채(Double 유동부채) {
		this.유동부채 = 유동부채;
	}
	public Double get유동부채_매입채무() {
		return 유동부채_매입채무;
	}
	public void set유동부채_매입채무(Double 유동부채_매입채무) {
		this.유동부채_매입채무 = 유동부채_매입채무;
	}
	public Double get유동부채_매입채무_매입채무() {
		return 유동부채_매입채무_매입채무;
	}
	public void set유동부채_매입채무_매입채무(Double 유동부채_매입채무_매입채무) {
		this.유동부채_매입채무_매입채무 = 유동부채_매입채무_매입채무;
	}
	public Double get유동부채_매입채무_외화매입채무() {
		return 유동부채_매입채무_외화매입채무;
	}
	public void set유동부채_매입채무_외화매입채무(Double 유동부채_매입채무_외화매입채무) {
		this.유동부채_매입채무_외화매입채무 = 유동부채_매입채무_외화매입채무;
	}
	public Double get유동부채_매입채무_기타() {
		return 유동부채_매입채무_기타;
	}
	public void set유동부채_매입채무_기타(Double 유동부채_매입채무_기타) {
		this.유동부채_매입채무_기타 = 유동부채_매입채무_기타;
	}
	public Double get유동부채_단기차입금() {
		return 유동부채_단기차입금;
	}
	public void set유동부채_단기차입금(Double 유동부채_단기차입금) {
		this.유동부채_단기차입금 = 유동부채_단기차입금;
	}
	public Double get유동부채_단기차입금_단기차입금() {
		return 유동부채_단기차입금_단기차입금;
	}
	public void set유동부채_단기차입금_단기차입금(Double 유동부채_단기차입금_단기차입금) {
		this.유동부채_단기차입금_단기차입금 = 유동부채_단기차입금_단기차입금;
	}
	public Double get유동부채_단기차입금_외화단기차입금() {
		return 유동부채_단기차입금_외화단기차입금;
	}
	public void set유동부채_단기차입금_외화단기차입금(Double 유동부채_단기차입금_외화단기차입금) {
		this.유동부채_단기차입금_외화단기차입금 = 유동부채_단기차입금_외화단기차입금;
	}
	public Double get유동부채_단기차입금_기타() {
		return 유동부채_단기차입금_기타;
	}
	public void set유동부채_단기차입금_기타(Double 유동부채_단기차입금_기타) {
		this.유동부채_단기차입금_기타 = 유동부채_단기차입금_기타;
	}
	public Double get유동부채_유동성장기부채() {
		return 유동부채_유동성장기부채;
	}
	public void set유동부채_유동성장기부채(Double 유동부채_유동성장기부채) {
		this.유동부채_유동성장기부채 = 유동부채_유동성장기부채;
	}
	public Double get유동부채_선수금() {
		return 유동부채_선수금;
	}
	public void set유동부채_선수금(Double 유동부채_선수금) {
		this.유동부채_선수금 = 유동부채_선수금;
	}
	public Double get유동부채_기타() {
		return 유동부채_기타;
	}
	public void set유동부채_기타(Double 유동부채_기타) {
		this.유동부채_기타 = 유동부채_기타;
	}
	public Double get비유동부채() {
		return 비유동부채;
	}
	public void set비유동부채(Double 비유동부채) {
		this.비유동부채 = 비유동부채;
	}
	public Double get비유동부채_사채() {
		return 비유동부채_사채;
	}
	public void set비유동부채_사채(Double 비유동부채_사채) {
		this.비유동부채_사채 = 비유동부채_사채;
	}
	public Double get비유동부채_장기차입금() {
		return 비유동부채_장기차입금;
	}
	public void set비유동부채_장기차입금(Double 비유동부채_장기차입금) {
		this.비유동부채_장기차입금 = 비유동부채_장기차입금;
	}
	public Double get비유동부채_장기차입금_장기차입금() {
		return 비유동부채_장기차입금_장기차입금;
	}
	public void set비유동부채_장기차입금_장기차입금(Double 비유동부채_장기차입금_장기차입금) {
		this.비유동부채_장기차입금_장기차입금 = 비유동부채_장기차입금_장기차입금;
	}
	public Double get비유동부채_장기차입금_외화장기차입금() {
		return 비유동부채_장기차입금_외화장기차입금;
	}
	public void set비유동부채_장기차입금_외화장기차입금(Double 비유동부채_장기차입금_외화장기차입금) {
		this.비유동부채_장기차입금_외화장기차입금 = 비유동부채_장기차입금_외화장기차입금;
	}
	public Double get비유동부채_장기차입금_기타() {
		return 비유동부채_장기차입금_기타;
	}
	public void set비유동부채_장기차입금_기타(Double 비유동부채_장기차입금_기타) {
		this.비유동부채_장기차입금_기타 = 비유동부채_장기차입금_기타;
	}
	public Double get비유동부채_장기선수금() {
		return 비유동부채_장기선수금;
	}
	public void set비유동부채_장기선수금(Double 비유동부채_장기선수금) {
		this.비유동부채_장기선수금 = 비유동부채_장기선수금;
	}
	public Double get비유동부채_파생상품부채() {
		return 비유동부채_파생상품부채;
	}
	public void set비유동부채_파생상품부채(Double 비유동부채_파생상품부채) {
		this.비유동부채_파생상품부채 = 비유동부채_파생상품부채;
	}
	public Double get비유동부채_기타() {
		return 비유동부채_기타;
	}
	public void set비유동부채_기타(Double 비유동부채_기타) {
		this.비유동부채_기타 = 비유동부채_기타;
	}
	public Double get이연부채() {
		return 이연부채;
	}
	public void set이연부채(Double 이연부채) {
		this.이연부채 = 이연부채;
	}
	public Double get부채총계() {
		return 부채총계;
	}
	public void set부채총계(Double 부채총계) {
		this.부채총계 = 부채총계;
	}
	public Double get자본금() {
		return 자본금;
	}
	public void set자본금(Double 자본금) {
		this.자본금 = 자본금;
	}
	public Double get자본금_보통주자본금() {
		return 자본금_보통주자본금;
	}
	public void set자본금_보통주자본금(Double 자본금_보통주자본금) {
		this.자본금_보통주자본금 = 자본금_보통주자본금;
	}
	public Double get자본금_우선주자본금() {
		return 자본금_우선주자본금;
	}
	public void set자본금_우선주자본금(Double 자본금_우선주자본금) {
		this.자본금_우선주자본금 = 자본금_우선주자본금;
	}
	public Double get자본잉여금() {
		return 자본잉여금;
	}
	public void set자본잉여금(Double 자본잉여금) {
		this.자본잉여금 = 자본잉여금;
	}
	public Double get이익잉여금() {
		return 이익잉여금;
	}
	public void set이익잉여금(Double 이익잉여금) {
		this.이익잉여금 = 이익잉여금;
	}
	public Double get자본조정() {
		return 자본조정;
	}
	public void set자본조정(Double 자본조정) {
		this.자본조정 = 자본조정;
	}
	public Double get자본조정_자기주식() {
		return 자본조정_자기주식;
	}
	public void set자본조정_자기주식(Double 자본조정_자기주식) {
		this.자본조정_자기주식 = 자본조정_자기주식;
	}
	public Double get자본조정_기타() {
		return 자본조정_기타;
	}
	public void set자본조정_기타(Double 자본조정_기타) {
		this.자본조정_기타 = 자본조정_기타;
	}
	public Double get기타포괄손익누계액() {
		return 기타포괄손익누계액;
	}
	public void set기타포괄손익누계액(Double 기타포괄손익누계액) {
		this.기타포괄손익누계액 = 기타포괄손익누계액;
	}
	public Double get기타포괄손익누계액_매도가능증권평가손익() {
		return 기타포괄손익누계액_매도가능증권평가손익;
	}
	public void set기타포괄손익누계액_매도가능증권평가손익(Double 기타포괄손익누계액_매도가능증권평가손익) {
		this.기타포괄손익누계액_매도가능증권평가손익 = 기타포괄손익누계액_매도가능증권평가손익;
	}
	public Double get기타포괄손익누계액_파생상품평가손익() {
		return 기타포괄손익누계액_파생상품평가손익;
	}
	public void set기타포괄손익누계액_파생상품평가손익(Double 기타포괄손익누계액_파생상품평가손익) {
		this.기타포괄손익누계액_파생상품평가손익 = 기타포괄손익누계액_파생상품평가손익;
	}
	public Double get기타포괄손익누계액_외화환산손익() {
		return 기타포괄손익누계액_외화환산손익;
	}
	public void set기타포괄손익누계액_외화환산손익(Double 기타포괄손익누계액_외화환산손익) {
		this.기타포괄손익누계액_외화환산손익 = 기타포괄손익누계액_외화환산손익;
	}
	public Double get기타포괄손익누계액_자산재평가손익() {
		return 기타포괄손익누계액_자산재평가손익;
	}
	public void set기타포괄손익누계액_자산재평가손익(Double 기타포괄손익누계액_자산재평가손익) {
		this.기타포괄손익누계액_자산재평가손익 = 기타포괄손익누계액_자산재평가손익;
	}
	public Double get기타포괄손익누계액_기타손익() {
		return 기타포괄손익누계액_기타손익;
	}
	public void set기타포괄손익누계액_기타손익(Double 기타포괄손익누계액_기타손익) {
		this.기타포괄손익누계액_기타손익 = 기타포괄손익누계액_기타손익;
	}
	public Double get자본총계() {
		return 자본총계;
	}
	public void set자본총계(Double 자본총계) {
		this.자본총계 = 자본총계;
	}
	
}
