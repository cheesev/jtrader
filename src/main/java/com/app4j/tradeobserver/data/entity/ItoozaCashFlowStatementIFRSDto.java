package com.app4j.tradeobserver.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.app4j.tradeobserver.data.entity.id.ItoozaCashFlowStatementIFRSDtoId;

@Entity
@Table (name = "ITOOZA_CASH_FLOW_STATEMENT_IFRS")
@IdClass(ItoozaCashFlowStatementIFRSDtoId.class)
public class ItoozaCashFlowStatementIFRSDto {
	
	@Id
    @Column (name = "STOCK_CODE")
    private String stockCode;	
	@Id
	@Column (name="YM_DAY")
	private String ymDay;
	@Id
	@Column (name="DATE_TYPE")
	private String dateType;		// 분기, 연간 (Q, Y)
	
	@Column(name="영업활동으로인한현금흐름")
	private Double 영업활동으로인한현금흐름;
	@Column(name="당기순이익")
	private Double 당기순이익;
	@Column(name="법인세비용차감전계속사업이익")
	private Double 법인세비용차감전계속사업이익;
	@Column(name="현금유출이없는비용")
	private Double 현금유출이없는비용;
	@Column(name="현금유출이없는비용_유형자산감가상각비")
	private Double 현금유출이없는비용_유형자산감가상각비;
	@Column(name="현금유출이없는비용_개발비상각")
	private Double 현금유출이없는비용_개발비상각;
	@Column(name="현금유출이없는비용_무형자산상각비")
	private Double 현금유출이없는비용_무형자산상각비;
	@Column(name="현금유출이없는비용_대손상각비")
	private Double 현금유출이없는비용_대손상각비;
	@Column(name="현금유출이없는비용_금융비용")
	private Double 현금유출이없는비용_금융비용;
	@Column(name="현금유출이없는비용_이자비용")
	private Double 현금유출이없는비용_이자비용;
	@Column(name="현금유출이없는비용_외환거래손실")
	private Double 현금유출이없는비용_외환거래손실;
	@Column(name="현금유출이없는비용_충당부채전입액")
	private Double 현금유출이없는비용_충당부채전입액;
	@Column(name="현금유출이없는비용_법인세비용")
	private Double 현금유출이없는비용_법인세비용;
	@Column(name="현금유출이없는비용_기타")
	private Double 현금유출이없는비용_기타;
	@Column(name="현금유입이없는수익")
	private Double 현금유입이없는수익;
	@Column(name="현금유입이없는수익_지분법이익")
	private Double 현금유입이없는수익_지분법이익;
	@Column(name="현금유입이없는수익_금융자산평가이익")
	private Double 현금유입이없는수익_금융자산평가이익;
	@Column(name="현금유입이없는수익_투자자산평가이익")
	private Double 현금유입이없는수익_투자자산평가이익;
	@Column(name="현금유입이없는수익_파생상품이익")
	private Double 현금유입이없는수익_파생상품이익;
	@Column(name="현금유입이없는수익_금융수익")
	private Double 현금유입이없는수익_금융수익;
	@Column(name="현금유입이없는수익_이자수익")
	private Double 현금유입이없는수익_이자수익;
	@Column(name="현금유입이없는수익_외환거래이익")
	private Double 현금유입이없는수익_외환거래이익;
	@Column(name="현금유입이없는수익_기타")
	private Double 현금유입이없는수익_기타;
	@Column(name="영업자산부채변동")
	private Double 영업자산부채변동;
	@Column(name="영업자산부채변동_영업활동자산의감소")
	private Double 영업자산부채변동_영업활동자산의감소;
	@Column(name="영업자산부채변동_영업활동자산의감소_매출채권감소")
	private Double 영업자산부채변동_영업활동자산의감소_매출채권감소;
	@Column(name="영업자산부채변동_영업활동자산의감소_재고자산의감소")
	private Double 영업자산부채변동_영업활동자산의감소_재고자산의감소;
	@Column(name="영업자산부채변동_영업활동자산의감소_선급금감소")
	private Double 영업자산부채변동_영업활동자산의감소_선급금감소;
	@Column(name="영업자산부채변동_영업활동자산의감소_선급비용감소")
	private Double 영업자산부채변동_영업활동자산의감소_선급비용감소;
	@Column(name="영업자산부채변동_영업활동자산의감소_매각예정자산감소")
	private Double 영업자산부채변동_영업활동자산의감소_매각예정자산감소;
	@Column(name="영업자산부채변동_영업활동자산의감소_기타영업자산감소")
	private Double 영업자산부채변동_영업활동자산의감소_기타영업자산감소;
	@Column(name="영업자산부채변동_영업활동부채의증가")
	private Double 영업자산부채변동_영업활동부채의증가;
	@Column(name="영업자산부채변동_영업활동부채의증가_매입채무증가")
	private Double 영업자산부채변동_영업활동부채의증가_매입채무증가;
	@Column(name="영업자산부채변동_영업활동부채의증가_선수금증가")
	private Double 영업자산부채변동_영업활동부채의증가_선수금증가;
	@Column(name="영업자산부채변동_영업활동부채의증가_선수수익증가")
	private Double 영업자산부채변동_영업활동부채의증가_선수수익증가;
	@Column(name="영업자산부채변동_영업활동부채의증가_예수금의증가")
	private Double 영업자산부채변동_영업활동부채의증가_예수금의증가;
	@Column(name="영업자산부채변동_영업활동부채의증가_미지급비용증가")
	private Double 영업자산부채변동_영업활동부채의증가_미지급비용증가;
	@Column(name="영업자산부채변동_영업활동부채의증가_충당부채의증가")
	private Double 영업자산부채변동_영업활동부채의증가_충당부채의증가;
	@Column(name="영업자산부채변동_영업활동부채의증가_퇴직금관련")
	private Double 영업자산부채변동_영업활동부채의증가_퇴직금관련;
	@Column(name="영업자산부채변동_영업활동부채의증가_기타영업부채증가")
	private Double 영업자산부채변동_영업활동부채의증가_기타영업부채증가;
	@Column(name="이자배당금법인세")
	private Double 이자배당금법인세;
	@Column(name="이자배당금법인세_이자수취")
	private Double 이자배당금법인세_이자수취;
	@Column(name="이자배당금법인세_이자지급")
	private Double 이자배당금법인세_이자지급;
	@Column(name="이자배당금법인세_배당금수입")
	private Double 이자배당금법인세_배당금수입;
	@Column(name="이자배당금법인세_배당금지급")
	private Double 이자배당금법인세_배당금지급;
	@Column(name="이자배당금법인세_법인세환입")
	private Double 이자배당금법인세_법인세환입;
	@Column(name="이자배당금법인세_법인세납부")
	private Double 이자배당금법인세_법인세납부;
	@Column(name="투자활동으로인한현금흐름")
	private Double 투자활동으로인한현금흐름;
	@Column(name="투자활동현금유입액")
	private Double 투자활동현금유입액;
	@Column(name="투자활동현금유입액_단기금융자산감소")
	private Double 투자활동현금유입액_단기금융자산감소;
	@Column(name="투자활동현금유입액_매도가능금융자산감소")
	private Double 투자활동현금유입액_매도가능금융자산감소;
	@Column(name="투자활동현금유입액_지분법주식감소")
	private Double 투자활동현금유입액_지분법주식감소;
	@Column(name="투자활동현금유입액_기타투자활동감소")
	private Double 투자활동현금유입액_기타투자활동감소;
	@Column(name="투자활동현금유출액")
	private Double 투자활동현금유출액;
	@Column(name="투자활동현금유출액_단기금융자산증가")
	private Double 투자활동현금유출액_단기금융자산증가;
	@Column(name="투자활동현금유출액_매도가능금융자산증가")
	private Double 투자활동현금유출액_매도가능금융자산증가;
	@Column(name="투자활동현금유출액_지분법주식증가")
	private Double 투자활동현금유출액_지분법주식증가;
	@Column(name="투자활동현금유출액_유형자산의증가")
	private Double 투자활동현금유출액_유형자산의증가;
	@Column(name="투자활동현금유출액_유형자산의증가_토지의증가")
	private Double 투자활동현금유출액_유형자산의증가_토지의증가;
	@Column(name="투자활동현금유출액_유형자산의증가_건물및부속설비의증가")
	private Double 투자활동현금유출액_유형자산의증가_건물및부속설비의증가;
	@Column(name="투자활동현금유출액_유형자산의증가_구축물의증가")
	private Double 투자활동현금유출액_유형자산의증가_구축물의증가;
	@Column(name="투자활동현금유출액_유형자산의증가_기계장치의증가")
	private Double 투자활동현금유출액_유형자산의증가_기계장치의증가;
	@Column(name="투자활동현금유출액_유형자산의증가_중장비의증가")
	private Double 투자활동현금유출액_유형자산의증가_중장비의증가;
	@Column(name="투자활동현금유출액_유형자산의증가_선박의증가")
	private Double 투자활동현금유출액_유형자산의증가_선박의증가;
	@Column(name="투자활동현금유출액_유형자산의증가_항공기의증가")
	private Double 투자활동현금유출액_유형자산의증가_항공기의증가;
	@Column(name="투자활동현금유출액_유형자산의증가_건설중인자산의증가")
	private Double 투자활동현금유출액_유형자산의증가_건설중인자산의증가;
	@Column(name="투자활동현금유출액_유형자산의증가_기타유형자산의증가")
	private Double 투자활동현금유출액_유형자산의증가_기타유형자산의증가;
	@Column(name="투자활동현금유출액_기타투자활동증가")
	private Double 투자활동현금유출액_기타투자활동증가;
	@Column(name="재무활동으로인한현금흐름")
	private Double 재무활동으로인한현금흐름;
	@Column(name="재무활동현금유입액")
	private Double 재무활동현금유입액;
	@Column(name="재무활동현금유입액_단기차입금의증가")
	private Double 재무활동현금유입액_단기차입금의증가;
	@Column(name="재무활동현금유입액_장기차입금의증가")
	private Double 재무활동현금유입액_장기차입금의증가;
	@Column(name="재무활동현금유입액_사채의증가")
	private Double 재무활동현금유입액_사채의증가;
	@Column(name="재무활동현금유입액_유동성장기부채의증가")
	private Double 재무활동현금유입액_유동성장기부채의증가;
	@Column(name="재무활동현금유입액_상환우선주의증가")
	private Double 재무활동현금유입액_상환우선주의증가;
	@Column(name="재무활동현금유입액_자본금및자본잉여금증가")
	private Double 재무활동현금유입액_자본금및자본잉여금증가;
	@Column(name="재무활동현금유입액_자기주식의처분")
	private Double 재무활동현금유입액_자기주식의처분;
	@Column(name="재무활동현금유입액_기타재무활동현금유입")
	private Double 재무활동현금유입액_기타재무활동현금유입;
	@Column(name="재무활동현금유출액")
	private Double 재무활동현금유출액;
	@Column(name="재무활동현금유출액_단기차입금의감소")
	private Double 재무활동현금유출액_단기차입금의감소;
	@Column(name="재무활동현금유출액_장기차입금의감소")
	private Double 재무활동현금유출액_장기차입금의감소;
	@Column(name="재무활동현금유출액_사채의감소")
	private Double 재무활동현금유출액_사채의감소;
	@Column(name="재무활동현금유출액_유동성장기부채의감소")
	private Double 재무활동현금유출액_유동성장기부채의감소;
	@Column(name="재무활동현금유출액_상환우선주의감소")
	private Double 재무활동현금유출액_상환우선주의감소;
	@Column(name="재무활동현금유출액_자본금및자본잉여금감소")
	private Double 재무활동현금유출액_자본금및자본잉여금감소;
	@Column(name="재무활동현금유출액_자기주식의취득")
	private Double 재무활동현금유출액_자기주식의취득;
	@Column(name="재무활동현금유출액_기타재무활동현금유출")
	private Double 재무활동현금유출액_기타재무활동현금유출;
	@Column(name="이자배당금법인세2")
	private Double 이자배당금법인세2;
	@Column(name="이자배당금법인세2_이자수취")
	private Double 이자배당금법인세2_이자수취;
	@Column(name="이자배당금법인세2_이자지급")
	private Double 이자배당금법인세2_이자지급;
	@Column(name="이자배당금법인세2_배당금수입")
	private Double 이자배당금법인세2_배당금수입;
	@Column(name="이자배당금법인세2_배당금지급")
	private Double 이자배당금법인세2_배당금지급;
	@Column(name="이자배당금법인세2_법인세환입")
	private Double 이자배당금법인세2_법인세환입;
	@Column(name="이자배당금법인세2_법인세납부")
	private Double 이자배당금법인세2_법인세납부;
	@Column(name="현금의증감")
	private Double 현금의증감;
	@Column(name="기초의현금")
	private Double 기초의현금;
	@Column(name="기말의현금")
	private Double 기말의현금;
	@Column(name="잉여현금흐름")
	private Double 잉여현금흐름;
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getYmDay() {
		return ymDay;
	}
	public void setYmDay(String ymDay) {
		this.ymDay = ymDay;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public Double get영업활동으로인한현금흐름() {
		return 영업활동으로인한현금흐름;
	}
	public void set영업활동으로인한현금흐름(Double 영업활동으로인한현금흐름) {
		this.영업활동으로인한현금흐름 = 영업활동으로인한현금흐름;
	}
	public Double get당기순이익() {
		return 당기순이익;
	}
	public void set당기순이익(Double 당기순이익) {
		this.당기순이익 = 당기순이익;
	}
	public Double get법인세비용차감전계속사업이익() {
		return 법인세비용차감전계속사업이익;
	}
	public void set법인세비용차감전계속사업이익(Double 법인세비용차감전계속사업이익) {
		this.법인세비용차감전계속사업이익 = 법인세비용차감전계속사업이익;
	}
	public Double get현금유출이없는비용() {
		return 현금유출이없는비용;
	}
	public void set현금유출이없는비용(Double 현금유출이없는비용) {
		this.현금유출이없는비용 = 현금유출이없는비용;
	}
	public Double get현금유출이없는비용_유형자산감가상각비() {
		return 현금유출이없는비용_유형자산감가상각비;
	}
	public void set현금유출이없는비용_유형자산감가상각비(Double 현금유출이없는비용_유형자산감가상각비) {
		this.현금유출이없는비용_유형자산감가상각비 = 현금유출이없는비용_유형자산감가상각비;
	}
	public Double get현금유출이없는비용_개발비상각() {
		return 현금유출이없는비용_개발비상각;
	}
	public void set현금유출이없는비용_개발비상각(Double 현금유출이없는비용_개발비상각) {
		this.현금유출이없는비용_개발비상각 = 현금유출이없는비용_개발비상각;
	}
	public Double get현금유출이없는비용_무형자산상각비() {
		return 현금유출이없는비용_무형자산상각비;
	}
	public void set현금유출이없는비용_무형자산상각비(Double 현금유출이없는비용_무형자산상각비) {
		this.현금유출이없는비용_무형자산상각비 = 현금유출이없는비용_무형자산상각비;
	}
	public Double get현금유출이없는비용_대손상각비() {
		return 현금유출이없는비용_대손상각비;
	}
	public void set현금유출이없는비용_대손상각비(Double 현금유출이없는비용_대손상각비) {
		this.현금유출이없는비용_대손상각비 = 현금유출이없는비용_대손상각비;
	}
	public Double get현금유출이없는비용_금융비용() {
		return 현금유출이없는비용_금융비용;
	}
	public void set현금유출이없는비용_금융비용(Double 현금유출이없는비용_금융비용) {
		this.현금유출이없는비용_금융비용 = 현금유출이없는비용_금융비용;
	}
	public Double get현금유출이없는비용_이자비용() {
		return 현금유출이없는비용_이자비용;
	}
	public void set현금유출이없는비용_이자비용(Double 현금유출이없는비용_이자비용) {
		this.현금유출이없는비용_이자비용 = 현금유출이없는비용_이자비용;
	}
	public Double get현금유출이없는비용_외환거래손실() {
		return 현금유출이없는비용_외환거래손실;
	}
	public void set현금유출이없는비용_외환거래손실(Double 현금유출이없는비용_외환거래손실) {
		this.현금유출이없는비용_외환거래손실 = 현금유출이없는비용_외환거래손실;
	}
	public Double get현금유출이없는비용_충당부채전입액() {
		return 현금유출이없는비용_충당부채전입액;
	}
	public void set현금유출이없는비용_충당부채전입액(Double 현금유출이없는비용_충당부채전입액) {
		this.현금유출이없는비용_충당부채전입액 = 현금유출이없는비용_충당부채전입액;
	}
	public Double get현금유출이없는비용_법인세비용() {
		return 현금유출이없는비용_법인세비용;
	}
	public void set현금유출이없는비용_법인세비용(Double 현금유출이없는비용_법인세비용) {
		this.현금유출이없는비용_법인세비용 = 현금유출이없는비용_법인세비용;
	}
	public Double get현금유출이없는비용_기타() {
		return 현금유출이없는비용_기타;
	}
	public void set현금유출이없는비용_기타(Double 현금유출이없는비용_기타) {
		this.현금유출이없는비용_기타 = 현금유출이없는비용_기타;
	}
	public Double get현금유입이없는수익() {
		return 현금유입이없는수익;
	}
	public void set현금유입이없는수익(Double 현금유입이없는수익) {
		this.현금유입이없는수익 = 현금유입이없는수익;
	}
	public Double get현금유입이없는수익_지분법이익() {
		return 현금유입이없는수익_지분법이익;
	}
	public void set현금유입이없는수익_지분법이익(Double 현금유입이없는수익_지분법이익) {
		this.현금유입이없는수익_지분법이익 = 현금유입이없는수익_지분법이익;
	}
	public Double get현금유입이없는수익_금융자산평가이익() {
		return 현금유입이없는수익_금융자산평가이익;
	}
	public void set현금유입이없는수익_금융자산평가이익(Double 현금유입이없는수익_금융자산평가이익) {
		this.현금유입이없는수익_금융자산평가이익 = 현금유입이없는수익_금융자산평가이익;
	}
	public Double get현금유입이없는수익_투자자산평가이익() {
		return 현금유입이없는수익_투자자산평가이익;
	}
	public void set현금유입이없는수익_투자자산평가이익(Double 현금유입이없는수익_투자자산평가이익) {
		this.현금유입이없는수익_투자자산평가이익 = 현금유입이없는수익_투자자산평가이익;
	}
	public Double get현금유입이없는수익_파생상품이익() {
		return 현금유입이없는수익_파생상품이익;
	}
	public void set현금유입이없는수익_파생상품이익(Double 현금유입이없는수익_파생상품이익) {
		this.현금유입이없는수익_파생상품이익 = 현금유입이없는수익_파생상품이익;
	}
	public Double get현금유입이없는수익_금융수익() {
		return 현금유입이없는수익_금융수익;
	}
	public void set현금유입이없는수익_금융수익(Double 현금유입이없는수익_금융수익) {
		this.현금유입이없는수익_금융수익 = 현금유입이없는수익_금융수익;
	}
	public Double get현금유입이없는수익_이자수익() {
		return 현금유입이없는수익_이자수익;
	}
	public void set현금유입이없는수익_이자수익(Double 현금유입이없는수익_이자수익) {
		this.현금유입이없는수익_이자수익 = 현금유입이없는수익_이자수익;
	}
	public Double get현금유입이없는수익_외환거래이익() {
		return 현금유입이없는수익_외환거래이익;
	}
	public void set현금유입이없는수익_외환거래이익(Double 현금유입이없는수익_외환거래이익) {
		this.현금유입이없는수익_외환거래이익 = 현금유입이없는수익_외환거래이익;
	}
	public Double get현금유입이없는수익_기타() {
		return 현금유입이없는수익_기타;
	}
	public void set현금유입이없는수익_기타(Double 현금유입이없는수익_기타) {
		this.현금유입이없는수익_기타 = 현금유입이없는수익_기타;
	}
	public Double get영업자산부채변동() {
		return 영업자산부채변동;
	}
	public void set영업자산부채변동(Double 영업자산부채변동) {
		this.영업자산부채변동 = 영업자산부채변동;
	}
	public Double get영업자산부채변동_영업활동자산의감소() {
		return 영업자산부채변동_영업활동자산의감소;
	}
	public void set영업자산부채변동_영업활동자산의감소(Double 영업자산부채변동_영업활동자산의감소) {
		this.영업자산부채변동_영업활동자산의감소 = 영업자산부채변동_영업활동자산의감소;
	}
	public Double get영업자산부채변동_영업활동자산의감소_매출채권감소() {
		return 영업자산부채변동_영업활동자산의감소_매출채권감소;
	}
	public void set영업자산부채변동_영업활동자산의감소_매출채권감소(Double 영업자산부채변동_영업활동자산의감소_매출채권감소) {
		this.영업자산부채변동_영업활동자산의감소_매출채권감소 = 영업자산부채변동_영업활동자산의감소_매출채권감소;
	}
	public Double get영업자산부채변동_영업활동자산의감소_재고자산의감소() {
		return 영업자산부채변동_영업활동자산의감소_재고자산의감소;
	}
	public void set영업자산부채변동_영업활동자산의감소_재고자산의감소(Double 영업자산부채변동_영업활동자산의감소_재고자산의감소) {
		this.영업자산부채변동_영업활동자산의감소_재고자산의감소 = 영업자산부채변동_영업활동자산의감소_재고자산의감소;
	}
	public Double get영업자산부채변동_영업활동자산의감소_선급금감소() {
		return 영업자산부채변동_영업활동자산의감소_선급금감소;
	}
	public void set영업자산부채변동_영업활동자산의감소_선급금감소(Double 영업자산부채변동_영업활동자산의감소_선급금감소) {
		this.영업자산부채변동_영업활동자산의감소_선급금감소 = 영업자산부채변동_영업활동자산의감소_선급금감소;
	}
	public Double get영업자산부채변동_영업활동자산의감소_선급비용감소() {
		return 영업자산부채변동_영업활동자산의감소_선급비용감소;
	}
	public void set영업자산부채변동_영업활동자산의감소_선급비용감소(Double 영업자산부채변동_영업활동자산의감소_선급비용감소) {
		this.영업자산부채변동_영업활동자산의감소_선급비용감소 = 영업자산부채변동_영업활동자산의감소_선급비용감소;
	}
	public Double get영업자산부채변동_영업활동자산의감소_매각예정자산감소() {
		return 영업자산부채변동_영업활동자산의감소_매각예정자산감소;
	}
	public void set영업자산부채변동_영업활동자산의감소_매각예정자산감소(Double 영업자산부채변동_영업활동자산의감소_매각예정자산감소) {
		this.영업자산부채변동_영업활동자산의감소_매각예정자산감소 = 영업자산부채변동_영업활동자산의감소_매각예정자산감소;
	}
	public Double get영업자산부채변동_영업활동자산의감소_기타영업자산감소() {
		return 영업자산부채변동_영업활동자산의감소_기타영업자산감소;
	}
	public void set영업자산부채변동_영업활동자산의감소_기타영업자산감소(Double 영업자산부채변동_영업활동자산의감소_기타영업자산감소) {
		this.영업자산부채변동_영업활동자산의감소_기타영업자산감소 = 영업자산부채변동_영업활동자산의감소_기타영업자산감소;
	}
	public Double get영업자산부채변동_영업활동부채의증가() {
		return 영업자산부채변동_영업활동부채의증가;
	}
	public void set영업자산부채변동_영업활동부채의증가(Double 영업자산부채변동_영업활동부채의증가) {
		this.영업자산부채변동_영업활동부채의증가 = 영업자산부채변동_영업활동부채의증가;
	}
	public Double get영업자산부채변동_영업활동부채의증가_매입채무증가() {
		return 영업자산부채변동_영업활동부채의증가_매입채무증가;
	}
	public void set영업자산부채변동_영업활동부채의증가_매입채무증가(Double 영업자산부채변동_영업활동부채의증가_매입채무증가) {
		this.영업자산부채변동_영업활동부채의증가_매입채무증가 = 영업자산부채변동_영업활동부채의증가_매입채무증가;
	}
	public Double get영업자산부채변동_영업활동부채의증가_선수금증가() {
		return 영업자산부채변동_영업활동부채의증가_선수금증가;
	}
	public void set영업자산부채변동_영업활동부채의증가_선수금증가(Double 영업자산부채변동_영업활동부채의증가_선수금증가) {
		this.영업자산부채변동_영업활동부채의증가_선수금증가 = 영업자산부채변동_영업활동부채의증가_선수금증가;
	}
	public Double get영업자산부채변동_영업활동부채의증가_선수수익증가() {
		return 영업자산부채변동_영업활동부채의증가_선수수익증가;
	}
	public void set영업자산부채변동_영업활동부채의증가_선수수익증가(Double 영업자산부채변동_영업활동부채의증가_선수수익증가) {
		this.영업자산부채변동_영업활동부채의증가_선수수익증가 = 영업자산부채변동_영업활동부채의증가_선수수익증가;
	}
	public Double get영업자산부채변동_영업활동부채의증가_예수금의증가() {
		return 영업자산부채변동_영업활동부채의증가_예수금의증가;
	}
	public void set영업자산부채변동_영업활동부채의증가_예수금의증가(Double 영업자산부채변동_영업활동부채의증가_예수금의증가) {
		this.영업자산부채변동_영업활동부채의증가_예수금의증가 = 영업자산부채변동_영업활동부채의증가_예수금의증가;
	}
	public Double get영업자산부채변동_영업활동부채의증가_미지급비용증가() {
		return 영업자산부채변동_영업활동부채의증가_미지급비용증가;
	}
	public void set영업자산부채변동_영업활동부채의증가_미지급비용증가(Double 영업자산부채변동_영업활동부채의증가_미지급비용증가) {
		this.영업자산부채변동_영업활동부채의증가_미지급비용증가 = 영업자산부채변동_영업활동부채의증가_미지급비용증가;
	}
	public Double get영업자산부채변동_영업활동부채의증가_충당부채의증가() {
		return 영업자산부채변동_영업활동부채의증가_충당부채의증가;
	}
	public void set영업자산부채변동_영업활동부채의증가_충당부채의증가(Double 영업자산부채변동_영업활동부채의증가_충당부채의증가) {
		this.영업자산부채변동_영업활동부채의증가_충당부채의증가 = 영업자산부채변동_영업활동부채의증가_충당부채의증가;
	}
	public Double get영업자산부채변동_영업활동부채의증가_퇴직금관련() {
		return 영업자산부채변동_영업활동부채의증가_퇴직금관련;
	}
	public void set영업자산부채변동_영업활동부채의증가_퇴직금관련(Double 영업자산부채변동_영업활동부채의증가_퇴직금관련) {
		this.영업자산부채변동_영업활동부채의증가_퇴직금관련 = 영업자산부채변동_영업활동부채의증가_퇴직금관련;
	}
	public Double get영업자산부채변동_영업활동부채의증가_기타영업부채증가() {
		return 영업자산부채변동_영업활동부채의증가_기타영업부채증가;
	}
	public void set영업자산부채변동_영업활동부채의증가_기타영업부채증가(Double 영업자산부채변동_영업활동부채의증가_기타영업부채증가) {
		this.영업자산부채변동_영업활동부채의증가_기타영업부채증가 = 영업자산부채변동_영업활동부채의증가_기타영업부채증가;
	}
	public Double get이자배당금법인세() {
		return 이자배당금법인세;
	}
	public void set이자배당금법인세(Double 이자배당금법인세) {
		this.이자배당금법인세 = 이자배당금법인세;
	}
	public Double get이자배당금법인세_이자수취() {
		return 이자배당금법인세_이자수취;
	}
	public void set이자배당금법인세_이자수취(Double 이자배당금법인세_이자수취) {
		this.이자배당금법인세_이자수취 = 이자배당금법인세_이자수취;
	}
	public Double get이자배당금법인세_이자지급() {
		return 이자배당금법인세_이자지급;
	}
	public void set이자배당금법인세_이자지급(Double 이자배당금법인세_이자지급) {
		this.이자배당금법인세_이자지급 = 이자배당금법인세_이자지급;
	}
	public Double get이자배당금법인세_배당금수입() {
		return 이자배당금법인세_배당금수입;
	}
	public void set이자배당금법인세_배당금수입(Double 이자배당금법인세_배당금수입) {
		this.이자배당금법인세_배당금수입 = 이자배당금법인세_배당금수입;
	}
	public Double get이자배당금법인세_배당금지급() {
		return 이자배당금법인세_배당금지급;
	}
	public void set이자배당금법인세_배당금지급(Double 이자배당금법인세_배당금지급) {
		this.이자배당금법인세_배당금지급 = 이자배당금법인세_배당금지급;
	}
	public Double get이자배당금법인세_법인세환입() {
		return 이자배당금법인세_법인세환입;
	}
	public void set이자배당금법인세_법인세환입(Double 이자배당금법인세_법인세환입) {
		this.이자배당금법인세_법인세환입 = 이자배당금법인세_법인세환입;
	}
	public Double get이자배당금법인세_법인세납부() {
		return 이자배당금법인세_법인세납부;
	}
	public void set이자배당금법인세_법인세납부(Double 이자배당금법인세_법인세납부) {
		this.이자배당금법인세_법인세납부 = 이자배당금법인세_법인세납부;
	}
	public Double get투자활동으로인한현금흐름() {
		return 투자활동으로인한현금흐름;
	}
	public void set투자활동으로인한현금흐름(Double 투자활동으로인한현금흐름) {
		this.투자활동으로인한현금흐름 = 투자활동으로인한현금흐름;
	}
	public Double get투자활동현금유입액() {
		return 투자활동현금유입액;
	}
	public void set투자활동현금유입액(Double 투자활동현금유입액) {
		this.투자활동현금유입액 = 투자활동현금유입액;
	}
	public Double get투자활동현금유입액_단기금융자산감소() {
		return 투자활동현금유입액_단기금융자산감소;
	}
	public void set투자활동현금유입액_단기금융자산감소(Double 투자활동현금유입액_단기금융자산감소) {
		this.투자활동현금유입액_단기금융자산감소 = 투자활동현금유입액_단기금융자산감소;
	}
	public Double get투자활동현금유입액_매도가능금융자산감소() {
		return 투자활동현금유입액_매도가능금융자산감소;
	}
	public void set투자활동현금유입액_매도가능금융자산감소(Double 투자활동현금유입액_매도가능금융자산감소) {
		this.투자활동현금유입액_매도가능금융자산감소 = 투자활동현금유입액_매도가능금융자산감소;
	}
	public Double get투자활동현금유입액_지분법주식감소() {
		return 투자활동현금유입액_지분법주식감소;
	}
	public void set투자활동현금유입액_지분법주식감소(Double 투자활동현금유입액_지분법주식감소) {
		this.투자활동현금유입액_지분법주식감소 = 투자활동현금유입액_지분법주식감소;
	}
	public Double get투자활동현금유입액_기타투자활동감소() {
		return 투자활동현금유입액_기타투자활동감소;
	}
	public void set투자활동현금유입액_기타투자활동감소(Double 투자활동현금유입액_기타투자활동감소) {
		this.투자활동현금유입액_기타투자활동감소 = 투자활동현금유입액_기타투자활동감소;
	}
	public Double get투자활동현금유출액() {
		return 투자활동현금유출액;
	}
	public void set투자활동현금유출액(Double 투자활동현금유출액) {
		this.투자활동현금유출액 = 투자활동현금유출액;
	}
	public Double get투자활동현금유출액_단기금융자산증가() {
		return 투자활동현금유출액_단기금융자산증가;
	}
	public void set투자활동현금유출액_단기금융자산증가(Double 투자활동현금유출액_단기금융자산증가) {
		this.투자활동현금유출액_단기금융자산증가 = 투자활동현금유출액_단기금융자산증가;
	}
	public Double get투자활동현금유출액_매도가능금융자산증가() {
		return 투자활동현금유출액_매도가능금융자산증가;
	}
	public void set투자활동현금유출액_매도가능금융자산증가(Double 투자활동현금유출액_매도가능금융자산증가) {
		this.투자활동현금유출액_매도가능금융자산증가 = 투자활동현금유출액_매도가능금융자산증가;
	}
	public Double get투자활동현금유출액_지분법주식증가() {
		return 투자활동현금유출액_지분법주식증가;
	}
	public void set투자활동현금유출액_지분법주식증가(Double 투자활동현금유출액_지분법주식증가) {
		this.투자활동현금유출액_지분법주식증가 = 투자활동현금유출액_지분법주식증가;
	}
	public Double get투자활동현금유출액_유형자산의증가() {
		return 투자활동현금유출액_유형자산의증가;
	}
	public void set투자활동현금유출액_유형자산의증가(Double 투자활동현금유출액_유형자산의증가) {
		this.투자활동현금유출액_유형자산의증가 = 투자활동현금유출액_유형자산의증가;
	}
	public Double get투자활동현금유출액_유형자산의증가_토지의증가() {
		return 투자활동현금유출액_유형자산의증가_토지의증가;
	}
	public void set투자활동현금유출액_유형자산의증가_토지의증가(Double 투자활동현금유출액_유형자산의증가_토지의증가) {
		this.투자활동현금유출액_유형자산의증가_토지의증가 = 투자활동현금유출액_유형자산의증가_토지의증가;
	}
	public Double get투자활동현금유출액_유형자산의증가_건물및부속설비의증가() {
		return 투자활동현금유출액_유형자산의증가_건물및부속설비의증가;
	}
	public void set투자활동현금유출액_유형자산의증가_건물및부속설비의증가(Double 투자활동현금유출액_유형자산의증가_건물및부속설비의증가) {
		this.투자활동현금유출액_유형자산의증가_건물및부속설비의증가 = 투자활동현금유출액_유형자산의증가_건물및부속설비의증가;
	}
	public Double get투자활동현금유출액_유형자산의증가_구축물의증가() {
		return 투자활동현금유출액_유형자산의증가_구축물의증가;
	}
	public void set투자활동현금유출액_유형자산의증가_구축물의증가(Double 투자활동현금유출액_유형자산의증가_구축물의증가) {
		this.투자활동현금유출액_유형자산의증가_구축물의증가 = 투자활동현금유출액_유형자산의증가_구축물의증가;
	}
	public Double get투자활동현금유출액_유형자산의증가_기계장치의증가() {
		return 투자활동현금유출액_유형자산의증가_기계장치의증가;
	}
	public void set투자활동현금유출액_유형자산의증가_기계장치의증가(Double 투자활동현금유출액_유형자산의증가_기계장치의증가) {
		this.투자활동현금유출액_유형자산의증가_기계장치의증가 = 투자활동현금유출액_유형자산의증가_기계장치의증가;
	}
	public Double get투자활동현금유출액_유형자산의증가_중장비의증가() {
		return 투자활동현금유출액_유형자산의증가_중장비의증가;
	}
	public void set투자활동현금유출액_유형자산의증가_중장비의증가(Double 투자활동현금유출액_유형자산의증가_중장비의증가) {
		this.투자활동현금유출액_유형자산의증가_중장비의증가 = 투자활동현금유출액_유형자산의증가_중장비의증가;
	}
	public Double get투자활동현금유출액_유형자산의증가_선박의증가() {
		return 투자활동현금유출액_유형자산의증가_선박의증가;
	}
	public void set투자활동현금유출액_유형자산의증가_선박의증가(Double 투자활동현금유출액_유형자산의증가_선박의증가) {
		this.투자활동현금유출액_유형자산의증가_선박의증가 = 투자활동현금유출액_유형자산의증가_선박의증가;
	}
	public Double get투자활동현금유출액_유형자산의증가_항공기의증가() {
		return 투자활동현금유출액_유형자산의증가_항공기의증가;
	}
	public void set투자활동현금유출액_유형자산의증가_항공기의증가(Double 투자활동현금유출액_유형자산의증가_항공기의증가) {
		this.투자활동현금유출액_유형자산의증가_항공기의증가 = 투자활동현금유출액_유형자산의증가_항공기의증가;
	}
	public Double get투자활동현금유출액_유형자산의증가_건설중인자산의증가() {
		return 투자활동현금유출액_유형자산의증가_건설중인자산의증가;
	}
	public void set투자활동현금유출액_유형자산의증가_건설중인자산의증가(Double 투자활동현금유출액_유형자산의증가_건설중인자산의증가) {
		this.투자활동현금유출액_유형자산의증가_건설중인자산의증가 = 투자활동현금유출액_유형자산의증가_건설중인자산의증가;
	}
	public Double get투자활동현금유출액_유형자산의증가_기타유형자산의증가() {
		return 투자활동현금유출액_유형자산의증가_기타유형자산의증가;
	}
	public void set투자활동현금유출액_유형자산의증가_기타유형자산의증가(Double 투자활동현금유출액_유형자산의증가_기타유형자산의증가) {
		this.투자활동현금유출액_유형자산의증가_기타유형자산의증가 = 투자활동현금유출액_유형자산의증가_기타유형자산의증가;
	}
	public Double get투자활동현금유출액_기타투자활동증가() {
		return 투자활동현금유출액_기타투자활동증가;
	}
	public void set투자활동현금유출액_기타투자활동증가(Double 투자활동현금유출액_기타투자활동증가) {
		this.투자활동현금유출액_기타투자활동증가 = 투자활동현금유출액_기타투자활동증가;
	}
	public Double get재무활동으로인한현금흐름() {
		return 재무활동으로인한현금흐름;
	}
	public void set재무활동으로인한현금흐름(Double 재무활동으로인한현금흐름) {
		this.재무활동으로인한현금흐름 = 재무활동으로인한현금흐름;
	}
	public Double get재무활동현금유입액() {
		return 재무활동현금유입액;
	}
	public void set재무활동현금유입액(Double 재무활동현금유입액) {
		this.재무활동현금유입액 = 재무활동현금유입액;
	}
	public Double get재무활동현금유입액_단기차입금의증가() {
		return 재무활동현금유입액_단기차입금의증가;
	}
	public void set재무활동현금유입액_단기차입금의증가(Double 재무활동현금유입액_단기차입금의증가) {
		this.재무활동현금유입액_단기차입금의증가 = 재무활동현금유입액_단기차입금의증가;
	}
	public Double get재무활동현금유입액_장기차입금의증가() {
		return 재무활동현금유입액_장기차입금의증가;
	}
	public void set재무활동현금유입액_장기차입금의증가(Double 재무활동현금유입액_장기차입금의증가) {
		this.재무활동현금유입액_장기차입금의증가 = 재무활동현금유입액_장기차입금의증가;
	}
	public Double get재무활동현금유입액_사채의증가() {
		return 재무활동현금유입액_사채의증가;
	}
	public void set재무활동현금유입액_사채의증가(Double 재무활동현금유입액_사채의증가) {
		this.재무활동현금유입액_사채의증가 = 재무활동현금유입액_사채의증가;
	}
	public Double get재무활동현금유입액_유동성장기부채의증가() {
		return 재무활동현금유입액_유동성장기부채의증가;
	}
	public void set재무활동현금유입액_유동성장기부채의증가(Double 재무활동현금유입액_유동성장기부채의증가) {
		this.재무활동현금유입액_유동성장기부채의증가 = 재무활동현금유입액_유동성장기부채의증가;
	}
	public Double get재무활동현금유입액_상환우선주의증가() {
		return 재무활동현금유입액_상환우선주의증가;
	}
	public void set재무활동현금유입액_상환우선주의증가(Double 재무활동현금유입액_상환우선주의증가) {
		this.재무활동현금유입액_상환우선주의증가 = 재무활동현금유입액_상환우선주의증가;
	}
	public Double get재무활동현금유입액_자본금및자본잉여금증가() {
		return 재무활동현금유입액_자본금및자본잉여금증가;
	}
	public void set재무활동현금유입액_자본금및자본잉여금증가(Double 재무활동현금유입액_자본금및자본잉여금증가) {
		this.재무활동현금유입액_자본금및자본잉여금증가 = 재무활동현금유입액_자본금및자본잉여금증가;
	}
	public Double get재무활동현금유입액_자기주식의처분() {
		return 재무활동현금유입액_자기주식의처분;
	}
	public void set재무활동현금유입액_자기주식의처분(Double 재무활동현금유입액_자기주식의처분) {
		this.재무활동현금유입액_자기주식의처분 = 재무활동현금유입액_자기주식의처분;
	}
	public Double get재무활동현금유입액_기타재무활동현금유입() {
		return 재무활동현금유입액_기타재무활동현금유입;
	}
	public void set재무활동현금유입액_기타재무활동현금유입(Double 재무활동현금유입액_기타재무활동현금유입) {
		this.재무활동현금유입액_기타재무활동현금유입 = 재무활동현금유입액_기타재무활동현금유입;
	}
	public Double get재무활동현금유출액() {
		return 재무활동현금유출액;
	}
	public void set재무활동현금유출액(Double 재무활동현금유출액) {
		this.재무활동현금유출액 = 재무활동현금유출액;
	}
	public Double get재무활동현금유출액_단기차입금의감소() {
		return 재무활동현금유출액_단기차입금의감소;
	}
	public void set재무활동현금유출액_단기차입금의감소(Double 재무활동현금유출액_단기차입금의감소) {
		this.재무활동현금유출액_단기차입금의감소 = 재무활동현금유출액_단기차입금의감소;
	}
	public Double get재무활동현금유출액_장기차입금의감소() {
		return 재무활동현금유출액_장기차입금의감소;
	}
	public void set재무활동현금유출액_장기차입금의감소(Double 재무활동현금유출액_장기차입금의감소) {
		this.재무활동현금유출액_장기차입금의감소 = 재무활동현금유출액_장기차입금의감소;
	}
	public Double get재무활동현금유출액_사채의감소() {
		return 재무활동현금유출액_사채의감소;
	}
	public void set재무활동현금유출액_사채의감소(Double 재무활동현금유출액_사채의감소) {
		this.재무활동현금유출액_사채의감소 = 재무활동현금유출액_사채의감소;
	}
	public Double get재무활동현금유출액_유동성장기부채의감소() {
		return 재무활동현금유출액_유동성장기부채의감소;
	}
	public void set재무활동현금유출액_유동성장기부채의감소(Double 재무활동현금유출액_유동성장기부채의감소) {
		this.재무활동현금유출액_유동성장기부채의감소 = 재무활동현금유출액_유동성장기부채의감소;
	}
	public Double get재무활동현금유출액_상환우선주의감소() {
		return 재무활동현금유출액_상환우선주의감소;
	}
	public void set재무활동현금유출액_상환우선주의감소(Double 재무활동현금유출액_상환우선주의감소) {
		this.재무활동현금유출액_상환우선주의감소 = 재무활동현금유출액_상환우선주의감소;
	}
	public Double get재무활동현금유출액_자본금및자본잉여금감소() {
		return 재무활동현금유출액_자본금및자본잉여금감소;
	}
	public void set재무활동현금유출액_자본금및자본잉여금감소(Double 재무활동현금유출액_자본금및자본잉여금감소) {
		this.재무활동현금유출액_자본금및자본잉여금감소 = 재무활동현금유출액_자본금및자본잉여금감소;
	}
	public Double get재무활동현금유출액_자기주식의취득() {
		return 재무활동현금유출액_자기주식의취득;
	}
	public void set재무활동현금유출액_자기주식의취득(Double 재무활동현금유출액_자기주식의취득) {
		this.재무활동현금유출액_자기주식의취득 = 재무활동현금유출액_자기주식의취득;
	}
	public Double get재무활동현금유출액_기타재무활동현금유출() {
		return 재무활동현금유출액_기타재무활동현금유출;
	}
	public void set재무활동현금유출액_기타재무활동현금유출(Double 재무활동현금유출액_기타재무활동현금유출) {
		this.재무활동현금유출액_기타재무활동현금유출 = 재무활동현금유출액_기타재무활동현금유출;
	}
	public Double get이자배당금법인세2() {
		return 이자배당금법인세2;
	}
	public void set이자배당금법인세2(Double 이자배당금법인세2) {
		this.이자배당금법인세2 = 이자배당금법인세2;
	}
	public Double get이자배당금법인세2_이자수취() {
		return 이자배당금법인세2_이자수취;
	}
	public void set이자배당금법인세2_이자수취(Double 이자배당금법인세2_이자수취) {
		this.이자배당금법인세2_이자수취 = 이자배당금법인세2_이자수취;
	}
	public Double get이자배당금법인세2_이자지급() {
		return 이자배당금법인세2_이자지급;
	}
	public void set이자배당금법인세2_이자지급(Double 이자배당금법인세2_이자지급) {
		this.이자배당금법인세2_이자지급 = 이자배당금법인세2_이자지급;
	}
	public Double get이자배당금법인세2_배당금수입() {
		return 이자배당금법인세2_배당금수입;
	}
	public void set이자배당금법인세2_배당금수입(Double 이자배당금법인세2_배당금수입) {
		this.이자배당금법인세2_배당금수입 = 이자배당금법인세2_배당금수입;
	}
	public Double get이자배당금법인세2_배당금지급() {
		return 이자배당금법인세2_배당금지급;
	}
	public void set이자배당금법인세2_배당금지급(Double 이자배당금법인세2_배당금지급) {
		this.이자배당금법인세2_배당금지급 = 이자배당금법인세2_배당금지급;
	}
	public Double get이자배당금법인세2_법인세환입() {
		return 이자배당금법인세2_법인세환입;
	}
	public void set이자배당금법인세2_법인세환입(Double 이자배당금법인세2_법인세환입) {
		this.이자배당금법인세2_법인세환입 = 이자배당금법인세2_법인세환입;
	}
	public Double get이자배당금법인세2_법인세납부() {
		return 이자배당금법인세2_법인세납부;
	}
	public void set이자배당금법인세2_법인세납부(Double 이자배당금법인세2_법인세납부) {
		this.이자배당금법인세2_법인세납부 = 이자배당금법인세2_법인세납부;
	}
	public Double get현금의증감() {
		return 현금의증감;
	}
	public void set현금의증감(Double 현금의증감) {
		this.현금의증감 = 현금의증감;
	}
	public Double get기초의현금() {
		return 기초의현금;
	}
	public void set기초의현금(Double 기초의현금) {
		this.기초의현금 = 기초의현금;
	}
	public Double get기말의현금() {
		return 기말의현금;
	}
	public void set기말의현금(Double 기말의현금) {
		this.기말의현금 = 기말의현금;
	}
	public Double get잉여현금흐름() {
		return 잉여현금흐름;
	}
	public void set잉여현금흐름(Double 잉여현금흐름) {
		this.잉여현금흐름 = 잉여현금흐름;
	}
	
	
	

	
}
