package com.app4j.tradeobserver.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.app4j.tradeobserver.data.entity.id.FinancialStatementCommonDtoId;

@Entity
@Table (name = "VALUE_INDEX")
@IdClass(FinancialStatementCommonDtoId.class)
public class ValueIndexDto {
	
	@Id
    @Column (name = "STOCK_CODE")
    private String stockCode;	
	@Id
	@Column (name="YM_DAY")
	private String ymDay;
	@Id
	@Column (name="DATE_TYPE")
	private String dateType;		// 분기, 연간 (Q, Y)
	@Id
	@Column (name="DATA_TYPE")
	private String dataType;		// 연결, 별도 (L, S)
	@Column(name="순이익률")
	private Double 순이익률;
	@Column(name="영업이익률")
	private Double 영업이익률;
	@Column(name="매출액")
	private Double 매출액;
	@Column(name="ROIC")
	private Double ROIC;
	@Column(name="ROE")
	private Double ROE;
	@Column(name="ROA")
	private Double ROA;
	@Column(name="당기순이익")
	private Double 당기순이익;
	@Column(name="순이익증가율")
	private Double 순이익증가율;
	@Column(name="영업이익증가율")
	private Double 영업이익증가율;
	@Column(name="매출액증가율")
	private Double 매출액증가율;
	@Column(name="자기자본증가율")
	private Double 자기자본증가율;
	@Column(name="유형자산증가율")
	private Double 유형자산증가율;
	@Column(name="유동자산증가율")
	private Double 유동자산증가율;
	@Column(name="총자산증가율")
	private Double 총자산증가율;
	@Column(name="비유동부채비율")
	private Double 비유동부채비율;
	@Column(name="유동부채비율")
	private Double 유동부채비율;
	@Column(name="부채비율")
	private Double 부채비율;
	@Column(name="이자보상배율")
	private Double 이자보상배율;
	@Column(name="순부채")
	private Double 순부채;
	@Column(name="이자발생부채")
	private Double 이자발생부채;
	@Column(name="매입채무회전율")
	private Double 매입채무회전율;
	@Column(name="재고자산회전율")
	private Double 재고자산회전율;
	@Column(name="매출채권회전율")
	private Double 매출채권회전율;
	@Column(name="총자산회전율")
	private Double 총자산회전율;
	@Column(name="CashCycle")
	private Double CashCycle;
	@Column(name="매입채무회전일수")
	private Double 매입채무회전일수;
	@Column(name="재고자산회전일수")
	private Double 재고자산회전일수;
	@Column(name="매출채권회전일수")
	private Double 매출채권회전일수;
	@Column(name="EPS")
	private Double EPS;
	@Column(name="BPS")
	private Double BPS;
	@Column(name="SPS")
	private Double SPS;
	@Column(name="PBR")
	private Double PBR;
	@Column(name="PER")
	private Double PER;
	@Column(name="PSR")
	private Double PSR;
	
	
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getYmDay() {
		return ymDay;
	}
	public void setYmDay(String ymDay) {
		this.ymDay = ymDay;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public Double get순이익률() {
		return 순이익률;
	}
	public void set순이익률(Double 순이익률) {
		this.순이익률 = 순이익률;
	}
	public Double get영업이익률() {
		return 영업이익률;
	}
	public void set영업이익률(Double 영업이익률) {
		this.영업이익률 = 영업이익률;
	}
	public Double get매출액() {
		return 매출액;
	}
	public void set매출액(Double 매출액) {
		this.매출액 = 매출액;
	}
	public Double getROIC() {
		return ROIC;
	}
	public void setROIC(Double rOIC) {
		ROIC = rOIC;
	}
	public Double getROE() {
		return ROE;
	}
	public void setROE(Double rOE) {
		ROE = rOE;
	}
	public Double getROA() {
		return ROA;
	}
	public void setROA(Double rOA) {
		ROA = rOA;
	}
	public Double get당기순이익() {
		return 당기순이익;
	}
	public void set당기순이익(Double 당기순이익) {
		this.당기순이익 = 당기순이익;
	}
	public Double get순이익증가율() {
		return 순이익증가율;
	}
	public void set순이익증가율(Double 순이익증가율) {
		this.순이익증가율 = 순이익증가율;
	}
	public Double get영업이익증가율() {
		return 영업이익증가율;
	}
	public void set영업이익증가율(Double 영업이익증가율) {
		this.영업이익증가율 = 영업이익증가율;
	}
	public Double get매출액증가율() {
		return 매출액증가율;
	}
	public void set매출액증가율(Double 매출액증가율) {
		this.매출액증가율 = 매출액증가율;
	}
	public Double get자기자본증가율() {
		return 자기자본증가율;
	}
	public void set자기자본증가율(Double 자기자본증가율) {
		this.자기자본증가율 = 자기자본증가율;
	}
	public Double get유형자산증가율() {
		return 유형자산증가율;
	}
	public void set유형자산증가율(Double 유형자산증가율) {
		this.유형자산증가율 = 유형자산증가율;
	}
	public Double get유동자산증가율() {
		return 유동자산증가율;
	}
	public void set유동자산증가율(Double 유동자산증가율) {
		this.유동자산증가율 = 유동자산증가율;
	}
	public Double get총자산증가율() {
		return 총자산증가율;
	}
	public void set총자산증가율(Double 총자산증가율) {
		this.총자산증가율 = 총자산증가율;
	}
	public Double get비유동부채비율() {
		return 비유동부채비율;
	}
	public void set비유동부채비율(Double 비유동부채비율) {
		this.비유동부채비율 = 비유동부채비율;
	}
	public Double get유동부채비율() {
		return 유동부채비율;
	}
	public void set유동부채비율(Double 유동부채비율) {
		this.유동부채비율 = 유동부채비율;
	}
	public Double get부채비율() {
		return 부채비율;
	}
	public void set부채비율(Double 부채비율) {
		this.부채비율 = 부채비율;
	}
	public Double get이자보상배율() {
		return 이자보상배율;
	}
	public void set이자보상배율(Double 이자보상배율) {
		this.이자보상배율 = 이자보상배율;
	}
	public Double get순부채() {
		return 순부채;
	}
	public void set순부채(Double 순부채) {
		this.순부채 = 순부채;
	}
	public Double get이자발생부채() {
		return 이자발생부채;
	}
	public void set이자발생부채(Double 이자발생부채) {
		this.이자발생부채 = 이자발생부채;
	}
	public Double get매입채무회전율() {
		return 매입채무회전율;
	}
	public void set매입채무회전율(Double 매입채무회전율) {
		this.매입채무회전율 = 매입채무회전율;
	}
	public Double get재고자산회전율() {
		return 재고자산회전율;
	}
	public void set재고자산회전율(Double 재고자산회전율) {
		this.재고자산회전율 = 재고자산회전율;
	}
	public Double get매출채권회전율() {
		return 매출채권회전율;
	}
	public void set매출채권회전율(Double 매출채권회전율) {
		this.매출채권회전율 = 매출채권회전율;
	}
	public Double get총자산회전율() {
		return 총자산회전율;
	}
	public void set총자산회전율(Double 총자산회전율) {
		this.총자산회전율 = 총자산회전율;
	}
	public Double getCashCycle() {
		return CashCycle;
	}
	public void setCashCycle(Double cashCycle) {
		CashCycle = cashCycle;
	}
	public Double get매입채무회전일수() {
		return 매입채무회전일수;
	}
	public void set매입채무회전일수(Double 매입채무회전일수) {
		this.매입채무회전일수 = 매입채무회전일수;
	}
	public Double get재고자산회전일수() {
		return 재고자산회전일수;
	}
	public void set재고자산회전일수(Double 재고자산회전일수) {
		this.재고자산회전일수 = 재고자산회전일수;
	}
	public Double get매출채권회전일수() {
		return 매출채권회전일수;
	}
	public void set매출채권회전일수(Double 매출채권회전일수) {
		this.매출채권회전일수 = 매출채권회전일수;
	}
	public Double getEPS() {
		return EPS;
	}
	public void setEPS(Double ePS) {
		EPS = ePS;
	}
	public Double getBPS() {
		return BPS;
	}
	public void setBPS(Double bPS) {
		BPS = bPS;
	}
	public Double getSPS() {
		return SPS;
	}
	public void setSPS(Double sPS) {
		SPS = sPS;
	}
	public Double getPBR() {
		return PBR;
	}
	public void setPBR(Double pBR) {
		PBR = pBR;
	}
	public Double getPER() {
		return PER;
	}
	public void setPER(Double pER) {
		PER = pER;
	}
	public Double getPSR() {
		return PSR;
	}
	public void setPSR(Double pSR) {
		PSR = pSR;
	}

	
	
	


}
