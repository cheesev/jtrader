package com.app4j.tradeobserver.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.app4j.tradeobserver.data.entity.id.ItoozaValueIndexDtoId;

@Entity
@Table (name = "ITOOZA_VALUE_INDEX")
@IdClass(ItoozaValueIndexDtoId.class)
public class ItoozaValueIndexDto {
	
	@Id
    @Column (name = "STOCK_CODE")
    private String stockCode;	
	@Id
	@Column (name="YM_DAY")
	private String ymDay;
	@Id
	@Column (name="DATE_TYPE")
	private String dateType;		// 분기, 연간 (Q, Y)
	
	@Column (name="주당순이익_연결지배")
	private Double 주당순이익_연결지배;
	@Column (name="주당순이익_개별")
	private Double 주당순이익_개별;
	@Column (name="주가수익배수")
	private Double 주가수익배수;
	@Column (name="주당순자산")
	private Double 주당순자산;
	@Column (name="주가순자산배수")
	private Double 주가순자산배수;
	@Column (name="주당현금흐름")
	private Double 주당현금흐름;
	@Column (name="주가현금흐름배수")
	private Double 주가현금흐름배수;
	@Column (name="주당매출액")
	private Double 주당매출액;
	@Column (name="주가매출액배수")
	private Double 주가매출액배수;
	@Column (name="자기자본이익률")
	private Double 자기자본이익률;
	@Column (name="순이익률")
	private Double 순이익률;
	@Column (name="총자산회전율")
	private Double 총자산회전율;
	@Column (name="재무레버리지")
	private Double 재무레버리지;
	@Column (name="총자산이익률")
	private Double 총자산이익률;
	@Column (name="매출액순이익률")
	private Double 매출액순이익률;
	@Column (name="매출액영업이익률")
	private Double 매출액영업이익률;
	@Column (name="매출액증가율")
	private Double 매출액증가율;
	@Column (name="영업이익증가율")
	private Double 영업이익증가율;
	@Column (name="순이익증가율")
	private Double 순이익증가율;
	@Column (name="자기자본증가율")
	private Double 자기자본증가율;
	@Column (name="부채비율")
	private Double 부채비율;
	@Column (name="유동비율")
	private Double 유동비율;
	@Column (name="이자보상배율")
	private Double 이자보상배율;
	
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getYmDay() {
		return ymDay;
	}
	public void setYmDay(String ymDay) {
		this.ymDay = ymDay;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public Double get주당순이익_연결지배() {
		return 주당순이익_연결지배;
	}
	public void set주당순이익_연결지배(Double 주당순이익_연결지배) {
		this.주당순이익_연결지배 = 주당순이익_연결지배;
	}
	public Double get주당순이익_개별() {
		return 주당순이익_개별;
	}
	public void set주당순이익_개별(Double 주당순이익_개별) {
		this.주당순이익_개별 = 주당순이익_개별;
	}
	public Double get주가수익배수() {
		return 주가수익배수;
	}
	public void set주가수익배수(Double 주가수익배수) {
		this.주가수익배수 = 주가수익배수;
	}
	public Double get주당순자산() {
		return 주당순자산;
	}
	public void set주당순자산(Double 주당순자산) {
		this.주당순자산 = 주당순자산;
	}
	public Double get주가순자산배수() {
		return 주가순자산배수;
	}
	public void set주가순자산배수(Double 주가순자산배수) {
		this.주가순자산배수 = 주가순자산배수;
	}
	public Double get주당현금흐름() {
		return 주당현금흐름;
	}
	public void set주당현금흐름(Double 주당현금흐름) {
		this.주당현금흐름 = 주당현금흐름;
	}
	public Double get주가현금흐름배수() {
		return 주가현금흐름배수;
	}
	public void set주가현금흐름배수(Double 주가현금흐름배수) {
		this.주가현금흐름배수 = 주가현금흐름배수;
	}
	public Double get주당매출액() {
		return 주당매출액;
	}
	public void set주당매출액(Double 주당매출액) {
		this.주당매출액 = 주당매출액;
	}
	public Double get주가매출액배수() {
		return 주가매출액배수;
	}
	public void set주가매출액배수(Double 주가매출액배수) {
		this.주가매출액배수 = 주가매출액배수;
	}
	public Double get자기자본이익률() {
		return 자기자본이익률;
	}
	public void set자기자본이익률(Double 자기자본이익률) {
		this.자기자본이익률 = 자기자본이익률;
	}
	public Double get순이익률() {
		return 순이익률;
	}
	public void set순이익률(Double 순이익률) {
		this.순이익률 = 순이익률;
	}
	public Double get총자산회전율() {
		return 총자산회전율;
	}
	public void set총자산회전율(Double 총자산회전율) {
		this.총자산회전율 = 총자산회전율;
	}
	public Double get재무레버리지() {
		return 재무레버리지;
	}
	public void set재무레버리지(Double 재무레버리지) {
		this.재무레버리지 = 재무레버리지;
	}
	public Double get총자산이익률() {
		return 총자산이익률;
	}
	public void set총자산이익률(Double 총자산이익률) {
		this.총자산이익률 = 총자산이익률;
	}
	public Double get매출액순이익률() {
		return 매출액순이익률;
	}
	public void set매출액순이익률(Double 매출액순이익률) {
		this.매출액순이익률 = 매출액순이익률;
	}
	public Double get매출액영업이익률() {
		return 매출액영업이익률;
	}
	public void set매출액영업이익률(Double 매출액영업이익률) {
		this.매출액영업이익률 = 매출액영업이익률;
	}
	public Double get매출액증가율() {
		return 매출액증가율;
	}
	public void set매출액증가율(Double 매출액증가율) {
		this.매출액증가율 = 매출액증가율;
	}
	public Double get영업이익증가율() {
		return 영업이익증가율;
	}
	public void set영업이익증가율(Double 영업이익증가율) {
		this.영업이익증가율 = 영업이익증가율;
	}
	public Double get순이익증가율() {
		return 순이익증가율;
	}
	public void set순이익증가율(Double 순이익증가율) {
		this.순이익증가율 = 순이익증가율;
	}
	public Double get자기자본증가율() {
		return 자기자본증가율;
	}
	public void set자기자본증가율(Double 자기자본증가율) {
		this.자기자본증가율 = 자기자본증가율;
	}
	public Double get부채비율() {
		return 부채비율;
	}
	public void set부채비율(Double 부채비율) {
		this.부채비율 = 부채비율;
	}
	public Double get유동비율() {
		return 유동비율;
	}
	public void set유동비율(Double 유동비율) {
		this.유동비율 = 유동비율;
	}
	public Double get이자보상배율() {
		return 이자보상배율;
	}
	public void set이자보상배율(Double 이자보상배율) {
		this.이자보상배율 = 이자보상배율;
	}

}
