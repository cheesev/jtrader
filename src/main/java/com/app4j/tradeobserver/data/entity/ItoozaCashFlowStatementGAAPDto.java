package com.app4j.tradeobserver.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.app4j.tradeobserver.data.entity.id.ItoozaCashFlowStatementGAAPDtoId;

@Entity
@Table (name = "ITOOZA_CASH_FLOW_STATEMENT_GAAP")
@IdClass(ItoozaCashFlowStatementGAAPDtoId.class)
public class ItoozaCashFlowStatementGAAPDto {
	
	@Id
    @Column (name = "STOCK_CODE")
    private String stockCode;	
	@Id
	@Column (name="YM_DAY")
	private String ymDay;
	@Id
	@Column (name="DATE_TYPE")
	private String dateType;		// 분기, 연간 (Q, Y)
	
	@Column (name="영업활동현금흐름")
	private Double 영업활동현금흐름;
	@Column (name="당기순이익")
	private Double 당기순이익;
	@Column (name="현금유출없는비용")
	private Double 현금유출없는비용;
	@Column (name="현금유출없는비용_유형자산감가상각비")
	private Double 현금유출없는비용_유형자산감가상각비;
	@Column (name="현금유출없는비용_무형자산상각비")
	private Double 현금유출없는비용_무형자산상각비;
	@Column (name="현금유출없는비용_대손상각비")
	private Double 현금유출없는비용_대손상각비;
	@Column (name="현금유출없는비용_기타")
	private Double 현금유출없는비용_기타;
	@Column (name="현금유입없는수익")
	private Double 현금유입없는수익;
	@Column (name="영업활동자산부채변동")
	private Double 영업활동자산부채변동;
	@Column (name="영업활동자산부채변동_영업자산증감")
	private Double 영업활동자산부채변동_영업자산증감;
	@Column (name="영업활동자산부채변동_영업자산증감_매출채권증감")
	private Double 영업활동자산부채변동_영업자산증감_매출채권증감;
	@Column (name="영업활동자산부채변동_영업자산증감_재고자산증감")
	private Double 영업활동자산부채변동_영업자산증감_재고자산증감;
	@Column (name="영업활동자산부채변동_영업자산증감_기타자산증감")
	private Double 영업활동자산부채변동_영업자산증감_기타자산증감;
	@Column (name="영업활동자산부채변동_영업부채증감")
	private Double 영업활동자산부채변동_영업부채증감;
	@Column (name="영업활동자산부채변동_영업부채증감_매입채무증감")
	private Double 영업활동자산부채변동_영업부채증감_매입채무증감;
	@Column (name="영업활동자산부채변동_영업부채증감_기타부채증감")
	private Double 영업활동자산부채변동_영업부채증감_기타부채증감;
	@Column (name="투자활동현금흐름")
	private Double 투자활동현금흐름;
	@Column (name="투자활동현금유입액")
	private Double 투자활동현금유입액;
	@Column (name="투자활동현금유출액")
	private Double 투자활동현금유출액;
	@Column (name="투자활동현금유출액_매도가능증권증가")
	private Double 투자활동현금유출액_매도가능증권증가;
	@Column (name="투자활동현금유출액_지분법적용투자주식증가")
	private Double 투자활동현금유출액_지분법적용투자주식증가;
	@Column (name="투자활동현금유출액_토지의증가")
	private Double 투자활동현금유출액_토지의증가;
	@Column (name="투자활동현금유출액_건물의증가")
	private Double 투자활동현금유출액_건물의증가;
	@Column (name="투자활동현금유출액_구축물의증가")
	private Double 투자활동현금유출액_구축물의증가;
	@Column (name="투자활동현금유출액_기계의증가")
	private Double 투자활동현금유출액_기계의증가;
	@Column (name="투자활동현금유출액_건설중인자산")
	private Double 투자활동현금유출액_건설중인자산;
	@Column (name="투자활동현금유출액_기타자산의증가")
	private Double 투자활동현금유출액_기타자산의증가;
	@Column (name="재무활동현금흐름")
	private Double 재무활동현금흐름;
	@Column (name="재무활동현금유입액")
	private Double 재무활동현금유입액;
	@Column (name="재무활동현금유입액_단기차입금증가")
	private Double 재무활동현금유입액_단기차입금증가;
	@Column (name="재무활동현금유입액_장기차입금증가")
	private Double 재무활동현금유입액_장기차입금증가;
	@Column (name="재무활동현금유입액_사채증가")
	private Double 재무활동현금유입액_사채증가;
	@Column (name="재무활동현금유입액_유동성장기부채증가")
	private Double 재무활동현금유입액_유동성장기부채증가;
	@Column (name="재무활동현금유입액_자본금및자본잉여금증가")
	private Double 재무활동현금유입액_자본금및자본잉여금증가;
	@Column (name="재무활동현금유입액_자사주처분")
	private Double 재무활동현금유입액_자사주처분;
	@Column (name="재무활동현금유입액_기타재무활동현금유입")
	private Double 재무활동현금유입액_기타재무활동현금유입;
	@Column (name="재무활동현금유출액")
	private Double 재무활동현금유출액;
	@Column (name="재무활동현금유출액_단기차입금감소")
	private Double 재무활동현금유출액_단기차입금감소;
	@Column (name="재무활동현금유출액_장기차입금감소")
	private Double 재무활동현금유출액_장기차입금감소;
	@Column (name="재무활동현금유출액_사채감소")
	private Double 재무활동현금유출액_사채감소;
	@Column (name="재무활동현금유출액_유동성장기부채감소")
	private Double 재무활동현금유출액_유동성장기부채감소;
	@Column (name="재무활동현금유출액_자본금및자본잉여금감소")
	private Double 재무활동현금유출액_자본금및자본잉여금감소;
	@Column (name="재무활동현금유출액_자사주취득")
	private Double 재무활동현금유출액_자사주취득;
	@Column (name="재무활동현금유출액_배당금지급")
	private Double 재무활동현금유출액_배당금지급;
	@Column (name="재무활동현금유출액_기타재무활동현금유출")
	private Double 재무활동현금유출액_기타재무활동현금유출;
	@Column (name="현금의증감")
	private Double 현금의증감;
	@Column (name="기초현금")
	private Double 기초현금;
	@Column (name="기말현금")
	private Double 기말현금;
	
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getYmDay() {
		return ymDay;
	}
	public void setYmDay(String ymDay) {
		this.ymDay = ymDay;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public Double get영업활동현금흐름() {
		return 영업활동현금흐름;
	}
	public void set영업활동현금흐름(Double 영업활동현금흐름) {
		this.영업활동현금흐름 = 영업활동현금흐름;
	}
	public Double get당기순이익() {
		return 당기순이익;
	}
	public void set당기순이익(Double 당기순이익) {
		this.당기순이익 = 당기순이익;
	}
	public Double get현금유출없는비용() {
		return 현금유출없는비용;
	}
	public void set현금유출없는비용(Double 현금유출없는비용) {
		this.현금유출없는비용 = 현금유출없는비용;
	}
	public Double get현금유출없는비용_유형자산감가상각비() {
		return 현금유출없는비용_유형자산감가상각비;
	}
	public void set현금유출없는비용_유형자산감가상각비(Double 현금유출없는비용_유형자산감가상각비) {
		this.현금유출없는비용_유형자산감가상각비 = 현금유출없는비용_유형자산감가상각비;
	}
	public Double get현금유출없는비용_무형자산상각비() {
		return 현금유출없는비용_무형자산상각비;
	}
	public void set현금유출없는비용_무형자산상각비(Double 현금유출없는비용_무형자산상각비) {
		this.현금유출없는비용_무형자산상각비 = 현금유출없는비용_무형자산상각비;
	}
	public Double get현금유출없는비용_대손상각비() {
		return 현금유출없는비용_대손상각비;
	}
	public void set현금유출없는비용_대손상각비(Double 현금유출없는비용_대손상각비) {
		this.현금유출없는비용_대손상각비 = 현금유출없는비용_대손상각비;
	}
	public Double get현금유출없는비용_기타() {
		return 현금유출없는비용_기타;
	}
	public void set현금유출없는비용_기타(Double 현금유출없는비용_기타) {
		this.현금유출없는비용_기타 = 현금유출없는비용_기타;
	}
	public Double get현금유입없는수익() {
		return 현금유입없는수익;
	}
	public void set현금유입없는수익(Double 현금유입없는수익) {
		this.현금유입없는수익 = 현금유입없는수익;
	}
	public Double get영업활동자산부채변동() {
		return 영업활동자산부채변동;
	}
	public void set영업활동자산부채변동(Double 영업활동자산부채변동) {
		this.영업활동자산부채변동 = 영업활동자산부채변동;
	}
	public Double get영업활동자산부채변동_영업자산증감() {
		return 영업활동자산부채변동_영업자산증감;
	}
	public void set영업활동자산부채변동_영업자산증감(Double 영업활동자산부채변동_영업자산증감) {
		this.영업활동자산부채변동_영업자산증감 = 영업활동자산부채변동_영업자산증감;
	}
	public Double get영업활동자산부채변동_영업자산증감_매출채권증감() {
		return 영업활동자산부채변동_영업자산증감_매출채권증감;
	}
	public void set영업활동자산부채변동_영업자산증감_매출채권증감(Double 영업활동자산부채변동_영업자산증감_매출채권증감) {
		this.영업활동자산부채변동_영업자산증감_매출채권증감 = 영업활동자산부채변동_영업자산증감_매출채권증감;
	}
	public Double get영업활동자산부채변동_영업자산증감_재고자산증감() {
		return 영업활동자산부채변동_영업자산증감_재고자산증감;
	}
	public void set영업활동자산부채변동_영업자산증감_재고자산증감(Double 영업활동자산부채변동_영업자산증감_재고자산증감) {
		this.영업활동자산부채변동_영업자산증감_재고자산증감 = 영업활동자산부채변동_영업자산증감_재고자산증감;
	}
	public Double get영업활동자산부채변동_영업자산증감_기타자산증감() {
		return 영업활동자산부채변동_영업자산증감_기타자산증감;
	}
	public void set영업활동자산부채변동_영업자산증감_기타자산증감(Double 영업활동자산부채변동_영업자산증감_기타자산증감) {
		this.영업활동자산부채변동_영업자산증감_기타자산증감 = 영업활동자산부채변동_영업자산증감_기타자산증감;
	}
	public Double get영업활동자산부채변동_영업부채증감() {
		return 영업활동자산부채변동_영업부채증감;
	}
	public void set영업활동자산부채변동_영업부채증감(Double 영업활동자산부채변동_영업부채증감) {
		this.영업활동자산부채변동_영업부채증감 = 영업활동자산부채변동_영업부채증감;
	}
	public Double get영업활동자산부채변동_영업부채증감_매입채무증감() {
		return 영업활동자산부채변동_영업부채증감_매입채무증감;
	}
	public void set영업활동자산부채변동_영업부채증감_매입채무증감(Double 영업활동자산부채변동_영업부채증감_매입채무증감) {
		this.영업활동자산부채변동_영업부채증감_매입채무증감 = 영업활동자산부채변동_영업부채증감_매입채무증감;
	}
	public Double get영업활동자산부채변동_영업부채증감_기타부채증감() {
		return 영업활동자산부채변동_영업부채증감_기타부채증감;
	}
	public void set영업활동자산부채변동_영업부채증감_기타부채증감(Double 영업활동자산부채변동_영업부채증감_기타부채증감) {
		this.영업활동자산부채변동_영업부채증감_기타부채증감 = 영업활동자산부채변동_영업부채증감_기타부채증감;
	}
	public Double get투자활동현금흐름() {
		return 투자활동현금흐름;
	}
	public void set투자활동현금흐름(Double 투자활동현금흐름) {
		this.투자활동현금흐름 = 투자활동현금흐름;
	}
	public Double get투자활동현금유입액() {
		return 투자활동현금유입액;
	}
	public void set투자활동현금유입액(Double 투자활동현금유입액) {
		this.투자활동현금유입액 = 투자활동현금유입액;
	}
	public Double get투자활동현금유출액() {
		return 투자활동현금유출액;
	}
	public void set투자활동현금유출액(Double 투자활동현금유출액) {
		this.투자활동현금유출액 = 투자활동현금유출액;
	}
	public Double get투자활동현금유출액_매도가능증권증가() {
		return 투자활동현금유출액_매도가능증권증가;
	}
	public void set투자활동현금유출액_매도가능증권증가(Double 투자활동현금유출액_매도가능증권증가) {
		this.투자활동현금유출액_매도가능증권증가 = 투자활동현금유출액_매도가능증권증가;
	}
	public Double get투자활동현금유출액_지분법적용투자주식증가() {
		return 투자활동현금유출액_지분법적용투자주식증가;
	}
	public void set투자활동현금유출액_지분법적용투자주식증가(Double 투자활동현금유출액_지분법적용투자주식증가) {
		this.투자활동현금유출액_지분법적용투자주식증가 = 투자활동현금유출액_지분법적용투자주식증가;
	}
	public Double get투자활동현금유출액_토지의증가() {
		return 투자활동현금유출액_토지의증가;
	}
	public void set투자활동현금유출액_토지의증가(Double 투자활동현금유출액_토지의증가) {
		this.투자활동현금유출액_토지의증가 = 투자활동현금유출액_토지의증가;
	}
	public Double get투자활동현금유출액_건물의증가() {
		return 투자활동현금유출액_건물의증가;
	}
	public void set투자활동현금유출액_건물의증가(Double 투자활동현금유출액_건물의증가) {
		this.투자활동현금유출액_건물의증가 = 투자활동현금유출액_건물의증가;
	}
	public Double get투자활동현금유출액_구축물의증가() {
		return 투자활동현금유출액_구축물의증가;
	}
	public void set투자활동현금유출액_구축물의증가(Double 투자활동현금유출액_구축물의증가) {
		this.투자활동현금유출액_구축물의증가 = 투자활동현금유출액_구축물의증가;
	}
	public Double get투자활동현금유출액_기계의증가() {
		return 투자활동현금유출액_기계의증가;
	}
	public void set투자활동현금유출액_기계의증가(Double 투자활동현금유출액_기계의증가) {
		this.투자활동현금유출액_기계의증가 = 투자활동현금유출액_기계의증가;
	}
	public Double get투자활동현금유출액_건설중인자산() {
		return 투자활동현금유출액_건설중인자산;
	}
	public void set투자활동현금유출액_건설중인자산(Double 투자활동현금유출액_건설중인자산) {
		this.투자활동현금유출액_건설중인자산 = 투자활동현금유출액_건설중인자산;
	}
	public Double get투자활동현금유출액_기타자산의증가() {
		return 투자활동현금유출액_기타자산의증가;
	}
	public void set투자활동현금유출액_기타자산의증가(Double 투자활동현금유출액_기타자산의증가) {
		this.투자활동현금유출액_기타자산의증가 = 투자활동현금유출액_기타자산의증가;
	}
	public Double get재무활동현금흐름() {
		return 재무활동현금흐름;
	}
	public void set재무활동현금흐름(Double 재무활동현금흐름) {
		this.재무활동현금흐름 = 재무활동현금흐름;
	}
	public Double get재무활동현금유입액() {
		return 재무활동현금유입액;
	}
	public void set재무활동현금유입액(Double 재무활동현금유입액) {
		this.재무활동현금유입액 = 재무활동현금유입액;
	}
	public Double get재무활동현금유입액_단기차입금증가() {
		return 재무활동현금유입액_단기차입금증가;
	}
	public void set재무활동현금유입액_단기차입금증가(Double 재무활동현금유입액_단기차입금증가) {
		this.재무활동현금유입액_단기차입금증가 = 재무활동현금유입액_단기차입금증가;
	}
	public Double get재무활동현금유입액_장기차입금증가() {
		return 재무활동현금유입액_장기차입금증가;
	}
	public void set재무활동현금유입액_장기차입금증가(Double 재무활동현금유입액_장기차입금증가) {
		this.재무활동현금유입액_장기차입금증가 = 재무활동현금유입액_장기차입금증가;
	}
	public Double get재무활동현금유입액_사채증가() {
		return 재무활동현금유입액_사채증가;
	}
	public void set재무활동현금유입액_사채증가(Double 재무활동현금유입액_사채증가) {
		this.재무활동현금유입액_사채증가 = 재무활동현금유입액_사채증가;
	}
	public Double get재무활동현금유입액_유동성장기부채증가() {
		return 재무활동현금유입액_유동성장기부채증가;
	}
	public void set재무활동현금유입액_유동성장기부채증가(Double 재무활동현금유입액_유동성장기부채증가) {
		this.재무활동현금유입액_유동성장기부채증가 = 재무활동현금유입액_유동성장기부채증가;
	}
	public Double get재무활동현금유입액_자본금및자본잉여금증가() {
		return 재무활동현금유입액_자본금및자본잉여금증가;
	}
	public void set재무활동현금유입액_자본금및자본잉여금증가(Double 재무활동현금유입액_자본금및자본잉여금증가) {
		this.재무활동현금유입액_자본금및자본잉여금증가 = 재무활동현금유입액_자본금및자본잉여금증가;
	}
	public Double get재무활동현금유입액_자사주처분() {
		return 재무활동현금유입액_자사주처분;
	}
	public void set재무활동현금유입액_자사주처분(Double 재무활동현금유입액_자사주처분) {
		this.재무활동현금유입액_자사주처분 = 재무활동현금유입액_자사주처분;
	}
	public Double get재무활동현금유입액_기타재무활동현금유입() {
		return 재무활동현금유입액_기타재무활동현금유입;
	}
	public void set재무활동현금유입액_기타재무활동현금유입(Double 재무활동현금유입액_기타재무활동현금유입) {
		this.재무활동현금유입액_기타재무활동현금유입 = 재무활동현금유입액_기타재무활동현금유입;
	}
	public Double get재무활동현금유출액() {
		return 재무활동현금유출액;
	}
	public void set재무활동현금유출액(Double 재무활동현금유출액) {
		this.재무활동현금유출액 = 재무활동현금유출액;
	}
	public Double get재무활동현금유출액_단기차입금감소() {
		return 재무활동현금유출액_단기차입금감소;
	}
	public void set재무활동현금유출액_단기차입금감소(Double 재무활동현금유출액_단기차입금감소) {
		this.재무활동현금유출액_단기차입금감소 = 재무활동현금유출액_단기차입금감소;
	}
	public Double get재무활동현금유출액_장기차입금감소() {
		return 재무활동현금유출액_장기차입금감소;
	}
	public void set재무활동현금유출액_장기차입금감소(Double 재무활동현금유출액_장기차입금감소) {
		this.재무활동현금유출액_장기차입금감소 = 재무활동현금유출액_장기차입금감소;
	}
	public Double get재무활동현금유출액_사채감소() {
		return 재무활동현금유출액_사채감소;
	}
	public void set재무활동현금유출액_사채감소(Double 재무활동현금유출액_사채감소) {
		this.재무활동현금유출액_사채감소 = 재무활동현금유출액_사채감소;
	}
	public Double get재무활동현금유출액_유동성장기부채감소() {
		return 재무활동현금유출액_유동성장기부채감소;
	}
	public void set재무활동현금유출액_유동성장기부채감소(Double 재무활동현금유출액_유동성장기부채감소) {
		this.재무활동현금유출액_유동성장기부채감소 = 재무활동현금유출액_유동성장기부채감소;
	}
	public Double get재무활동현금유출액_자본금및자본잉여금감소() {
		return 재무활동현금유출액_자본금및자본잉여금감소;
	}
	public void set재무활동현금유출액_자본금및자본잉여금감소(Double 재무활동현금유출액_자본금및자본잉여금감소) {
		this.재무활동현금유출액_자본금및자본잉여금감소 = 재무활동현금유출액_자본금및자본잉여금감소;
	}
	public Double get재무활동현금유출액_자사주취득() {
		return 재무활동현금유출액_자사주취득;
	}
	public void set재무활동현금유출액_자사주취득(Double 재무활동현금유출액_자사주취득) {
		this.재무활동현금유출액_자사주취득 = 재무활동현금유출액_자사주취득;
	}
	public Double get재무활동현금유출액_배당금지급() {
		return 재무활동현금유출액_배당금지급;
	}
	public void set재무활동현금유출액_배당금지급(Double 재무활동현금유출액_배당금지급) {
		this.재무활동현금유출액_배당금지급 = 재무활동현금유출액_배당금지급;
	}
	public Double get재무활동현금유출액_기타재무활동현금유출() {
		return 재무활동현금유출액_기타재무활동현금유출;
	}
	public void set재무활동현금유출액_기타재무활동현금유출(Double 재무활동현금유출액_기타재무활동현금유출) {
		this.재무활동현금유출액_기타재무활동현금유출 = 재무활동현금유출액_기타재무활동현금유출;
	}
	public Double get현금의증감() {
		return 현금의증감;
	}
	public void set현금의증감(Double 현금의증감) {
		this.현금의증감 = 현금의증감;
	}
	public Double get기초현금() {
		return 기초현금;
	}
	public void set기초현금(Double 기초현금) {
		this.기초현금 = 기초현금;
	}
	public Double get기말현금() {
		return 기말현금;
	}
	public void set기말현금(Double 기말현금) {
		this.기말현금 = 기말현금;
	}

	
		
}
