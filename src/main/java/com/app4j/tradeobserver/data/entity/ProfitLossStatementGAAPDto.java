package com.app4j.tradeobserver.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.app4j.tradeobserver.data.entity.id.ProfitLossStatementGAAPDtoId;

@Entity
@Table (name = "PROFIT_LOSS_STATEMENT_GAAP")
@IdClass(ProfitLossStatementGAAPDtoId.class)
public class ProfitLossStatementGAAPDto {
	
	@Id
    @Column (name = "STOCK_CODE")
    private String stockCode;	
	@Id
	@Column (name="YM_DAY")
	private String ymDay;
	@Id
	@Column (name="DATE_TYPE")
	private String dateType;		// 분기, 연간 (Q, Y)
	@Id
	@Column (name="DATA_TYPE")
	private String dataType;		// 연결, 별도 (L, S)
	
	@Column (name="매출액")
	private Double 매출액;
	@Column (name="매출액_상품매출액")
	private Double 매출액_상품매출액;
	@Column (name="매출액_제품매출액")
	private Double 매출액_제품매출액;
	@Column (name="매출액_상품제품매출액")
	private Double 매출액_상품제품매출액;
	@Column (name="매출액_공사수입")
	private Double 매출액_공사수입;
	@Column (name="매출액_분양수입")
	private Double 매출액_분양수입;
	@Column (name="매출액_용역매출액")
	private Double 매출액_용역매출액;
	@Column (name="매출액_금융수익")
	private Double 매출액_금융수익;
	@Column (name="매출액_지분법이익")
	private Double 매출액_지분법이익;
	@Column (name="매출액_수출")
	private Double 매출액_수출;
	@Column (name="매출액_내수")
	private Double 매출액_내수;
	@Column (name="매출액_기타매출액")
	private Double 매출액_기타매출액;
	@Column (name="매출원가")
	private Double 매출원가;
	@Column (name="매출원가_상품매출원가")
	private Double 매출원가_상품매출원가;
	@Column (name="매출원가_제품매출원가")
	private Double 매출원가_제품매출원가;
	@Column (name="매출원가_상품제품매출원가")
	private Double 매출원가_상품제품매출원가;
	@Column (name="매출원가_공사원가")
	private Double 매출원가_공사원가;
	@Column (name="매출원가_분양원가")
	private Double 매출원가_분양원가;
	@Column (name="매출원가_용역원가")
	private Double 매출원가_용역원가;
	@Column (name="매출원가_금융비용")
	private Double 매출원가_금융비용;
	@Column (name="매출원가_지분법손실")
	private Double 매출원가_지분법손실;
	@Column (name="매출원가_기타매출원가")
	private Double 매출원가_기타매출원가;
	@Column (name="매출총이익")
	private Double 매출총이익;
	@Column (name="판매비와관리비")
	private Double 판매비와관리비;
	@Column (name="판매비와관리비_급여")
	private Double 판매비와관리비_급여;
	@Column (name="판매비와관리비_퇴직급여")
	private Double 판매비와관리비_퇴직급여;
	@Column (name="판매비와관리비_복리후생비")
	private Double 판매비와관리비_복리후생비;
	@Column (name="판매비와관리비_주식보상비")
	private Double 판매비와관리비_주식보상비;
	@Column (name="판매비와관리비_수도광열비")
	private Double 판매비와관리비_수도광열비;
	@Column (name="판매비와관리비_세금과공과")
	private Double 판매비와관리비_세금과공과;
	@Column (name="판매비와관리비_임차료")
	private Double 판매비와관리비_임차료;
	@Column (name="판매비와관리비_보험료")
	private Double 판매비와관리비_보험료;
	@Column (name="판매비와관리비_지급수수료")
	private Double 판매비와관리비_지급수수료;
	@Column (name="판매비와관리비_감가상각비")
	private Double 판매비와관리비_감가상각비;
	@Column (name="판매비와관리비_기타관리비")
	private Double 판매비와관리비_기타관리비;
	@Column (name="판매비와관리비_접대비")
	private Double 판매비와관리비_접대비;
	@Column (name="판매비와관리비_기밀비")
	private Double 판매비와관리비_기밀비;
	@Column (name="판매비와관리비_광고선전비")
	private Double 판매비와관리비_광고선전비;
	@Column (name="판매비와관리비_수출비용")
	private Double 판매비와관리비_수출비용;
	@Column (name="판매비와관리비_판매촉진비")
	private Double 판매비와관리비_판매촉진비;
	@Column (name="판매비와관리비_판매수수료")
	private Double 판매비와관리비_판매수수료;
	@Column (name="판매비와관리비_기타판매비")
	private Double 판매비와관리비_기타판매비;
	@Column (name="판매비와관리비_대손상각비")
	private Double 판매비와관리비_대손상각비;
	@Column (name="판매비와관리비_무형자산상각비")
	private Double 판매비와관리비_무형자산상각비;
	@Column (name="판매비와관리비_연구비")
	private Double 판매비와관리비_연구비;
	@Column (name="판매비와관리비_경상개발비")
	private Double 판매비와관리비_경상개발비;
	@Column (name="판매비와관리비_기술개발준비금")
	private Double 판매비와관리비_기술개발준비금;
	@Column (name="판매비와관리비_특허권사용료")
	private Double 판매비와관리비_특허권사용료;
	@Column (name="판매비와관리비_기타일반비")
	private Double 판매비와관리비_기타일반비;
	@Column (name="영업이익")
	private Double 영업이익;
	@Column (name="영업외수익")
	private Double 영업외수익;
	@Column (name="영업외수익_이자수익")
	private Double 영업외수익_이자수익;
	@Column (name="영업외수익_배당금수익")
	private Double 영업외수익_배당금수익;
	@Column (name="영업외수익_임대료")
	private Double 영업외수익_임대료;
	@Column (name="영업외수익_단기투자자산처분이익")
	private Double 영업외수익_단기투자자산처분이익;
	@Column (name="영업외수익_외환차익")
	private Double 영업외수익_외환차익;
	@Column (name="영업외수익_외화환산이익")
	private Double 영업외수익_외화환산이익;
	@Column (name="영업외수익_대손충당금환입")
	private Double 영업외수익_대손충당금환입;
	@Column (name="영업외수익_단기투자자산평가이익")
	private Double 영업외수익_단기투자자산평가이익;
	@Column (name="영업외수익_투자자산평가이익")
	private Double 영업외수익_투자자산평가이익;
	@Column (name="영업외수익_투자자산처분이익")
	private Double 영업외수익_투자자산처분이익;
	@Column (name="영업외수익_장기투자증권처분이익")
	private Double 영업외수익_장기투자증권처분이익;
	@Column (name="영업외수익_유형자산처분이익")
	private Double 영업외수익_유형자산처분이익;
	@Column (name="영업외수익_지분법이익")
	private Double 영업외수익_지분법이익;
	@Column (name="영업외수익_장기투자증권손상차손환입")
	private Double 영업외수익_장기투자증권손상차손환입;
	@Column (name="영업외수익_파생상품이익")
	private Double 영업외수익_파생상품이익;
	@Column (name="영업외수익_법인세환급액")
	private Double 영업외수익_법인세환급액;
	@Column (name="영업외수익_기타영업외수익")
	private Double 영업외수익_기타영업외수익;
	@Column (name="영업외비용")
	private Double 영업외비용;
	@Column (name="영업외비용_이자비용")
	private Double 영업외비용_이자비용;
	@Column (name="영업외비용_개발비상각")
	private Double 영업외비용_개발비상각;
	@Column (name="영업외비용_부채성충당부채전입액")
	private Double 영업외비용_부채성충당부채전입액;
	@Column (name="영업외비용_기타대손상각비")
	private Double 영업외비용_기타대손상각비;
	@Column (name="영업외비용_단기투자자산처분손실")
	private Double 영업외비용_단기투자자산처분손실;
	@Column (name="영업외비용_단기투자자산평가손실")
	private Double 영업외비용_단기투자자산평가손실;
	@Column (name="영업외비용_투자자산평가손실")
	private Double 영업외비용_투자자산평가손실;
	@Column (name="영업외비용_외환차손")
	private Double 영업외비용_외환차손;
	@Column (name="영업외비용_외화환산손실")
	private Double 영업외비용_외화환산손실;
	@Column (name="영업외비용_투자자산처분손실")
	private Double 영업외비용_투자자산처분손실;
	@Column (name="영업외비용_장기투자증권처분손실")
	private Double 영업외비용_장기투자증권처분손실;
	@Column (name="영업외비용_유형자산처분손실")
	private Double 영업외비용_유형자산처분손실;
	@Column (name="영업외비용_지분법손실")
	private Double 영업외비용_지분법손실;
	@Column (name="영업외비용_장기투자증권손상차손")
	private Double 영업외비용_장기투자증권손상차손;
	@Column (name="영업외비용_파생상품손실")
	private Double 영업외비용_파생상품손실;
	@Column (name="영업외비용_운휴자산상각비")
	private Double 영업외비용_운휴자산상각비;
	@Column (name="영업외비용_기타영업외비용")
	private Double 영업외비용_기타영업외비용;
	@Column (name="법인세차감전계속사업이익")
	private Double 법인세차감전계속사업이익;
	@Column (name="법인세비용")
	private Double 법인세비용;
	@Column (name="계속사업이익")
	private Double 계속사업이익;
	@Column (name="중단사업이익")
	private Double 중단사업이익;
	@Column (name="당기순이익")
	private Double 당기순이익;
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getYmDay() {
		return ymDay;
	}
	public void setYmDay(String ymDay) {
		this.ymDay = ymDay;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public Double get매출액() {
		return 매출액;
	}
	public void set매출액(Double 매출액) {
		this.매출액 = 매출액;
	}
	public Double get매출액_상품매출액() {
		return 매출액_상품매출액;
	}
	public void set매출액_상품매출액(Double 매출액_상품매출액) {
		this.매출액_상품매출액 = 매출액_상품매출액;
	}
	public Double get매출액_제품매출액() {
		return 매출액_제품매출액;
	}
	public void set매출액_제품매출액(Double 매출액_제품매출액) {
		this.매출액_제품매출액 = 매출액_제품매출액;
	}
	public Double get매출액_상품제품매출액() {
		return 매출액_상품제품매출액;
	}
	public void set매출액_상품제품매출액(Double 매출액_상품제품매출액) {
		this.매출액_상품제품매출액 = 매출액_상품제품매출액;
	}
	public Double get매출액_공사수입() {
		return 매출액_공사수입;
	}
	public void set매출액_공사수입(Double 매출액_공사수입) {
		this.매출액_공사수입 = 매출액_공사수입;
	}
	public Double get매출액_분양수입() {
		return 매출액_분양수입;
	}
	public void set매출액_분양수입(Double 매출액_분양수입) {
		this.매출액_분양수입 = 매출액_분양수입;
	}
	public Double get매출액_용역매출액() {
		return 매출액_용역매출액;
	}
	public void set매출액_용역매출액(Double 매출액_용역매출액) {
		this.매출액_용역매출액 = 매출액_용역매출액;
	}
	public Double get매출액_금융수익() {
		return 매출액_금융수익;
	}
	public void set매출액_금융수익(Double 매출액_금융수익) {
		this.매출액_금융수익 = 매출액_금융수익;
	}
	public Double get매출액_지분법이익() {
		return 매출액_지분법이익;
	}
	public void set매출액_지분법이익(Double 매출액_지분법이익) {
		this.매출액_지분법이익 = 매출액_지분법이익;
	}
	public Double get매출액_수출() {
		return 매출액_수출;
	}
	public void set매출액_수출(Double 매출액_수출) {
		this.매출액_수출 = 매출액_수출;
	}
	public Double get매출액_내수() {
		return 매출액_내수;
	}
	public void set매출액_내수(Double 매출액_내수) {
		this.매출액_내수 = 매출액_내수;
	}
	public Double get매출액_기타매출액() {
		return 매출액_기타매출액;
	}
	public void set매출액_기타매출액(Double 매출액_기타매출액) {
		this.매출액_기타매출액 = 매출액_기타매출액;
	}
	public Double get매출원가() {
		return 매출원가;
	}
	public void set매출원가(Double 매출원가) {
		this.매출원가 = 매출원가;
	}
	public Double get매출원가_상품매출원가() {
		return 매출원가_상품매출원가;
	}
	public void set매출원가_상품매출원가(Double 매출원가_상품매출원가) {
		this.매출원가_상품매출원가 = 매출원가_상품매출원가;
	}
	public Double get매출원가_제품매출원가() {
		return 매출원가_제품매출원가;
	}
	public void set매출원가_제품매출원가(Double 매출원가_제품매출원가) {
		this.매출원가_제품매출원가 = 매출원가_제품매출원가;
	}
	public Double get매출원가_상품제품매출원가() {
		return 매출원가_상품제품매출원가;
	}
	public void set매출원가_상품제품매출원가(Double 매출원가_상품제품매출원가) {
		this.매출원가_상품제품매출원가 = 매출원가_상품제품매출원가;
	}
	public Double get매출원가_공사원가() {
		return 매출원가_공사원가;
	}
	public void set매출원가_공사원가(Double 매출원가_공사원가) {
		this.매출원가_공사원가 = 매출원가_공사원가;
	}
	public Double get매출원가_분양원가() {
		return 매출원가_분양원가;
	}
	public void set매출원가_분양원가(Double 매출원가_분양원가) {
		this.매출원가_분양원가 = 매출원가_분양원가;
	}
	public Double get매출원가_용역원가() {
		return 매출원가_용역원가;
	}
	public void set매출원가_용역원가(Double 매출원가_용역원가) {
		this.매출원가_용역원가 = 매출원가_용역원가;
	}
	public Double get매출원가_금융비용() {
		return 매출원가_금융비용;
	}
	public void set매출원가_금융비용(Double 매출원가_금융비용) {
		this.매출원가_금융비용 = 매출원가_금융비용;
	}
	public Double get매출원가_지분법손실() {
		return 매출원가_지분법손실;
	}
	public void set매출원가_지분법손실(Double 매출원가_지분법손실) {
		this.매출원가_지분법손실 = 매출원가_지분법손실;
	}
	public Double get매출원가_기타매출원가() {
		return 매출원가_기타매출원가;
	}
	public void set매출원가_기타매출원가(Double 매출원가_기타매출원가) {
		this.매출원가_기타매출원가 = 매출원가_기타매출원가;
	}
	public Double get매출총이익() {
		return 매출총이익;
	}
	public void set매출총이익(Double 매출총이익) {
		this.매출총이익 = 매출총이익;
	}
	public Double get판매비와관리비() {
		return 판매비와관리비;
	}
	public void set판매비와관리비(Double 판매비와관리비) {
		this.판매비와관리비 = 판매비와관리비;
	}
	public Double get판매비와관리비_급여() {
		return 판매비와관리비_급여;
	}
	public void set판매비와관리비_급여(Double 판매비와관리비_급여) {
		this.판매비와관리비_급여 = 판매비와관리비_급여;
	}
	public Double get판매비와관리비_퇴직급여() {
		return 판매비와관리비_퇴직급여;
	}
	public void set판매비와관리비_퇴직급여(Double 판매비와관리비_퇴직급여) {
		this.판매비와관리비_퇴직급여 = 판매비와관리비_퇴직급여;
	}
	public Double get판매비와관리비_복리후생비() {
		return 판매비와관리비_복리후생비;
	}
	public void set판매비와관리비_복리후생비(Double 판매비와관리비_복리후생비) {
		this.판매비와관리비_복리후생비 = 판매비와관리비_복리후생비;
	}
	public Double get판매비와관리비_주식보상비() {
		return 판매비와관리비_주식보상비;
	}
	public void set판매비와관리비_주식보상비(Double 판매비와관리비_주식보상비) {
		this.판매비와관리비_주식보상비 = 판매비와관리비_주식보상비;
	}
	public Double get판매비와관리비_수도광열비() {
		return 판매비와관리비_수도광열비;
	}
	public void set판매비와관리비_수도광열비(Double 판매비와관리비_수도광열비) {
		this.판매비와관리비_수도광열비 = 판매비와관리비_수도광열비;
	}
	public Double get판매비와관리비_세금과공과() {
		return 판매비와관리비_세금과공과;
	}
	public void set판매비와관리비_세금과공과(Double 판매비와관리비_세금과공과) {
		this.판매비와관리비_세금과공과 = 판매비와관리비_세금과공과;
	}
	public Double get판매비와관리비_임차료() {
		return 판매비와관리비_임차료;
	}
	public void set판매비와관리비_임차료(Double 판매비와관리비_임차료) {
		this.판매비와관리비_임차료 = 판매비와관리비_임차료;
	}
	public Double get판매비와관리비_보험료() {
		return 판매비와관리비_보험료;
	}
	public void set판매비와관리비_보험료(Double 판매비와관리비_보험료) {
		this.판매비와관리비_보험료 = 판매비와관리비_보험료;
	}
	public Double get판매비와관리비_지급수수료() {
		return 판매비와관리비_지급수수료;
	}
	public void set판매비와관리비_지급수수료(Double 판매비와관리비_지급수수료) {
		this.판매비와관리비_지급수수료 = 판매비와관리비_지급수수료;
	}
	public Double get판매비와관리비_감가상각비() {
		return 판매비와관리비_감가상각비;
	}
	public void set판매비와관리비_감가상각비(Double 판매비와관리비_감가상각비) {
		this.판매비와관리비_감가상각비 = 판매비와관리비_감가상각비;
	}
	public Double get판매비와관리비_기타관리비() {
		return 판매비와관리비_기타관리비;
	}
	public void set판매비와관리비_기타관리비(Double 판매비와관리비_기타관리비) {
		this.판매비와관리비_기타관리비 = 판매비와관리비_기타관리비;
	}
	public Double get판매비와관리비_접대비() {
		return 판매비와관리비_접대비;
	}
	public void set판매비와관리비_접대비(Double 판매비와관리비_접대비) {
		this.판매비와관리비_접대비 = 판매비와관리비_접대비;
	}
	public Double get판매비와관리비_기밀비() {
		return 판매비와관리비_기밀비;
	}
	public void set판매비와관리비_기밀비(Double 판매비와관리비_기밀비) {
		this.판매비와관리비_기밀비 = 판매비와관리비_기밀비;
	}
	public Double get판매비와관리비_광고선전비() {
		return 판매비와관리비_광고선전비;
	}
	public void set판매비와관리비_광고선전비(Double 판매비와관리비_광고선전비) {
		this.판매비와관리비_광고선전비 = 판매비와관리비_광고선전비;
	}
	public Double get판매비와관리비_수출비용() {
		return 판매비와관리비_수출비용;
	}
	public void set판매비와관리비_수출비용(Double 판매비와관리비_수출비용) {
		this.판매비와관리비_수출비용 = 판매비와관리비_수출비용;
	}
	public Double get판매비와관리비_판매촉진비() {
		return 판매비와관리비_판매촉진비;
	}
	public void set판매비와관리비_판매촉진비(Double 판매비와관리비_판매촉진비) {
		this.판매비와관리비_판매촉진비 = 판매비와관리비_판매촉진비;
	}
	public Double get판매비와관리비_판매수수료() {
		return 판매비와관리비_판매수수료;
	}
	public void set판매비와관리비_판매수수료(Double 판매비와관리비_판매수수료) {
		this.판매비와관리비_판매수수료 = 판매비와관리비_판매수수료;
	}
	public Double get판매비와관리비_기타판매비() {
		return 판매비와관리비_기타판매비;
	}
	public void set판매비와관리비_기타판매비(Double 판매비와관리비_기타판매비) {
		this.판매비와관리비_기타판매비 = 판매비와관리비_기타판매비;
	}
	public Double get판매비와관리비_대손상각비() {
		return 판매비와관리비_대손상각비;
	}
	public void set판매비와관리비_대손상각비(Double 판매비와관리비_대손상각비) {
		this.판매비와관리비_대손상각비 = 판매비와관리비_대손상각비;
	}
	public Double get판매비와관리비_무형자산상각비() {
		return 판매비와관리비_무형자산상각비;
	}
	public void set판매비와관리비_무형자산상각비(Double 판매비와관리비_무형자산상각비) {
		this.판매비와관리비_무형자산상각비 = 판매비와관리비_무형자산상각비;
	}
	public Double get판매비와관리비_연구비() {
		return 판매비와관리비_연구비;
	}
	public void set판매비와관리비_연구비(Double 판매비와관리비_연구비) {
		this.판매비와관리비_연구비 = 판매비와관리비_연구비;
	}
	public Double get판매비와관리비_경상개발비() {
		return 판매비와관리비_경상개발비;
	}
	public void set판매비와관리비_경상개발비(Double 판매비와관리비_경상개발비) {
		this.판매비와관리비_경상개발비 = 판매비와관리비_경상개발비;
	}
	public Double get판매비와관리비_기술개발준비금() {
		return 판매비와관리비_기술개발준비금;
	}
	public void set판매비와관리비_기술개발준비금(Double 판매비와관리비_기술개발준비금) {
		this.판매비와관리비_기술개발준비금 = 판매비와관리비_기술개발준비금;
	}
	public Double get판매비와관리비_특허권사용료() {
		return 판매비와관리비_특허권사용료;
	}
	public void set판매비와관리비_특허권사용료(Double 판매비와관리비_특허권사용료) {
		this.판매비와관리비_특허권사용료 = 판매비와관리비_특허권사용료;
	}
	public Double get판매비와관리비_기타일반비() {
		return 판매비와관리비_기타일반비;
	}
	public void set판매비와관리비_기타일반비(Double 판매비와관리비_기타일반비) {
		this.판매비와관리비_기타일반비 = 판매비와관리비_기타일반비;
	}
	public Double get영업이익() {
		return 영업이익;
	}
	public void set영업이익(Double 영업이익) {
		this.영업이익 = 영업이익;
	}
	public Double get영업외수익() {
		return 영업외수익;
	}
	public void set영업외수익(Double 영업외수익) {
		this.영업외수익 = 영업외수익;
	}
	public Double get영업외수익_이자수익() {
		return 영업외수익_이자수익;
	}
	public void set영업외수익_이자수익(Double 영업외수익_이자수익) {
		this.영업외수익_이자수익 = 영업외수익_이자수익;
	}
	public Double get영업외수익_배당금수익() {
		return 영업외수익_배당금수익;
	}
	public void set영업외수익_배당금수익(Double 영업외수익_배당금수익) {
		this.영업외수익_배당금수익 = 영업외수익_배당금수익;
	}
	public Double get영업외수익_임대료() {
		return 영업외수익_임대료;
	}
	public void set영업외수익_임대료(Double 영업외수익_임대료) {
		this.영업외수익_임대료 = 영업외수익_임대료;
	}
	public Double get영업외수익_단기투자자산처분이익() {
		return 영업외수익_단기투자자산처분이익;
	}
	public void set영업외수익_단기투자자산처분이익(Double 영업외수익_단기투자자산처분이익) {
		this.영업외수익_단기투자자산처분이익 = 영업외수익_단기투자자산처분이익;
	}
	public Double get영업외수익_외환차익() {
		return 영업외수익_외환차익;
	}
	public void set영업외수익_외환차익(Double 영업외수익_외환차익) {
		this.영업외수익_외환차익 = 영업외수익_외환차익;
	}
	public Double get영업외수익_외화환산이익() {
		return 영업외수익_외화환산이익;
	}
	public void set영업외수익_외화환산이익(Double 영업외수익_외화환산이익) {
		this.영업외수익_외화환산이익 = 영업외수익_외화환산이익;
	}
	public Double get영업외수익_대손충당금환입() {
		return 영업외수익_대손충당금환입;
	}
	public void set영업외수익_대손충당금환입(Double 영업외수익_대손충당금환입) {
		this.영업외수익_대손충당금환입 = 영업외수익_대손충당금환입;
	}
	public Double get영업외수익_단기투자자산평가이익() {
		return 영업외수익_단기투자자산평가이익;
	}
	public void set영업외수익_단기투자자산평가이익(Double 영업외수익_단기투자자산평가이익) {
		this.영업외수익_단기투자자산평가이익 = 영업외수익_단기투자자산평가이익;
	}
	public Double get영업외수익_투자자산평가이익() {
		return 영업외수익_투자자산평가이익;
	}
	public void set영업외수익_투자자산평가이익(Double 영업외수익_투자자산평가이익) {
		this.영업외수익_투자자산평가이익 = 영업외수익_투자자산평가이익;
	}
	public Double get영업외수익_투자자산처분이익() {
		return 영업외수익_투자자산처분이익;
	}
	public void set영업외수익_투자자산처분이익(Double 영업외수익_투자자산처분이익) {
		this.영업외수익_투자자산처분이익 = 영업외수익_투자자산처분이익;
	}
	public Double get영업외수익_장기투자증권처분이익() {
		return 영업외수익_장기투자증권처분이익;
	}
	public void set영업외수익_장기투자증권처분이익(Double 영업외수익_장기투자증권처분이익) {
		this.영업외수익_장기투자증권처분이익 = 영업외수익_장기투자증권처분이익;
	}
	public Double get영업외수익_유형자산처분이익() {
		return 영업외수익_유형자산처분이익;
	}
	public void set영업외수익_유형자산처분이익(Double 영업외수익_유형자산처분이익) {
		this.영업외수익_유형자산처분이익 = 영업외수익_유형자산처분이익;
	}
	public Double get영업외수익_지분법이익() {
		return 영업외수익_지분법이익;
	}
	public void set영업외수익_지분법이익(Double 영업외수익_지분법이익) {
		this.영업외수익_지분법이익 = 영업외수익_지분법이익;
	}
	public Double get영업외수익_장기투자증권손상차손환입() {
		return 영업외수익_장기투자증권손상차손환입;
	}
	public void set영업외수익_장기투자증권손상차손환입(Double 영업외수익_장기투자증권손상차손환입) {
		this.영업외수익_장기투자증권손상차손환입 = 영업외수익_장기투자증권손상차손환입;
	}
	public Double get영업외수익_파생상품이익() {
		return 영업외수익_파생상품이익;
	}
	public void set영업외수익_파생상품이익(Double 영업외수익_파생상품이익) {
		this.영업외수익_파생상품이익 = 영업외수익_파생상품이익;
	}
	public Double get영업외수익_법인세환급액() {
		return 영업외수익_법인세환급액;
	}
	public void set영업외수익_법인세환급액(Double 영업외수익_법인세환급액) {
		this.영업외수익_법인세환급액 = 영업외수익_법인세환급액;
	}
	public Double get영업외수익_기타영업외수익() {
		return 영업외수익_기타영업외수익;
	}
	public void set영업외수익_기타영업외수익(Double 영업외수익_기타영업외수익) {
		this.영업외수익_기타영업외수익 = 영업외수익_기타영업외수익;
	}
	public Double get영업외비용() {
		return 영업외비용;
	}
	public void set영업외비용(Double 영업외비용) {
		this.영업외비용 = 영업외비용;
	}
	public Double get영업외비용_이자비용() {
		return 영업외비용_이자비용;
	}
	public void set영업외비용_이자비용(Double 영업외비용_이자비용) {
		this.영업외비용_이자비용 = 영업외비용_이자비용;
	}
	public Double get영업외비용_개발비상각() {
		return 영업외비용_개발비상각;
	}
	public void set영업외비용_개발비상각(Double 영업외비용_개발비상각) {
		this.영업외비용_개발비상각 = 영업외비용_개발비상각;
	}
	public Double get영업외비용_부채성충당부채전입액() {
		return 영업외비용_부채성충당부채전입액;
	}
	public void set영업외비용_부채성충당부채전입액(Double 영업외비용_부채성충당부채전입액) {
		this.영업외비용_부채성충당부채전입액 = 영업외비용_부채성충당부채전입액;
	}
	public Double get영업외비용_기타대손상각비() {
		return 영업외비용_기타대손상각비;
	}
	public void set영업외비용_기타대손상각비(Double 영업외비용_기타대손상각비) {
		this.영업외비용_기타대손상각비 = 영업외비용_기타대손상각비;
	}
	public Double get영업외비용_단기투자자산처분손실() {
		return 영업외비용_단기투자자산처분손실;
	}
	public void set영업외비용_단기투자자산처분손실(Double 영업외비용_단기투자자산처분손실) {
		this.영업외비용_단기투자자산처분손실 = 영업외비용_단기투자자산처분손실;
	}
	public Double get영업외비용_단기투자자산평가손실() {
		return 영업외비용_단기투자자산평가손실;
	}
	public void set영업외비용_단기투자자산평가손실(Double 영업외비용_단기투자자산평가손실) {
		this.영업외비용_단기투자자산평가손실 = 영업외비용_단기투자자산평가손실;
	}
	public Double get영업외비용_투자자산평가손실() {
		return 영업외비용_투자자산평가손실;
	}
	public void set영업외비용_투자자산평가손실(Double 영업외비용_투자자산평가손실) {
		this.영업외비용_투자자산평가손실 = 영업외비용_투자자산평가손실;
	}
	public Double get영업외비용_외환차손() {
		return 영업외비용_외환차손;
	}
	public void set영업외비용_외환차손(Double 영업외비용_외환차손) {
		this.영업외비용_외환차손 = 영업외비용_외환차손;
	}
	public Double get영업외비용_외화환산손실() {
		return 영업외비용_외화환산손실;
	}
	public void set영업외비용_외화환산손실(Double 영업외비용_외화환산손실) {
		this.영업외비용_외화환산손실 = 영업외비용_외화환산손실;
	}
	public Double get영업외비용_투자자산처분손실() {
		return 영업외비용_투자자산처분손실;
	}
	public void set영업외비용_투자자산처분손실(Double 영업외비용_투자자산처분손실) {
		this.영업외비용_투자자산처분손실 = 영업외비용_투자자산처분손실;
	}
	public Double get영업외비용_장기투자증권처분손실() {
		return 영업외비용_장기투자증권처분손실;
	}
	public void set영업외비용_장기투자증권처분손실(Double 영업외비용_장기투자증권처분손실) {
		this.영업외비용_장기투자증권처분손실 = 영업외비용_장기투자증권처분손실;
	}
	public Double get영업외비용_유형자산처분손실() {
		return 영업외비용_유형자산처분손실;
	}
	public void set영업외비용_유형자산처분손실(Double 영업외비용_유형자산처분손실) {
		this.영업외비용_유형자산처분손실 = 영업외비용_유형자산처분손실;
	}
	public Double get영업외비용_지분법손실() {
		return 영업외비용_지분법손실;
	}
	public void set영업외비용_지분법손실(Double 영업외비용_지분법손실) {
		this.영업외비용_지분법손실 = 영업외비용_지분법손실;
	}
	public Double get영업외비용_장기투자증권손상차손() {
		return 영업외비용_장기투자증권손상차손;
	}
	public void set영업외비용_장기투자증권손상차손(Double 영업외비용_장기투자증권손상차손) {
		this.영업외비용_장기투자증권손상차손 = 영업외비용_장기투자증권손상차손;
	}
	public Double get영업외비용_파생상품손실() {
		return 영업외비용_파생상품손실;
	}
	public void set영업외비용_파생상품손실(Double 영업외비용_파생상품손실) {
		this.영업외비용_파생상품손실 = 영업외비용_파생상품손실;
	}
	public Double get영업외비용_운휴자산상각비() {
		return 영업외비용_운휴자산상각비;
	}
	public void set영업외비용_운휴자산상각비(Double 영업외비용_운휴자산상각비) {
		this.영업외비용_운휴자산상각비 = 영업외비용_운휴자산상각비;
	}
	public Double get영업외비용_기타영업외비용() {
		return 영업외비용_기타영업외비용;
	}
	public void set영업외비용_기타영업외비용(Double 영업외비용_기타영업외비용) {
		this.영업외비용_기타영업외비용 = 영업외비용_기타영업외비용;
	}
	public Double get법인세차감전계속사업이익() {
		return 법인세차감전계속사업이익;
	}
	public void set법인세차감전계속사업이익(Double 법인세차감전계속사업이익) {
		this.법인세차감전계속사업이익 = 법인세차감전계속사업이익;
	}
	public Double get법인세비용() {
		return 법인세비용;
	}
	public void set법인세비용(Double 법인세비용) {
		this.법인세비용 = 법인세비용;
	}
	public Double get계속사업이익() {
		return 계속사업이익;
	}
	public void set계속사업이익(Double 계속사업이익) {
		this.계속사업이익 = 계속사업이익;
	}
	public Double get중단사업이익() {
		return 중단사업이익;
	}
	public void set중단사업이익(Double 중단사업이익) {
		this.중단사업이익 = 중단사업이익;
	}
	public Double get당기순이익() {
		return 당기순이익;
	}
	public void set당기순이익(Double 당기순이익) {
		this.당기순이익 = 당기순이익;
	}
}
