package com.app4j.tradeobserver.data.entity.id;

import java.io.Serializable;

import com.app4j.tradeobserver.data.entity.ProfitLossStatementGAAPDto;

public class ProfitLossStatementGAAPDtoId implements Serializable {
	protected String stockCode;
	protected String ymDay;
	protected String dateType;
	protected String dataType;
	
	public ProfitLossStatementGAAPDtoId() {
	}

	public ProfitLossStatementGAAPDtoId(String stockCode, String ymDay, String dateType, String dataType) {
		this.ymDay = ymDay;
		this.stockCode = stockCode;
		this.dateType = dateType;
		this.dataType = dataType;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ProfitLossStatementGAAPDto balanceSheetGAAPDto = (ProfitLossStatementGAAPDto) o;

		return stockCode.equals(balanceSheetGAAPDto.getStockCode())
				&& ymDay.equals(balanceSheetGAAPDto.getYmDay())
			    && dateType.equals(balanceSheetGAAPDto.getDateType())
			    && dataType.equals(balanceSheetGAAPDto.getDataType());
	}

	public int hashCode() {
		int result;
		result = dataType.hashCode();
		result = 13 * dateType.hashCode();
		result = 17 * result + ymDay.hashCode();
		result = 29 * result + stockCode.hashCode();
		return result;
	}

}