package com.app4j.tradeobserver.data.entity.id;

import java.io.Serializable;

import com.app4j.tradeobserver.data.entity.ItoozaCashFlowStatementIFRSDto;

public class ItoozaCashFlowStatementIFRSDtoId implements Serializable {
	
	protected String stockCode;
	protected String ymDay;
	protected String dateType;
	
	public ItoozaCashFlowStatementIFRSDtoId() {
	}

	public ItoozaCashFlowStatementIFRSDtoId(String stockCode, String ymDay, String dateType) {
		this.ymDay = ymDay;
		this.stockCode = stockCode;
		this.dateType = dateType;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ItoozaCashFlowStatementIFRSDto itoozaCashFlowStatementIFRSDto = (ItoozaCashFlowStatementIFRSDto) o;

		return stockCode.equals(itoozaCashFlowStatementIFRSDto.getStockCode())
				&& ymDay.equals(itoozaCashFlowStatementIFRSDto.getYmDay())
			    && dateType.equals(itoozaCashFlowStatementIFRSDto.getDateType());
	}

	public int hashCode() {
		int result;
		result = 13 * dateType.hashCode();
		result = 17 * result + ymDay.hashCode();
		result = 29 * result + stockCode.hashCode();
		return result;
	}

}
