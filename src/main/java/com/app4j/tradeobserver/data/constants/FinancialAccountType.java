package com.app4j.tradeobserver.data.constants;

public enum FinancialAccountType {
	
	IFRS_EACH("IFRSS"),
	IFRS_LINK("IFRSL"),
	GAAP_EACH("GAAPS"),
	GAAP_LINK("GAAPL"),
	
	ProfitLossStatement("2"),
	CashFlowStatement("4");	
	
	private String accountType;
	
	private FinancialAccountType() {
		
	}
	
	private FinancialAccountType(String statementType) {		
		this.accountType = statementType;		
	}

	public String getStatementType() {
		return accountType;
	}
	
	
	
	
	

}

