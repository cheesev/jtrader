package com.app4j.tradeobserver.data.constants;

public enum StatementType {
	
	FinancialPositionStatement("1"),
	ProfitLossStatement("2"),
	CashFlowStatement("4");	
	
	private String statementType;
	
	private StatementType() {
		
	}
	
	private StatementType(String statementType) {		
		this.statementType = statementType;		
	}

	public String getStatementType() {
		return statementType;
	}
	
	
	
	
	

}
