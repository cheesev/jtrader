package com.app4j.tradeobserver.data.constants;

public enum ItoozaModeType {
	
	ccmodeIFRS("1"),
	ccmodeGAAP("2"),
	
	lkmodeLink("1"),
	lkmodeEach("2"),
	
	dateQuater("db"),
	dateYear("dy");
	
	
	private String modeType;
	
	private ItoozaModeType() {
		
	}
	
	private ItoozaModeType(String modeType) {		
		this.modeType = modeType;		
	}

	public String getModeType() {
		return modeType;
	}
	
	
	
	
	

}
