package com.app4j.tradeobserver.data.constants;

public class DataCommonConstants {
	
	public static final int STOCK_CODE_LENGTH = 6;
	
	//NAVER
	public static final String PROFILT_LOSS_STATEMENT_URL_TEMPLATE = "http://companyinfo.stock.naver.com/v1/company/cF3002.aspx?cmp_cd=STOCK_CODE0&frq=0&rpt=1&finGubun=DATA_TYPE";	
	public static final String BALANCE_SHEET_GAAP_URL_TEMPLATE = "http://companyinfo.stock.naver.com/v1/company/cF3002.aspx?cmp_cd=STOCK_CODE&frq=0&rpt=2&finGubun=DATA_TYPE";
	
	// 위 두개는 지워도.. 될듯.	
	public static final String FINANCIAL_STATEMENT_URL_TEMPLATE = "http://companyinfo.stock.naver.com/v1/company/cF3002.aspx?cmp_cd=STOCK_CODE0&frq=0&rpt=1&finGubun=DATA_TYPE";	
	public static final int DATA_CNT = 5; // 재무제표 데이터의 열개수..
	
	public static final String VALUE_INDEX_URL_TEMPLATE = "http://companyinfo.stock.naver.com/v1/company/cF4001.aspx?cmp_cd=STOCK_CODE&frq=DATE_TYPE&rpt=VALUE_TYPE&finGubun=DATA_TYPE"; 
		
	public static final String[] VALUE_INDEX_ARRAY = {"profitability" , "growth", "stability", "turnover_ratio", "default"};
	
	//이건 테스트용으로 남겨둠.. for test
	public static final String PROFITLOSS_STATEMENT_YEAR_TABLE_SELECTOR = "#cTB31_10";
	public static final String PROFITLOSS_STATEMENT_QUATER_TABLE_SELECTOR = "#cTB31_11";	
	
	public static final String FINANCIAL_POSITION_STATMENT_YEAR_TABLE_SELECTOR = "#cTB31_20";
	public static final String FINANCIAL_POSITION_STATMENT_QUATER_TABLE_SELECTOR = "#cTB31_21";
	
	public static final String CASHFLOW_STATEMENT_YEAR_TABLE_SELECTOR = "#cTB31_30";
	public static final String CASHFLOW_STATEMENT_QUATER_TABLE_SELECTOR = "#cTB31_31";
	
	//위에까지 for test
	
	public static final String[] PROFITLOSS_STATEMENT_TABLE = {"#cTB31_10", "#cTB31_11"}; // 반드시 0번째가 연간, 1번째가 분기
	public static final String[] FINANCIAL_POSITION_STATMENT_TABLE = {"#cTB31_20", "#cTB31_21"}; // 반드시 0번째가 연간, 1번째가 분기	
	public static final String[] CASHFLOW_STATEMENT_TABLE = {"#cTB31_30", "#cTB31_30"}; // 반드시 0번째가 연간, 1번째가 분기
	
	
	
	
	public static final int LINE_START_NUM = 2;
	public static final String LINE_SELECTOR = ".c%start_num%.line.num";
	
	
	/*
	http://www.itooza.com/vclub/y10_page.php?cmp_cd=037560&mode=db&ss=10&sv=1&lsmode=1&lkmode=2&accmode=1  재무상태표 IFRS 분기
	http://www.itooza.com/vclub/y10_page.php?cmp_cd=037560&mode=db&ss=10&sv=1&lsmode=1&lkmode=2&accmode=2  재무상태표 GAAP 분기 (개별)

	http://www.itooza.com/vclub/y10_page.php?cmp_cd=037560&mode=db&ss=10&sv=1&lsmode=1&accmode=2&lkmode=1 	GAAP 연결.
	
	http://www.itooza.com/vclub/y10_page.php?cmp_cd=037560&mode=db&ss=10&sv=2&lsmode=1&accmode=2&lkmode=1 손익 계산서
	http://www.itooza.com/vclub/y10_page.php?cmp_cd=037560&mode=db&ss=10&sv=4&lsmode=1&accmode=2&lkmode=1 현금 흐름표.
	sv 재무상태표, 손익계산서 , 현금흐름표 (1, 2, 4)
	ccmode ifrs ,gaap (1, 2)
	lkmode 연결 개별 (1, 2)
 */				
	//ITOOZA
	public static final String ITOOZA_FINANCIAL_STATEMENT_URL_TEMPLATE = "http://www.itooza.com/vclub/y10_page.php?cmp_cd=%stockCode%&mode=%dateType%&ss=10&sv=%statement%&lsmode=1&lkmode=%lkmode%&accmode=%ccmode%";	
	
	public static final String BALANCE_SHEET_YMDAY_SELECTOR = "#y10_tb_2 thead tr th"; 	// 분기 데이터 셀렉터
	public static final String BALANCE_SHEET_DATA_SELECTOR = "#y10_tb_2 tbody tr td";	// 모든 데이터 셀렉터
	
	public static final String NOT_APPLICABLE = "N/A";
	
	public static final String ITOOZA_VALUE_INDEX_URL_TEMPLATE = "http://www.itooza.com/vclub/y10_page.php?ss=10&sv=10&cmp_cd=%stockCode%&mode=%dateType%";
	
	public static final String VALUE_INDEX_YMDAY_SELECTOR = "#y10_tb_1 thead tr th"; 	//value index 분기 데이터 셀렉터
	public static final String VALUE_INDEX_DATA_SELECTOR = "#y10_tb_1 tbody tr td"; 	//value index 모든 데이터 셀렉터
	
	
	

}

